updates and edits to the grid generator plus investigation of optimizing the generation for 
asymmetric and or full torus fields

Most of the genrator is used as is. 
tracer/W7X     has make_W7X-grid_phg_test.py which expands significantly on the original tracer
combine     mostly holds the analysis code. it also  contains philip_zb_w7xt_version2.py which 
is the usefacing generation conde which wraps the whole generation code and which can be 
imported to generate grids. See any of the versionX_widget jupyter notebooks for usage 
examples.

As the bulk of the generation code is the same to felix reimolds generator, some 
storageintensive parts were not included into my git and can be gotten from felix. see 
.gitignore
