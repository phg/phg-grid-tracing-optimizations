import numpy as np
from combine.quick_g import quick_g_plot

# not whole loop, only tenth of torus -> phi doesnt loop
# would be way nicer with xarray.rolling() [+edge periodicity differentiation]
def yield_grid_cells(g, r_lahead=1, tht_lahead=1, phi_lahead=0):
    g = g[:,:-1] # remove double tht=0,2pi values
    # implement general looping edges g[r,tht,phi,c]; tht loops, r, phi not
    ors, othts, ophis, _ = g.shape  # original xxx size
    g = np.pad(g, [(0,0), (0,tht_lahead), (0,0), (0,0)], mode='wrap')
    
    # create smaller output array
    h = np.zeros((ors-r_lahead, othts, ophis-phi_lahead,
                  1+r_lahead, tht_lahead+1, phi_lahead+1, 3))
    
    # iterate over all parameter coordinates
    for cr in range(ors-r_lahead):
        for ctht in range(othts): #tht loops
            for cphi in range(ophis-phi_lahead):
                h[cr, ctht, cphi] = g[cr:cr+r_lahead+1,
                                      ctht:ctht+tht_lahead+1,
                                      cphi:cphi+phi_lahead+1]    
    return h

# convert the direction dependent functions to one that
# returns the results of both directions as a tuple
def r_tht_direction_tuple(f):
    return lambda g: (f(g, 'r'), f(g, 'tht'))

def _cell_centers(g, x, direction):
    mid = np.mean(x[0:2,0:2], axis=(0,1))[0]
    if direction == "r":
        mid_af = np.mean(x[1:3,0:2], axis=(0,1))[0]
    else:
        mid_af = np.mean(x[0:2,1:3], axis=(0,1))[0]
    return mid, mid_af
         
def _facemid(g, x, direction):
    if direction == "r":
        facemid_af = np.mean(x[1,0:2], axis=0)[0]
    else:
        facemid_af = np.mean(x[0:2,1], axis=0)[0]
    return facemid_af

# see triangulated_volume
# surfaces: toroidal_[up/down]stream, [radial/poloidal]_[up/down]stream_[1/2]
# for 00->11, 1 is (0,0)-(1,0)-(1,1), 2 is (0,0)-(1,1)-(0,1) in correct order
# for 01->10, 1 is (0,1)-(1,0)-(0,0), 2 is(0,1)-(1,0)-(1,1) in correct order
def triangulated_surface(g, triangle_cut={"r":"00->11", "tht":"00->11"},
                         surface="all", coords=False, wv=None, xyz_g=None):
    # give option to provide pregenerated xyz_g to save time, used for "all"
    if xyz_g is None:
        xyz_g = np.empty_like(g)
        xyz_g[...,0] = g[...,0] * np.cos(g[...,2])  # x = r cos phi
        xyz_g[...,1] = g[...,0] * np.sin(g[...,2])  # y = r sin phi
        xyz_g[...,2] = g[...,1]                     # z = z
    # give option to provide pregenerated wv to save time, used for "all"
    if wv is None:
        wv = yield_grid_cells(xyz_g, 1, 1, 1)

    tc = lambda d, v1, v2: v1 if triangle_cut[d] == "00->11" else v2
    # r,tht,phi indices list for surface in cell
    # hopefully oriented so all calculated normals point outwards
    surfaces_indices = {
        "toroidal_upstream": [(0,0,0), (1,0,0), (1,1,0), (0,1,0)],
        "toroidal_downstream": [(0,0,1), (0,1,1), (1,1,1), (1,0,1)],
        "radial_upstream_1":
            tc("r", [(0,0,0),(0,1,0),(0,1,1)], [(0,0,0),(0,1,0),(0,0,1)]),
        "radial_upstream_2":
            tc("r", [(0,0,0),(0,1,1),(0,0,1)], [(0,0,1),(0,1,0),(0,1,1)]),
        "radial_downstream_1":
            tc("r", [(1,0,0),(1,1,1),(1,1,0)], [(1,0,0),(1,0,1),(1,1,0)]),
        "radial_downstream_2":
            tc("r", [(1,0,0),(1,0,1),(1,1,1)], [(1,1,0),(1,0,1),(1,1,1)]),
        "poloidal_upstream_1":
            tc("tht", [(0,0,0),(1,0,1),(1,0,0)], [(0,0,0),(0,0,1),(1,0,0)]),
        "poloidal_upstream_2":
            tc("tht", [(0,0,0),(0,0,1),(1,0,1)], [(1,0,0),(0,0,1),(1,0,1)]),
        "poloidal_downstream_1":
            tc("tht", [(0,1,0),(1,1,0),(1,1,1)], [(0,1,0),(1,1,0),(0,1,1)]),
        "poloidal_downstream_2":
            tc("tht", [(0,1,0),(1,1,1),(0,1,1)], [(0,1,1),(1,1,1),(0,1,1)]),
    }
    if surface != "all":
        assert surfaces_indices.get(surface, None) is not None

        c_len = 4 if "toroidal" in surface else 3

        out_coords = np.empty((*wv.shape[:3], c_len, 3)) # r, tht, phi, point, xyz
        for i, pnt_index in enumerate(surfaces_indices[surface]):
            p_r, p_tht, p_phi = pnt_index
            out_coords[:,:,:,i,:] = wv[:,:,:,p_r,p_tht,p_phi,:]

        if coords: return out_coords

        # otherwise calculate surface area from coords
        if "toroidal" in surface:
            return np.linalg.norm(np.cross( # first triangle in quad
                out_coords[...,1,:] - out_coords[...,0,:],
                out_coords[...,2,:] - out_coords[...,0,:],
            ), axis=-1)/2 + np.linalg.norm(np.cross( # 2nd in quad
                out_coords[...,2,:] - out_coords[...,0,:],
                out_coords[...,3,:] - out_coords[...,0,:],
            ), axis=-1)/2
        else:
            return np.linalg.norm(np.cross(
                out_coords[...,1,:] - out_coords[...,0,:],
                out_coords[...,2,:] - out_coords[...,0,:],
            ), axis=-1)/2

    else: # ergo need to calculate for "all"
        assert coords == False  # coords=True, surface="all" not allowed by me

        surface_sum = np.zeros(wv.shape[:3])
        for surface_name in surfaces_indices.keys():
            surface_sum += triangulated_surface(g, triangle_cut=triangle_cut,
                                     surface=surface_name, coords=False,
                                     wv=wv, xyz_g=xyz_g)
        return surface_sum


# calculate the cell volume of each cell.
# the surfaces with normals in r or tht directions are in general curved
# so we approximate the curved quad by two planar triangles.
# the line inside of the cell surface
# between the triangles either goes   (0,1) @-------------@ (1,1)
# from vertex[0,0] to vertex[1,1] or       / \.       ./'/
# from vertex[1,0] to vertex[0,1]. This   /    \. ./'   /    \. = 01->10
# coice is specified in triangle_cut     /    ./'\.    /    ./' = 00->11
# (dict) with either "00->11" or        / ./'      \. /
# "01->10" as the value correspond.    @-------------@ (r=1, tht=0)
# to the "r" & "tht" key.           (0,0)
def triangulated_volume(g, triangle_cut={"r":"00->11", "tht":"00->11"}):
    # we can fill the whole cell volume via 6 tetrahedrons
    xyz_g = np.empty_like(g)
    xyz_g[...,0] = g[...,0] * np.cos(g[...,2])  # x = r cos phi
    xyz_g[...,1] = g[...,0] * np.sin(g[...,2])  # y = r sin phi
    xyz_g[...,2] = g[...,1]                     # z = z

    wv = yield_grid_cells(xyz_g, 1, 1, 1)
    rs, thts, phis = wv.shape[:3]
    vols = np.empty((rs, thts, phis))
    subvols = np.empty(6)

    if triangle_cut["r"]=="00->11" and triangle_cut["tht"]=="00->11":
        prism_tetra = _tri_antiprism_tetrahedrons([
            (0,0,0), (0,1,1), (1,1,0), (0,0,1), (1,1,1), (1,0,0)])
        tetrahedrons = [[(0,0,0), (0,1,1), (1,1,0), (0,1,0)],
                [(0,0,1), (1,1,1), (1,0,0), (1,0,1)], *prism_tetra]

    elif triangle_cut["r"]=="01->10" and triangle_cut["tht"]=="00->11":
        prism_tetra = _tri_antiprism_tetrahedrons([
            (1,0,0), (0,0,1), (0,1,0), (1,0,1), (0,1,1), (1,1,0)])
        tetrahedrons = [[(1,0,0), (0,0,1), (0,1,0), (0,0,0)],
                [(1,0,1), (0,1,1), (1,1,0), (1,1,1)], *prism_tetra]

    elif triangle_cut["r"]=="00->11" and triangle_cut["tht"]=="01->10":
        prism_tetra = _tri_antiprism_tetrahedrons([
            (0,0,0), (1,0,1), (0,1,1), (1,0,0), (1,1,1), (0,1,0)])
        tetrahedrons = [[(0,0,0), (1,0,1), (0,1,1), (0,0,1)],
                [(1,0,0), (1,1,1), (0,1,0), (1,1,0)], *prism_tetra]
    else:  # triangle_cut["r"]=="01->10" and triangle_cut["tht"]=="01->10"
        prism_tetra = _tri_antiprism_tetrahedrons([
            (0,1,0), (1,1,1), (0,0,1), (1,1,0), (1,0,1), (0,0,0)])
        tetrahedrons = [[(0,1,0), (1,1,1), (0,0,1), (0,1,1)],
                [(1,1,0), (1,0,1), (0,0,0), (1,0,0)], *prism_tetra]

    for r in range(rs):
        for tht in range(thts):
            for phi in range(phis):
                for i in range(6):
                    subvols[i] = _tetrahedron_volume(wv[r,tht,phi],
                                                    tetrahedrons[i])
                    vols[r, tht, phi] = np.abs(subvols).sum()

    return vols

# https://en.wikipedia.org/wiki/Tetrahedron#Irregular_tetrahedra
def _tetrahedron_volume(cell, indices):
    det_mat = np.empty((3,3))
    det_mat[0] = cell[indices[0]] - cell[indices[3]]
    det_mat[1] = cell[indices[1]] - cell[indices[3]]
    det_mat[2] = cell[indices[2]] - cell[indices[3]]
    return np.linalg.det(det_mat)/6

# converts antiprism to 4 tetrahedron indices
# indices are given as upper1, upper2 upper3, lower1 lower2 lower3 with
# lower1 being the index connecting to upper1 and upper2
# and lower2 to upper2 and upper3
def _tri_antiprism_tetrahedrons(indices):
    u1, u2, u3, l1, l2, l3 = indices
    # cut into two parts along u1, u3, l2, l1 with crease edge as l1, u3
    # and then into two tetrahedrons
    return [[u1, u3, l1, l3], [l3, l1, u3, l2],
            [l1, u1, u3, u2], [l1, l2, u3, u2]]



def approx_volume(g): # APPROXIMATION
    wv = yield_grid_cells(g, 1, 1, 1)  # window view
    # calculate coord differences 
    r = wv[...,0,0,0,0]
    dr = np.diff(wv[...,:,0,0,:], axis=-2)[...,0,:]
    dz = np.diff(wv[...,0,:,0,:], axis=-2)[...,0,:]
    drphi = np.diff(wv[...,0,0,:,:], axis=-2)[...,0,:]
    # change phi angle difference to space difference by multiplying with r
    drphi *= r.reshape(r.shape + (1,)) # dphi * r
    # calculate volume from column vector determinant
    vectors = np.empty(dr.shape + (3,))
    vectors[...,0], vectors[...,1], vectors[...,2] = dr, dz, drphi
    vols = np.linalg.det(vectors)
    return vols

@r_tht_direction_tuple
def non_orthogonality(g, direction):
    assert direction == "r" or direction == "tht"
    ws = (2,1,0) if direction=="r" else (1,2,0)  # window size
    wv = yield_grid_cells(g, *ws)  # window view
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # angles on faces
    af = np.empty((rs, thts, phis))
    
    # using numpy vector functions is fast, but takes ages to code. will loop
    for r in range(rs):
        for tht in range(thts):
            for phi in range(phis):
                x = wv[r,tht,phi]
                # cell centers
                mid, mid_af = _cell_centers(g, x, direction)
                # direction of cell midpoint connections 
                v_af = (mid_af - mid)/np.linalg.norm(mid_af - mid)
                # direction of edges 
                t_af = x[1,1]-x[1,0] if direction == "r" else x[1,1]-x[0,1]
                tn_af = t_af/np.linalg.norm(t_af)  # don't need normal
                # angle between edge direction and midpoint direction
                # angle(v,n) = arcsin abs (v.t)/(||v||*||t||)
                # with t tangential and n corresponding normal vector
                af[r,tht,phi] = np.arcsin(np.abs(np.dot(tn_af, v_af)))
    return af

@r_tht_direction_tuple
def unevenness(g, direction):
    assert direction == "r" or direction == "tht"
    ws = (2,1,0) if direction=="r" else (1,2,0)  # window size
    wv = yield_grid_cells(g, *ws)  # window view
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # unevenness on faces, also named a
    af = np.empty((rs, thts, phis))
    
    # using numpy vector functions is fast, but takes ages to code. will loop
    for r in range(rs):
        for tht in range(thts):
            for phi in range(phis):
                x = wv[r,tht,phi,...,:2]
                # cell centers
                mid, mid_af = _cell_centers(g, x, direction)
                # direction of cell midpoint connections 
                v_af = (mid_af - mid)/np.linalg.norm(mid_af - mid)
                # cell midpoint line midpoint m_f
                linemid_af = np.mean([mid, mid_af], axis=0)
                # cell corner connection line (edge) midpoint c_f
                facemid_af = _facemid(g, x, direction)
                # line distance to point
                ld_af = np.linalg.norm(np.cross(v_af, facemid_af-mid_af))
                # rotated cell mitpoint dir by 90 deg
                n_af = np.array([-v_af[1], v_af[0]])
                # check if needed to add or remove
                f_af = np.sign(np.dot(n_af, facemid_af-mid_af))
                # move corner connection line (edge) midpoint on line c_f'
                proj_facemid_af = facemid_af - f_af*ld_af*n_af
                # distance of c_f' and m_f
                num_dist_af = np.linalg.norm(proj_facemid_af-linemid_af)
                # distance between two cell midpoints 
                den_dist_af= np.linalg.norm(mid_af - mid)
                # measure of unevenness
                af[r,tht,phi] = num_dist_af/den_dist_af
    return af

@r_tht_direction_tuple
def skewness(g, direction):
    assert direction == "r" or direction == "tht"
    ws = (2,1,0) if direction=="r" else (1,2,0)  # window size
    wv = yield_grid_cells(g, *ws)  # window view
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # unevenness on faces, also named a
    af = np.empty((rs, thts, phis))
    
    # using numpy vector functions is fast, but takes ages to code. will loop
    for r in range(rs):
        for tht in range(thts):
            for phi in range(phis):
                x = wv[r,tht,phi,...,:2]
                # cell centers
                mid, mid_af = _cell_centers(g, x, direction)
                # direction of cell midpoint connections 
                v_af = (mid_af - mid)/np.linalg.norm(mid_af - mid)
                # cell corner connection line (edge) midpoint c_f
                facemid_af = _facemid(g, x, direction)
                # line distance to point == ||c_f' - c_f||
                ld_af = np.abs(np.cross(v_af, facemid_af-mid_af))
                # distance between two cell midpoints 
                den_dist_af = np.linalg.norm(mid_af - mid)
                # measure of skewness
                af[r,tht,phi] = ld_af/den_dist_af
    return af

# THIS WORKS NOW, but not before !!!!
def convex(g):
    wv = yield_grid_cells(g, 1, 1, 0)  # window view
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # skewness on r and tht faces, also named a
    is_convex = np.zeros((rs, thts, phis), dtype=int)
    
    # using numpy vector functions is fast, but takes ages to code. will loop
    for r in range(rs):
        for tht in range(thts):
            for phi in range(phis):
                x = wv[r,tht,phi,...,0,:2]
                # from point, to point, *other point indices to check against
                # its just looping through the subarray, feel free to improve
                indices = [[(0,0), (1,0), (1,1), (0,1)],
                           [(1,0), (1,1), (0,1), (0,0)],
                           [(1,1), (0,1), (0,0), (1,0)],
                           [(0,1), (0,0), (1,0), (1,1)]]
                for (of, to, A, B) in indices:
                    di = x[to] - x[of] # direction of line
                    # check if A and B on same side of line. If so, then 
                    # the crossproduct with the direction has the same sign
                    vA = np.sign(np.cross(di, x[A]-x[of]))
                    vB = np.sign(np.cross(di, x[B]-x[of]))
                    is_convex[r,tht,phi] += vA + vB
    # there are 8 relations per cell that are tested (and added) if all are
    # true, the result will be 8. The smaller the number the worse the cell
    is_convex = ( is_convex == 8 )
    return is_convex

def fast_convex(g):
    wv = yield_grid_cells(g, 1, 1, 0)  # window view
    wv =  wv[...,0,:2] # ignore phi component in cell and coord
    # the two directions of cell edges, along r and along tht
    rs, thts, phis = wv.shape[:3]
    # the check works in the way, that the cell has 4 edges and for
    # each edge (going counterclockwise), the other cell corners 
    # need to be on the left side of the edge for the cell to be convex
    # cell edge directions along parametrisation in r and tht
    dir_r, dir_tht  = np.diff(wv, axis=3), np.diff(wv, axis=4)
    # we need to invert half the edge- (2nd in r and 1st in tht) 
    # direction vectors so they point conterclockwise
    # views into dir_r, d_tht for easy access
    dir_r0, dir_r1 = dir_r[...,0,0,:], dir_r[...,0,1,:] 
    dir_tht0, dir_tht1 = dir_tht[...,0,0,:], dir_tht[...,1,0,:]
    dir_r1 *= -1; dir_tht0 *= -1
    # now we need to check if the vectors in order of
    # r[0], tht[1], (-)r[1], (-)tht[0] are always turning left
    # from the last vector (between 0° and 180°).
    # cell convex IFF above true for all four edges
    cross_values = np.empty((rs, thts, phis, 4))
    cross_values[...,0] = np.cross(dir_r0, dir_tht1)
    cross_values[...,1] = np.cross(dir_tht1, dir_r1)
    cross_values[...,2] = np.cross(dir_r1, dir_tht0)
    cross_values[...,3] = np.cross(dir_tht0, dir_r0)
    # we can take the min > 0 to see if all of them are > 0
    is_convex = cross_values.min(axis=-1) > 0
    return is_convex
    
def _stats(arr, pprint):
    arr = arr.flatten()
    print(arr.shape)
    mean = np.mean(arr)
    median = np.median(arr)
    std = np.std(arr)
    minv = np.min(arr)
    maxv = np.max(arr)
    
    if pprint:
        print(f"""Stat values for {pprint}: 
        µ={mean:.3e}, σ={std:.3e},
        min:{minv:.3e}, max:{maxv:.3e}, med:{median:.3e}""" )
    return {"mean":mean, "std":std, "min":minv, "max":maxv, "median":median}

# return (mean, std, minv, maxv, median) for every argument
def stats(*arrs, combine=True, pprint=False):
    if combine:
        if len(arrs) == 1:
            arrs = arrs[0]
        arrs = np.concatenate(arrs)
        return _stats(arrs, pprint)
    else:
        stat_outputs = []
        for i, arr in enumerate(arrs):
            stat_outputs.append(_stats(arr, pprint + f" {i}"))
        return stat_outputs