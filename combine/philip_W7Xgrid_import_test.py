pp = False
from combine.quick_g import quick_g_plot
import tracer.W7X.make_W7X_grid_phg_test as w7xt

consts = {"NirS": 5, "NirC": 5, "NirN": 10,  # in -> outside cell number 
     "Niphalf": 50,  # half pol cell number
     "eq_space": True, "eq_space_vessel": True, "Nit": 37,
     "zoidpol": False, "trace_angle": 10, # angle is int in [0,36]
     "center_delta_R": .15, "dRSOL": -0.08, }

g, glob, consts = w7xt.make_inner_grid(consts=consts)
g, glob, consts = w7xt.make_outer_boundary(g, glob, consts)
g = w7xt.make_outer_grid(g, glob, consts)

#print(g, g.shape)
quick_g_plot(g, phi=0)