# SETUP
from matplotlib import pyplot as plt
import numpy as np

# ZOIDBERG STUFF
# import Zoidberg.zoidpol as zp
import Zoidberg.zoidpol_phg_test as zp
import Zoidberg.rzline as rz

# W7XGRID STUFF
import tracer.W7X.make_W7X_grid_phg_test as w7xt
from combine.quick_g import multi_bump

# GRID ANALYZE STUFF
# import combine.grid_analyze as ga
from combine.quick_g import quick_g_plot

def gen_grid(constants, load=None, save=None, all_grids_folder=False,
             pp=False, pl=False):
    consts = w7xt.process_consts_load_Bfield(constants, pprint=pp)

    if load is not None: # False when you want to generate new g
        try:
            g = np.load(load)
        except:
            assert False # couldnd load given grid
    else:
        # 1) Generate the inner grid with the W7Xgrid routines
        ''' this returns the first grid version 
        g[r,tht,phi] = [R,z,phi] as well as some parameters needed for the other
        functions and a figure axis tuple for looking at the plotted grid.
        to change the parameters of this grid, one needs to change the inputs
        of the constants dict given as the first param of make_inner_grid.
        '''
        g, consts = w7xt.make_inner_grid(consts, pprint=pp)

        # 2) Set the outermost layer of the grid to the shape of the plasma vessel
        ''' this changes g[-1,:,:,:] to be the shape of the plasma vessel,
        while taking the outputs of the functions above, changing them
        approprietly (g[-1], possible changes in glob, new plots in ax)
        '''
        g, consts = w7xt.make_outer_boundary(g, consts, pprint=pp)
        
        # OLD 3) would naively fill the middle layers outside of the islands
        if not constants["zoidpol"]:
            g = w7xt.make_outer_grid(g, consts, pprint=pp)
    
    ### KEEP EDGE POINTS (g[-1] & g[-1-NirN]) CONST, ZOID MIDDLE
    if consts["zoidpol"]:
        for phi in range(g.shape[2]):
            rvres, polres = consts["NirN"], 2*consts["Niphalf"]  # rvesselres
            # define edge lines. zoidberg doesnt want equal beginning and end points
            # therefor the last point of the line gets cut of :-1 (*)
            inEdge, outEdge = g[-1-rvres,:-1,phi,:], g[-1,:-1,phi,:]
            inner = rz.RZline(inEdge[:,0],inEdge[:,1])
            outer = rz.RZline(outEdge[:,0],outEdge[:,1])
            
            # make zoidberg interpolation
            # inner wall, outer wall, radial resolution, poloidal resolution
            grid = zp.grid_elliptic(inner, outer, rvres+1, polres, align=False, show=pl)
            
            # place grid into g (edges that are the same will also be inserted)
            # -1-constants["NirN"]: -> every radius from the plasma boundary to the vessel
            # :-1 -> every poloidal angle except the last because grid doesnt contain it
            #        the last is the same as the first so it is copied extra
            # PHISTUFF
            # 0 or 1 == R or z that need to be replaced. 2 == phi = phi_r==0
            g[-1-rvres:,:-1,phi,0] = grid.R; g[-1-rvres:,-1,phi,0] = grid.R[:,0]
            g[-1-rvres:,:-1,phi,1] = grid.Z; g[-1-rvres:,-1,phi,1] = grid.Z[:,0]
            g[:,:,phi,2] = g[0,0,phi,2]

    if all_grids_folder != False:
        # False == dont do it, None == do it with defaults
        w7xt.target_and_EMC3_compliance(g, consts, outputf=all_grids_folder)

    ### possibly Analyze the given grid

    # save and return grid
    if save is not None:
        # np.save(save, g)
        np.savez(save, data=g)
    return g

if __name__ == "__main__":
    # should the w7xgridcode print (pp) info and plot (pl) some of the grid?
    pp = False
    
    # grid parameters, you can add more!
    consts = {
        # in -> outside cell numbers (sum is redial resolution)
        "NirC": 3, "NirS": 2, "NirN": 2,
        # half(-1/2) poloidal and toroidal cell number (per half-module)
        "Niphalf": 30, "Nit": 37, "full_torus": False,
        # poloidal grid spacing and smoothing options implemented by phg
        "eq_space": True, "eq_space_vessel": False, "zoidpol": False,
        # strating angle in [0,Nit[ and offset list of projection center
        "trace_angle": 0, "center_delta_R": .15, # [0,0,0,0.2,.15],
        # path to magnetic field file
        "magnetic_field_file": "./Fields/Field-EIM-std.dat", # ErrFullT-
        # Big radius values for field surfaces (default EIM) and axis + accuracy
        # multiple values lead to multiple points beeing traced for one surface
        # this makes sense when they are chaotic
        # EIM: [5.9014041, 6.1014041, 6.2014041, 6.2864041], 5.944e+00, 72
        # FTM: [5.99, 6.09, 6.1751, np.linspace(6.2129, 6.2129001, 10)], 5.96957, 720
        # EIMError: [5.8807, 6.095, 6.175, 6.246], 5.9315, 720
        "surface_radii": [5.9014041, 6.1014041, 6.2014041, 6.2864041],
        "Rcax": 5.944e+00, "tr_acc": 72,
        # parameters for outer surface fit: alpha value for alpha shape,
        # buffer size for erosion antierosion, maximum deviation of simplified
        # shape, the numper of nearest neighbour smoothings and if it gets plotted
        #"shrink_fit_outer_surface": (60, 0.04, 0.002, 10, True)
    }

    # vpdf = np.ones((consts["Niphalf"]*2+1, consts["Nit"]))
    # vpdf[:,0] = [1]*10 + [2]*10 + [3]*10 + [4]*10 + [1]*61
    # consts["vessel_pdf"] = vpdf

    g = gen_grid(consts, pp=pp)
    quick_g_plot(g, phi=consts["trace_angle"])

