#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 09:42:14 2022

@author: phg
"""

# ZOIDBERG STUFF
# import Zoidberg.zoidpol as zp
import Zoidberg.zoidpol_phg_test as zp
import Zoidberg.rzline as rz

inner = rz.shaped_line(R0=3.0, a=0.75, elong=1.0, triang=0.0, indent=1.0, n=50)
outer = rz.shaped_line(R0=2.8, a=1.5, elong=1.0, triang=0.5, indent=0.4, n=50)
''' # square example
import numpy as np
F = False; t = 20
R = np.append(np.append(np.append(np.linspace( 1, 1, t, endpoint=F),
                                  np.linspace( 1,-1, t, endpoint=F)),
                                  np.linspace(-1,-1, t, endpoint=F)),
                                  np.linspace(-1, 1, t, endpoint=F))
                        
z = np.append(np.append(np.append(np.linspace( 1,-1, t, endpoint=F),
                                  np.linspace(-1,-1, t, endpoint=F)),
                                  np.linspace(-1, 1, t, endpoint=F)),
                                  np.linspace( 1, 1, t, endpoint=F))

inner = rz.RZline(R,z)
outer = rz.RZline(10*R, 10*z)'''

# inner wall, outer wall, radial resolution, poloidal resolution
grid = zp.grid_elliptic(inner, outer, 30, 100, show=True)

#x, z = grid.findIndex([2.0, 1.9], [1.5, 2.0])
#print(x, z)


