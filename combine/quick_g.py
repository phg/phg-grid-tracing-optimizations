import numpy as np
from matplotlib import pyplot as plt
import mpl_interactions.ipyplot as iplt
import ipywidgets as widgets

def widget_g_plot(g, phi=0, title=None, widget=True, Ri=None, THTi=None, **kwargs):
    fig, ax = plt.subplots(figsize=(8,8))
    ax.set_aspect("equal")
    if(title is not None): ax.set_title(title)
    fig.tight_layout()
    
    def g_plot(phi):
        ax.clear()
        print(g.shape, phi)
        R, z, _ = np.transpose(g[:,:,phi,:], (2,0,1))
        ax.plot(R,z, c="blue", lw=0.3, **kwargs)
        if Ri is not None:
            ax.plot(R[Ri],z[Ri], c="darkred", lw=2, **kwargs)
        ax.plot(R.T,z.T, c="red", lw=0.3, **kwargs)
        if THTi is not None:
            ax.plot(R.T[THTi],z.T[THTi], c="darkblue", lw=2, **kwargs)
    
    if widget:
        phi = widgets.IntSlider(min=0, max=g.shape[2]-1, value=phi, step=1)
        widgets.interact(g_plot, phi = phi)
    else:
        g_plot(phi)

def quick_g_plot(g, phi=0, title=None, lw=0.3):
    R, z, _ = np.transpose(g[:,:,phi,:], (2,0,1))
    fig, ax = plt.subplots(figsize=(8,8))
    ax.set_aspect("equal")
    ax.plot(R,z, c="blue", lw=lw)
    ax.plot(R.T,z.T, c="red", lw=lw)
    if title:
        ax.set_title(title)
    elif title is None:
        ax.set_title(f"Crossection at the {phi}. slice")
    fig.tight_layout()
    return fig, ax

# covariance arrays have tiny values, so per default the cov values are divided by 1000
def gaussian_bump(array, scale, mu, cov=np.identity(2)):
    # covariance: [[a,b],[c,d]] =^= ( a b \\ c d ) with (tht, phi) vectors 
    cov = np.array(cov)  # force cov to array for math operations
    tht_size, phi_size = array.shape
    tht, phi = np.linspace(0,1,tht_size), np.linspace(0,1,phi_size)
    Phi, Tht = np.meshgrid(phi, tht)
    vec_dist = np.array([Tht-mu[0], Phi-mu[1]]).transpose((1,2,0))
    cov_distance = np.sum(vec_dist * (vec_dist @ np.linalg.inv(cov.T)), axis=-1)
    norm_distr = np.exp(-1/2 * cov_distance)
    return array + norm_distr*scale

def multi_bump(size, scales_mus_covs=[], divide_cov_by_1000=True):
    # scales mus covs is a list of
    # (scale, [mux,muy], [[cxx,cxy],[cyx,cyy]]) tuples
    assert(len(size) == 2)
    arr = np.ones(size)  # tht, phi
    for scale, mu, cov in scales_mus_covs:
        # force cov to array for math operations
        cov = np.array(cov) / (1000 if divide_cov_by_1000 else 1) 
        arr = gaussian_bump(arr, scale, mu, cov)
    return arr