Widget versions:
example: examples 
3:  erste richtige interaktive version, erzeugt alle EIM base gitter aus denen einer der Violinplot erzeugt wurde
3b: default version abgeändert für 360° felder ohne das eigentliche original verändern zu müssen
4:  Test bzgl der funktion von bumps für equal spacing
5:  multibump optimierung für EIM gitter
5b: minimal version von 5
5c: multibump optimierung für FTM gitter
5d: 5b mit neuer variante und EIM ERR
5e: FTM normals optimierung
6:  nicht viel anders als 5
7:  FTM alpha shape testing
8:  interaktive EIM + Error Field version

Visualize island versions:
1: create data
2: look at data through mpl / mplot3d
postprocess: make viable for mayavi
plot islands: plot via mayavi (and save obj)