import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

import sys
flr_path='./'
sys.path.append(flr_path+'build/')
from field_routines import baxisym as bas
from field_routines import bprecalc as bp
from field_routines import bfield as bf
from field_routines import trace as tr
from field_routines import tools

sys.path.append(flr_path+'AUG/')
from machine_geometry import * 
import time

sys.path.append(flr_path)
from load_magnetics import *
import scipy.ndimage
import argparse

#read the command line parameters or define default values
parser=argparse.ArgumentParser()
parser.add_argument("-fn","--filename",default='',type=str)
parser.add_argument("-s","--shot",default=-1,type=int)
parser.add_argument("-t","--time",default=2.0,type=float)
parser.add_argument("-Nrc",default=30,type=int)
parser.add_argument("-Nrs",default=30,type=int)
parser.add_argument("-Nrp",default=20,type=int)
parser.add_argument("-Np",default=400,type=int)
parser.add_argument("-refrad",default=6,type=int)
parser.add_argument("-refpol",default=6,type=int)
parser.add_argument("-psiNmin",default=0.96,type=float)
parser.add_argument("-psiNpmin",default=0.98,type=float)
parser.add_argument("-psiNmax",default=1.04,type=float)
parser.add_argument("-Lcmin",default=10,type=float)
parser.add_argument("-Lcmax",default=2000,type=float)
parser.add_argument("-phi0",default=0.0,type=float)
parser.add_argument("-dphi",default=15.0,type=float)
parser.add_argument("-o","--output",default='',type=str,nargs='?')
parser.add_argument("--direction",default=0,type=int)
args=parser.parse_args()


if args.filename!='':
  magxml=read_from_file(args.filename)
elif args.shot<0:
  magxml=read_from_file('AUG/magnetics_AUGD_EQH_30839_03200ms_0.xml')#AUG/magnetics_AUGD_EQH_26081_04000ms_0.xml
else:  
  magxml=read_AUG_magnetics(ishot=args.shot,time=args.time)

load_magnetics(magxml)  # passes the magnetic configuration to the fortan modules baxisym,bprecalc, etc...

AUGxml=open_machine(fn=flr_path+'AUG/MD_AUG.xml')

RMPs=xml_get_elements(AUGxml,".//coils/coil[@type='RMP']/wires")

wires=np.zeros((0,4))

for RMPcurr in magxml.find('currents').findall('RMPcoil'):
  nam=RMPcurr.attrib['name']
  IRMP=xml2np(RMPcurr)
  print nam,'%1f A' % IRMP
  if ~np.isnan(IRMP):
    w=RMPs[nam]*1.0;w[:,3]*=IRMP
    wires=np.vstack((wires,w))

bp.set_current_wires_xyzi(wires)

fn='RMP_field_%s_%ims.dat' % (magxml.attrib['discharge'],int(float(magxml.attrib['time'])*1000))

print 'This script typicall runs a few hours, be patient.'

if False:#set false to restore the precomputed field (faster)
  #precompute the magnetic field and its derivatives (i.e.12 components in total) on an equidistant R,z,phi grid 
  bp.prepare_field(1.0,2.25,128, -1.3,1.3,128, 0.0,2*np.pi,256, 12)  
  bp.save_field(fn) # and store it in a file
else:
  bp.restore_field(fn)

fig,ax =plt.subplots(1,2,sharey=True,figsize=(15,10))
fig.subplots_adjust(top=0.96,wspace=0.02,left=0.08,right=0.98,bottom=0.1)

#load the machine geometry information from the xml file and plot them. 
AUGxml=open_machine('AUG/MD_AUG.xml')

bas.find_separatrix(args.Np,int(args.Np/12.),int(args.Np/17.),ll=0.5,lr=0.5)#number of poloidal points on separatrix in core, right leg and left leg respectively

bf.scale_components([1.0,1.0,1.0])# switch on the perturbation field

pfcs=xml_get_element(AUGxml,'.//modelling_elements/*[@name="all_pfcs"]/pol_contour')
tr.unset_all_limits()
tr.add_axisym_target_curve(pfcs)

psiNc=1.0+(args.psiNmin-1.0)*np.linspace(0.0,1.0,args.Nrc)**2
psiNs=1.0+(args.psiNmax-1.0)*np.linspace(0.0,1.0,args.Nrs)**2
psiNp=np.linspace(-0.999999,-args.psiNpmin,args.Nrp)[::-1]

norm=mpl.colors.LogNorm(vmin=args.Lcmin,vmax=args.Lcmax)

start=time.time()
for psiN in [psiNs,psiNc,psiNp]:
    bas.find_flxsurface(psiN[1])
    ii=np.where(~np.isnan(bas.sf[:,0]))[0]

    Rzp=np.zeros((psiN.shape[0],len(ii),3))

    for ir,p in enumerate(psiN):
        bas.find_flxsurface(p)
        Rzp[ir,:,0:2]=bas.sf[ii,:]
        
    Rzp[:,:,2]=args.phi0
    
    Rzp=scipy.ndimage.zoom(Rzp,[args.refrad,args.refpol,1])

    Rzpc=0.5*(Rzp[:-1,:-1,:]+Rzp[1:,1:,:]) # determine  the cell centers

    Lc=Rzpc[:,:,0:2]*np.nan # create a field with the same radial and poloidal resolution as Rzpc and fill with nan
    PsiNmin=Rzpc[:,:,0]*np.nan 

    Nir,Nip,_=Rzpc.shape

    for ir in range(Nir):
        print ir, Nir
        for ip in range(Nip):
            if tools.point_in_poly(Rzpc[ir,ip,0:2],pfcs):
                fl=tr.field_line(Rzpc[ir,ip],args.dphi*np.pi/180.,50000,m=4,dir=args.direction)
                Lc[ir,ip,:]=tr.lcd*1.0 #store the result (the factor 1.0 forces a copy of the result)
                PsiNmin[ir,ip]=np.nanmin(fl[:,3])
    
    
    pLc=ax[0].pcolorfast(Rzp[:,:,0],Rzp[:,:,1],np.sum(np.abs(Lc),axis=2),norm=norm,vmin=args.Lcmin,vmax=args.Lcmax,cmap='jet',zorder=0)    
    pPm=ax[1].pcolorfast(Rzp[:,:,0],Rzp[:,:,1],PsiNmin,vmin=args.psiNmin,vmax=args.psiNmax,cmap='jet_r',zorder=0)    

print 'Elapsed time %.1f s' % (time.time()-start)

for axx in ax:
    plot_machine(axx,AUGxml.find('components'))
    axx.set_aspect('equal')
    axx.set_xlabel('R [m]',fontsize=20)
    axx.set_xlim(1.1,1.8)
    axx.set_ylim(-1.2,-0.4)

ax[0].text(1.3,-0.45,r'$\phi=%.1f^o$' % args.phi0,fontsize=24)
ax[0].set_ylabel('z [m]',fontsize=20)


b=ax[0].get_position().bounds
cbax=fig.add_axes([b[0]+b[2]*0.4,b[1]+0.65*b[3],0.05*b[2],0.25*b[3]])
cb = plt.colorbar(pLc, cax=cbax,cmap=mpl.cm.jet)	 
cb.set_label('$L_c$ [m]',fontsize=20)

b=ax[1].get_position().bounds
cbax=fig.add_axes([b[0]+b[2]*0.4,b[1]+0.65*b[3],0.05*b[2],0.25*b[3]])
cb = plt.colorbar(pPm, cax=cbax,cmap=mpl.cm.jet)	 
cb.set_label('$\Psi_{min}$',fontsize=20)


if args.output!='':
  fig.savefig(args.output)
else:
  plt.show()
