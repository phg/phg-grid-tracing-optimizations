#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 09:28:09 2019

@author: viwi
Read and modify Kisslinger meshes
"""

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from mayavi import mlab

degtorad = np.pi/180

def cylindertocartesian(r,z,p): 
  r,z,p = np.array(r),np.array(z),np.array(p)
  x = r*np.cos(p)
  y = r*np.sin(p)
  z = z
  return [x,y,z]

def load_components(pn='./W7X/'):
  '''
  Load components from Kisslinger files
  '''
  #List of components to use
  lcomp = [pn+'WALL/vessel.medium']

  pn = pn+'/PARTS/'
  tmp = [\
  'baf_lower_left_0_21',\
  'baf_lower_left_19_28',\
  'baf_lower_right',\
  'baf_upper_left',\
  'baf_upper_right',\
  'div_hor_lower',\
  'div_hor_upper',\
  'div_ver_upper',\
  'shield_0_21_fix2.wvn',\
  'shield_21_28_fix2.wvn',\
  'shield_28_31p5_fix2.wvn',\
  'shield_31p5_32_fix2.wvn',\
  'shield_32_32p5_fix2.wvn',\
  'shield_32p5_35_fix2.wvn',\
  'shield_35_36_fix2.wvn',\
  'cover_lower_chamber_gap',\
  'cover_lower_gap_0-1.0',\
  'cover_lower_hor_div',\
  'cover_lower_phi=21',\
  'cover_lower_phi=28',\
  'cover_upper_chamber_gap',\
  'cover_upper_gap_0-1.5',\
  'cover_upper_hor_div',\
  'cover_upper_ver_div',\
  'cover_upper_phi=19',\
  'cut_lowerChamber',\
  'cut_upperChamber'\
  ]
  
  lcol = [(0.7,0.7,0.7)]
  for i in range(len(tmp)):
    lcol.append((0.0,0.0,0.0))

  for i,item in enumerate(tmp):
    lcomp.append(pn+item)

  #Load components
  lc = []
  for item in lcomp:
    print(item)
    flag = is_upper(item)
    if flag:
      lc.append(Kisslinger(item).mirror_component())
    else:
      lc.append(Kisslinger(item))
  return [lc, lcomp,lcol]

def get_color(comp):
  col = (0.2,0.2,0.2)
  idx = comp.split('/')[-1]
  if 'covers_' in idx:
      col = (0,1,0)
  elif 'div_' in idx:
       col = (1,0,0)
  elif 'baf_' in idx:
       col = (0,0,1)
  elif 'shield_' in idx:
       col = (0.5,0.5,0.5)
  else:
       col = (1,1,1)
  return col

def is_upper(comp):
  idx = comp.split('/')[-1]
  return 'upper' in idx

def plot_components(lc,phi=0.,idx=[],fig=None,ax=None):
  '''
  Plot components
  '''
  if fig is None: fig = plt.gcf()
  if ax is None: ax = fig.gca()
  for i,item in enumerate(lc):
      if i in idx:
          item.plot_phi_lineout(phi,fig,ax,color='r')
      else:
          item.plot_phi_lineout(phi,fig,ax,color='k')
  plt.draw()
  return fig

def plot_mayavi_components(lc,op=0.995,skip=[],lcol=None,fig=None):
  '''
  Plot components with Mayavi
  '''
  if fig is None: fig = mlab.figure()
  if lcol is None: lcol = [(1,1,1)]*len(lc)
  for i,item in enumerate(lc):
      if not(i in skip):
        item.plot_mayavi_mesh(op=op,color=lcol[i],fig=fig)
  return fig

class Kisslinger():
    """
    Read, plot and modify Kisslinger meshes
    
    input: 
        fn - filename of the mesh
        
    output:
        Kisslinger.R - Radial values of surface mesh
        Kisslinger.Z - Z values of the surface mesh
        Kisslinger.P - Phi values of the surface mesh
    """
    def __init__(self, fn):
        """
        Read Kisslinger Mesh
        
        File structure:
            
            First row: description of mesh
            
            Second row:
                First number  - nPhi in mesh (int)
                Second number - nRZ in mesh (int)
                Third number  - nfold periodicity (int)
                Fourth number - R reference point (float)
                Fifth number  - Z reference point (float)
                
            Third row onwards: mesh data
        """
        header=np.loadtxt(fn,skiprows=1, max_rows=1)
        print(header)
        nphi=int(header[0])
        nRZ=int(header[1])
    #    print(' '.join(np.loadtxt(fn,dtype='str',max_rows=1)))
        print('File: %s' %fn.split('/')[-1])
        print('Kisslinger mesh with %d Phi values with %d R,Z values per phi.'%(nphi, nRZ))
        print('%d-fold periodicity'%header[2])
        print('Reference point: %f [cm] %f [cm] (R,Z)'%(header[3],header[4]))
        
        R=np.zeros((nphi,nRZ))
        Z=np.zeros(np.shape(R))
        P=np.ones(np.shape(R))
        for i in range(nphi):
            P[i,:]=np.multiply(P[i,:],np.loadtxt(fn,skiprows=2+(nRZ+1)*i, max_rows=1))
            tmp=np.loadtxt(fn,skiprows=3+(nRZ+1)*i,max_rows=nRZ,usecols=(0,1))
            R[i,:]=tmp[:,0]
            Z[i,:]=tmp[:,1]
        
        self.R=R
        self.Z=Z
        self.P=P
        self.nphi=nphi
        self.nRZ=nRZ
        self.Rref=header[3]
        self.Zref=header[4]
        self.nfold=header[2]
        return

    def mirror_component(self):
      ''' 
        Mirror component 
      '''
      self.R = self.R[::-1,:]
      self.Z = -self.Z[::-1,:]
      self.P = -self.P[::-1,:]
      return self
    
    def rotate_component(self,phi=36.):
      ''' 
        Rotate component by angle phi 
      '''
      self.P += phi
      return self

    def refine_poloidal_resolution(self,fac):
        '''
        Refine poloidal resolution by fac
        '''
        R,Z,P =[],[],[]
        for i in range(self.nRZ-1):
            R0,Z0,R1,Z1 = self.R[:,i],self.Z[:,i],self.R[:,i+1],self.Z[:,i+1]
            P0 = self.P[:,i]
            dR, dZ = (R1-R0)/fac, (Z1-Z0)/fac
            for j in range(fac):
                R.append(R0+dR*j)
                Z.append(Z0+dZ*j)
                P.append(P0)
            #Add last point of last intervall (not in refinement)
            R.append(R1)
            Z.append(Z1)
            P.append(P0)
    
        R,Z,P = np.array(R).T,np.array(Z).T,np.array(P).T
        #dat = data.data()
        #dat.R, dat.Z, dat.P = R,Z,P
        #self.plot_mayavi_mesh(self)
        #self.plot_mayavi_mesh(dat)
        self.R,self.Z,self.P = R,Z,P
        self.nRZ = self.R.shape[1]
        return
    
    def refine_toroidal_resolution(self,fac):
        '''
        Refine toroidal resolution by fac
        '''
        R,Z,P = [],[],[]
        for i in range(self.nphi-1):
            P0,P1 = self.P[i,0],self.P[i+1,0]
            dphi = (P1- P0)/fac
            for j in range(fac):
                P.append(np.ones(self.nRZ)*P0+j*dphi)
                Rp,Zp = self.get_phi_lineout(P[-1][0])
                R.append(Rp)
                Z.append(Zp)
        #Add last point of last intervall (not in refinement)
        P.append(self.P[-1,:])
        Rp,Zp = self.get_phi_lineout(P[-1][0])
        R.append(Rp)
        Z.append(Zp)

        R,Z,P = np.array(R),np.array(Z),np.array(P)
        #dat = data.data()
        #dat.R, dat.Z, dat.P = R, Z, P
        #self.plot_mayavi_mesh(self)
        #self.plot_mayavi_mesh(dat)
        self.R,self.Z,self.P = R,Z,P
        self.nphi = self.R.shape[0]
        return

    def refine_resolution(self,facpol,factor):
        '''
        Refine poloidal/toroidal resolution by facpol/factor
        '''
        self.refine_poloidal_resolution(facpol)
        self.refine_toroidal_resolution(factor)
        return

    def to_tri(self):
        """
        Convert Kisslinger data to triangles as done in EMC3
        
        In Kisslinger.py - R=shape(nphi, nRZ)
        TRI_ELE = number of triangles made from the kisslinger mesh
        X_TRIA,Y_TRIA, Z_TRIA = x,y,z vertices of the triangles
        V_TRIA_X, V_TRIA_Y, V_TRIA_Z = x,y,z components of surface normal for  each triangle
        """
        #check the total number of triangles 
        TRI_ELE=0
        for j in range(self.nphi-1):
            for i in range(self.nRZ-1):
                if self.R[j,i]!=self.R[j,i+1] or self.Z[j,i]!=self.Z[j,i+1]:
                    TRI_ELE=TRI_ELE+1
                if self.R[j+1,i]!=self.R[j+1,i+1] or self.Z[j+1,i]!=self.Z[j+1,i+1]:
                    TRI_ELE=TRI_ELE+1
        print('%i Triangles' %TRI_ELE)
        
        #compute vertices of triangle, done similarly to L1367 of NEUTRAL.f
        #subroutine LIM_ADD
        X_TRIA=np.zeros((3,TRI_ELE))
        Y_TRIA=np.zeros((3,TRI_ELE))
        Z_TRIA=np.zeros((3,TRI_ELE))
        TRI_ELE=0
        for j in range(self.nphi-1):
            for i in range(self.nRZ-1):
                COS1=np.cos(self.P[j,1]*np.pi/180)
                COS2=np.cos(self.P[j+1,1]*np.pi/180)
                SIN1=np.sin(self.P[j,1]*np.pi/180)
                SIN2=np.sin(self.P[j+1,1]*np.pi/180)
                if self.R[j+1,i]!=self.R[j+1,i+1] or self.Z[j+1,i]!=self.Z[j+1,i+1]:
                    X_TRIA[0,TRI_ELE]=self.R[j,i]*COS1
                    Y_TRIA[0,TRI_ELE]=self.R[j,i]*SIN1
                    Z_TRIA[0,TRI_ELE]=self.Z[j,i]
                    
                    X_TRIA[1,TRI_ELE]=self.R[j+1,i+1]*COS2
                    Y_TRIA[1,TRI_ELE]=self.R[j+1,i+1]*SIN2
                    Z_TRIA[1,TRI_ELE]=self.Z[j+1,i+1]
                    
                    X_TRIA[2,TRI_ELE]=self.R[j+1,i]*COS2
                    Y_TRIA[2,TRI_ELE]=self.R[j+1,i]*SIN2
                    Z_TRIA[2,TRI_ELE]=self.Z[j+1,i]
                    TRI_ELE=TRI_ELE+1
                
                if self.R[j,i]!=self.R[j,i+1] or self.Z[j,i]!=self.Z[j,i+1]:
                    X_TRIA[0,TRI_ELE]=self.R[j,i]*COS1
                    Y_TRIA[0,TRI_ELE]=self.R[j,i]*SIN1
                    Z_TRIA[0,TRI_ELE]=self.Z[j,i]
                    
                    X_TRIA[1,TRI_ELE]=self.R[j,i+1]*COS1
                    Y_TRIA[1,TRI_ELE]=self.R[j,i+1]*SIN1
                    Z_TRIA[1,TRI_ELE]=self.Z[j,i+1]
                    
                    X_TRIA[2,TRI_ELE]=self.R[j+1,i+1]*COS2
                    Y_TRIA[2,TRI_ELE]=self.R[j+1,i+1]*SIN2
                    Z_TRIA[2,TRI_ELE]=self.Z[j+1,i+1]
                    TRI_ELE=TRI_ELE+1
        
        #now we have the vertices. Compute the vertex normals according to L1008 of NEUTRAL.f
        #subroutine SETUP_ADD_SF_N0
        V_TRIA_X=np.zeros((TRI_ELE,))
        V_TRIA_Y=np.zeros((TRI_ELE,))
        V_TRIA_Z=np.zeros((TRI_ELE,))
        
        for i in range(TRI_ELE):
            V_TRIA_X[i]=((Y_TRIA[1,i]-Y_TRIA[0,i])*(Z_TRIA[2,i]-Z_TRIA[0,i])) - ((Z_TRIA[1,i]-Z_TRIA[0,i])*(Y_TRIA[2,i]-Y_TRIA[0,i]))
            
            V_TRIA_Y[i]=((Z_TRIA[1,i]-Z_TRIA[0,i])*(X_TRIA[2,i]-X_TRIA[0,i])) - ((X_TRIA[1,i]-X_TRIA[0,i])*(Z_TRIA[2,i]-Z_TRIA[0,i]))
            
            V_TRIA_Z[i]=((X_TRIA[1,i]-X_TRIA[0,i])*(Y_TRIA[2,i]-Y_TRIA[0,i])) - ((Y_TRIA[1,i]-Y_TRIA[0,i])*(X_TRIA[2,i]-X_TRIA[0,i]))
            
        self.xtri=X_TRIA
        self.ytri=Y_TRIA
        self.ztri=Z_TRIA
        self.vx=V_TRIA_X
        self.vy=V_TRIA_Y
        self.vz=V_TRIA_Z
        return
    
    def plot_tri(self, oplot=False, fig=None, ax=None):
        """
        Plot triangles with their vertices
        
        oplot: option to overplot on top of figure given by fig, ax
        
        """
        x=np.reshape(self.xtri, (np.size(self.xtri),), order='F')
        print(x[0:3])
        print(self.xtri[:,0])
        y=np.reshape(self.ytri, (np.size(self.ytri),), order='F')
        z=np.reshape(self.ztri, (np.size(self.ztri),), order='F')
        shape=np.shape(self.xtri)
        triangles=np.zeros((shape[1],3))
        for i in range(shape[1]):
            triangles[i,0]=3*i
            triangles[i,1]=3*i+1
            triangles[i,2]=3*i+2
            
        if not oplot:     
            fig=plt.figure()
            ax=fig.gca(projection='3d')
            ax.plot_trisurf(x,y,z, triangles=triangles)
        else:
            ax.plot_trisurf(x,y,z, triangles=triangles)
            
        return fig, ax

    def make_solid(self,dw=0.1):
      """
      Add second recessed surface to make wall thick. 
      Recessment in direction of local normals
      """

    def modify_normals(self):
        """
        Change order of phi to see if this adequately modifies the surface normals
        """
        P=np.zeros(np.shape(self.P))
        R=np.zeros(np.shape(self.R))
        Z=np.zeros(np.shape(self.Z))
        for i in range(self.nphi):
            P[i,:]=self.P[-1-i,:]
            R[i,:]=self.R[-1-i,:]
            Z[i,:]=self.Z[-1-i,:]
        self.Pmod=P
        self.Rmod=R
        self.Zmod=Z
        return
            
    def get_phi_lineout(self, phi, verb=False):
        """
        grabs the R and Z data for a given phi value
        """
        if np.min(self.P)>phi or np.max(self.P)<phi:
            if verb: print('Component out of range of phi value of interest')
            R=None; Z=None
        else:
            ind=np.where(self.P[:,0] == phi)
            #print(ind[0])
            R=np.squeeze(self.R[ind,:])
            Z=np.squeeze(self.Z[ind,:])
            
            #perform linear interpolation between points if phi lies between
            #two of the self.P points
            if len(ind[0])==0:
                
                #find the phi points that are closest to the value of phi
                tmp=self.P[:,0] - phi
                indn=np.where(tmp<0)
                negval=np.max(tmp[indn])
                indn=np.where(tmp==negval)
                
                indp=np.where(tmp>0)
                posval=np.min(tmp[indp])
                indp=np.where(tmp==posval)
                
                #linear interpolation
                m=np.divide(self.R[indp,:]-self.R[indn,:],self.P[indp,0]-self.P[indn,0])
                b=self.R[indp,:]-np.multiply(m,self.P[indp,0])
                R=np.squeeze(np.multiply(m,phi)+b)

                
                m=np.divide(self.Z[indp,:]-self.Z[indn,:],self.P[indp,0]-self.P[indn,0])
                b=self.Z[indp,:]-np.multiply(m,self.P[indp,0])
                Z=np.squeeze(np.multiply(m,phi)+b)
            
        return R,Z
    
    def plot_phi_lineout(self,phi,fig,ax,color='r'):
        R,z = self.get_phi_lineout(phi)
        if R is None: return
        print(R)
        nl = R.shape[0]
        for i in range(nl-1):
            ax.plot([R[i],R[i+1]],[z[i],z[i+1]],color=color)
        return fig

    def get_points_sample(self,rres,tres,pcells, g3d,report=False):
        '''
        Function to get a sampling of points along a Kisslinger mesh
        '''

        f=open(g3d, 'r')
        nr,nz,nt=np.fromfile(f, dtype='int', sep=' ', count=3)
        R=np.zeros((nr,nz,nt))
        Z=np.zeros((nr,nz,nt))
        X=np.zeros((nr,nz,nt))
        Y=np.zeros((nr,nz,nt))

        phi=np.zeros((nt,))
        for i in range(nt):
            phi[i]=np.fromfile(f,dtype='float', sep=' ', count=1)
            tmpR=np.fromfile(f,dtype='float', sep=' ', count=nr*nz)
            tmpR=np.reshape(tmpR,(nr,nz),order='F')
            R[:,:,i]=tmpR
            tmpZ=np.fromfile(f,dtype='float',sep=' ', count=nr*nz)
            tmpZ=np.reshape(tmpZ,(nr,nz), order='F')
            Z[:,:,i]=tmpZ
            X[:,:,i]=tmpR*np.cos(phi[i]*np.pi/180)
            Y[:,:,i]=tmpR*np.sin(phi[i]*np.pi/180)
        f.close()

        #read CELL_GEO file
        f=open(pcells, 'r')
        #nc = number of cells; npl=#cells for plasma transport; nn=#cells for neutral transport
        nc, npl, nn=np.fromfile(f,dtype='int', sep=' ', count=3)
        idcell=np.fromfile(f,dtype='int', sep=' ', count=np.size(R))
        f.close()
        #form boolean matrix for whether cell is a plasma cell or not
        ingrid=np.zeros((nr-1,nz-1,nt-1))
        ingrid=np.reshape(ingrid,(np.size(ingrid),), order='F')
        for i in range(len(ingrid)):
            if idcell[i] > npl:
                ingrid[i]=0
            else:
                ingrid[i]=1
 
        ingrid=np.reshape(ingrid,(nr-1,nz-1,nt-1),order='F')

        self.to_tri() #obtain coordinates as triangles
        shape=np.shape(self.xtri)
        centroids=np.zeros((shape[1],3))
        #get the centroids of the triangles of the component
        for i in range(shape[1]):
            centroids[i,0]=np.sum(self.xtri[:,i])/3
            centroids[i,1]=np.sum(self.ytri[:,i])/3
            centroids[i,2]=np.sum(self.ztri[:,i])/3

        nlineout=(self.nRZ-1) #number of centroids for a given phi (taking only 1 of triangle pair)
        ntor=self.nphi-1    #number of centroids toroidally at one R,Z position

        print('%i points per poloidal cross section' %nlineout)
        print('%i points toroidally per RZ pair' %ntor)

        vectors=np.zeros((shape[1],3))
        vectors[:,0]=self.vx
        vectors[:,1]=self.vy
        vectors[:,2]=self.vz

        #reducing the number of sampled points
        easyredp=np.floor(nlineout/rres)
        if (easyredp < 1):
            print('Error: poloidal resolution desired is higher than resolution of component.\n Using default value...')
            easyredp=1; fineredp=0

        easyredt=np.floor(ntor/tres)
        if (easyredt < 1):
            print('Error: toroidal resolution desired is higher than resolution of component. \n Using default value...')
            easyredt=1; fineredt=0

        #reduce in poloidal plane first
        centroids=centroids[::2,:] #take only one of the triangles
        vectors=vectors[::2,:]     #take only one of the triangles

        #normalize vectors to unity
        for i in range(np.shape(vectors)[0]):
            mag=np.sqrt(np.multiply(vectors[i,0],vectors[i,0])+np.multiply(vectors[i,1],vectors[i,1])+np.multiply(vectors[i,2],vectors[i,2]))
            vectors[i,:]/=mag

	#create masked array of the original resolution
        marray=np.array(np.ones((ntor,nlineout)))
        marray2=np.array(np.ones((ntor,nlineout)))

	#reduce toroidal/poloidal resolution
        marray[::int(easyredt),:]=0
        marray2=np.reshape(marray2,[len(centroids[:,0]),],order='F')
        marray2[::int(easyredp)]=0
        marray2=np.reshape(marray2,[ntor,nlineout], order='F')

        #combining toroidal and poloidal reductions
        marray+=marray2
        marray[np.where(marray==2)]=1

        #mask values which are not wanted in the point sample
        marray=np.reshape(marray,[len(centroids[:,0]),], order='C')
        centroids=np.ma.array(centroids)
        vectors=np.ma.array(vectors)
        for i in range(3):
            centroids[:,i]=np.ma.masked_where(marray == 1,centroids[:,i])
            vectors[:,i]=np.ma.masked_where(marray == 1, vectors[:,i])

        #check whether unmasked points are inside or outside of the plasma grid
        shape=np.shape(centroids)
        centroids2=np.zeros(np.shape(centroids))
        for i in range(shape[0]):
            if centroids.mask[i,0]==True:
                continue
            else:
                centroids2[i,:]=self.point_in_grid(centroids[i,:],vectors[i,:], R, phi, X, Y, Z, ingrid,report)
        ind=np.where(centroids2[:,0]!=0)
        centroids2=centroids2[ind[0],:]
        vectors=vectors[ind[0],:]
        ind=np.where(np.isnan(centroids2[:,0]) == False)
        centroids2=centroids2[ind[0],:]
        vectors=vectors[ind[0],:]
	
        #convert from x,y,z to cylindrical coordinates
        #centroidsf[:,0] = R
	#centroidsf[:,1] = Z
        #centroidsf[:,2] = phi
        #similarly with directions for vectorsf

        centroidsf=np.zeros(np.shape(centroids2))
        centroidsf[:,0]=np.sqrt(np.multiply(centroids2[:,0],centroids2[:,0])+np.multiply(centroids2[:,1],centroids2[:,1]))
        centroidsf[:,2]=np.arctan2(centroids2[:,1],centroids2[:,0])
        centroidsf[:,1]=centroids2[:,2]

        vectorsf=np.zeros(np.shape(centroids2))
        vectorsf[:,0]=np.multiply(np.cos(np.arctan2(vectors[:,1],vectors[:,0])),vectors[:,0])+np.multiply(np.sin(np.arctan2(vectors[:,1],vectors[:,0])),vectors[:,1])
        vectorsf[:,2]=np.multiply(-np.sin(np.arctan2(vectors[:,1],vectors[:,0])),vectors[:,0])+np.multiply(np.cos(np.arctan2(vectors[:,1],vectors[:,0])),vectors[:,1])
        vectorsf[:,1]=vectors[:,2]
        ind=np.where(vectorsf[:,2] < 1E-5)
        vectorsf[ind[0],2]=0
        centroidsf[:,2]=centroidsf[:,2]*180/np.pi
        return centroidsf,vectorsf

    def point_in_grid(self, p ,v,R,phi,X,Y,Z,ingrid, report=False):
        """
        checks is point is within plasma grid
        requires CELL_GEO, GRID_3D_DATA

	if it is not in the grid, it will move the point along its vector until it is in the grid
        """

        p_not_in_grid=True
        loopcount=0

        while p_not_in_grid:
            #move point along its vector if not in grid
            p[0]=p[0]+loopcount*v[0]; p[1]=p[1]+loopcount*v[1]; p[2]=p[2]+loopcount*v[2]
            pr=np.sqrt(np.multiply(p[0],p[0])+np.multiply(p[1],p[1]))
            pz=p[2]
            pt=np.arctan2(p[1],p[0])*180/np.pi
            pphi=np.arctan2(p[1],p[0])
            #determine where in toroidal cross section grid is
            resid=np.subtract(phi,pt)
            indn=np.where(resid < 0)
            indp=np.where(resid > 0)
            valn=np.max(resid[indn]); valp=np.min(resid[indp])
            ind1=np.where(resid == valn); ind2=np.where(resid == valp)
            phi1=phi[ind1]*np.pi/180;phi2=phi[ind2]*np.pi/180
            if report:
                fig,ax=plt.subplots()
                ax.pcolormesh(R[:,:,np.squeeze(ind1)],Z[:,:,np.squeeze(ind1)],np.zeros(np.shape(R[:,:,np.squeeze(ind1)])))
                ax.scatter(pr,pz)
                r2,z2=self.get_phi_lineout(pt)
                ax.plot(r2,z2)
                plt.show()
            #determine where in R, Z index the closest gridpoint is
            rtmp=np.squeeze(R[:,:,np.squeeze(ind1):np.squeeze(ind2)+1])
            ztmp=np.squeeze(Z[:,:,np.squeeze(ind1):np.squeeze(ind2)+1])
            xtmp=np.squeeze(X[:,:,np.squeeze(ind1):np.squeeze(ind2)+1])
            ytmp=np.squeeze(Y[:,:,np.squeeze(ind1):np.squeeze(ind2)+1])
            
            xtmp2=np.reshape(xtmp,np.size(rtmp),order='F')
            ytmp2=np.reshape(ytmp,np.size(ytmp),order='F')
            ztmp2=np.reshape(ztmp,np.size(ztmp),order='F')
            rx=np.subtract(xtmp2,p[0]); ry=np.subtract(ytmp2, p[1]); rz=np.subtract(ztmp2,p[2])
            
            resid2=np.sqrt(np.multiply(rx,rx)+np.multiply(ry,ry)+np.multiply(rz,rz))
            ind=np.where(resid2 == np.min(resid2))
            indg=np.where(xtmp == xtmp2[ind])


            '''
            Finding cell which contains the point.
            Center around gridpoints where residual is the smallest (indg)
            '''   
            pp=p
            #get faces of cube
            indg0=np.squeeze(indg[0]); indg1=np.squeeze(indg[1])
            indx1=indg0-10
            if indg0-10 < 0:
                indx1=0
            indx2=indg0+10
            if indg0+10 > len(xtmp[:,0,0])-1:
                indx2=len(xtmp[:,0,0])-1

            indy1=indg1-10
            if indg1-10 < 0:
                indy1=0
            indy2=indg1+10
            if indg1+10 > len(xtmp[0,:,0])-1:
                 indy2=len(xtmp[0,:,0])-1
            cind=[0,0,0]
            for i in range(indx1,indx2):
                for j in range(indy1,indy2):
                    points=np.zeros((8,3))
                    points[0,:]=np.array([xtmp[i,j,0],    ytmp[i,j,0],    ztmp[i,j,0]])
                    points[3,:]=np.array([xtmp[1+i,j,0],  ytmp[1+i,j,0],  ztmp[1+i,j,0]])
                    points[4,:]=np.array([xtmp[i,1+j,0],  ytmp[i,1+j,0],  ztmp[i,1+j,0]])
                    points[1,:]=np.array([xtmp[i,j,1],    ytmp[i,j,1],    ztmp[i,j,1]])
                    points[2,:]=np.array([xtmp[1+i,j,1],  ytmp[1+i,j,1],  ztmp[1+i,j,1]])
                    points[5,:]=np.array([xtmp[i,1+j,1],  ytmp[i,1+j,1],  ztmp[i,1+j,1]])
                    points[6,:]=np.array([xtmp[1+i,1+j,1],ytmp[1+i,1+j,1],ztmp[1+i,1+j,1]])
                    points[7,:]=np.array([xtmp[1+i,1+j,0],ytmp[1+i,1+j,0],ztmp[1+i,1+j,0]])
                    vect=np.zeros((6,3))
                    vect[0,:]=-np.cross(points[0,:]-points[1,:],points[0,:]-points[4,:])
                    vect[1,:]=-np.cross(points[0,:]-points[4,:],points[0,:]-points[3,:])
                    vect[2,:]=np.cross(points[0,:]-points[1,:],points[0,:]-points[3,:])
                    vect[3,:]=np.cross(points[6,:]-points[5,:],points[6,:]-points[7,:])
                    vect[4,:]=np.cross(points[6,:]-points[7,:],points[6,:]-points[2,:])
                    vect[5,:]=np.cross(points[6,:]-points[2,:],points[6,:]-points[5,:])
                    for k in range(6):
                        vect[k,:]=vect[k,:]/np.sqrt(np.multiply(vect[k,0],vect[k,0])+np.multiply(vect[k,1],vect[k,1])+np.multiply(vect[k,2],vect[k,2]))

                    cond=np.zeros((6,))
                    faces=np.zeros((6,3)) # face centers of cell
                    faces[0,:]=(points[0,:]+points[1,:]+points[4,:]+points[5,:])/4
                    faces[1,:]=(points[0,:]+points[3,:]+points[7,:]+points[4,:])/4
                    faces[2,:]=(points[0,:]+points[3,:]+points[2,:]+points[1,:])/4
                    faces[3,:]=(points[6,:]+points[5,:]+points[4,:]+points[7,:])/4
                    faces[4,:]=(points[6,:]+points[7,:]+points[3,:]+points[2,:])/4
                    faces[5,:]=(points[6,:]+points[5,:]+points[1,:]+points[2,:])/4
                    for k in range(6):
                        if np.matmul(pp-faces[k,:], vect[k,:].T) < 0:
                            cond[k]=1
                        else:
                            continue

                    ind=np.where(cond == 1)
                    if (len(ind[0]) == len(cond)):
                        if report:
                            print('Cell Index of Point: %i %i %i' %(i,j,np.squeeze(ind1)))
                        '''
                        fig=plt.figure()
                        ax=fig.add_subplot(111, projection='3d')
                        ax.scatter(faces[:,0],faces[:,1],faces[:,2])
                        ax.scatter(points[:,0],points[:,1],points[:,2])
                        ax.scatter(pp[0],pp[1],pp[2], color='k')
                        ax.plot([points[0,0],points[3,0]],[points[0,1],points[3,1]], [points[0,2],points[3,2]], color='b')
                        ax.plot([points[0,0],points[4,0]],[points[0,1],points[4,1]], [points[0,2],points[4,2]], color='b')
                        ax.plot([points[0,0],points[1,0]],[points[0,1],points[1,1]], [points[0,2],points[1,2]], color='b')
                        ax.plot([points[3,0],points[7,0]],[points[3,1],points[7,1]], [points[3,2],points[7,2]], color='b')
                        ax.plot([points[4,0],points[7,0]],[points[4,1],points[7,1]], [points[4,2],points[7,2]], color='b')
                        ax.plot([points[4,0],points[5,0]],[points[4,1],points[5,1]], [points[4,2],points[5,2]], color='b')
                        ax.plot([points[5,0],points[6,0]],[points[5,1],points[6,1]], [points[5,2],points[6,2]], color='b')
                        ax.plot([points[6,0],points[2,0]],[points[6,1],points[2,1]], [points[6,2],points[2,2]], color='b')
                        ax.plot([points[2,0],points[3,0]],[points[2,1],points[3,1]], [points[2,2],points[3,2]], color='b')
                        ax.plot([points[1,0],points[5,0]],[points[1,1],points[5,1]], [points[1,2],points[5,2]], color='b')
                        ax.plot([points[6,0],points[7,0]],[points[6,1],points[7,1]], [points[6,2],points[7,2]], color='b')
                        ax.plot([points[1,0],points[2,0]],[points[1,1],points[2,1]], [points[1,2],points[2,2]], color='b')
                        for k in range(6):
                            ax.plot([faces[k,0],faces[k,0]+vect[k,0]*2],[faces[k,1],faces[k,1]+vect[k,1]*2],[faces[k,2],faces[k,2]+vect[k,2]*2],color=(k*(1/6),k*(1/6),k*(1/6)), linewidth=3)
                        plt.show()
                        '''
                        cind=[i,j,np.squeeze(ind1)]
                        break
                if (len(ind[0]) == len(cond)):
                    break

            '''
            Cell index of point location has been found
            Check whether it is located in a plasma cell 
            '''
            if cind == [0,0,0]:
                if report:
                    print('Point cannot be placed in plasma grid!')
                return
            if ingrid[cind[0],cind[1],cind[2]]==1:
                if report:
                    print('Point is inside the plasma grid!')
                    print('P(X,Y,Z)= %f, %f, %f'%(p[0],p[1],p[2]))
                p_not_in_grid=False
            else:
                if report:
                    print('Point is not inside the plasma grid. :( ')
                loopcount+=1

        return p
    
    def plot_mesh(self, R=None, P=None, Z=None, oplot=False, fig=None,ax=None):
        """
        Plots Kisslinger mesh with Axes3D
        
        Option: 
            Plot modified mesh
            Inputs:
                R - R values of mesh surface
                P - phi values of mesh surface
                Z - Z values of mesh surface
        """
        if oplot:
            if np.size(R)==1:
                ax.plot_surface(self.R, self.P, self.Z, alpha=0.5, edgecolor='black')
            else:
                ax.plot_surface(R,P,Z, alpha=0.5, edgecolor='black')
        else:
            fig=plt.figure()
            ax=fig.add_subplot(111,projection='3d')
            if np.size(R)==1:
                ax.plot_surface(self.R, self.P, self.Z, alpha=0.5, edgecolor='black')
            else:
                ax.plot_surface(R,P,Z, alpha=0.5, edgecolor='black')
        ax.set_xlabel('Radius [cm]', fontsize=14, fontweight='bold')
        ax.set_ylabel('Phi [deg]', fontsize=14, fontweight='bold')
        ax.set_zlabel('Z [cm]', fontsize=14, fontweight='bold')
        
        return fig, ax
        
    def plot_mayavi_mesh(self, dat=None, fig=None, color=None,op=0.5):
        """
        Plots Kisslinger mesh with Axes3D (Mayavi)
        
        Option: 
            Plot modified mesh
            Inputs:
                dat with memebers:
                R - R values of mesh surface
                P - phi values of mesh surface
                Z - Z values of mesh surface
        """
        if fig is None: fig=mlab.figure()
        if color is None: color = (1,1,1)
        if dat is None: dat = self
        
        X,Y,Z = cylindertocartesian(dat.R,dat.Z,dat.P*degtorad)
        mlab.mesh(X,Y,Z,opacity=op,color=color,representation='surface')
        #ax.set_xlabel('Radius [cm]', fontsize=14, fontweight='bold')
        #ax.set_ylabel('Phi [deg]', fontsize=14, fontweight='bold')
        #ax.set_zlabel('Z [cm]', fontsize=14, fontweight='bold')
        return fig
  
    def set_modified_mesh(self):
        """
        Set modified mesh for saving
        """
        self.Rmod = self.R
        self.Zmod = self.Z
        self.Pmod = self.P
        return

    def modify_mesh(self, data, varname):
        """
        Provide modified mesh data to class for saving
        input: 
            data - modified R, P, or Z matrix
            varname - variable to save to, choices:
                        0=Rmod - modified R matrix
                        1=Pmod - modified phi matrix
                        2=Zmod - modified Z matrix
        """
        if varname==0:
            self.Rmod=data
        elif varname==1:
            self.Pmod=data
        else:
            self.Zmod=data
        return
        
    def save_mesh(self, header, fn ,fset=True):
        """
        Saves Kisslinger mesh
        
        input:
            header - Comment on description of mesh
            fn     - filename to save mesh to
            fset   - Set class mesh as modified
            
        """
        if fset: self.set_modified_mesh()
        size=np.shape(self.Rmod)
        np.savetxt(fn, [], header=header)
        f=open(fn,'ab')
        np.savetxt(f,np.column_stack((size[0], size[1],int(self.nfold),self.Rref, self.Zref)),fmt='%d %d %d %f %f')
        for i in range(size[0]):
            np.savetxt(f, [self.Pmod[i, 0]], fmt="%.5f")
            np.savetxt(
                f,
                np.asarray([self.Rmod[i, :], self.Zmod[i, :]]).T,
                delimiter=" ",
                fmt="%.5f",
            )
        f.close()
        return

if __name__=='__main__':
  fn='./W7X/PARTS/DIV/2012/divhorn9.t'
  f=Kisslinger(fn)
  f.modify_normals()
  p,v=f.get_points_sample(5,5, 'CELL_GEO', 'GRID_3D_DATA')
  shape=np.shape(p)
  for i in range(shape[0]):
    if p[i,0]==0:
      continue
    else:
      print(p[i,:])
      print(v[i,:])
'''
#  f.refine_poloidal_resolution(12)
  f.to_tri()
  x=f.xtri
  y=f.ytri
  z=f.ztri
  vx=f.vx
  vy=f.vy
  vz=f.vz
  shape=np.shape(x)
  #get centroid of triangles
  c=np.zeros((shape[1],3))
  for i in range(len(c)):
    c[i,0]=np.sum(x[:,i])/3
    c[i,1]=np.sum(y[:,i])/3
    c[i,2]=np.sum(z[:,i])/3
  c2=np.zeros((int(len(c[:,0])/2),3))
  c2=c[::2,:]
  vx=vx[::2]
  vy=vy[::2]
  vz=vz[::2]
  print(len(c2[:,0]))
  c3=np.zeros((221,3))
  print(len(c2[:,0])/3)
  vx2=np.zeros((221,))
  vy2=np.zeros((221,))
  vz2=np.zeros((221,))
  for i in range(17):
    c3[13*i:13*i+13,:]=c2[26*i:26*i+13,:]
    vx2[13*i:13*i+13]=vx[26*i:26*i+13]
    vy2[13*i:13*i+13]=vy[26*i:26*i+13]
    vz2[13*i:13*i+13]=vz[26*i:26*i+13]
  print(len(c3))
  fig,ax=f.plot_tri()
#  ax.scatter(c3[::2,0], c3[::2,1], c3[::2,2], c='k')
  c4=c3[::2,:]
  vx2=vx2[::2]
  vy2=vy2[::2]
  vz2=vz2[::2]
  result=np.zeros((48,3))
  result2=np.zeros((48,3))
  for i in range(8):
    tmp=np.sqrt(np.multiply(vx2[13*i+7:13*i+13],vx2[13*i+7:13*i+13])+np.multiply(vy2[13*i+7:13*i+13],vy2[13*i+7:13*i+13])+np.multiply(vz2[13*i+7:13*i+13],vz2[13*i+7:13*i+13]))
    print(np.shape(tmp))
    result2[6*i:6*i+6,0]=np.divide(vx2[13*i+7:13*i+13],tmp)
    result2[6*i:6*i+6,1]=np.divide(vy2[13*i+7:13*i+13],tmp)
    result2[6*i:6*i+6,2]=np.divide(vz2[13*i+7:13*i+13],tmp)
    ax.scatter(c4[13*i+7:13*i+13,0],c4[13*i+7:13*i+13,1],c4[13*i+7:13*i+13,2], c='k')
    ax.scatter(c4[13*i+7:13*i+13,0]+result2[6*i:6*i+6,0],c4[13*i+7:13*i+13,1]+result2[6*i:6*i+6,1],c4[13*i+7:13*i+13,2]+result2[6*i:6*i+6,2], c='k')

  print(result2)
  plt.show()
'''
        
