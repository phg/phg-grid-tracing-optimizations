import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
sys.path.append('../build/.')
from field_routines import baxisym as bas 
from field_routines import trace as tr
from field_routines import bfield as bf 
from field_routines import bprecalc as bpre
from field_routines import grid as gr 
#sys.path.append('../grid/.')
#from curve import *

twpi = 2.*np.pi
degtorad = twpi/360.

NirS=30;NirC=12;NirN=3 # radial resolution of the SOL, core and outer neutral regions
Niphalf=200;Nit=37 # poloidal and toroidal resolutions
dRcore=0.18;dRneut=0.25

Nir=NirC+NirS+NirN;Nip=2*Niphalf+1

dphi_fp=2*np.pi/5.0 # toroidal extension of the field period
phitri=2*np.pi/10.0 # toroidal coordinate in which the plasma has a triangular shape
phibean=2*np.pi/5.0 # toroidal coordinate in which the plasma a bean shape

phi_tar=phibean-2.0*np.pi/180.;dphi_tar=0.1*np.pi/180. # toroidal plane, where the limiter is located and its extension

phi_offset=0.0#-phitri  # phi phi_offset for grid and targets

Rlcfs_bean=6.200;Rax_bean=5.944e+00;zax_bean=0.0

Rlcfs_tri=6.0166;
Rax_tri=(4.2091**2+3.0581**2)**0.5;zax_tri=0.0

ltar=np.array([[5.3497,-0.7137],[5.5357,-0.3635],[5.5738,-0.3672],[5.4295,-0.7113],[5.3580,-0.9332],[5.7088,-0.9006],[5.9715,-0.7563],[5.9994,-0.8013],[5.7053,-0.9622],[5.2192,-1.0148],[5.3497,-0.7137]])

utar=np.array([[5.8122,0.9000],[6.0405,0.7212],[6.0252,0.6928],[5.7917,0.8398],[5.4456,0.9199],[5.5095,0.7766],[5.5885,0.5497],[5.5637,0.5378],[5.4625,0.7575],[5.2744,1.0124],[5.5506,1.0007],[5.8122,0.9000]])

def set_magnetic_field(fn='W7XField2.dat'):
  # Restore pre-calculated mag. field
  bpre.restore_field(fn) 
  #turn on the pre-calculated 3D magnetic field and the axi-symmetric Tokamak field off
  bf.scale_components([0,0,1.0]) 
  return

def make_fluxsurface(R,z,phi,Nip,dphi=72.):
    Rlcfs=Rlcfs_tri;zax=zax_tri;Rax=Rax_tri
    dphi = dphi*degtorad
    fl=tr.field_line([R,z,phi],dphi,Nip,substeps=360,dir=1)

    #order the points according to the theta coordinate
    thetafl=np.arctan2(fl[:,1]-zax,fl[:,0]-Rax) 
    ii=np.argsort(thetafl)
    fl=fl[ii];thetafl=thetafl[ii]    
    fl=np.vstack((fl,fl,fl))
    thetafl=np.hstack((thetafl-2*np.pi,thetafl,thetafl+2*np.pi))
    
    theta=np.linspace(-np.pi,np.pi,Nip)
    fs = np.zeros((Nip,3),order='F')*np.nan
    fs[:,0]=np.interp(theta, thetafl, fl[:,0])
    fs[:,1]=np.interp(theta, thetafl, fl[:,1])
    return fs

'''
import make_grid as mg; reload(mg)
mg.set_magnetic_field()
fig = mg.plot_magnetic_field()
plt.show(block=False)
fs = mg.make_fluxsurface(6.0,0.,0.,36)
plt.plot(fs[:,0],fs[:,1],'r+')

g=np.zeros((Nir,Nip,Nit,3),order='F')*np.nan #Grid
'''
