# -*- coding: utf-8 -*-
import os
import sys
import numpy as np
import copy
import math
#import utils.plot as pu
#import utils.colors as colors; clrs = colors.clrs()

breakpoint()
import shapely.geometry as geometry
from shapely.ops import cascaded_union, polygonize
from scipy.spatial import Delaunay
from descartes import PolygonPatch

import matplotlib.pyplot as plt
import matplotlib.tri as tri
import utils.plot as pu
import utils.constants as const



degtorad = const.degtorad

#Get concave hull
def alpha_shape(points, alpha):
    """
    Compute the alpha shape (concave hull) of a set
    of points.
    @param points: Iterable container of points.
    @param alpha: alpha value to influence the
        gooeyness of the border. Smaller numbers
        don't fall inward as much as larger numbers.
        Too large, and you lose everything!
    """
    if len(points) < 4:
        # When you have a triangle, there is no sense
        # in computing an alpha shape.
        return geometry.MultiPoint(list(points)).convex_hull
    def add_edge(edges, edge_points, coords, i, j):
        """
        Add a line between the i-th and j-th points,
        if not in the list already
        """
        if (i, j) in edges or (j, i) in edges:
        # already added
            return
        edges.add( (i, j) )
        edge_points.append(coords[ [i, j] ])
        return
    coords = np.array([point.coords[0]
                       for point in points])
    tri = Delaunay(coords)
    edges = set()
    edge_points = []
    # loop over triangles:
    # ia, ib, ic = indices of corner points of the
    # triangle
    for ia, ib, ic in tri.vertices:
        pa = coords[ia]
        pb = coords[ib]
        pc = coords[ic]
        # Lengths of sides of triangle
        a = math.sqrt((pa[0]-pb[0])**2 + (pa[1]-pb[1])**2)
        b = math.sqrt((pb[0]-pc[0])**2 + (pb[1]-pc[1])**2)
        c = math.sqrt((pc[0]-pa[0])**2 + (pc[1]-pa[1])**2)
        # Semiperimeter of triangle
        s = (a + b + c)/2.0
        # Area of triangle by Heron's formula
        area = math.sqrt(s*(s-a)*(s-b)*(s-c))
        circum_r = a*b*c/(4.0*area)
        # Here's the radius filter.
        #print circum_r
        if circum_r < 1.0/alpha:
            add_edge(edges, edge_points, coords, ia, ib)
            add_edge(edges, edge_points, coords, ib, ic)
            add_edge(edges, edge_points, coords, ic, ia)
    m = geometry.MultiLineString(edge_points)
    triangles = list(polygonize(m))
    return cascaded_union(triangles), edge_points

#Get equally spaced points along line
def get_points(line,nmp):
    #nmp includes first and last (same)
    nwp = [line.interpolate(i/float(nmp - 1), normalized=True) for i in range(nmp)]
    nwp = np.array([item.coords.xy for item in nwp]).squeeze()
    return nwp

#Get angle of connection line of point outside (x,y) to nearest point on line
def get_angle(x,y,line):
    p = Point(x,y)
    #np = line.interpolate(line.project(p))
    npt = np.array(nearest_points(line,p)[0].coords.xy).squeeze()
    vpt = np.array([x,y]).squeeze()-npt
    vpt = vpt/geom.lenvec(vpt)
    if vpt[0] < 0.:
        d = np.dot(vpt,[0.,-1.])
    else:
        d = np.dot(vpt,[0.,1.])
    ang = np.arccos(d)*180/np.pi
    #print(i, tmp1, tmp2,ang[-1])
    return ang,npt,vpt

def plot_polygon(polygon,fig=None,ls='-',ec='#000000',fc='#999999'):
    if fig is None:
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
    else:
        ax = fig.gca()
    margin = .3
    if fc is None:
        fill = False
    else:
        fill = True
    x_min, y_min, x_max, y_max = polygon.bounds
    ax.set_xlim([x_min-margin, x_max+margin])
    ax.set_ylim([y_min-margin, y_max+margin])
    patch = PolygonPatch(polygon, fc=fc,ls=ls,
                         ec=ec,fill=fill)
#                         zorder=-1)
    ax.add_patch(patch)
    return fig