       MODULE INSTALLATION
       IMPLICIT NONE

       INTEGER :: NTOL,NPOL
       REAL*8,DIMENSION(:  ),ALLOCATABLE :: PHILIM
       REAL*8,DIMENSION(:,:),ALLOCATABLE :: RLIM,ZLIM
       INTEGER :: PHIMIN,PHIMAX
       CONTAINS
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       SUBROUTINE OPEN_FILE(FILEL)
       CHARACTER (*):: FILEL

       INTEGER :: NTPER,I,J
       CHARACTER*72 :: READF
       REAL*8 :: RSHIFT,ZSHIFT
        open(99,file=FILEL)
        read(99,*) READF
        read(99,*) NTOL,NPOL,NTPER,RSHIFT,ZSHIFT

        ALLOCATE ( PHILIM(NTOL), RLIM(NPOL,NTOL),ZLIM(NPOL,NTOL) )
        PHIMIN  = NTOL
        PHIMAX  = 1
        DO I=1,NTOL
         READ(99,*) PHILIM(I)
         IF( PHILIM(I)>=0.) THEN 
           PHIMIN = MIN(I,PHIMIN)
           PHIMAX = MAX(I,PHIMAX)
         ENDIF
         DO J=1,NPOL
          READ(99,*) RLIM(J,I),ZLIM(J,I)
         ENDDO
        ENDDO
        CLOSE(99)
        END SUBROUTINE OPEN_FILE 

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       SUBROUTINE WRITE_FILE(FILEL)
       CHARACTER (*):: FILEL

       INTEGER :: NTPER,I,J
       CHARACTER*72 :: READF

        open(99,file=FILEL)
        WRITE(99,'(A)') FILEL 
        WRITE(99,*) PHIMAX-PHIMIN+1,NPOL,5,0.,0.              

        DO I=PHIMIN,PHIMAX
         WRITE(99,'(F7.3)') PHILIM(I)
         DO J=1,NPOL
          WRITE(99,'(2f11.3)') RLIM(J,I),ZLIM(J,I)
         ENDDO
        ENDDO
        CLOSE(99)
        END SUBROUTINE WRITE_FILE 

        END MODULE INSTALLATION

        program top_to_bot
        USE INSTALLATION
        implicit none
        CHARACTER*72 :: FN,fil
        integer :: IP
       
        open(1,file='../FULL/BOT/files')

        do 
        read(1,'(A)',end=10) fil
        fn = '../FULL/BOT/'//fil
        write(6,'(A)') fn
           
        call OPEN_FILE(fn)

        fn = 'BOT/'//fil

        if(PHIMAX-PHIMIN>=1) then
          write(6,'(20x,A)') fn
          call WRITE_FILE(fn)
         endif

        DEALLOCATE ( PHILIM, RLIM,ZLIM )

        ENDDO
10      continue

        end program top_to_bot
