import matplotlib.pyplot as plt
import Kisslinger
import utils.colors as colors; clrs = colors.clrs()

basepn = '~/python/tracer/W7X/structures/'
#basepn = '/draco/u/flr/GG/W7X/'

#List of components to use
pn = basepn+'vacuumvessel/'
#pn = basepn+'WALL/'
lcomp = [pn+'vessel.medium']

if True:
  pn = basepn+'components/2020/'
  #pn = basepn+'PARTS/'
  grid = [\
  'baf_lower_left_0_21',\
  'baf_lower_left_19_28',\
  'baf_lower_right',\
  'baf_upper_left',\
  'baf_upper_right',\
  'cover_lower_chamber_gap',\
  'cover_lower_gap_0-1.0',\
  'cover_lower_hor_div',\
  'cover_lower_phi=21',\
  'cover_lower_phi=28',\
  'cover_upper_chamber_gap',\
  'cover_upper_gap_0-1.5',\
  'cover_upper_hor_div',\
  'cover_upper_phi=19',\
  'cover_upper_ver_div',\
  'cut_lowerChamber',\
  'cut_upperChamber',\
  'div_hor_lower',\
  'div_hor_upper',\
  'div_ver_upper',\
  'shield_0_21_fix2.wvn',\
  'shield_21_28_fix2.wvn',\
  'shield_28_31p5_fix2.wvn',\
  'shield_31p5_32_fix2.wvn',\
  'shield_32_32p5_fix2.wvn',\
  'shield_32p5_35_fix2.wvn',\
  'shield_35_36_fix2.wvn'\
  ]

  covers = [
  'cover_lower_chamber_gap',\
  'cover_lower_gap_0-1.0',\
  'cover_lower_hor_div',\
  #'cover_lower_phi=21',\
  #'cover_lower_phi=28',\
  'cover_upper_chamber_gap',\
  'cover_upper_gap_0-1.5',\
  'cover_upper_hor_div',\
  #'cover_upper_phi=19',\
  'cover_upper_ver_div',\
  'cut_lowerChamber',\
  'cut_upperChamber',\
  ]
  sim = grid[:]
  for item in covers:
      sim.remove(item)
  print(sim)

if False:
  pn = basepn+'components/2012/'
  grid = [\
  'bafhor1.b', 'baf_m02.t', 'bafver2.b' ,\
  'bafhor1.t', 'baf_m03.t', 'bafver3.b' ,\
  'bafhor2.b', 'bafmod1.t', 'bafvern8.b',\
  'divhoran7wings.b', 'divvern8.t',\
  'divhorn9.t', 'divhoran7.b',\
  'tor_cover_1.b', 'tor_cover_2.b',\
  'tor_cover_3.t', 'tor_cover_4.t',\
  'bafhormid.b'  , 'bafver1.b', 'bafvern8.t',\
  'bafhormid.t'  , 'bafver1.t',\
  ]

  sim = grid

#Create data for grid construction
if True:
  import os
  dn = '/targets/'
  cwd = os.getcwd()
  dn = cwd+dn
  if os.path.isdir(dn):
    raise('Directory exists')
  else:
    os.mkdir(dn)

  #MAKE SET_GRID
  #Includes covers to supress plasma cells behind structures
  prefix = '~/GG/W7X/PARTS/'
  ngrid = len(grid)
  fp = open(dn+'SET_GRID','w')
  fp.write('%i      ! total number of plates\n' %(ngrid))
  for item in grid:
    fp.write("%s%s\n" %(prefix,item))
  fp.close()
  

#Plot Mayavi of grid construction structures (incl. covers)
if True:
  for i,item in enumerate(grid):
    lcomp.append(pn+item)

  #Load components
  lc = []
  for item in lcomp:
    lc.append(Kisslinger.Kisslinger(item))

  #Plot components

  col = clrs.hextorgbn(clrs.next())
  fig = lc[0].plot_mayavi_mesh(col=col,op=0.75)
  for i,item in enumerate(lc[1:]):
    print(lcomp[i])
    col = clrs.hextorgbn(clrs.next())
    print(col)
    item.plot_mayavi_mesh(fig=fig,col=col,op=0.75)

  plt.show(block=False)
  
#Create data for post-processing & Neutral modeling
#Increase resolution
if True:
  tmpr = []
  for item in sim:
    fn = pn+item
    kk = Kisslinger.Kisslinger(fn)
    kk.refine_resolution(10,5) #pol./tor.
    kk.save_mesh('Increased resolution',fn+'_res',fset=True)
    tmpr.append(item+'_res')
  tmp = tmpr
 
  #Make DEPO_TARGETS
  nsim = len(sim)
  prefix = '../x-geo/targets/'
  fp = open(dn+'DEPO_TARGETS','w')
  fp.write('*Targets to calculate the deposition on\n')
  fp.write('*File   tor. resolution    pol. resolution\n')
  fp.write('%i      ! total number of plates\n' %(nsim))
  for item in sim:
    fp.write("'%s%s'  1   1\n" %(prefix,item))
    #print(str('cp %s%s %s' %(pn,item,dn)))
    os.system(str('cp %s%s %s/' %(pn,item,dn)))
  fp.close()
  
  #MAKE ADD_SF_N0
  fp = open(dn+'ADD_SF_N0','w')
  fp.write('* The additional surfaces are represented by triangles.\n')
  fp.write('* You can either give the triangles directly, or indirectly give\n')
  fp.write('* the file containing the plate with NTRIANG=0.\n')
  fp.write('%i      ! total number of plates\n' %(nsim))
  for item in sim:
    fp.write("%5i%5i%5i\n" %(0,-4,1))
    fp.write("%s%s\n" %(prefix,item))
  fp.close()

#Plot Mayavi of simulation structures (no covers)
if True:
  for i,item in enumerate(sim):
    lcomp.append(pn+item)

  #Load components
  lc = []
  for item in lcomp:
    lc.append(Kisslinger.Kisslinger(item))

  #Plot components

  col = clrs.hextorgbn(clrs.next())
  fig = lc[0].plot_mayavi_mesh(col=col,op=0.75)
  for i,item in enumerate(lc[1:]):
    print(lcomp[i])
    col = clrs.hextorgbn(clrs.next())
    print(col)
    item.plot_mayavi_mesh(fig=fig,col=col,op=0.75)

  plt.show(block=False)

# Make surface
'''
nphi = 10
lphi = np.linspace(0.,36.,nphi)
nres = 5
for phi in lphi:
  plot Kisslinger components
  for i in range(nres):
    Take clicks on 2d Plane
    Finish with right click
    Remove with middle click
'''
