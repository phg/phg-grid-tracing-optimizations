import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
sys.path.append('../build/.')
from field_routines import baxisym as bas 
from field_routines import trace as tr
from field_routines import bprecalc as bpre 
from field_routines import bfield as bf 
from field_routines import grid as gr 
#sys.path.append('../grid/.')
#from curve import *


NirS=30;NirC=12;NirN=3 # radial resolution of the SOL, core and outer neutral regions
Niphalf=200;Nit=37 # poloidal and toroidal resolutions
dRcore=0.18;dRneut=0.25

Nir=NirC+NirS+NirN;Nip=2*Niphalf+1

dphi_fp=2*np.pi/5.0 # toroidal extension of the field period
phitri=2*np.pi/10.0 # toroidal coordinate in which the plasma has a triangular shape
phibean=2*np.pi/5.0 # toroidal coordinate in which the plasma a bean shape

phi_tar=phibean-2.0*np.pi/180.;dphi_tar=0.1*np.pi/180. # toroidal plane, where the limiter is located and its extension

phi_offset=0.0#-phitri  # phi phi_offset for grid and targets

Rlcfs_bean=6.200;Rax_bean=5.944e+00;zax_bean=0.0

Rlcfs_tri=6.0166;
Rax_tri=(4.2091**2+3.0581**2)**0.5;zax_tri=0.0

ltar=np.array([[5.3497,-0.7137],[5.5357,-0.3635],[5.5738,-0.3672],[5.4295,-0.7113],[5.3580,-0.9332],[5.7088,-0.9006],[5.9715,-0.7563],[5.9994,-0.8013],[5.7053,-0.9622],[5.2192,-1.0148],[5.3497,-0.7137]])

utar=np.array([[5.8122,0.9000],[6.0405,0.7212],[6.0252,0.6928],[5.7917,0.8398],[5.4456,0.9199],[5.5095,0.7766],[5.5885,0.5497],[5.5637,0.5378],[5.4625,0.7575],[5.2744,1.0124],[5.5506,1.0007],[5.8122,0.9000]])



fn='W7XField-EIM.dat'

if False: # set True to re-compute the field
    
    cc=pickle.load(open('W7XCoils.pik'))
    II=pickle.load(open('Currents.pik'))['I']

    wires=np.zeros((0,4))

    for k,c in cc.items():
        if abs(II[k])>1e-3:
            c[:,3]=II[k]
            c[-1,3]=0.0
            wires=np.vstack((wires,c))

    bpre.set_current_wires_xyzi(wires)
    
    epsilon=1e-10 

    bpre.prepare_field(4.2,6.5,128, -1.15,1.15,128, -epsilon,2*np.pi/5.+epsilon,72,12)# precompute the field defining radial, vertical and toroidal ranges and resolution of the grid. In toroidal direction the grid is extended by an infinitisimal epsilon to avoid aliasing effects

    bpre.save_field(fn) # store the result in a fortran format file

else:
    
    bpre.restore_field(fn) # restore the result (faster than computing)


bf.scale_components([0,0,1.0]) #turn on the pre-calculated 3D magnetic field and the axi-symmetric Tokamak field off

fig,ax=plt.subplots(1,2,figsize=(5,5),sharey=True)
def onclick(event):print('[%.4f,%.4f],' % (event.xdata,event.ydata))

fig.canvas.mpl_connect('button_press_event', onclick)

RR=np.linspace(bpre.rapre,bpre.rbpre,bpre.nrpre)
zz=np.linspace(bpre.zapre,bpre.zbpre,bpre.nzpre)

for i,phi,Rlcfs in [[0,phitri,Rlcfs_tri],[1,phibean,Rlcfs_bean]]:
    it=int((phi-bpre.fapre)/(bpre.fbpre-bpre.fapre)*bpre.nfpre) % (bpre.nfpre - 1)
    Bpol=(bpre.ba[0,it,:,:]**2+bpre.ba[1,it,:,:]**2)**0.5
    Btor=bpre.ba[2,it,:,:]
    ax[i].pcolorfast(RR,zz,Bpol.T,vmin=0,vmax=3.0)

    for dR in np.linspace(-0.05,0.1,20):
        fl=tr.field_line([Rlcfs+dR,0.0,phi],2*np.pi/5.,200,substeps=360,dir=1)
        ax[i].scatter(fl[:,0],fl[:,1],marker='x',s=1,color='black')

plt.show(block=False)

print('construct flux surfaces in confinement region')

g=np.zeros((Nir,Nip,Nit,3),order='F')*np.nan

Rlcfs=Rlcfs_tri;zax=zax_tri;Rax=Rax_tri

dR=np.hstack((dRneut,np.linspace(dRcore,1e-4,NirC-1)))

for ifs,Rfs in [[0,dRneut],[1,dRcore],[NirC-1,0.0]]:  #enumerate(dR):#[[0,Rlcfs-0.15],[1,Rlcfs-1e-4]]:
    #b=tr.field_line([Rfs,zax,phitri],2*np.pi/5.0, 1,substeps=72*5,dir=-1)
    #fl=tr.field_line(b[0,:]*1.0,np.pi,Nip,substeps=180*5,dir=1)
    fl=tr.field_line([Rlcfs-Rfs,0,phitri],dphi_fp,Nip,substeps=72*5,dir=1) #construct a flux surface by field line tracing 

    #order the points according to the theta coordinate
    thetafl=np.arctan2(fl[:,1]-zax,fl[:,0]-Rax) 
    ii=np.argsort(thetafl)
    fl=fl[ii];thetafl=thetafl[ii]    
    fl=np.vstack((fl,fl,fl))
    thetafl=np.hstack((thetafl-2*np.pi,thetafl,thetafl+2*np.pi))
    
    theta=np.linspace(-np.pi,np.pi,Nip)

    g[ifs,:,0,0]=np.interp(theta, thetafl, fl[:,0])
    g[ifs,:,0,1]=np.interp(theta, thetafl, fl[:,1])
    

print('extrapoloate intermediate surfaces linearily') 

x1=np.linspace(0.0,1.0,NirC-1)[1:-1]
x2=np.linspace(0.0,1.0,NirS+1)[1:]
x3=np.linspace(1.0,1.8,NirN+1)[1:]

irin=1;irsep=NirC-1;irneut=NirC+NirS-1
for ip in range(Nip):    
  for ic in range(2):
    g[irin+1:irsep,ip,0,ic]   =g[irin,ip,0,ic]+(g[irsep,ip,0,ic]-g[irin,ip,0,ic])*x1      
    g[irsep+1:irneut+1,ip,0,ic]=g[irsep,ip,0,ic]+(g[irsep,ip,0,ic]-g[irin,ip,0,ic])*x2
    g[irneut+1:Nir,ip,0,ic]    =g[irsep,ip,0,ic]+(g[irsep,ip,0,ic]-g[irin,ip,0,ic])*x3


print('force up/down symmetry in triangular plane.')
for ir in range(Nir):
    #ir=NirC+NirS+i
    for ip in range(Niphalf+1):
        g[ir,Nip-1-ip,0,0]= g[ir,ip,0,0]
        g[ir,Nip-1-ip,0,1]=-g[ir,ip,0,1]

g[:,:,0,2]=phitri  

for ir in range(Nir):ax[0].plot(g[ir,:,0,0],g[ir,:,0,1],color='blue',lw=0.3)
for ip in range(Nip):ax[0].plot(g[:,ip,0,0],g[:,ip,0,1],color='blue',lw=0.3)

ax[0].plot([4.25,6.5],[0]*2,color='red',ls='--')

fig.show()


fig.savefig('poincare.png')
#plt.close(fig)



#g=np.zeros((Nir,Nip,Nit,3),order='F')*np.nan

##core, SOL and neutral region separations
#x2=np.linspace(0.0,1.0,NirS+1)[:-1]
#x3=np.linspace(0.0,1.0,NirN)



print('extend along fieldline.')
for ir in range(Nir):
  for ip in range(Nip):    
    g[ir,ip,:,:]=tr.field_line(g[ir,ip,0,:], (phibean-phitri)/(Nit-1),Nit-1,substeps=10,dir=1) # note that Nit (and not Nit-1) points along the fieldline are given back
    

if len(np.where(np.isnan(g))[0])>0:
    raise Exception('invalid grid cells.')

err=gr.grid3d_check(g)
print('grid has %i errors' % (len(np.where(err)[0])))

exit
if False:
    print('write grid.')
    fg=open('grid.txt','w')
    fg.write('%10i %10i %10i\n' % tuple(g.shape[0:3]))
    for it in range(g.shape[2]): #iterate over poloidal planes
            fg.write('%16.8f \n' % ((g[0,0,it,2]+phi_offset)*180./np.pi))  # angle in deg
            np.savetxt(fg,np.array(g[:,:,it,0]*100).reshape(-1,order='F')) # R-coordinate in cm
            np.savetxt(fg,np.array(g[:,:,it,1]*100).reshape(-1,order='F')) # z-coordinate in cm

            
    fg.close()

    print('write field.')
    B=gr.get_b_grid(g)
    B[NirC+NirS+1:,:,:]=-1.0
    fb=open('field.txt','w')
    np.savetxt(fb,B.reshape(-1,order='F'))
    fb.close()


print('writing input files')
for fninput in ['input.geo','input.n0g']:
    s=open('template_'+fninput).read()
    s=s.replace('#Nir#',str(Nir)).replace('#Nir-1#',str(Nir-1)).replace('#Nir-2#',str(Nir-2)).replace('#Nrplasma#',str(NirC+NirS-2))
    s=s.replace('#Nip#',str(Nip)).replace('#Nip-1#',str(Nip-1)).replace('#Nip-2#',str(Nip-2))
    s=s.replace('#Nit#',str(Nit)).replace('#Nit-1#',str(Nit-1)).replace('#Nit-2#',str(Nit-2))
    open(fninput,'w').write(s)

def write_EMC3_target_file(fn,Rzp,comment=None,scaling_factor=100.0,phi_scaling_factor=1.0,phi_offset=0.0):
    if comment==None:comment=fn

    #print 'phi_scaling_factor',phi_scaling_factor
    f=open(fn,'w')
    f.write(comment+'\n')
    f.write('%i  %i   1      0.00000      0.00000\n'% (Rzp.shape[1],Rzp.shape[0]))
    for it in range(Rzp.shape[1]):
      f.write("%15.5f\n" % ((Rzp[0,it,2]+phi_offset)*phi_scaling_factor))
      for x in Rzp[:,it,0:2]*scaling_factor:
          f.write("%15.5f   %15.5f\n" % (x[0],x[1]))
    f.close()

'''
#define a grid aligned target. Its surface is shifted by 1% of the cell size radially outward to avoid aliasing-effect of the intersection routine
irt=NirC+5;itt=Nit-3;alpha=0.99
tar3D=np.zeros((Nip*2+1,2,3))

tar3D[:-1,0,:]=np.vstack((alpha*g[irt,:,itt,:]+(1.0-alpha)*g[irt+1,:,itt,:],g[Nir-1,::-1,itt,:]))
tar3D[:-1,0,2]-=0.1*np.pi/180.
tar3D[:-1,1,:]=np.vstack((alpha*g[irt,:,itt,:]+(1.0-alpha)*g[irt+1,:,itt,:],g[Nir-1,::-1,itt,:]))
tar3D[:-1,1,2]+=0.1*np.pi/180.
tar3D[-1,:,:]=tar3D[0,:,:] # close the target

write_EMC3_target_file('bean_target.txt',tar3D,phi_scaling_factor=180./np.pi,phi_offset=phi_offset)
# you could trivially mark the cells writing
# targetcells[irt:,:,itt-1:itt+1]=1
# but let's use the intersection search routine that might be required when defining more general target shapes:

'''

targetcells=np.zeros((Nir-1,Nip-1,Nit-1),dtype=np.int32,order='F') # an array to mark cells as inside (=1) or outside (=0) the targets

print('search grid intersection with targets and write out target files')

for tar,fn in [[ltar,'lower_target.txt'],[utar,'upper_target.txt']]:
    tar3D=np.zeros((tar.shape[0],2,3))
    tar3D[:,0,0:2]=tar*1.0;tar3D[:,0,2]=phi_tar-dphi_tar/2.0
    tar3D[:,1,0:2]=tar*1.0;tar3D[:,1,2]=phi_tar+dphi_tar/2.0
    
    write_EMC3_target_file(fn,tar3D,phi_scaling_factor=180./np.pi,phi_offset=phi_offset)
    
    gr.intersect3d(g,targetcells,tar3D)





print('number of targetcells found',len(np.where(targetcells)[0]))

f=open('intersection.txt','w')
pm,Npm=gr.plates_mag(targetcells) # prepare the format required by EMC3
nzone=0
for i in range(Npm): #...and write it into a file
        k=pm[i,2]+3
        f.write('%i  ' % (nzone))
        f.write((('%i ' * k) % tuple(pm[i,0:k]))+'\n')

f.close()


print('plot grid and intersection.')    

itt=34
for it in [itt]:#[0,itt-2,itt-1,itt,itt+1]:
    fig,ax=plt.subplots(figsize=(15,10))
    fig.suptitle('$\phi=%.1f^o$' % (g[0,0,it,2]*180./np.pi),fontsize=22)
    fig.canvas.mpl_connect('button_press_event', onclick)

    
    for ip in range(Nip):ax.plot(g[:,ip,it,0],g[:,ip,it,1],color='black',lw=0.3)
    for ir in range(Nir):ax.plot(g[ir,:,it,0],g[ir,:,it,1],color='black',lw=0.3)

    iir,iip=np.where(targetcells[:,:,it])
    
    for ir,ip in zip(iir,iip):
        jr=[ir,ir+1,ir+1,ir,ir]
        jp=[ip,ip,ip+1,ip+1,ip]
        ax.fill(g[jr,jp,it,0],g[jr,jp,it,1],color='orange',lw=2,zorder=-5) # color cells that are marked as target cells

    if (it==itt):
        ax.plot(tar3D[:,0,0],tar3D[:,0,1],color='blue',lw=1) # show the actual target contour
        
        for tar in [utar,ltar]:ax.plot(tar[:,0],tar[:,1],color='red',lw=2)

    ax.set_aspect('equal')
    fig.savefig('grid_plane_%03i.png' % it)
    #plt.close(fig)

    fig.show()



'''
Rt=5.9;rt=1.5;th=np.linspace(0,2*np.pi,1000)

v=curve(np.array([Rt+np.cos(th)*rt,np.sin(th)*rt]).T)

for it in range(Nit):
    g[:,:,it,2]=g[0,0,it,2]

    lv=v.totl*1.0
    
    for ip in range(Nip):
        c=curve([g[0,ip,it,0:2],g[NirC+NirS-1,ip,it,0:2]]) 
        c.extend(5.0)
        ccc=c.collide(v)
        #lpp[i]=ccc[3]*1.0
        for ic in range(2):
            g[NirC+NirS:,ip,it,ic]=c.Rz[1,ic]+(ccc[ic]-c.Rz[1,ic])*x3
    
'''
