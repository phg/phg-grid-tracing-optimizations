import numpy as np


# intersection calculation helper
# normal arr is a POINT ARRAY from which the normals get calculated
def normal_vessel_int(inner_coords, outer_coords, tht,
                      inner_coord_offset, normal_arr=None):
    if normal_arr is None:
        normal_arr = inner_coords
    tmpR, tmpz = inner_coord_offset
    R, Z = outer_coords
    # take care of edges in g (point doubling g[0] = g[-1] is bad here)
    pre_c = tht-1 if (tht-1 >= 0) else tht-2
    post_c = tht+1 if (tht+1 < inner_coords.shape[0]) else (tht+2) % inner_coords.shape[0]
    # calculate tangential vector of first inner grid point
    # and rotate it 90° for a perpendicular vector (in outer direction)
    tan_v = normal_arr[post_c] - normal_arr[pre_c]
    perp_v = np.array([tan_v[1], -tan_v[0]])
    # now find intersection point of perp line (p1,p2) through first
    # inner grid point with every vessel line (p3, p4) and chose closest one
    p_intersect = np.array([np.inf, np.inf]) # intersec. points
    relevant_i = None
    p1 = inner_coords[tht,:2] - [tmpR, tmpz]
    p2 = p1 + perp_v
    for i, (pr1, pz1, pr2, pz2) in enumerate(zip(R[:-1], Z[:-1], R[1:], Z[1:])):
        p3, p4 = np.array([pr1,pz1]), np.array([pr2,pz2])
        # https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
        D = (p1[0] - p2[0])*(p3[1] - p4[1]) - (p1[1] - p2[1])*(p3[0] - p4[0])
        if D == 0: continue # points get set to np.inf
        detA, detB = p1[0]*p2[1]-p1[1]*p2[0], p3[0]*p4[1]-p3[1]*p4[0]
        new_p_intersect = (detA*(p3-p4) - detB*(p1-p2))/D
        # check if point between vessel points & in right direction
        u = ((p1[0]-p3[0])*(p1[1]-p2[1])-(p1[1]-p3[1])*(p1[0]-p2[0]))/D
        t = np.dot(perp_v, new_p_intersect - p1)
        if 0 <= u <= 1 and t > 0 :
            curr_dist = np.linalg.norm(p_intersect - p1)
            new_dist = np.linalg.norm(new_p_intersect - p1)
            if new_dist < curr_dist:
                p_intersect = new_p_intersect
                relevant_i = i

    assert relevant_i is not None
    return p_intersect, relevant_i

# takes a thtsize x phisize input raw density function
# it then converts it to an inverse cdf along the tht-axis
# (so changes in other phi coords dont affect the output at the orig. phi coord)
# pdf(tht) = const -> invcdf(x) = x
def convert_rawpdf_to_invcdf(pdf, invcdf_resolution):
    pdf = pdf / pdf.sum(axis=0)
    thtsize, phisize = pdf.shape
    invcdf_x1d = np.linspace(0, 1, invcdf_resolution)
    cdf_x1d = np.linspace(0, 1, thtsize+1)
    cdf_y = np.cumsum(np.insert(pdf, 0, np.zeros(phisize), axis=0), axis=0)
    inv_cdf = np.empty((invcdf_resolution, phisize))
    for i, cdf_y1d in enumerate(cdf_y.T):
        inv_cdf[:,i] = np.interp(invcdf_x1d, cdf_y1d, cdf_x1d)
    return inv_cdf

def equal_spaced_points(x_coords, y_coords, n, x_out, y_out,
                        fractional_space=None, pprint=False):
    # x and y coord differences between points 
    xcd = np.diff(x_coords) # 
    ycd = np.diff(y_coords)
    # calulate length of path and therefore length between points
    point_dists = (xcd**2 + ycd**2)**(1/2)
    arc_len = np.sum(point_dists)
    point_dist = arc_len/(n-1)
    if(pprint): 
        print("\n", arc_len, point_dist)
        print("equal" if fractional_space is None else "fractional", "spacing")
        
    # find coord of mth point by going along subsampling points
    # until it would be too ling, and then interpolating
    for i in range(n-1): # [0, n]
        if fractional_space is None:
            # naive version, every point has same distance
            full_len = i*point_dist
        else:
            # point distance fractions along the path are given by frac._sp.
            full_len = fractional_space[i]*arc_len
            
        path_len, c = 0, 0
        while True: # go along polypath
            # would you be further than intended at the next point
            if path_len + point_dists[c] >= full_len:
                # YES: interpolate the goal between current and next point
                frac = (full_len - path_len)/point_dists[c]
                x_out[i] = x_coords[c] + frac*xcd[c]
                y_out[i] = y_coords[c] + frac*ycd[c]
                if(pprint): print(i, full_len)
                break
            else:
                # NO: go to next path segment
                path_len += point_dists[c]
                c += 1
    # place last point at same coord as first one
    x_out[-1] = x_coords[0]
    y_out[-1] = y_coords[0]

def multi_smooth(arr, num):
    def smooth(arr):
        padded_arr = np.pad(arr, [(1,1),(0,0)], mode="wrap")
        return 0.5*arr + 0.25*(padded_arr[:-2] + padded_arr[2:])
    
    for i in range(num):
        arr = smooth(arr)
    return arr