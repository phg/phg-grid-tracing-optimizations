import numpy as np
import matplotlib.pyplot as plt
from importlib import reload
import pickle
import sys
sys.path.append('../build/.')
from field_routines import baxisym as bas
from field_routines import bprecalc as bpc
from field_routines import bfield as bf
from field_routines import trace as tr
from field_routines import tools

import utils.data as data; reload(data)
import geometry.w7x.w7xgeo as wg; reload(wg)

#-------------------
twpi = 2.*np.pi
deg2rad = np.pi/180.

#Define number of windings
nwind = data.data()
nwind.IP  = np.array([-108,-108,-108,-108,-108,-36,-36])
nwind.ICC = np.array([8, -8,8, -8,8, -8,8, -8,8, -8])
nwind.ITC = np.array([48,72,48,48,48])
  # at module 2 smaller coil corss section but with more windings

#Define Coils index list
#5x(10xNPA), 10xPA, 10xPB,10xICC,5xITC
cid = [   #NP1
          160, 165, 170, 175, 180, 185, 190, 195, 200, 205,
          #NP2
          161, 166, 171, 176, 181, 186, 191, 196, 201, 206,
          #NP3
          162, 167, 172, 177, 182, 187, 192, 197, 202, 207,
          #NP4
          163, 168, 173, 178, 183, 188, 193, 198, 203, 208,
          #NP5
          164, 169, 174, 179, 184, 189, 194, 199, 204, 209,
          #P1
          210, 212, 214, 216, 218, 220, 222, 224, 226, 228,
          #P2
          211, 213, 215, 217, 219, 221, 223, 225, 227, 229,
          #ICC
          230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
          #ITC
          350, 241, 351, 352, 353]
cid = np.array(cid)
cids = {}
ckeys = ['np1','np2','np3','np4','np5','p1','p2','icc','itc']
cids['np1']= [160, 165, 170, 175, 180, 185, 190, 195, 200, 205]
cids['np2']= [161, 166, 171, 176, 181, 186, 191, 196, 201, 206]
cids['np3']= [162, 167, 172, 177, 182, 187, 192, 197, 202, 207]
cids['np4']= [163, 168, 173, 178, 183, 188, 193, 198, 203, 208]
cids['np5']= [164, 169, 174, 179, 184, 189, 194, 199, 204, 209]
cids['p1'] = [210, 212, 214, 216, 218, 220, 222, 224, 226, 228]
cids['p2'] = [211, 213, 215, 217, 219, 221, 223, 225, 227, 229]
cids['icc']= [230, 231, 232, 233, 234, 235, 236, 237, 238, 239]
cids['itc']= [350, 241, 351, 352, 353]
nwis = {}
nwis['np1']= [-108]*10
nwis['np2']= [-108]*10
nwis['np3']= [-108]*10
nwis['np4']= [-108]*10
nwis['np5']= [-108]*10
nwis['p1'] = [-36]*10
nwis['p2'] = [-36]*10
nwis['icc']= [8, -8,8, -8,8, -8,8, -8,8, -8] #Size
#nwis['icc']= [8, 8,8, 8,8, 8,8, 8,8, 8] #Sweep
nwis['itc']= [48,72,48,48,48]

#-------------------
def plot_coils(coils):
  from mayavi import mlab
  for i,item in enumerate(cid):
    ww = coils[item]
    x,y,z = ww.x,ww.y,ww.z
    mlab.plot3d(x,y,z)
  return

#-------------------
def get_coils(fn='W7XCoils.pik',fplt=False):
  #Get and assemble coil dataset
  coils = data.data()
  for i,item in enumerate(cid):
      x,y,z = wg.coil_data(item)
      #I=np.zeros(x.shape)+Icur[i]
      coils[item] = data.data()
      coils[item].x, coils[item].y, coils[item].z = x, y, z
  if fn != '':
    coils.save(fn,2)

  #Visualize coils
  if fplt:
    plot_coils(coils)
  return coils

#-------------------
def load_coils(fn='W7XCoils.pik',fplt=False):
  coils = data.data()
  coils.load(fn)
  #Visualize coils
  if fplt:
    plot_coils(coils)
  return coils
  
#-------------------
def get_config(config='EIM'):
  if config == 'EIM':
    #IP  = [12881,12881,12881,12881,12881,0,0] #2.5T average(?)
    IP  = [13600,13600,13600,13600,13600,0,0] #2.5T on-axis
  elif config == 'KJM': #(HM)
    IP  = [13125,12757,12153,11550,11182,0,0]
  elif config == 'AIM': #(LM)
    IP  = [12635,13166,13166,14239,14239,0,0]
  elif config == 'FTM': #(HI)
    IP  = [14075,14075,14075,14075,14075,-9712,-9712]
  elif config == 'DBM': #(LI)
    IP  = [11769,11769,11769,11769,11769,8827,8827]
  else:
    raise('No such config defined')

  ICC = np.zeros(10)
  ITC = np.zeros(5)
  return IP,ICC,ITC

#Default EIM
#-------------------
def get_currents(IP=[13600,13600,13600,13600,13600,0,0],\
                 ICC=np.zeros(10),\
                 ITC=np.zeros(5),\
                 fn='W7XCurrents.pik'):
  Icur = []
  #10 Planar coils each type
  for i in range(7):
       Icur.extend([IP[i]*nwind.IP[i]]*10)
  Icur.extend(list(ICC*nwind.ICC))
  Icur.extend(list(ITC*nwind.ITC))
  Icur = np.array(Icur)
  cur = data.data()
  cur.I = {}
  for i,item in enumerate(cid):
      cur.I[item] = Icur[i]
  if fn != '':
    cur.save(fn,2)
  return cur

#-------------------
def load_currents(fn='W7XCurrents.pik'):
  cur = data.data()
  cur.load(fn)
  return cur

#-------------------
def set_resolution(res,verb=True):
  if res == 'std':
    nr,nz,nt = 128,128,72  #(Std)
  elif res == 'small':
    nr,nz,nt = 64,64,36    #(Small)
  elif res == 'high':
    nr,nz,nt = 256,256,144 #(High)
  elif res == 'webservice':
    #nr,nz,nt = 181,181,481 #(Ref - FLT W7X-Webservice full tors)
    nr,nz,nt = 181,181,96  #(Ref - FLT W7X-Webservice)
  elif res == "full_t_std":
    nr,nz,nt = 128,128,72*5  # std with 5 module
  elif res == "full_t_high":
    nr,nz,nt = 256,256,144*5  # high with 5 module
  else:
      try:
        nr,nz,nt = res['nr'],res['nz'],res['nt']
      except Exception:
        raise('Please specify resolution or std. keyword')
  if verb:
    print('Resolution selected: %s' %res)
    print('nr,nz,nt = %i, %i, %i' %(nr,nz,nt))
    print('')
  return nr,nz,nt

#-------------------
def make_wire(c,I):
  wire = np.stack((c.x,c.y,c.z,np.zeros(c.x.shape))).T
  wire[:,3]=I
  wire[-1,3]=0.0
  return wire
  
#-------------------
def make_wires(cur,coils):
  I = cur['I']
  wires = np.zeros((0,4))
  for k,c in coils.__dict__.items():
      if abs(I[k])>1e-3:
        wire = make_wire(c,I[k])
        wires=np.vstack((wires,wire))
  return wires

#-------------------
def make_coil_field(coils=None,cid=[160],\
                    res='small',fnb='W7XField'):
  if coils is None:
    coils = load_coils()

  for k in cid:
    wires = make_wire(coils[k],1.0)

    # Initialize B-field calculation with current filament
    bpc.set_current_wires_xyzi(wires)

    # precompute the field defining radial, vertical and toroidal ranges and 
    # resolution of the grid. In toroidal direction the grid is extended by an 
    # infinitisimal epsilon to avoid aliasing effects
    eps=1e-10

    nr,nz,nt = set_resolution(res)
    rr = [4.2,6.5]
    zr = [-1.2,1.2]
    tr = [-eps,twpi/5.+eps]
    #nc=3 or 12: Number of components (3 = Br,z,p ; 12 = Br,z,p, dr,dz,dp,...)
    nc = 12
    print('Initialize field')
    bpc.initialize_ba(rr[0],rr[1],nr,zr[0],zr[1],nz,tr[0],tr[1],nt,nc)
    print('Calculate field')
    bpc.prepare_field(rr[0],rr[1],nr,zr[0],zr[1],nz,tr[0],tr[1],nt,nc)

    # store the result in a fortran format file 
    if not(fnb is None):
      fn = fnb+'-C'+str('%03i' %k)+'.dat'
      print('Save to '+fn)
      bpc.save_field(fn)
  return

#-------------------
def make_coilgroup_field(coils=None,cids=cids,\
                    res='std',fnb='W7XField'):
  if coils is None:
    coils = load_coils()

  for name,cid in zip(cids.keys(),cids.values()):
    print(name)
    cur = data.data()
    cur.I = {}
    cls = data.data()
    for i,k in enumerate(cid):
      cur.I[k] = nwis[name][i]
      cls[k] = coils[k]
    print(cls)
    wires = make_wires(cur,cls)

    # Initialize B-field calculation with current filament
    bpc.set_current_wires_xyzi(wires)

    # precompute the field defining radial, vertical and toroidal ranges and 
    # resolution of the grid. In toroidal direction the grid is extended by an 
    # infinitisimal epsilon to avoid aliasing effects
    eps=1e-10

    nr,nz,nt = set_resolution(res)
    print(nr,nz,nt)
    rr = [4.2,6.5]
    zr = [-1.2,1.2]
    tr = [-eps,twpi/5.+eps]
    #nc=3 or 12: Number of components (3 = Br,z,p ; 12 = Br,z,p, dr,dz,dp,...)
    nc = 12
    print('Initialize field')
    bpc.initialize_ba(rr[0],rr[1],nr,zr[0],zr[1],nz,tr[0],tr[1],nt,nc)
    print('Calculate field')
    bpc.prepare_field(rr[0],rr[1],nr,zr[0],zr[1],nz,tr[0],tr[1],nt,nc)

    # store the result in a fortran format file 
    if not(fnb is None):
      fn = fnb+'-'+str('%s' %name)+'.dat'
      print('Save to '+fn)
      bpc.save_field(fn)
  return

#-------------------
def make_field(cur,coils,res='small',fn=None):
  wires = make_wires(cur,coils)
  # Initialize B-field calculation with current filament
  bpc.set_current_wires_xyzi(wires)

  # precompute the field defining radial, vertical and toroidal ranges and 
  # resolution of the grid. In toroidal direction the grid is extended by an 
  # infinitisimal epsilon to avoid aliasing effects
  eps=1e-10

  nr,nz,nt = set_resolution(res)
  rr = [4.2,6.5]
  zr = [-1.2,1.2]
  tr = [-eps,twpi/5.+eps]
  if "full_t" in res:
      tr = [-eps,twpi+eps]
  #nc=3 or 12: Number of components (3 = Br,z,p ; 12 = Br,z,p, dr,dz,dp,...)
  nc = 12
  print('Initialize field')
  bpc.initialize_ba(rr[0],rr[1],nr,zr[0],zr[1],nz,tr[0],tr[1],nt,nc)
  print('Calculate field')
  bpc.prepare_field(rr[0],rr[1],nr,zr[0],zr[1],nz,tr[0],tr[1],nt,nc)

  # store the result in a fortran format file
  if not(fn is None):
    print('Save to '+fn)
    bpc.save_field(fn)
  return

#-------------------
def compile_field(cur=None,fnb='W7XField',fsav=False):
  if cur is None:
    cur = get_currents()
  I = cur['I']
  #Get parameters
  item = cid[0]
  fn = fnb+'-C'+str('%03i' %item)+'.dat'
  bpc.restore_field(fn)
  bf.scale_components([0,0,1.0])
  dat = get_parameter()

  #Initialize B-field
  rr,zr,tr,nr,nz,nt = dat['rr'],dat['zr'],dat['tr'],dat['nr'],dat['nz'],dat['nt']
  nc = 12
  bpc.initialize_ba(rr[0],rr[1],nr,zr[0],zr[1],nz,tr[0],tr[1],nt,nc)

  #Restore pre-calculated mag. field (scaled with currents)
  for i, item in enumerate(cid):
    # Restore pre-calculated mag. field
    fn = fnb+'-C'+str('%03i' %item)+'.dat'
    bpc.add_restored_field(fn,I[item])

  #turn on the pre-calculated 3D magnetic field and the axi-symmetric Tokamak field off
  bf.scale_components([0,0,1.0])

  # store the result in a fortran format file
  if fsav:
    print('Save to '+fn)
    bpc.save_field(fn)
  return

#-------------------
def make_I(tmp=None,nwi=False):
  I = {}
  if tmp is None:
    tmp = [13.65E3]*5+[0.E3,0.E3,0.E3,0.E3]
  for i,key in enumerate(ckeys):
    I[key] = tmp[i]
  #Take windings into account (Wrong for Trim-Coils)
  #for key,nwi in zip(ckeys,[108,108,108,108,108,36,36,8,1]):
  #for key,nwi in zip(ckeys,[1,1,1,1,1,1,1,1,1]):
  #    I[key] = I[key]*nwi
  return I
  
#-------------------
def compile_field_group(I,fnb='W7XField',fsav=False):
  #Get parameters
  name = list(cids.keys())[0]
  fn = fnb+'-'+str('%s' %name)+'.dat'
  bpc.restore_field(fn)
  bf.scale_components([0,0,1.0])
  dat = get_parameter()

  #Initialize B-field
  rr,zr,tr,nr,nz,nt = dat['rr'],dat['zr'],dat['tr'],dat['nr'],dat['nz'],dat['nt']
  nc = 12
  bpc.initialize_ba(rr[0],rr[1],nr,zr[0],zr[1],nz,tr[0],tr[1],nt,nc)

  #Restore pre-calculated mag. field (scaled with currents)
  for i, item in enumerate(cids.keys()):
    print(item,I[item])
    # Restore pre-calculated mag. field
    fn = fnb+'-'+str('%s' %item)+'.dat'
    bpc.add_restored_field(fn,I[item])

  #turn on the pre-calculated 3D magnetic field and the axi-symmetric Tokamak field off
  bf.scale_components([0,0,1.0])

  # store the result in a fortran format file
  if fsav:
    print('Save to CField.dat')
    bpc.save_field('CField.dat')
  return

#-------------------
def onclick(event): 
  print('[%.4f,%.4f],' % (event.xdata,event.ydata))
  return

#-------------------
def get_parameter():
  dat = data.data()
  dat['nt'] = bpc.nfpre
  dat['nr'] = bpc.nrpre
  dat['nz'] = bpc.nzpre
  dat['nc'] = bpc.ncomppre
  dat['rr'] = [bpc.rapre, bpc.rbpre]
  dat['zr'] = [bpc.zapre, bpc.zbpre]
  dat['tr'] = [bpc.fapre, bpc.fbpre]
  return dat

#-------------------
def set_magnetic_field(fn='W7XField.dat',fplt=False):
    # Restore pre-calculated mag. field
    bpc.restore_field(fn)
    bf.scale_components([0,0,1.0])
    if fplt:
      plot_magnetic_field()
    return

def find_opoint():
  pass
  return

#-------------------
def plot_magnetic_field(phil=[0.,36.*deg2rad],Rsepl=[6.200,6.0166],
                        xr=[4.5,6.35],yr=[-1.2,1.2],fig=None):
  if fig is None:
    fig,ax=plt.subplots(1,2,figsize=(12,8),sharey=True)
  else:
    ax = fig.get_axes()
  fig.canvas.mpl_connect('button_press_event', onclick)

  #Plot poloidal field in triangular and bean shaped plane
  RR=np.linspace(bpc.rapre,bpc.rbpre,bpc.nrpre) #R-range vector
  zz=np.linspace(bpc.zapre,bpc.zbpre,bpc.nzpre) #z-range vector
  pp=np.linspace(bpc.fapre, bpc.fbpre,bpc.nfpre) #phi-range vector

  #Get field on-axis in bean-shaped plane
  Raxis,zaxis,phiaxis = 5.95, 0.0, 0.0
  idxR = np.argmin((RR-Raxis)**2.)
  idxz = np.argmin((zz-zaxis)**2.)
  idxp = np.argmin((pp-phiaxis)**2)
  Bpolaxis=(bpc.ba[2,idxp,idxR,idxz]**2+bpc.ba[1,idxp,idxR,idxz]**2)**0.5
  Btoraxis=bpc.ba[0,idxp,idxR,idxz]
  print('')
  print('Bfield on-axis:')
  print('phi = %.2f ; B = %.2f;  %.2f; %.2f' %(phiaxis/deg2rad,bpc.ba[0,idxp,idxR,idxz],bpc.ba[1,idxp,idxR,idxz],bpc.ba[2,idxp,idxR,idxz]))
  print('phi = %.2f ; Btor = %.2f; Bpol = %.2f' %(phiaxis/deg2rad,Btoraxis,Bpolaxis))
  print('')

  il = [i for i in range(len(phil))]
  for i,phi,Rlcfs in zip(il,phil,Rsepl):
      #Get index closest to planes
      it=int((phi-bpc.fapre)/(bpc.fbpre-bpc.fapre)*bpc.nfpre) % (bpc.nfpre - 1)
      #Get fields
      #Initiallz Bp 0+1 und Bt 2
      Bpol=(bpc.ba[2,it,:,:]**2+bpc.ba[1,it,:,:]**2)**0.5
      Btor=bpc.ba[0,it,:,:]
      #Plot
      ax[i].pcolorfast(RR,zz,Bpol.T,vmin=0,vmax=3.0)
      #ax[i].pcolorfast(RR,zz,Btor.T,vmin=0,vmax=3.0)
      #ax[i].pcolorfast(RR,zz,bpc.ba[2,it,:,:].T,vmin=0,vmax=3.0)
      #print('phi = %.2f ; Btor = %.2f; Bpol = %.2f' %(phi/deg2rad,np.mean(Btor),np.mean(Bpol)))
      #print('phi = %.2f ; Btor = %.2f; Bpol = %.2f' %(phi/deg2rad,np.mean(Btor),np.mean(Bpol)))

      #Trace field lines around LCFS
      nr = 30
      R0 = 5.95

      #Core flux surfaces
      R = np.linspace(5.8, 6.25, 25)
      z = np.zeros(25)
      for j,(Rl,zl) in enumerate(zip(R,z)):
          fl=tr.field_line([Rl,zl,phi],2*np.pi/5.,500,substeps=360*10,dir=-1)
          ax[i].scatter(fl[:,0],fl[:,1],marker='x',s=1,color=f'C{j}')

      #for dR in np.linspace(-0.05,0.1,25):
      #    fl=tr.field_line([Rlcfs+dR,0.0,phi],2*np.pi/5.,500,substeps=360*1,dir=-1)
      #    ax[i].scatter(fl[:,0],fl[:,1],marker='x',s=1,color='black')
  for item in ax:
    item.set_xlim(xr)
    item.set_ylim(yr)
  plt.show(block=False)
  return fig

#-------------------
'''
sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)
#coils = mf.get_coils()
coils = mf.load_coils()
config = 'EIM' # <- configuration
IP,ICC,ITC = mf.get_config(config)
#ITC = np.array([6950.,0.,-6990.,-4330.,4340.])/48
cur = mf.get_currents(IP,ICC,ITC)
mf.make_field(cur,coils,res='small',fn='W7XField-'+config+'-Test.dat')
#mf.make_field(cur,coils,res='std',fn='W7XField-'+config+'-Test.dat')
mf.plot_magnetic_field()
'''

'''
sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)
coils = mf.load_coils()
res = {'nr':128,'nz':256,'nt':72}
mf.make_coil_field(coils=coils,cid=mf.cid,\
                res='std',fnb='W7XField')
'''

'''
sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)
coils = mf.load_coils()
res = {'nr':128,'nz':512,'nt':72}
#res = 'std'
mf.make_coilgroup_field(coils=coils,cids=mf.cids,\
                res=res,fnb='W7XField-Custom')
'''

'''
sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)
#config = 'EIM'
#IP,ICC,ITC = mf.get_config(config)
#cur = mf.get_currents(IP,ICC,ITC)
cur = mf.get_currents([13500]*5+[0.,0.],np.ones(10)*1.E3,np.zeros(5))
mf.compile_field(cur=cur,fnb='W7XField-Small/W7XField')
mf.plot_magnetic_field()
'''

'''
sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)
cur = mf.make_I([13.5E3]*5+[0.E3,0.E3,2.E3,0.E3])
mf.compile_field_group(cur,fnb='W7XField-Std/W7XFieldStd')
mf.plot_magnetic_field()
'''


'''
sys.path.append('/afs/ipp/u/flr/python')l
import make_field as mf ; reload(mf)
mf.set_magnetic_field('W7X-EJM.dat')
mf.plot_magnetic_field()
'''

'''
IP   = [13650,13650,13650,13650,13650,0.,0.]
#IP   = [14400,14000,13333.33,12666.67,12266.67,0,0]
ICC = [-531.77,-378.91, 255.18, 269.14,-102.66,   1.31,-300.86,-401.67, 391.24, 483.75] #Error-Field corrected exp.
#ICC  = [ 531.77, 378.91,-255.18,-269.14, 102.66,-  1.31, 300.86, 401.67,-391.24,-483.75]
ITC = [-80.82, 32.61, 100.09, 32.31,-81.02] #Error-Field corrected exp.
#ITC  = [ 80.82,-32.61,-100.09,-32.31, 81.02]
#ITC = [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
#ITC = [0.,0.,0.,0.,0.]
sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)
coils = mf.load_coils()
cur = mf.get_currents(IP,ICC,ITC)
mf.make_field(cur,coils,res='small',fn='W7XField-EJM-Test.dat')
#mf.make_field(cur,coils,res='std',fn='W7XField-'+config+'-Test.dat')
mf.plot_magnetic_field()
'''


'''
IP = [13.52E3]*5+[0.]*2
ICC = 1.*np.array([-531.77,-378.91,255.18,269.14,-102.66,1.31,-300.86,-401.67,391.24,483.75])
ITC = 1.*np.array([-80.82,32.61,100.09,32.31,-81.02])
#ITC = [0.]*5
#ICC = [0.]*10
sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)
coils = mf.load_coils()
cur = mf.get_currents(IP,ICC,ITC)
fn = 'W7XField-EJM-ErrorField.dat'
fn = None
mf.make_field(cur,coils,res='small',fn=fn)
mf.plot_magnetic_field()
'''
