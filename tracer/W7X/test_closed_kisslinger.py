#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import numpy as np
import tracer.W7X.Kisslinger as Kisslinger
from mayavi import mlab

pathtohere = os.path.realpath(__file__)
pathtofolder = "/".join(pathtohere.split("/")[:-1])

from tracer.build.field_routines import trace as tr
from tracer.build.field_routines import bprecalc as bpre 
from tracer.build.field_routines import bfield as bf 
from tracer.build.field_routines import grid as gr

offset = 0.01 # cm

def load_targets(pathtofolder, halfmodule_indices=[0], pprint=False):
    #List of components to use
    tmp = [] # f'{pathtofolder}/structures/vacuumvessel/vessel.medium']

    pn = f'{pathtofolder}/structures/components/2020/'
    tmp += [pn+item for item in [
    'baf_lower_left_0_21', 'baf_lower_left_19_28', 'baf_lower_right',
    'baf_upper_left', 'baf_upper_right', 'div_hor_lower',
    'div_hor_upper', 'div_ver_upper',
    'shield_0_21_fix2.wvn',
    'shield_21_28_fix2.wvn', 'shield_28_31p5_fix2.wvn',
    'shield_31p5_32_fix2.wvn', 'shield_32_32p5_fix2.wvn',
    'shield_32p5_35_fix2.wvn', 'shield_35_36_fix2.wvn',
    'cover_lower_chamber_gap', 'cover_lower_gap_0-1.0', 'cover_lower_hor_div',
    'cover_lower_phi=21', 'cover_lower_phi=28', 'cover_upper_chamber_gap',
    'cover_upper_gap_0-1.5', 'cover_upper_hor_div', 'cover_upper_ver_div',
    'cover_upper_phi=19', 'cut_lowerChamber', 'cut_upperChamber'
    ]]

    lcomp = []
    for i in halfmodule_indices:
        lcomp.extend([filepath for filepath in tmp])

    #Load components
    lc = []
    for item in lcomp:
        if(pprint):
            print(item)
        lc.append(Kisslinger.Kisslinger(item))
    return lc, lcomp

ls, lfilenames = load_targets(pathtofolder)

# center tracing
bpre.restore_field(f'{pathtofolder}/Fields/Field-EIM-std.dat')
bf.scale_components([0,0,1.0])
gax = tr.field_line([5.9315,0,0], 0.5*np.pi/180,
                    36*2, substeps=100, dir=1)
gax[:,2] *= 180/np.pi



for i, (l, lfilename) in enumerate(zip(ls, lfilenames)):
    with open(f"{lfilename}_extended", "w") as outfile:
        with open(f"{lfilename}", "r") as infile:
            outfile.write(next(infile))
            pol_others = np.loadtxt(lfilename, skiprows=1, max_rows=1)
            pol_others[1] *= 2
            for val in pol_others:
                outfile.write(str(val)+"\t")
            outfile.write("\n")
            next(infile)

            in_lines = [[] for _ in range(int(pol_others[0]))]
            for i, line in enumerate(infile):
                period = (int(pol_others[1])//2+1)
                print(i, i%period, pol_others)
                in_lines[i//period].append(line)

            # iterate from back
            for lineblock in in_lines:
                outfile.write(lineblock[0])
                angle = float(lineblock[0])
                center = gax[np.abs(gax[:,2]-angle).argmin()][:2] *100
                for line in lineblock[1:]:
                    outfile.write(line)
                for line in lineblock[:0:-1]:
                    values = np.fromstring(line, sep="\t")
                    diff = values - center
                    new_values = values + diff/np.linalg.norm(diff) * offset
                    for val in new_values:
                        outfile.write(str(val)+"\t")
                    outfile.write("\n")


fig = mlab.figure()
for i, (l, lfilename) in enumerate(zip(ls, lfilenames)):
    Kisslinger.Kisslinger(f'{lfilename}_extended').plot_mayavi_mesh(fig=fig)
    #l.plot_mayavi_mesh(fig=fig)
