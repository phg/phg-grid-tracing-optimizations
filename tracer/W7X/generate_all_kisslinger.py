#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 17:34:29 2022

@author: phg
"""

import os
import numpy as np
import tracer.W7X.Kisslinger as Kisslinger

pathtohere = os.path.realpath(__file__)
pathtofolder = "/".join(pathtohere.split("/")[:-1])
#List of components to use
# pn = f'{pathtofolder}/structures/vacuumvessel/'
# lcomp = [] # pn+'vessel.medium']

if True:
    pn = f'{pathtofolder}/structures/components/2020/'
    tmp = [
    'baf_lower_left_0_21', 'baf_lower_left_19_28', 'baf_lower_right',
    'baf_upper_left', 'baf_upper_right', 'div_hor_lower',
    'div_hor_upper', 'div_ver_upper', 'shield_0_21_fix2.wvn',
    'shield_21_28_fix2.wvn', 'shield_28_31p5_fix2.wvn',
    'shield_31p5_32_fix2.wvn', 'shield_32_32p5_fix2.wvn',
    'shield_32p5_35_fix2.wvn', 'shield_35_36_fix2.wvn',
    'cover_lower_chamber_gap', 'cover_lower_gap_0-1.0', 'cover_lower_hor_div',
    'cover_lower_phi=21', 'cover_lower_phi=28', 'cover_upper_chamber_gap',
    'cover_upper_gap_0-1.5', 'cover_upper_hor_div', 'cover_upper_ver_div',
    'cover_upper_phi=19', 'cut_lowerChamber', 'cut_upperChamber'
]

for i,item in enumerate(tmp):
    lcomp.append(pn+item+"_extended")


# lcomp = [f'{pathtofolder}/structures/vacuumvessel/vessel.medium']
for i in range(10):
    for item in lcomp:
        file = Kisslinger.Kisslinger(item)
        # odd components (second part of full module) need to be mirrored
        # however: phi[0,36] -> phi[0,-36] !
        if i%2:
            file = file.mirror_component()

        angle = 72 * ((i+1)//2) # odd rotated one more
        file = file.rotate_component(angle)

        header = np.loadtxt(item, skiprows=1, max_rows=1)
        with open(item) as fileraw:
            headerstr = next(fileraw)
        file.save_mesh(headerstr.strip(), item + f".hm{i}")
        print(header, headerstr)

        print(item, i, file.P.max(), file.P.min())

