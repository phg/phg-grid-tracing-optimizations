import matplotlib.pyplot as plt
import numpy as np
from importlib import reload
import utils.data as data

def make_currents(IP,ICC=None,ITC=None,fn='Currents.pik'):
  dat = data.data()
  coils = data.data()

  #Default values
  if IP is None:
    IP = np.array([13600]*5+[0.]*2)
  else:
    IP = np.array(IP)
  if ITC is None:
    ITC = np.zeros(5)
  else:
    ITC = np.array(ITC)
  if ICC is None:
    ICC = np.zeros(10)
  else:
    ICC = np.array(ICC)

  #Standard Coils
  IPG = np.array([108,108,108,108,108,36,36]) #Windings
  IPG = -1.*IPG #Set direction
  IP = np.array(IP)*IPG

  #Sweep/Control Coils
  IccG = np.array([1,-1,1,-1,1,-1,1,-1,1,-1])*IccG  #Set direction
  Icc = np.array(ICC)*IccG

  #Trim Coils
  # at module 2 smaller coil corss section but with more windings
  ItcG = np.array([48,72,48,48,48])
  Itc = np.array(ITC)*ItcG 

  # --- Assign currents --- #
  Icur = []
  #10 Planar coils each type
  tmp = np.ones(10)
  for I in IP:
       Icur.extend(list(I*tmp))
  Icur.extend(list(Icc))
  Icur.extend(list(Itc))
  Icur = np.array(Icur)
  print(Icur.shape)

#Define Coils index list
#5x(10xNPA), 10xPA, 10xPB,10xICC,5xITC
  cid = [ #NP1
          160, 165, 170, 175, 180, 185, 190, 195, 200, 205,
          #NP2
          161, 166, 171, 176, 181, 186, 191, 196, 201, 206,
          #NP3
          162, 167, 172, 177, 182, 187, 192, 197, 202, 207,
          #NP4
          163, 168, 173, 178, 183, 188, 193, 198, 203, 208,
          #NP5
          164, 169, 174, 179, 184, 189, 194, 199, 204, 209,
          #P1
          210, 212, 214, 216, 218, 220, 222, 224, 226, 228,
          #P2
          211, 213, 215, 217, 219, 221, 223, 225, 227, 229,
          #ICC
          230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
          #ITC
          350, 241, 351, 352, 353]
  wlist = np.array(cid)

  cur = data.data()
  cur.I = {}
  cur.id = cid
  for i,item in enumerate(cid):
      cur.I[item] = Icur[i]
  if fn != '':
    cur.save(fn,2)
  return cur
