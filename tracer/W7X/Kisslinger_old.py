#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 09:28:09 2019

@author: viwi
Read and modify Kisslinger meshes
"""

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from mayavi import mlab
import utils.data as data

degtorad = np.pi/180

def cylindertocartesian(r,z,p): 
  r,z,p = np.array(r),np.array(z),np.array(p)
  x = r*np.cos(p)
  y = r*np.sin(p)
  z = z
  return x,y,z



class Kisslinger():
    """
    Read, plot and modify Kisslinger meshes
    
    input: 
        fn - filename of the mesh
        
    output:
        Kisslinger.R - Radial values of surface mesh
        Kisslinger.Z - Z values of the surface mesh
        Kisslinger.P - Phi values of the surface mesh
    """
    def __init__(self, fn, pprint=True):
        """
        Read Kisslinger Mesh
        
        File structure:
            
            First row: description of mesh
            
            Second row:
                First number  - nPhi in mesh (int)
                Second number - nRZ in mesh (int)
                Third number  - nfold periodicity (int)
                Fourth number - R reference point (float)
                Fifth number  - Z reference point (float)
                
            Third row onwards: mesh data
        """
        header=np.loadtxt(fn,skiprows=1, max_rows=1)
        nphi=int(header[0])
        nRZ=int(header[1])
    #    print(' '.join(np.loadtxt(fn,dtype='str',max_rows=1)))
        if(pprint):
            print('File: %s' %fn.split('/')[-1])
            print('Kisslinger mesh with %d Phi values with %d R,Z values per phi.'%(nphi, nRZ))
            print('%d-fold periodicity'%header[2])
            print('Reference point: %f [cm] %f [cm] (R,Z)'%(header[3],header[4]))
        
        R=np.zeros((nphi,nRZ))
        Z=np.zeros(np.shape(R))
        P=np.ones(np.shape(R))
        for i in range(nphi):
            P[i,:]=np.multiply(P[i,:],np.loadtxt(fn,skiprows=2+(nRZ+1)*i, max_rows=1))
            tmp=np.loadtxt(fn,skiprows=3+(nRZ+1)*i,max_rows=nRZ,usecols=(0,1))
            R[i,:]=tmp[:,0]
            Z[i,:]=tmp[:,1]
        
        self.R=R
        self.Z=Z
        self.P=P
        self.nphi=nphi
        self.nRZ=nRZ
        self.Rref=header[3]
        self.Zref=header[4]
        self.nfold=header[2]
        return

    def refine_poloidal_resolution(self,fac):
        '''
        Refine poloidal resolution by fac
        '''
        R,Z,P =[],[],[]
        for i in range(self.nRZ-1):
            R0,Z0,R1,Z1 = self.R[:,i],self.Z[:,i],self.R[:,i+1],self.Z[:,i+1]
            P0 = self.P[:,i]
            dR, dZ = (R1-R0)/fac, (Z1-Z0)/fac
            for j in range(fac):
                R.append(R0+dR*j)
                Z.append(Z0+dZ*j)
                P.append(P0)
    
        R,Z,P = np.array(R).T,np.array(Z).T,np.array(P).T
        #dat = data.data()
        #dat.R, dat.Z, dat.P = R,Z,P
        #self.plot_mayavi_mesh(self)
        #self.plot_mayavi_mesh(dat)
        self.R,self.Z,self.P = R,Z,P
        self.nRZ = self.R.shape[1]
        return
    
    def refine_toroidal_resolution(self,fac):
        '''
        Refine toroidal resolution by fac
        '''
        R,Z,P = [],[],[]
        for i in range(self.nphi-1):
            P0,P1 = self.P[i,0],self.P[i+1,0]
            dphi = (P1- P0)/fac
            for j in range(fac):
                P.append(np.ones(self.nRZ)*P0+j*dphi)
                Rp,Zp = self.get_phi_lineout(P[-1][0])
                R.append(Rp)
                Z.append(Zp)
            
        P.append(self.P[-1,:])
        Rp,Zp = self.get_phi_lineout(P[-1][0])
        R.append(Rp)
        Z.append(Zp)
        R,Z,P = np.array(R),np.array(Z),np.array(P)
        #dat = data.data()
        #dat.R, dat.Z, dat.P = R, Z, P
        #self.plot_mayavi_mesh(self)
        #self.plot_mayavi_mesh(dat)
        self.R,self.Z,self.P = R,Z,P
        self.nphi = self.R.shape[0]
        return

    def refine_resolution(self,facpol,factor):
        '''
        Refine poloidal/toroidal resolution by facpol/factor
        '''
        self.refine_toroidal_resolution(factor)
        self.refine_poloidal_resolution(facpol)
        return

    def to_tri(self):
        """
        Convert Kisslinger data to triangles as done in EMC3
        
        In Kisslinger.py - R=shape(nphi, nRZ)
        TRI_ELE = number of triangles made from the kisslinger mesh
        X_TRIA,Y_TRIA, Z_TRIA = x,y,z vertices of the triangles
        V_TRIA_X, V_TRIA_Y, V_TRIA_Z = x,y,z components of surface normal for  each triangle
        """
        #check the total number of triangles 
        TRI_ELE=0
        for j in range(self.nphi-1):
            for i in range(self.nRZ-1):
                if self.R[j,i]!=self.R[j,i+1] or self.Z[j,i]!=self.Z[j,i+1]:
                    TRI_ELE=TRI_ELE+1
                if self.R[j+1,i]!=self.R[j+1,i+1] or self.Z[j+1,i]!=self.Z[j+1,i+1]:
                    TRI_ELE=TRI_ELE+1
        print('%i Triangles' %TRI_ELE)
        
        #compute vertices of triangle, done similarly to L1367 of NEUTRAL.f
        #subroutine LIM_ADD
        X_TRIA=np.zeros((3,TRI_ELE))
        Y_TRIA=np.zeros((3,TRI_ELE))
        Z_TRIA=np.zeros((3,TRI_ELE))
        TRI_ELE=0
        for j in range(self.nphi-1):
            for i in range(self.nRZ-1):
                COS1=np.cos(self.P[j,1]*np.pi/180)
                COS2=np.cos(self.P[j+1,1]*np.pi/180)
                SIN1=np.sin(self.P[j,1]*np.pi/180)
                SIN2=np.sin(self.P[j+1,1]*np.pi/180)
                if self.R[j+1,i]!=self.R[j+1,i+1] or self.Z[j+1,i]!=self.Z[j+1,i+1]:
                    X_TRIA[0,TRI_ELE]=self.R[j,i]*COS1
                    Y_TRIA[0,TRI_ELE]=self.R[j,i]*SIN1
                    Z_TRIA[0,TRI_ELE]=self.Z[j,i]
                    
                    X_TRIA[1,TRI_ELE]=self.R[j+1,i+1]*COS2
                    Y_TRIA[1,TRI_ELE]=self.R[j+1,i+1]*SIN2
                    Z_TRIA[1,TRI_ELE]=self.Z[j+1,i+1]
                    
                    X_TRIA[2,TRI_ELE]=self.R[j+1,i]*COS2
                    Y_TRIA[2,TRI_ELE]=self.R[j+1,i]*SIN2
                    Z_TRIA[2,TRI_ELE]=self.Z[j+1,i]
                    TRI_ELE=TRI_ELE+1
                
                if self.R[j,i]!=self.R[j,i+1] or self.Z[j,i]!=self.Z[j,i+1]:
                    X_TRIA[0,TRI_ELE]=self.R[j,i]*COS1
                    Y_TRIA[0,TRI_ELE]=self.R[j,i]*SIN1
                    Z_TRIA[0,TRI_ELE]=self.Z[j,i]
                    
                    X_TRIA[1,TRI_ELE]=self.R[j,i+1]*COS1
                    Y_TRIA[1,TRI_ELE]=self.R[j,i+1]*SIN1
                    Z_TRIA[1,TRI_ELE]=self.Z[j,i+1]
                    
                    X_TRIA[2,TRI_ELE]=self.R[j+1,i+1]*COS2
                    Y_TRIA[2,TRI_ELE]=self.R[j+1,i+1]*SIN2
                    Z_TRIA[2,TRI_ELE]=self.Z[j+1,i+1]
                    TRI_ELE=TRI_ELE+1
        
        #now we have the vertices. Compute the vertex normals according to L1008 of NEUTRAL.f
        #subroutine SETUP_ADD_SF_N0
        V_TRIA_X=np.zeros((TRI_ELE,))
        V_TRIA_Y=np.zeros((TRI_ELE,))
        V_TRIA_Z=np.zeros((TRI_ELE,))
        
        for i in range(TRI_ELE):
            V_TRIA_X[i]=((Y_TRIA[1,i]-Y_TRIA[0,i])*(Z_TRIA[2,i]-Z_TRIA[0,i])) - ((Z_TRIA[1,i]-Z_TRIA[0,i])*(Y_TRIA[2,i]-Y_TRIA[0,i]))
            
            V_TRIA_Y[i]=((Z_TRIA[1,i]-Z_TRIA[0,i])*(X_TRIA[2,i]-X_TRIA[0,i])) - ((X_TRIA[1,i]-X_TRIA[0,i])*(Z_TRIA[2,i]-Z_TRIA[0,i]))
            
            V_TRIA_Z[i]=((X_TRIA[1,i]-X_TRIA[0,i])*(Y_TRIA[2,i]-Y_TRIA[0,i])) - ((Y_TRIA[1,i]-Y_TRIA[0,i])*(X_TRIA[2,i]-X_TRIA[0,i]))
            
        self.xtri=X_TRIA
        self.ytri=Y_TRIA
        self.ztri=Z_TRIA
        self.vx=V_TRIA_X
        self.vy=V_TRIA_Y
        self.vz=V_TRIA_Z
        return
    
    def plot_tri(self, oplot=False, fig=None, ax=None):
        """
        Plot triangles with their vertices
        
        oplot: option to overplot on top of figure given by fig, ax
        
        """
        x=np.reshape(self.xtri, (np.size(self.xtri),), order='F')
        print(x[0:3])
        print(self.xtri[:,0])
        y=np.reshape(self.ytri, (np.size(self.ytri),), order='F')
        z=np.reshape(self.ztri, (np.size(self.ztri),), order='F')
        shape=np.shape(self.xtri)
        triangles=np.zeros((shape[1],3))
        for i in range(shape[1]):
            triangles[i,0]=3*i
            triangles[i,1]=3*i+1
            triangles[i,2]=3*i+2
            
        if not oplot:     
            fig=plt.figure()
            ax=fig.gca(projection='3d')
            ax.plot_trisurf(x,y,z, triangles=triangles)
        else:
            ax.plot_trisurf(x,y,z, triangles=triangles)
            
        return fig, ax

    def make_solid(self,dw=0.1):
      """
      Add second recessed surface to make wall thick. 
      Recessment in direction of local normals
      """

    def modify_normals(self):
        """
        Change order of phi to see if this adequately modifies the surface normals
        """
        P=np.zeros(np.shape(self.P))
        R=np.zeros(np.shape(self.R))
        Z=np.zeros(np.shape(self.Z))
        for i in range(self.nphi):
            P[i,:]=self.P[-1-i,:]
            R[i,:]=self.R[-1-i,:]
            Z[i,:]=self.Z[-1-i,:]
        self.Pmod=P
        self.Rmod=R
        self.Zmod=Z
        return
            
    def get_phi_lineout(self, phi):
        """
        grabs the R and Z data for a given phi value
        """
        if np.min(self.P)>phi or np.max(self.P)<phi:
            print('Component out of range of phi value of interest')
            R=None; Z=None
        else:
            ind=np.where(self.P[:,0] == phi)
            #print(ind[0])
            R=np.squeeze(self.R[ind,:])
            Z=np.squeeze(self.Z[ind,:])
            
            #perform linear interpolation between points if phi lies between
            #two of the self.P points
            if len(ind[0])==0:
                
                #find the phi points that are closest to the value of phi
                tmp=self.P[:,0] - phi
                indn=np.where(tmp<0)
                negval=np.max(tmp[indn])
                indn=np.where(tmp==negval)
                
                indp=np.where(tmp>0)
                posval=np.min(tmp[indp])
                indp=np.where(tmp==posval)
                
                #linear interpolation
                m=np.divide(self.R[indp,:]-self.R[indn,:],self.P[indp,0]-self.P[indn,0])
                b=self.R[indp,:]-np.multiply(m,self.P[indp,0])
                R=np.squeeze(np.multiply(m,phi)+b)

                
                m=np.divide(self.Z[indp,:]-self.Z[indn,:],self.P[indp,0]-self.P[indn,0])
                b=self.Z[indp,:]-np.multiply(m,self.P[indp,0])
                Z=np.squeeze(np.multiply(m,phi)+b)
            
        return R,Z

    def plot_phi_lineout(self,phi,fig,ax,color='r',norm=1.):
        R,z = self.get_phi_lineout(phi)
        if R is None: return
        print(R)
        nl = R.shape[0]
        for i in range(nl-1):
            ax.plot([R[i]*norm,R[i+1]*norm],[z[i]*norm,z[i+1]*norm],color=color)
        return fig
    
    def plot_mesh(self, R=None, P=None, Z=None, oplot=False, fig=None,ax=None):
        """
        Plots Kisslinger mesh with Axes3D
        
        Option: 
            Plot modified mesh
            Inputs:
                R - R values of mesh surface
                P - phi values of mesh surface
                Z - Z values of mesh surface
        """
        if oplot:
            if np.size(R)==1:
                ax.plot_surface(self.R, self.P, self.Z, alpha=0.5, edgecolor='black')
            else:
                ax.plot_surface(R,P,Z, alpha=0.5, edgecolor='black')
        else:
            fig=plt.figure()
            ax=fig.add_subplot(111,projection='3d')
            if np.size(R)==1:
                ax.plot_surface(self.R, self.P, self.Z, alpha=0.5, edgecolor='black')
            else:
                ax.plot_surface(R,P,Z, alpha=0.5, edgecolor='black')
        ax.set_xlabel('Radius [cm]', fontsize=14, fontweight='bold')
        ax.set_ylabel('Phi [deg]', fontsize=14, fontweight='bold')
        ax.set_zlabel('Z [cm]', fontsize=14, fontweight='bold')
        
        return fig, ax
        
    def plot_mayavi_mesh(self, dat=None, fig=None, col=None,op=0.5):
        """
        Plots Kisslinger mesh with Axes3D (Mayavi)
        
        Option: 
            Plot modified mesh
            Inputs:
                dat with memebers:
                R - R values of mesh surface
                P - phi values of mesh surface
                Z - Z values of mesh surface
        """
        if fig is None: fig=mlab.figure()
        if col is None: col = (1,1,1)
        if dat is None: dat = self
        
        X,Y,Z = cylindertocartesian(dat.R,dat.Z,dat.P*degtorad)
        mlab.mesh(X,Y,Z,opacity=op,color=col,representation='surface')
        #ax.set_xlabel('Radius [cm]', fontsize=14, fontweight='bold')
        #ax.set_ylabel('Phi [deg]', fontsize=14, fontweight='bold')
        #ax.set_zlabel('Z [cm]', fontsize=14, fontweight='bold')
        return fig
  
    def set_modified_mesh(self):
        """
        Set modified mesh for saving
        """
        self.Rmod = self.R
        self.Zmod = self.Z
        self.Pmod = self.P
        return

    def modify_mesh(self, data, varname):
        """
        Provide modified mesh data to class for saving
        input: 
            data - modified R, P, or Z matrix
            varname - variable to save to, choices:
                        0=Rmod - modified R matrix
                        1=Pmod - modified phi matrix
                        2=Zmod - modified Z matrix
        """
        if varname==0:
            self.Rmod=data
        elif varname==1:
            self.Pmod=data
        else:
            self.Zmod=data
        return
        
    def save_mesh(self, header, fn ,fset=True):
        """
        Saves Kisslinger mesh
        
        input:
            header - Comment on description of mesh
            fn     - filename to save mesh to
            fset   - Set class mesh as modified
            
        """
        if fset: self.set_modified_mesh()
        size=np.shape(self.Rmod)
        np.savetxt(fn, [], header=header)
        f=open(fn,'ab')
        np.savetxt(f,np.column_stack((size[0], size[1],int(self.nfold),self.Rref, self.Zref)),fmt='%d %d %d %f %f')
        for i in range(size[0]):
            np.savetxt(f,[self.Pmod[i,0]], fmt='%.1f')
            np.savetxt(f, np.asarray([self.Rmod[i,:],self.Zmod[i,:]]).T, delimiter=' ', fmt='%.5f')
        f.close()
        return
        
