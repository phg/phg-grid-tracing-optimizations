
import numpy as np
# import matplotlib.pyplot as plt
# import pickle
import sys
import os
#sys.path.append('../build/.')
#breakpoint()
# from tracer.build.field_routines import baxisym as bas 
from tracer.build.field_routines import trace as tr
from tracer.build.field_routines import bprecalc as bpre 
from tracer.build.field_routines import bfield as bf 
from tracer.build.field_routines import grid as gr 

from combine.quick_g import quick_g_plot
#sys.path.append('../grid/.')
#from curve import *
import tracer.W7X.Kisslinger as Kisslinger
import alphashape
import matplotlib.pyplot as plt

def process_consts(consts=dict(), pprint=False):
    #--------------------------
    if(pprint):
        print('Define grid parameters & locations')
        
    # radial resolution of the SOL, core and outer neutral regions
    NirS = consts.get("NirS", 100)
    NirC = consts.get("NirC", 25)
    NirN = consts.get("NirN", 5)
    consts["NirS"], consts["NirC"], consts["NirN"] = NirS, NirC, NirN 
    
    
    # poloidal and toroidal resolutions
    Niphalf = consts.get("Niphalf", 500)
    Nit = consts.get("Nit", 37)
    assert(Nit == 37)
    consts["Niphalf"], consts["Nit"] = Niphalf, Nit
    
    # start in bean crossection or triangle or somewhere else?
    # 0 == bean, np.pi/180 * 36 == triangle
    trace_angle = consts.get("trace_angle", 0)
    assert(0 <= trace_angle < Nit) # in first module
    assert(type(trace_angle) == int)
    consts["trace_angle"] = trace_angle * 2*np.pi/Nit/10
    consts["trace_index"] = trace_angle
    
    # bean starting points to trace to any angle (Defaults for EIM)
    # flux surface radii for tracing for bean and triangle crossection
    r1, r2, r3, r4 = consts.get("surface_radii",
        [5.9014041, 6.1014041, 6.2014041, 6.2864041])
    consts["surface_radii"] = (r1, r2, r3, r4)
    R_cax = consts.get("Rcax", 5.944e+00)
    consts["cax_bean"] = [R_cax, 0.0, 0.0]
    consts["Rlcfs_bean"] = r3
    consts["dRcore"] = r3 - r2
    consts["dRneut"] = r3 - r1
    consts["dRSOL"] = r3 - r4
    
    # specify tracing accuracy
    consts["tr_acc"] = consts.get("tr_acc", 72)
    
    # create poloidal points equally spaced from each other?
    consts["eq_space"] = consts.get("eq_space", False)
    consts["eq_space_vessel"] = consts.get("eq_space_vessel", False)
    consts["vessel_pdf"] = consts.get("vessel_pdf", None)
    if consts["eq_space_vessel"] and (consts["vessel_pdf"] is not None):
        # fallback_pdf = np.ones((1, consts["Nit"])) 
        # check that one pdf for every phi angle exists
        assert(consts["vessel_pdf"].shape[1] == consts["Nit"])
    
    # when parameterizing over the theta angle of the boundary, the vessel
    # shape can in certain circumstances not be monotonically increasing
    # in theta. as we are free to place the vesses grid points, this shifts
    # the midpoint a bit to the outside so that the middle bean shape
    # is monotonically increasing again
    # the shift will vary between machines and even configurations!!!
    consts["center_delta_R"] = consts.get("center_delta_R", 0)
    
    consts["shrink_fit_outer_surface"] = consts.get("shrink_fit_outer_surface", None)
    
    Nir=NirC+NirS+NirN; Nip=2*Niphalf+1
    consts["Nir"], consts["Nip"] = Nir, Nip
    
    # define file with magnetic field information
    pathtohere = os.path.realpath(__file__)
    pathtofolder = "/".join(pathtohere.split("/")[:-1])
    given_file = consts.get("magnetic_field_file", None)
    if given_file == None:  # no file given, make default
        consts["magnetic_field_file"] = f'{pathtofolder}/Fields/Field-EIM-std.dat'
    elif given_file[0] == ".":  # relative path given
        consts["magnetic_field_file"] = f'{pathtofolder}/{given_file}'
    else: # hopefully file path given
        consts["magnetic_field_file"] = given_file
    
    assert os.path.isfile(consts["magnetic_field_file"]) and "No file at given Path"
    return consts

def make_inner_grid(consts=dict(), pprint=False):
    c = consts
    Nir, Nip, Nit, Niphalf = c["Nir"], c["Nip"], c["Nit"], consts["Niphalf"]
    NirS, NirC = consts["NirS"], consts["NirC"]
    tracc, cax_bean = consts["tr_acc"], consts["cax_bean"]

    if(pprint):
        print('load/calculate mag. field')
    # restore the result (faster than computing)
    bpre.restore_field(consts["magnetic_field_file"])

    
    #Define magnetic field locations
    dphi_fp=2*np.pi/5.0 # toroidal extension of the field period
    phitri=2*np.pi/10.0 # toroidal coordinate in which the plasma has a triangular shape
    phibean=0.#2*np.pi/5.0 # toroidal coordinate in which the plasma a bean shape

    #turn on the pre-calculated 3D magnetic field and the axi-symmetric Tokamak field off
    #first to components are tokamak background field, third is 3D-field (pertubation for tokamaks)
    bf.scale_components([0,0,1.0])

    #Define grid
    g=np.zeros((Nir,Nip,Nit,3),order='F')*np.nan
    
    
    #--------------------------
    if(pprint):
        print('construct flux surfaces in confinement region')
        
    # trace from bean to starting surface
    ta, ti = consts["trace_angle"], consts["trace_index"]
    angle_step = (phitri - phibean)/(Nit-1)
    new_coords = [None]*4 # new Rfs's at new plane angle

    r_indices = [0,1,NirC-1,NirC+NirS-1]
    for i, (curr_gi, curr_r) in enumerate(zip(r_indices,
                                            consts["surface_radii"])):
        # single radius given
        if len(np.array(curr_r).shape) == 0:
            curr_r = [curr_r]
        # list of radii given (traced with higher accuracy)
        if len(np.array(curr_r).shape) == 1:
            new_coords[i] = [
                tr.field_line([cr,0,phibean],ta,1,substeps=tracc,dir=1)[-1]
                for cr in curr_r]
        else: raise Exception('radii list invalidly defined')

    if(pprint):
        print('Trace mag. axis')
        
    # trace axis starting cross section   
    new_cax = tr.field_line(cax_bean,ta,1,substeps=tracc,dir=1)[-1]
    # trace axis along whole segement
    gax = tr.field_line(cax_bean,angle_step,Nit-1,substeps=tracc,dir=1)

    #Construct a flux surface by field line tracing
    for ifs, new_coord_sublist in zip(r_indices, new_coords):
        if(pprint): print(ifs,new_coord_sublist)
        fl = np.empty((0,3)) # empty to concat to
        for new_coord in new_coord_sublist:
            cfl=tr.field_line(new_coord,dphi_fp,10*Nip,substeps=tracc*5,dir=1)
            fl = np.concatenate((fl, cfl), axis=0)
        fl = fl[~np.isnan(fl).all(axis=1)] # remove nans

        # if surface is an inner surface or if it is well behaved,
        # then just sort points and sample along them
        if ifs != r_indices[-1] or consts["shrink_fit_outer_surface"] is None:
            coords = fl[:,:2]
        # else shrink fit alpha shape and smooth it out, cutting off peaks
        else:
            alpha, bs, sd, n_smooth, plotting = consts["shrink_fit_outer_surface"]
            alpha_shape = alphashape.alphashape(fl[:,:2], alpha)
            # get polygon type
            try: polytype = type(alpha_shape.geoms[0])
            except Exception: polytype = type(alpha_shape)
            # get biggest polygon in alpha shape
            if type(alpha_shape) == polytype:
                main_shape = alpha_shape
            else:
                polys = {poly.area: poly for poly in alpha_shape.geoms}
                main_shape = polys[sorted(polys, reverse=True)[0]]
            # erode antierode to get rid of peaks and get biggest again
            cut_shapes = main_shape.buffer(-bs).buffer(2*bs).buffer(-bs)
            if type(cut_shapes) == polytype:
                cut_shape = cut_shapes
            else:
                cut_polys = {poly.area: poly for poly in cut_shapes.geoms}
                cut_shape = cut_polys[sorted(cut_polys, reverse=True)[0]]
            sampled_shape = cut_shape.simplify(sd)
            sampled_coords = np.array((sampled_shape.exterior.xy[0], sampled_shape.exterior.xy[1]))
            coords = multi_smooth(sampled_coords.T, n_smooth)

            if plotting:
                fig, ax = plt.subplots()
                ax.scatter(fl[:,0], fl[:,1], s=2, c="lightgray")
                ax.plot(main_shape.exterior.xy[0], main_shape.exterior.xy[1], c="C0")
                ax.plot(cut_shape.exterior.xy[0], cut_shape.exterior.xy[1], c="C1")
                ax.plot(*coords.T, c="C2")


        # #order the points according to the theta coordinate
        thetafl=np.arctan2(coords[:,1]-new_cax[1],coords[:,0]-new_cax[0])
        ii=np.argsort(thetafl)
        coords=coords[ii]; thetafl=thetafl[ii]
        fl=np.vstack((coords,coords,coords)) # repeat for useful bounds
        thetafl=np.hstack((thetafl-2*np.pi,thetafl,thetafl+2*np.pi))

        if consts["eq_space"]: # place points an equal distance apart
            # subsample fieldlinepoints (to remove jitter)
            theta=np.linspace(-np.pi,np.pi,5*Nip)
            Rc = np.interp(theta, thetafl, fl[:,0])
            Zc = np.interp(theta, thetafl, fl[:,1])
            equal_spaced_points(Rc, Zc, Nip, g[ifs,:,ti,0], g[ifs,:,ti,1])
            
        else: # just place points uniformly along polidal angle
            theta=np.linspace(-np.pi,np.pi,Nip)
            g[ifs,:,ti,0]=np.interp(theta, thetafl, fl[:,0])
            g[ifs,:,ti,1]=np.interp(theta, thetafl, fl[:,1])

    if(pprint):
        print('extrapoloate intermediate surfaces linearily') 
    x1=np.linspace(0.0,1.0,NirC-1)[1:-1]
    x2=np.linspace(0.0,1.0,NirS+1)[1:]
    
    irin=1;irsep=NirC-1;irneut=NirC+NirS-1
    for ip in range(Nip):    
        for ic in range(2):
            g[irin +1:irsep  ,ip,ti,ic] = g[irin ,ip,ti,ic]+\
                        (g[irsep,ip,ti,ic]-g[irin,ip,ti,ic])*x1
            g[irsep+1:irneut+1,ip,ti,ic] = g[irsep,ip,ti,ic]+\
                        (g[irneut,ip,ti,ic]-g[irsep,ip,ti,ic])*x2
    #--------------------------
    if(pprint):
        print('force up/down symmetry in triangular plane.')
    if(ta == 0 or ta == Nir-1):
        for ir in range(Nir):
            #ir=NirC+NirS+i
            for ip in range(Niphalf+1):
                g[ir,Nip-1-ip,ti,0]= g[ir,ip,ti,0]
                g[ir,Nip-1-ip,ti,1]=-g[ir,ip,ti,1]
        
    g[:,:,ti,2] = ta
    
    #--------------------------
    if(pprint):
        print('extend along fieldline')
    for ir in range(NirC+NirS): # only trace inner points not outside SOL
        for ip in range(Nip):  
            # note that Nit (and not Nit-1) points along the fieldline
            # are given back in the trace routine
    
            # trace forward
            g[ir,ip,ti:] = tr.field_line(g[ir,ip,ti], angle_step, Nit-1-ti, substeps=tracc, dir=1)
            # trace backward
            g[ir,ip,:ti+1] = tr.field_line(g[ir,ip,ti], angle_step, ti, substeps=tracc, dir=-1)
    
    old_globals = [phitri, gax, irneut]
    return g, old_globals, consts
    
    
def make_outer_boundary(g, old_globals, consts,
                        pprint=False, plot=False, show=False):
    phitri, gax, irneut = old_globals
    
    degtorad = np.pi/180.
    pathtohere = os.path.realpath(__file__)
    pathtofolder = "/".join(pathtohere.split("/")[:-1])
    wall = Kisslinger.Kisslinger(f'{pathtofolder}/W7X-VacuumVessel-Medium.kis')
    
    if pprint: print("make fractional Vessel spacing")
    if consts["vessel_pdf"] is not None:
        # generate fractional spacings inv_cdf[tht,phi]
        inv_cdf = convert_rawpdf_to_invcdf(consts["vessel_pdf"], consts["Nip"])
    
    #Interpolate points on outer boundary
    # dphi = phitri/(Nit-1)
    tmpR,tmpz,tmpp = gax[:,0],gax[:,1],gax[:,2]
    lphi = []
    for it in range(consts["Nit"]):
        phi = np.round(tmpp[it]/degtorad*1.E3)/1.E3#36.-dphi*it/degtorad
        lphi.append(phi)
        R, Z = wall.get_phi_lineout(phi)
        #Convert to SI-unit
        R, Z = R/100., Z/100.
        #Use vessel outline and thetavalues of grid boundary
        #Re-center on magnetic axis, so that the boundary is around 0,0
        R, Z = R-tmpR[it], Z-tmpz[it]
        # calculate the poloidal angle of every point on the boundary
        thetaves = np.arctan2(Z,R-consts["center_delta_R"])
        
        #Sort increasing atan2 for interpolation (ergo start at 180° going anticlockwise)
        idx = np.argsort(thetaves)
        R = R[idx]
        Z = Z[idx]
        thetaves = thetaves[idx]
        
        # one must not forget to close the theta boundary again after reordering,
        # so that for theta
        # values smaller than the first of the 73 and bigger than the last of the 73
        # numpy knows how to interpolate them
        thetaves = np.append(np.insert(thetaves, 0, thetaves[-1]-2*np.pi), thetaves[0]+2*np.pi)
        R = np.append(np.insert(R, 0, R[-1]), R[0])
        Z = np.append(np.insert(Z, 0, Z[-1]), Z[0])
        
        if not consts["eq_space_vessel"]:
            #Use theta values from grid boundary (sorted from -pi to pi for all 1001 points)
            thetagrid = np.arctan2(g[irneut,:,it,1]-tmpz[it],
                                   g[irneut,:,it,0]-tmpR[it]-consts["center_delta_R"])
            
            # here we have 2 thetalists. one for the vessel shape (thetaves[73]) and one for the 
            # plasmaboundary shape (thetagrid[Nip])
            
            # Get interpolated boundary
            # ergo: one knows the values of R at every point in thetaves,
            # but one would like to know what values R would take (linear approx)
            # at the values in between, specifically thetagrid.
            # so the new calculated R and Z lists list the coordinates of the vessel boundary 
            # at the poloidal angles thetagrid (plus the delta again)
            # RR, ZZ = R.copy(), Z.copy()
            R = np.interp(thetagrid, thetaves, R)+tmpR[it]
            Z = np.interp(thetagrid, thetaves, Z)+tmpz[it]
            # with that, one now has the 1001 edge points and 1001 vessel points that the edge points can connect to.
            
            #Force first and last point to coincide
            R[-1] = R[0]; Z[-1] = Z[0]
            # set outer boundary to the vessel points
            g[-1,:,it,0]=R
            g[-1,:,it,1]=Z
            
        else:
            # breakpoint()
            # make equally spaced points on vessel
            # find theta angle of first inner grid point
            theta0 = np.arctan2(g[irneut,0,it,1]-tmpz[it], g[irneut,0,it,0]-tmpR[it])
            # make new outer vessel point at this angle
            R0 = np.interp(theta0, thetaves, R)
            Z0 = np.interp(theta0, thetaves, Z)
            # add this point into R & Z and
            # resort so that the new point is the first one in the list
            i0 = np.argmax(thetaves>theta0) # index of point after new point
            assert(i0 != 0)
            R = np.concatenate(([R0], R[i0:-1], R[1:i0]))
            Z = np.concatenate(([Z0], Z[i0:-1], Z[1:i0]))
            # add first point to end again
            R = np.concatenate((R, R[0:1]))
            Z = np.concatenate((Z, Z[0:1]))
            
            if consts["vessel_pdf"] is None:
                invcdf1d = None # naive equal spacing version 
            else: # get fractional spacing for this phi
                invcdf1d = inv_cdf[:,it]
                
            equal_spaced_points(R+tmpR[it], Z+tmpz[it], consts["Nip"],
                                g[-1,:,it,0], g[-1,:,it,1],
                                fractional_space=invcdf1d)
                
      
    old_globals = [phitri, gax, irneut]
    return g, old_globals, consts

def make_outer_grid(g, old_globals, consts, pprint=False, show=False):
    phitri, gax, irneut = old_globals

    x3=np.linspace(1.0,0.0,consts["NirN"]+1)[1:]
    for it in range(consts["Nit"]):
        for ip in range(consts["Nip"]):
            # R,z coords
            for ic in range(2):
                coordv = g[-1,ip,it,ic]+(g[irneut,ip,it,ic]-g[-1,ip,it,ic])*x3
                g[irneut+1:consts["Nir"],ip,it,ic] = coordv
            # phi coords
            g[irneut+1:consts["Nir"], ip, it, 2] = g[0, ip, it, 2]
                                    
    #print(g[:,0,0,0])   
    #--------------------------
    if(pprint):
        print('Check grid')
    if np.any(np.isnan(g)):
        raise Exception('invalid grid cells.')
    
    err=gr.grid3d_check(g)
    if(pprint):
        print('grid has %i errors' % (len(np.where(err)[0])))

        #--------------------------
        print('Use vessel exterior for grid boundary')
        '''
        #--------------------------
        print('load vessel exterior')
        import utils.data as data
        ves = data.data()
        ves.load('W7XWall-Simple.pik')
        x = ves.lpR[37]
        y = np.array(ves.lpz[37])
        ax[0].plot(x,y,'+c')
        ax[1].plot(ves.lpR[2],-np.array(ves.lpz[2]),'+c')
        '''
            

    return g

# takes a thtsize x phisize input raw density function
# it then converts it to an inverse cdf along the tht-axis
# (so changes in other phi coords dont affect the output at the orig. phi coord)
# pdf(tht) = const -> invcdf(x) = x
def convert_rawpdf_to_invcdf(pdf, invcdf_resolution):
    pdf = pdf / pdf.sum(axis=0)
    thtsize, phisize = pdf.shape
    invcdf_x1d = np.linspace(0, 1, invcdf_resolution)
    cdf_x1d = np.linspace(0, 1, thtsize+1)
    cdf_y = np.cumsum(np.insert(pdf, 0, np.zeros(phisize), axis=0), axis=0)
    inv_cdf = np.empty((invcdf_resolution, phisize))
    for i, cdf_y1d in enumerate(cdf_y.T):
        inv_cdf[:,i] = np.interp(invcdf_x1d, cdf_y1d, cdf_x1d)
    return inv_cdf

def equal_spaced_points(x_coords, y_coords, n, x_out, y_out,
                        fractional_space=None, pprint=False):
    # x and y coord differences between points 
    xcd = np.diff(x_coords) # 
    ycd = np.diff(y_coords)
    # calulate length of path and therefore length between points
    point_dists = (xcd**2 + ycd**2)**(1/2)
    arc_len = np.sum(point_dists)
    point_dist = arc_len/(n-1)
    if(pprint): 
        print("\n", arc_len, point_dist)
        print("equal" if fractional_space is None else "fractional", "spacing")
        
    # find coord of mth point by going along subsampling points
    # until it would be too ling, and then interpolating
    for i in range(n-1): # [0, n]
        if fractional_space is None:
            # naive version, every point has same distance
            full_len = i*point_dist
        else:
            # point distance fractions along the path are given by frac._sp.
            full_len = fractional_space[i]*arc_len
            
        path_len, c = 0, 0
        while True: # go along polypath
            # would you be further than intended at the next point
            if path_len + point_dists[c] >= full_len:
                # YES: interpolate the goal between current and next point
                frac = (full_len - path_len)/point_dists[c]
                x_out[i] = x_coords[c] + frac*xcd[c]
                y_out[i] = y_coords[c] + frac*ycd[c]
                if(pprint): print(i, full_len)
                break
            else:
                # NO: go to next path segment
                path_len += point_dists[c]
                c += 1
    # place last point at same coord as first one
    x_out[-1] = x_coords[0]
    y_out[-1] = y_coords[0]

def multi_smooth(arr, num):
    def smooth(arr):
        padded_arr = np.pad(arr, [(1,1),(0,0)], mode="wrap")
        return 0.5*arr + 0.25*(padded_arr[:-2] + padded_arr[2:])
    
    for i in range(num):
        arr = smooth(arr)
    return arr

if __name__ == "__main__":
    # grid parameters, you can add more!
    consts = {
        # in -> outside cell numbers (sum is redial resolution)
        "NirC": 3, "NirS": 2, "NirN": 2,
        # half(-1/2) poloidal and toroidal cell number
        "Niphalf": 50, "Nit": 37,
        # poloidal grid spacing and smoothing options implemented by phg
        "eq_space": True, "eq_space_vessel": False, "zoidpol": False,
        # strating angle in [0,Nit[ and offset of vessel projection center
        "trace_angle": 10, "center_delta_R": .15,
        # path to magnetic field file
        "magnetic_field_file": "./Fields/Field-FTM-high.dat",
        # Big radius values for field surfaces (default EIM) and axis + accuracy
        # multiple values lead to multiple points beeing traced for one surface
        # this makes sense when they are chaotic
        # EIM: [5.9014041, 6.1014041, 6.2014041, 6.2864041], 5.944e+00, 72
        # FTM: [5.99, 6.09, 6.1751, np.linspace(6.2129, 6.2129001, 10)], 5.96957, 720
        "surface_radii": [5.99, 6.09, 6.1751, np.linspace(6.2129, 6.2129001, 20)],
        "Rcax": 5.96957, "tr_acc": 720,
        # parameters for outer surface fit: alpha value for alpha shape,
        # buffer size for erosion antierosion, maximum deviation of simplified
        # shape, the numper of nearest neighbour smoothings and if it gets plotted
        "shrink_fit_outer_surface": (60, 0.04, 0.002, 10, True)
    }

    # vpdf = np.ones((consts["Niphalf"]*2+1, consts["Nit"]))
    # vpdf[:,0] = [1]*10 + [2]*10 + [3]*10 + [4]*10 + [1]*61
    # consts["vessel_pdf"] = vpdf

    consts = process_consts(consts)
    g, glob, consts = make_inner_grid(consts=consts)
    g, glob, consts = make_outer_boundary(g, glob, consts)
    g = make_outer_grid(g, glob, consts)
    
    #print(g, g.shape)
    quick_g_plot(g, phi=10)
    #quick_g_plot(g, phi=consts["trace_index"])
    
    
if False: sys.exit()

