import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
sys.path.append('../build/.')
from field_routines import baxisym as bas 
from field_routines import trace as tr
from field_routines import bprecalc as bpre 
from field_routines import bfield as bf 
from field_routines import grid as gr 
#sys.path.append('../grid/.')
#from curve import *

#--------------------------
print('Define grid parameters & locations')
NirS=100;NirC=25;NirN=5 # radial resolution of the SOL, core and outer neutral regions
Niphalf=500;Nit=37 # poloidal and toroidal resolutions
dRcore=0.15;dRneut=0.50;dRSOL=-0.13

Nir=NirC+NirS+NirN; Nip=2*Niphalf+1

#Define magnetic field locations
dphi_fp=2*np.pi/5.0 # toroidal extension of the field period
phitri=2*np.pi/10.0 # toroidal coordinate in which the plasma has a triangular shape
phibean=2*np.pi*(35/360)#2*np.pi/5.0 # toroidal coordinate in which the plasma a bean shape

Rlcfs_bean=6.198;Rax_bean=5.944e+00;zax_bean=0.0
Rlcfs_tar=6.201

Rlcfs_tri=6.0166;
Rax_tri=(4.2091**2+3.0581**2)**0.5; zax_tri=0.0

#--------------------------
print('Load target geometry from Kisslinger files')
def load_targets():
  import Kisslinger
  #List of components to use
  pn = './structures/vacuumvessel/'
  lcomp = [pn+'vessel.medium']

  if True:
    pn = './structures/components/2020/'
    tmp = [\
    'baf_lower_left_0_21',\
    'baf_lower_left_19_28',\
    'baf_lower_right',\
    'baf_upper_left',\
    'baf_upper_right',\
    'div_hor_lower',\
    'div_hor_upper',\
    'div_ver_upper',\
    'shield_0_21_fix2.wvn',\
    'shield_21_28_fix2.wvn',\
    'shield_28_31p5_fix2.wvn',\
    'shield_31p5_32_fix2.wvn',\
    'shield_32_32p5_fix2.wvn',\
    'shield_32p5_35_fix2.wvn',\
    'shield_35_36_fix2.wvn',\
    'cover_lower_chamber_gap',\
    'cover_lower_gap_0-1.0',\
    'cover_lower_hor_div',\
    'cover_lower_phi=21',\
    'cover_lower_phi=28',\
    'cover_upper_chamber_gap',\
    'cover_upper_gap_0-1.5',\
    'cover_upper_hor_div',\
    'cover_upper_ver_div',\
    'cover_upper_phi=19',\
    'cut_lowerChamber',\
    'cut_upperChamber'\
  ]

  for i,item in enumerate(tmp):
    lcomp.append(pn+item)

  #Load components
  lc = []
  for item in lcomp:
    print(item)
    lc.append(Kisslinger.Kisslinger(item))
  return lc, lcomp

ltarx, ltarxid = load_targets()

#--------------------------
print('Define artificial target location')
#Dont'specify target at mapping plane (bean)
phi_tar=phibean+2.0*np.pi/180.;dphi_tar=0.1*np.pi/180. # toroidal plane, where the limiter is located and its extension
phi_tar=phibean
phi_offset=0.0#-phitri  # phi phi_offset for grid and targets
'''
#Use targets around grid for intersection tests (cell inside target polygon?)
#Use targets inside grid for actual target geometry in EMC3-runs to avoid recycling particles
# outside the plasma domain (mismapping at edges?)
ltars=np.array([[5.4973,-0.4677],[5.3513,-0.8740],[5.3782,-0.9335],[5.4550,-0.9699],[5.4915,-0.9784],[5.7316,-0.9116],[5.9122,-0.8279],[5.9196,-0.8346],[5.6115,-1.0197],[5.3556,-1.0607],[5.2155,-0.8411],[5.4658,-0.4708],[5.4973,-0.4677]])
ltar=np.array([[5.4973,-0.4677],[5.3397,-0.8170],[5.3513,-0.8740],[5.3782,-0.9335],[5.4550,-0.9699],[5.4915,-0.9784],[5.7316,-0.9116],[5.9122,-0.8279],[5.9430,-0.8631],[5.5434,-1.2012],[5.2148,-1.2036],[5.1553,-0.9153],[5.1591,-0.7564],[5.4166,-0.4919],[5.4973,-0.4677]])
utars=np.array([[5.9311,0.7593],[5.6181,0.9549],[5.5479,0.9616],[5.4146,0.9002],[5.4076,0.8456],[5.5564,0.5330],[5.5292,0.5316],[5.3320,0.9436],[5.4206,1.0677],[5.6692,0.9893],[5.8635,0.8640],[5.9549,0.7727],[5.9311,0.7593]])
utar=np.array([[5.9311,0.7593],[5.6181,0.9549],[5.5479,0.9616],[5.4146,0.9002],[5.4076,0.8456],[5.5564,0.5330],[5.5171,0.5282],[5.2434,0.9817],[5.3066,1.1275],[5.4820,1.1342],[5.7318,1.0268],[5.9928,0.7861],[5.9311,0.7593]])
'''
#--------------------------
print('load/calculate mag. field')
#fn='W7XField-Small-EIM.dat'
#fn='W7XField-VSmall-EIM-Test.dat'
#fn='W7XField-Small-EIM-Test.dat'
#fn='W7XField-VSmall-EIM2.dat'
fn='W7XField-EIM.dat'
#fn='W7X.dat'
if False: # set True to re-compute the field
    cc=pickle.load(open('W7XCoils.pik','rb'))
    II=pickle.load(open('Currents.pik','rb'))['I']

    wires=np.zeros((0,4))

    for k,c in cc.items():
        #c = np.stack((c.x,c.y,c.z,np.zeros(c.x.shape))).T
        if abs(II[k])>1e-3:
            c[:,3]=II[k]
            c[-1,3]=0.0
            wires=np.vstack((wires,c))

    bpre.set_current_wires_xyzi(wires)
    
    epsilon=1e-10 

    #NC=3/12 Number of components (3 = Br,z,p ; 12 = Br,z,p, dr,dz,dp,...)
    bpre.prepare_field(4.4,6.3,128,-1.15,1.15,128,-epsilon,2*np.pi/5.+epsilon,72,12)# precompute the field defining radial, vertical and toroidal ranges and resolution of the grid. In toroidal direction the grid is extended by an infinitisimal epsilon to avoid aliasing effects

    bpre.save_field(fn) # store the result in a fortran format file

else:
    
    bpre.restore_field(fn) # restore the result (faster than computing)

#turn on the pre-calculated 3D magnetic field and the axi-symmetric Tokamak field off
#first to components are tokamak background field, third is 3D-field (pertubation for tokamaks)
bf.scale_components([0,0,1.0])
#print(bf.b_rzp([6.20,0.,0.]))
#print(bf.b_rzp([5.95,0.,0.]))
#print(bf.b_rzp([6.10,0.,0.]))

fig,ax=plt.subplots(1,2,figsize=(6,6),sharey=True)
def onclick(event):print( '[%.4f,%.4f],' % (event.xdata,event.ydata))

fig.canvas.mpl_connect('button_press_event', onclick)

#Define r,z vectors
RR=np.linspace(bpre.rapre,bpre.rbpre,bpre.nrpre)
zz=np.linspace(bpre.zapre,bpre.zbpre,bpre.nzpre)
#Define grid
g=np.zeros((Nir,Nip,Nit,3),order='F')*np.nan

plt.show(block=False)
#--------------------------

#exit

#--------------------------
print('construct flux surfaces in confinement region')
#Use triangular plane as reference
Rlcfs=Rlcfs_tri;zax=zax_tri;Rax=Rax_tri
#dR=np.hstack((dRneut,np.linspace(dRcore,1e-4,NirC-1)))

#Trace separatrix in Target-shaped cross-section
fl=tr.field_line([Rlcfs_bean,0,phi_tar],dphi_fp,Nip,substeps=72*5,dir=1)
ax[1].scatter(fl[:,0],fl[:,1],marker='x',s=1,color='lime')

#Construct a flux surface by field line tracing 
for ifs,Rfs in [[0,dRneut],[1,dRcore],[NirC-1,0.0],[NirS+NirC-1,dRSOL]]:  #enumerate(dR):#[[0,Rlcfs-0.15],[1,Rlcfs-1e-4]]:
    print(ifs,Rfs)
    #b=tr.field_line([Rfs,zax,phitri],2*np.pi/5.0, 1,substeps=72*5,dir=-1)
    #fl=tr.field_line(b[0,:]*1.0,np.pi,Nip,substeps=180*5,dir=1)
    fl=tr.field_line([Rlcfs-Rfs,0,phitri],dphi_fp,Nip,substeps=72*5,dir=1)
    ax[0].scatter(fl[:,0],fl[:,1],marker='x',s=1,color='r')

    #order the points according to the theta coordinate
    thetafl=np.arctan2(fl[:,1]-zax,fl[:,0]-Rax)
    ii=np.argsort(thetafl)
    fl=fl[ii];thetafl=thetafl[ii]
    fl=np.vstack((fl,fl,fl))
    thetafl=np.hstack((thetafl-2*np.pi,thetafl,thetafl+2*np.pi))
    
    theta=np.linspace(-np.pi,np.pi,Nip)

    g[ifs,:,0,0]=np.interp(theta, thetafl, fl[:,0])
    g[ifs,:,0,1]=np.interp(theta, thetafl, fl[:,1])

plt.draw()

#--------------------------
print('extrapoloate intermediate surfaces linearily') 
x1=np.linspace(0.0,1.0,NirC-1)[1:-1]
x2=np.linspace(0.0,1.0,NirS+1)[1:]
x3=np.linspace(0.0,0.1,NirN+1)[1:]

irin=1;irsep=NirC-1;irneut=NirC+NirS-1
vax = [Rax,zax]
for ip in range(Nip):    
  for ic in range(2):
    g[irin +1:irsep   ,ip,0,ic] = g[irin ,ip,0,ic]+\
                                  (g[irsep,ip,0,ic]-g[irin,ip,0,ic])*x1
    g[irsep+1:irneut+1,ip,0,ic] = g[irsep,ip,0,ic]+\
                                  (g[irneut,ip,0,ic]-g[irsep,ip,0,ic])*x2
    #Rescale last flux-surface
    g[irneut+1:Nir    ,ip,0,ic] = g[irneut,ip,0,ic]+\
                                  (-vax[ic]+g[irneut,ip,0,ic])*x3

#--------------------------
print('force up/down symmetry in triangular plane.')
for ir in range(Nir):
    #ir=NirC+NirS+i
    for ip in range(Niphalf+1):
        g[ir,Nip-1-ip,0,0]= g[ir,ip,0,0]
        g[ir,Nip-1-ip,0,1]=-g[ir,ip,0,1]

g[:,:,0,2]=phitri

#--------------------------
print('Trace mag. axis')
gax = tr.field_line(np.array([Rax_tri,zax_tri,phitri]), \
                    (phibean-phitri)/(Nit-1),Nit-1,substeps=10,dir=1)
#from mayavi import mlab
#fig = mlab.figure()
#mlab.plot3d(gax[:,0],gax[:,1],gax[:,2])


#--------------------------
print('extend along fieldline')
for ir in range(Nir):
  for ip in range(Nip):    
    # note that Nit (and not Nit-1) points along the fieldline are given back
    g[ir,ip,:,:]=tr.field_line(g[ir,ip,0,:], \
                        (phibean-phitri)/(Nit-1),Nit-1,substeps=10,dir=1)

#--------------------------
print('Check grid')
if len(np.where(np.isnan(g))[0])>0:
    raise Exception('invalid grid cells.')

err=gr.grid3d_check(g)
print('grid has %i errors' % (len(np.where(err)[0])))

#--------------------------
print('Use vessel exterior for grid boundary')

import Kisslinger
import utils.geomshape as gs
import utils.plot as pu
import shapely.geometry as sgeo
degtorad = np.pi/180.
wall = Kisslinger.Kisslinger('W7X-VacuumVessel-Medium.kis')

ngg = g.copy()

#Interpolate points on outer boundary
dphi = phitri/(Nit-1)
# 
tmpR,tmpz,tmpp = gax[:,0],gax[:,1],gax[:,2]
lphi = []
for it in range(Nit):
  # calculate toroidal slice angle from tmpp and save into lphi (~0.033° per iteration)
  # somehow from 36 to 35 in 36 steps (???)
  phi = np.round(tmpp[it]/degtorad*1.E3)/1.E3#36.-dphi*it/degtorad
  lphi.append(phi)
  # Radius and Z values for outer Wall to extend grid to
  R, Z = wall.get_phi_lineout(phi)
  #Convert to SI-unit
  R, Z = R/100., Z/100.
  style = 2
  if style == 2: #Use vessel outline and thetavalues of grid boundary
    #Re-center on magnetic axis, so that the boundary is around 0,0
    R, Z = R-tmpR[it], Z-tmpz[it]
    # calculate the poloidal angle of every point on the boundary
    thetaves = np.arctan2(Z,R)
    #Sort increasing atan2 for interpolation (ergo start at 180° going anticlockwise)
    idx = np.argsort(thetaves)
    R = R[idx]
    Z = Z[idx]
    thetaves = thetaves[idx]
    # one must not forget to close the theta boundary again after reordering,
    # so that for theta
    # values smaller than the first of the 73 and bigger than the last of the 73
    # numpy knows how to interpolate them
    thetaves = np.append(np.insert(thetaves, 0, thetaves[-1]-2*np.pi), thetaves[0]+2*np.pi)
    R = np.append(np.insert(R, 0, R[-1]), R[0])
    Z = np.append(np.insert(Z, 0, Z[-1]), Z[0])
    
    #Use theta values from grid boundary (sorted from -pi to pi for all 1001 points)
    thetagrid = np.arctan2(g[irneut,:,it,1]-tmpz[it],g[irneut,:,it,0]-tmpR[it])
    
    # here we have 2 thetalists. one for the vessel shape (thetaves[73]) and one for the 
    # plasmaboundary shape (thetagrid[1001])
    
    #Get interpolated boundary
    # ergo: one knows the values of R at every point in thetaves,
    # but one would like to know what values R would take (linear approx)
    # at the values in between, specifically thetagrid.
    # so the new calculated R and Z lists list the coordinates of the vessel boundary 
    # at the poloidal angles thetagrid (plus the delta again)
    RR, ZZ = R.copy(), Z.copy()
    R = np.interp(thetagrid, thetaves, R)+tmpR[it]
    Z = np.interp(thetagrid, thetaves, Z)+tmpz[it]
    # with that, one now has the 1001 edge points and 1001 vessel points that the edge points can connect to.
    
    
    #Force first and last point to coincide
    R[-1] = R[0]; Z[-1] = Z[0]
  g[-1,:,it,0]=R
  g[-1,:,it,1]=Z
  '''
  if it == 35:
    fig = plt.figure()
    plt.plot(thetaves,x+tmpR)
    plt.plot(theta,g[-1,:,it,0])
  '''
  
sys.exit()

