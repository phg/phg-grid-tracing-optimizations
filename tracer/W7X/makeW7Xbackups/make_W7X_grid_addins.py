def load_targets(pprint=False):
  pathtohere = os.path.realpath(__file__)
  pathtofolder = "/".join(pathtohere.split("/")[:-1])
  
  #List of components to use
  pn = f'{pathtofolder}/structures/vacuumvessel/'
  lcomp = [pn+'vessel.medium']

  if True:
    pn = f'{pathtofolder}/structures/components/2020/'
    tmp = [\
    'baf_lower_left_0_21',\
    'baf_lower_left_19_28',\
    'baf_lower_right',\
    'baf_upper_left',\
    'baf_upper_right',\
    'div_hor_lower',\
    'div_hor_upper',\
    'div_ver_upper',\
    'shield_0_21_fix2.wvn',\
    'shield_21_28_fix2.wvn',\
    'shield_28_31p5_fix2.wvn',\
    'shield_31p5_32_fix2.wvn',\
    'shield_32_32p5_fix2.wvn',\
    'shield_32p5_35_fix2.wvn',\
    'shield_35_36_fix2.wvn',\
    'cover_lower_chamber_gap',\
    'cover_lower_gap_0-1.0',\
    'cover_lower_hor_div',\
    'cover_lower_phi=21',\
    'cover_lower_phi=28',\
    'cover_upper_chamber_gap',\
    'cover_upper_gap_0-1.5',\
    'cover_upper_hor_div',\
    'cover_upper_ver_div',\
    'cover_upper_phi=19',\
    'cut_lowerChamber',\
    'cut_upperChamber'\
  ]

  for i,item in enumerate(tmp):
    lcomp.append(pn+item)

  #Load components
  lc = []
  for item in lcomp:
    if(pprint):
        print(item)
    lc.append(Kisslinger.Kisslinger(item, pprint=pprint))
  return lc, lcomp


# #--------------------------
# if False:
#     print('write grid.')
#     fg=open('grid.txt','w')
#     fg.write('%10i %10i %10i\n' % tuple(g.shape[0:3]))
#     for it in range(g.shape[2]): #iterate over poloidal planes
#             fg.write('%16.8f \n' % ((g[0,0,it,2]+phi_offset)*180./np.pi))  # angle in deg
#             np.savetxt(fg,np.array(g[:,:,it,0]*100).reshape(-1,order='F')) # R-coordinate in cm
#             np.savetxt(fg,np.array(g[:,:,it,1]*100).reshape(-1,order='F')) # z-coordinate in cm
#     fg.close()

#     print('write field.')
#     B=gr.get_b_grid(g)
#     B[NirC+NirS+1:,:,:]=-1.0
#     fb=open('field.txt','w')
#     np.savetxt(fb,B.reshape(-1,order='F'))
#     fb.close()

# #--------------------------
# print('writing input files')
# for fninput in ['input.geo','input.n0g']:
#     s=open('template_'+fninput).read()
#     s=s.replace('#Nir#',str(Nir)).replace('#Nir-1#',str(Nir-1)).replace('#Nir-2#',str(Nir-2)).replace('#Nrplasma#',str(NirC+NirS-2))
#     s=s.replace('#Nip#',str(Nip)).replace('#Nip-1#',str(Nip-1)).replace('#Nip-2#',str(Nip-2))
#     s=s.replace('#Nit#',str(Nit)).replace('#Nit-1#',str(Nit-1)).replace('#Nit-2#',str(Nit-2))
#     open(fninput,'w').write(s)

# #--------------------------
# def write_EMC3_target_file(fn,Rzp,comment=None,scaling_factor=100.0,phi_scaling_factor=1.0,phi_offset=0.0):
#     if comment==None:comment=fn
#     #print 'phi_scaling_factor',phi_scaling_factor
#     f=open(fn,'w')
#     f.write(comment+'\n')
#     f.write('%i  %i   1      0.00000      0.00000\n'% (Rzp.shape[1],Rzp.shape[0]))
#     for it in range(Rzp.shape[1]):
#       f.write("%15.5f\n" % ((Rzp[0,it,2]+phi_offset)*phi_scaling_factor))
#       for x in Rzp[:,it,0:2]*scaling_factor:
#           f.write("%15.5f   %15.5f\n" % (x[0],x[1]))
#     f.close()

# #--------------------------

# exit

# #--------------------------
# print('search grid intersection with targets and write out target files')
# targetcells=np.zeros((Nir-1,Nip-1,Nit-1),dtype=np.int32,order='F') # an array to mark cells as inside (=1) or outside (=0) the targets
# #Determine target cells (non-plasma) from extended target boundaries
# for tar in ltarx:
#     tar3D=np.zeros((tar.nRZ,tar.nphi,3))
#     tar3D[:,:,0] = tar.R.T/100. ; tar3D[:,:,1] = tar.Z.T/100. ; tar3D[:,:,2] = tar.P.T/degtorad/100.
#     gr.intersect3d(g,targetcells,tar3D)

# print('number of targetcells found',len(np.where(targetcells)[0]))

# f=open('intersection.txt','w')
# pm,Npm=gr.plates_mag(targetcells) # prepare the format required by EMC3
# nzone=0
# for i in range(Npm): #...and write it into a file
#         k=pm[i,2]+3
#         f.write('%i  ' % (nzone))
#         f.write((('%i ' * k) % tuple(pm[i,0:k]))+'\n')
# f.close()


# #--------------------------
# print('plot grid and intersection.')
# itt=34
# for it in [itt]:#[0,itt-2,itt-1,itt,itt+1]:
#     #fig,ax=plt.subplots(figsize=(12,8))
#     #fig.suptitle('$\phi=%.1f^o$' % (g[0,0,it,2]*180./np.pi),fontsize=22)
#     #fig.canvas.mpl_connect('button_press_event', onclick)

#     #for ip in range(Nip):ax[1].plot(g[:,ip,it,0],g[:,ip,it,1],color='black',lw=0.3)
#     #for ir in range(Nir):ax[1].plot(g[ir,:,it,0],g[ir,:,it,1],color='black',lw=0.3)

#     iir,iip=np.where(targetcells[:,:,it])

#     if False:
#       for ir,ip in zip(iir,iip):
#           jr=[ir,ir+1,ir+1,ir,ir]
#           jp=[ip,ip,ip+1,ip+1,ip]
#           #ax[1].fill(g[jr,jp,it,0],g[jr,jp,it,1],color='orange',lw=2,zorder=5,alpha=0.2) # color cells that are marked as target cells
#           ax[1].plot(g[jr,jp,it,0],g[jr,jp,it,1],color='orange',lw=0.5)

#     #Plot target
#     '''
#     for tar in [utar,ltar]:ax[1].plot(tar[:,0],tar[:,1],color='red',lw=2)
#     for tar in [utars,ltars]:ax[1].plot(tar[:,0],tar[:,1],color='green',ls='--',lw=2)
#     '''
#     for tar in ltarx:
#         tar.plot_phi_lineout(phitri/degtorad ,fig=fig,ax=ax[0],color='k',norm=0.01)
#         tar.plot_phi_lineout(phi_tar/degtorad,fig=fig,ax=ax[1],color='k',norm=0.01)
#         #ax[0].plot(tar3D[:,-1,0],tar3D[:,-1,1],color='blue',lw=1) # show the actual target contour
#         #ax[1].plot(tar3D[:, 0,0],tar3D[:, 0,1],color='blue',lw=1) # show the actual target contour
#     #ax[1].set_aspect('equal')
#     #plt.draw()
#     fig.savefig('grid_plane_%03i.png' % it)
#     #plt.close(fig)
#     #fig.show()
    
# plt.draw()


