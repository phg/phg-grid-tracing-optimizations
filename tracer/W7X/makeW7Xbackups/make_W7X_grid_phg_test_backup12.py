import numpy as np
# import matplotlib.pyplot as plt
# import pickle
import sys
import os
import time
import json
#sys.path.append('../build/.')
#breakpoint()
# from tracer.build.field_routines import baxisym as bas 
from tracer.build.field_routines import trace as tr
from tracer.build.field_routines import bprecalc as bpre 
from tracer.build.field_routines import bfield as bf 
from tracer.build.field_routines import grid as gr

from combine.quick_g import quick_g_plot
from tracer.W7X import make_W7X_extras as w7e
#sys.path.append('../grid/.')
#from curve import *
import tracer.W7X.Kisslinger as Kisslinger
import alphashape
import matplotlib.pyplot as plt

def load_targets(pathtofolder, halfmodule_indices=[0], pprint=False):
    #List of components to use
    tmp = [] # [f'{pathtofolder}/structures/vacuumvessel/vessel.medium']

    pn = f'{pathtofolder}/structures/components/2020/'
    tmp += [pn+item for item in [
    'baf_lower_left_0_21', 'baf_lower_left_19_28', 'baf_lower_right',
    'baf_upper_left', 'baf_upper_right', 'div_hor_lower',
    'div_hor_upper', 'div_ver_upper',
    'shield_0_21_fix2.wvn',
    'shield_21_28_fix2.wvn', 'shield_28_31p5_fix2.wvn',
    'shield_31p5_32_fix2.wvn', 'shield_32_32p5_fix2.wvn',
    'shield_32p5_35_fix2.wvn', 'shield_35_36_fix2.wvn',
    'cover_lower_chamber_gap', 'cover_lower_gap_0-1.0', 'cover_lower_hor_div',
    'cover_lower_phi=21', 'cover_lower_phi=28', 'cover_upper_chamber_gap',
    'cover_upper_gap_0-1.5', 'cover_upper_hor_div', 'cover_upper_ver_div',
    'cover_upper_phi=19', 'cut_lowerChamber', 'cut_upperChamber'
    ]]

    lcomp = []
    for i in halfmodule_indices:
        lcomp.extend([filepath+f"_extended.hm{i}" for filepath in tmp])

    #Load components
    lc = []
    for item in lcomp:
        if(pprint):
            print(item)
        lc.append(Kisslinger.Kisslinger(item))
    return lc, lcomp

def process_consts_load_Bfield(consts=dict(), pprint=False):
    #--------------------------
    if(pprint):
        print('Define grid parameters & locations')

    # quick configuration via EIM / FTM / EIM-ErrorFullT
    consts["configuration"] = consts.get("configuration", None)
    if consts["configuration"] == "EIM":
        # Big radius values for field surfaces (default EIM) and axis + accuracy
        # multiple values lead to multiple points beeing traced for one surface
        # this makes sense when they are chaotic
        consts["surface_radii"] = [5.9014041, 6.1014041, 6.2014041,
                                   np.array([6.28861, 6.2886, 6.282])]
        consts["Rcax"] = 5.944e+00
        consts["tr_acc"] = 72
        consts["module"] = consts.get("module", "half_module")
        consts["shrink_fit_outer_surface"] = (20, 0.0, 0.0, 1, False)
    elif consts["configuration"] == "FTM":
        consts["surface_radii"] = [5.99, 6.09, 6.1751, np.linspace(6.2129, 6.2129001, 10)]
        consts["Rcax"] = 5.96957
        consts["tr_acc"] = 720
        consts["module"] = consts.get("module", "half_module")
        # parameters for outer surface fit: alpha value for alpha shape,
        # buffer size for erosion antierosion, maximum deviation of simplified
        # shape, the numper of nearest neighbour smoothings and if it gets plotted
        consts["shrink_fit_outer_surface"] = (40, 0.04, 0.002, 20, False)
    elif consts["configuration"] == "EIM-ErrorFullT":
        consts["surface_radii"] = [5.8807, 6.095, 6.171,
                                   np.array([6.246, 6.245, 6.244])]
        consts["Rcax"] = 5.9315
        consts["tr_acc"] = 720
        consts["module"] = consts.get("module", "full_torus")
        consts["shrink_fit_outer_surface"] = (5, 0.0, 0.0, 1, False)

    # radial resolution of the SOL, core and outer neutral regions
    NirS = consts.get("NirS", 100)
    NirC = consts.get("NirC", 25)
    NirN = consts.get("NirN", 5)
    consts["NirS"], consts["NirC"], consts["NirN"] = NirS, NirC, NirN

    # poloidal and toroidal resolutions
    Niphalf = consts.get("Niphalf", 500)
    Nit = consts.get("Nit", 37)
    # assert(Nit == 37)
    consts["Niphalf"], consts["Nit"] = Niphalf, Nit

    consts["normal_vessel"] = consts.get("normal_vessel", False)
    consts["normal_smooth"] = consts.get("normal_smooth", 5)

    consts["module_index"] = consts.get("module_index", 0) # default case
    consts["module"] = consts.get("module", "half_module") # default case
    assert(consts["module"] in {"full_torus", "full_module", "half_module"})
    if consts["module"] == "half_module":
        Nitfull = consts["Nitfull"] = Nit
    elif consts["module"] == "full_module":
        Nitfull = consts["Nitfull"] = 2*Nit-1
    else:
        Nitfull = consts["Nitfull"] = 10*Nit-9

    # start in bean crossection or triangle or somewhere else?
    # 0 == bean, np.pi/180 * 36 == triangle
    trace_index = consts.get("trace_index", 0)
    assert 0 <= trace_index < Nitfull  # in (first) module
    assert type(trace_index) == int
    assert consts.get("trace_angle", -1) == -1
    consts["trace_index"] = trace_index
    consts["trace_radian"] = trace_index * np.pi/5/(Nit-1) + 2*np.pi/5 * consts["module_index"]

    # bean starting points to trace to any angle (Defaults for EIM)
    # flux surface radii for tracing for bean and triangle crossection
    r1, r2, r3, r4 = consts.get("surface_radii",
        [5.9014041, 6.1014041, 6.2014041, 6.2864041])
    consts["surface_radii"] = (r1, r2, r3, r4)
    R_cax = consts.get("Rcax", 5.944e+00)
    consts["cax_bean"] = [R_cax, 0.0, np.pi/180*72*consts["module_index"]]
    consts["Rlcfs_bean"] = r3
    consts["dRcore"] = r3 - r2
    consts["dRneut"] = r3 - r1
    consts["dRSOL"] = r3 - r4

    consts["phitri"] = 2*np.pi/10.0

    # specify tracing accuracy
    consts["tr_acc"] = consts.get("tr_acc", 72)
    
    # create poloidal points equally spaced from each other?
    consts["eq_space"] = consts.get("eq_space", False)
    consts["eq_space_vessel"] = consts.get("eq_space_vessel", False)
    consts["eq_space_vessel_offset"] = consts.get("eq_space_vessel_offset", (0,0))
    consts["vessel_pdf"] = consts.get("vessel_pdf", None)
    if consts["eq_space_vessel"] and (consts["vessel_pdf"] is not None):
        # fallback_pdf = np.ones((1, consts["Nit"])) 
        # check that one pdf for every phi angle exists
        assert(consts["vessel_pdf"].shape[1] == consts["Nitfull"])
    
    # when parameterizing over the theta angle of the boundary, the vessel
    # shape can in certain circumstances not be monotonically increasing
    # in theta. as we are free to place the vesses grid points, this shifts
    # the midpoint a bit to the outside so that the middle bean shape
    # is monotonically increasing again
    # the shift will vary between machines and even configurations!!!
    consts["center_delta_R"] = consts.get("center_delta_R", [0])
    if len(np.array(consts["center_delta_R"]).shape) == 0:
        consts["center_delta_R"] = [consts["center_delta_R"]]
    if len(consts["center_delta_R"]) != 5:
        consts["center_delta_R"] = [0,0,0,0] + consts["center_delta_R"]
    assert len(consts["center_delta_R"]) == 5
    consts["center_delta_Z"] = consts.get("center_delta_Z", [0])
    if len(np.array(consts["center_delta_Z"]).shape) == 0:
        consts["center_delta_Z"] = [consts["center_delta_Z"]]
    if len(consts["center_delta_Z"]) != 5:
        consts["center_delta_Z"] = [0,0,0,0] + consts["center_delta_Z"]
    assert len(consts["center_delta_Z"]) == 5

    consts["shrink_fit_outer_surface"] = consts.get("shrink_fit_outer_surface", None)
    # give shappe used for outer surface. not used in normal use,
    consts["sfos_file"] = consts.get("sfos_file", None)
    
    Nir=NirC+NirS+NirN; Nip=2*Niphalf+1
    consts["Nir"], consts["Nip"] = Nir, Nip
    consts["irneut"] = NirC+NirS-1

    # define file with magnetic field information
    pathtohere = os.path.realpath(__file__)
    pathtofolder = "/".join(pathtohere.split("/")[:-1])
    given_file = consts.get("magnetic_field_file", None)
    if given_file == None:  # no file given, make default
        consts["magnetic_field_file"] = f'{pathtofolder}/Fields/Field-EIM-std.dat'
    elif given_file[0] == ".":  # relative path given
        consts["magnetic_field_file"] = f'{pathtofolder}/{given_file}'
    else: # hopefully file path given
        consts["magnetic_field_file"] = given_file
    
    assert os.path.isfile(consts["magnetic_field_file"]) and "No file at given Path"


    if(pprint):
        print('Trace mag. axis')
    #Define magnetic field locations
    phitri=consts["phitri"] # toroidal coordinate in which the plasma has a triangular shape
    consts["phibean"] = phibean = 0. #2*np.pi/5.0 # toroidal coordinate in which the plasma a bean shape

    if(pprint):
        print('load/calculate mag. field')
    # restore the result (faster than computing)
    bpre.restore_field(consts["magnetic_field_file"])
    #turn on the pre-calculated 3D magnetic field and the axi-symmetric Tokamak field off
    #first to components are tokamak background field, third is 3D-field (pertubation for tokamaks)
    bf.scale_components([0,0,1.0])

    # get info if magnetic field is calculated for one module or the whole torus
    # also find field periodicity for tracing
    Nitfullcheck = ( np.rint(bpre.fbpre*180/np.pi).astype(int) != 72 )
    consts["small_grid_large_field"] = False
    if Nitfullcheck != ( consts["module"] == "full_torus" ):
        print("GRID AND B FIELD RANGE MISSMATCH") # small grid can work on full field
        assert consts["module"] != "full_torus" # full grid needs full field
        consts["small_grid_large_field"] = True
    consts["tr_acc"] *= (consts["small_grid_large_field"]*4+1) # *5 if not periodic
    # toroidal extension of the field period
    if consts["small_grid_large_field"] or consts["module"] == "full_torus":
        consts["dphi_fp"] = 2*np.pi
    else:
        consts["dphi_fp"] = 2*np.pi * 1/5

    # trace axis along whole segement
    consts["angle_step"] = (phitri - phibean)/(Nit-1)
    gax = tr.field_line(consts["cax_bean"], consts["angle_step"],
                        consts["Nitfull"]-1, substeps=consts["tr_acc"], dir=1)
    consts["gax"] = gax

    #Use targets around grid for intersection tests (cell inside target polygon?)
    #Use targets inside grid for actual target geometry in EMC3-runs to avoid recycling particles
    # outside the plasma domain (mismapping at edges?)
    ltars=np.array([[5.4973,-0.4677],[5.3513,-0.8740],[5.3782,-0.9335],[5.4550,-0.9699],[5.4915,-0.9784],[5.7316,-0.9116],[5.9122,-0.8279],[5.9196,-0.8346],[5.6115,-1.0197],[5.3556,-1.0607],[5.2155,-0.8411],[5.4658,-0.4708],[5.4973,-0.4677]])
    ltar=np.array([[5.4973,-0.4677],[5.3397,-0.8170],[5.3513,-0.8740],[5.3782,-0.9335],[5.4550,-0.9699],[5.4915,-0.9784],[5.7316,-0.9116],[5.9122,-0.8279],[5.9430,-0.8631],[5.5434,-1.2012],[5.2148,-1.2036],[5.1553,-0.9153],[5.1591,-0.7564],[5.4166,-0.4919],[5.4973,-0.4677]])
    utars=np.array([[5.9311,0.7593],[5.6181,0.9549],[5.5479,0.9616],[5.4146,0.9002],[5.4076,0.8456],[5.5564,0.5330],[5.5292,0.5316],[5.3320,0.9436],[5.4206,1.0677],[5.6692,0.9893],[5.8635,0.8640],[5.9549,0.7727],[5.9311,0.7593]])
    utar=np.array([[5.9311,0.7593],[5.6181,0.9549],[5.5479,0.9616],[5.4146,0.9002],[5.4076,0.8456],[5.5564,0.5330],[5.5171,0.5282],[5.2434,0.9817],[5.3066,1.1275],[5.4820,1.1342],[5.7318,1.0268],[5.9928,0.7861],[5.9311,0.7593]])
    consts["utars"], consts["ltars"] = utars, ltars
    consts["utar"], consts["ltar"] = utar, ltar
    consts["phi_offset"] = consts.get("phi_offset", 0.0)
    consts["phi_tar"] = consts["phibean"]

    return consts

def make_inner_grid(consts, pprint=False):
    Nir, Nip = consts["Nir"], consts["Nip"]
    NirS, NirC = consts["NirS"], consts["NirC"]
    tracc = consts["tr_acc"]
    #Define grid
    g=np.zeros((Nir,Nip,consts["Nitfull"],3),order='F')*np.nan
    #--------------------------
    if(pprint):
        print('construct flux surfaces in confinement region')

    # trace from bean to starting surface
    ta, ti = consts["trace_radian"], consts["trace_index"]
    new_coords = [None]*4 # new Rfs's at new plane angle
    #
    r_indices = [0,1,NirC-1,NirC+NirS-1]
    curr_phi = consts["phibean"] + 2*np.pi/5 * consts["module_index"]
    for i, (curr_gi, curr_r) in enumerate(zip(r_indices,
                                            consts["surface_radii"])):
        # single radius given
        if len(np.array(curr_r).shape) == 0:
            curr_r = [curr_r]
        # list of radii given (traced with higher accuracy)
        if len(np.array(curr_r).shape) == 1:
            new_coords[i] = [
                tr.field_line([cr,0,curr_phi], ta, 1,
                              substeps=tracc, dir=1)[-1]
                for cr in curr_r]
        else: raise Exception('radii list invalidly defined')
    # trace axis from bean (magic number coords) to starting cross section
    # so that the given starting coords can stay the same for different angles
    new_cax = consts["gax"][ti]
    #Construct a flux surface by field line tracingr
    roffs = consts["center_delta_R"][:4]
    zoffs = consts["center_delta_Z"][:4]
    for ifs, new_coord_sublist, r_offset, z_offset in zip(r_indices, new_coords, roffs, zoffs):
        if(pprint): print(ifs,new_coord_sublist[0])
        fl = np.empty((0,3)) # empty to concat to
        for new_coord in new_coord_sublist:
            cfl=tr.field_line(new_coord,consts["dphi_fp"],10*Nip,substeps=tracc*5,dir=1)
            fl = np.concatenate((fl, cfl), axis=0)
        fl = fl[~np.isnan(fl).all(axis=1)] # remove nans
        #
        # if surface is an inner surface or if it is well behaved,
        # then just sort points and sample along them
        if ifs != r_indices[-1] or consts["shrink_fit_outer_surface"] is None:
            coords = fl[:,:2]
        # else shrink fit alpha shape and smooth it out, cutting off peaks
        else:
            if consts["sfos_file"] is not None and consts["sfos_file"] is not False:
                # just load file without doing all the work
                alpha, bs, sd, n_smooth, plotting = consts["shrink_fit_outer_surface"]
                sfosfile = f".{alpha}_{bs}_{sd}_{n_smooth}_sfosfile.npy"
                pathtohere = os.path.realpath(__file__)
                pathtofolder = "/".join(pathtohere.split("/")[:-1])
                coords = np.load(f"{pathtofolder}/{sfosfile}")
                print("loaded file")
            else:
                alpha, bs, sd, n_smooth, plotting = consts["shrink_fit_outer_surface"]
                alpha_shape = alphashape.alphashape(fl[:,:2], alpha)
                # get polygon type
                try: polytype = type(alpha_shape.geoms[0])
                except Exception: polytype = type(alpha_shape)
                # get biggest polygon in alpha shape
                if type(alpha_shape) == polytype:
                    main_shape = alpha_shape
                else:
                    polys = {poly.area: poly for poly in alpha_shape.geoms}
                    main_shape = polys[sorted(polys, reverse=True)[0]]
                # erode antierode to get rid of peaks and get biggest again
                cut_shapes = main_shape.buffer(-bs).buffer(2*bs).buffer(-bs)
                if type(cut_shapes) == polytype:
                    cut_shape = cut_shapes
                else:
                    cut_polys = {poly.area: poly for poly in cut_shapes.geoms}
                    cut_shape = cut_polys[sorted(cut_polys, reverse=True)[0]]
                sampled_shape = cut_shape.simplify(sd)
                sampled_coords = np.array((sampled_shape.exterior.xy[0], sampled_shape.exterior.xy[1]))
                coords = w7e.multi_smooth(sampled_coords.T, n_smooth)
                # sfosfile = f".{alpha}_{bs}_{sd}_{n_smooth}_sfosfile"
                # pathtohere = os.path.realpath(__file__)
                # pathtofolder = "/".join(pathtohere.split("/")[:-1])
                # np.save(f"{pathtofolder}/{sfosfile}", coords)
                # print("saved file")
            #
            if plotting:
                fig, ax = plt.subplots()
                ax.scatter(fl[:,0], fl[:,1], s=2, c="lightgray")
                ax.plot(main_shape.exterior.xy[0], main_shape.exterior.xy[1], c="C0")
                ax.plot(cut_shape.exterior.xy[0], cut_shape.exterior.xy[1], c="C1")
                ax.plot(*coords.T, c="C2")
                #
        # #order the points according to the theta coordinate
        thetafl=np.arctan2(coords[:,1]-new_cax[1]-z_offset,coords[:,0]-new_cax[0]-r_offset)
        ii=np.argsort(thetafl)
        coords=coords[ii]; thetafl=thetafl[ii]
        fl=np.vstack((coords,coords,coords)) # repeat for useful bounds
        thetafl=np.hstack((thetafl-2*np.pi,thetafl,thetafl+2*np.pi))
        #
        if consts["eq_space"]: # place points an equal distance apart
            # subsample fieldlinepoints (to remove jitter)
            theta=np.linspace(-np.pi,np.pi,5*Nip)
            Rc = np.interp(theta, thetafl, fl[:,0])
            Zc = np.interp(theta, thetafl, fl[:,1])
            w7e.equal_spaced_points(Rc, Zc, Nip, g[ifs,:,ti,0], g[ifs,:,ti,1])
            
        else: # just place points uniformly along polidal angle
            theta=np.linspace(-np.pi,np.pi,Nip)
            g[ifs,:,ti,0]=np.interp(theta, thetafl, fl[:,0])
            g[ifs,:,ti,1]=np.interp(theta, thetafl, fl[:,1])
            #
    if(pprint):
        print('extrapoloate intermediate surfaces linearily') 
    x1=np.linspace(0.0,1.0,NirC-1)[1:-1]
    x2=np.linspace(0.0,1.0,NirS+1)[1:]
    #
    irin=1;irsep=NirC-1;irneut=consts["irneut"]
    for ip in range(Nip):    
        for ic in range(2):
            g[irin +1:irsep  ,ip,ti,ic] = g[irin ,ip,ti,ic]+\
                        (g[irsep,ip,ti,ic]-g[irin,ip,ti,ic])*x1
            g[irsep+1:irneut+1,ip,ti,ic] = g[irsep,ip,ti,ic]+\
                        (g[irneut,ip,ti,ic]-g[irsep,ip,ti,ic])*x2
    # #--------------------------
    # # This may make stuff worse and is only usefull when starting in bean or
    # # triangle which we dont do anyway
    # if(pprint):
    #     print('force up/down symmetry if triangular/bean plane.')
    # if not (ti % (Nit-1)):
    #     for ir in range(Nir):
    #         #ir=NirC+NirS+i
    #         for ip in range(Niphalf+1):
    #             g[ir,Nip-1-ip,ti,0]= g[ir,ip,ti,0]
    #             g[ir,Nip-1-ip,ti,1]=-g[ir,ip,ti,1]

    g[:,:,ti,2] = ta
    #
    #--------------------------
    if(pprint):
        print('extend field surfaces along fieldline.')
    for ir in range(NirC+NirS): # only trace inner points not outside SOL
        if(pprint):
            print(f"r = {ir}", end=", ")
        for ip in range(Nip):  
            # note that Nit (and not Nit-1) points along the fieldline
            # are given back in the trace routine
            #
            # trace forward
            g[ir,ip,ti:] = tr.field_line(g[ir,ip,ti], consts["angle_step"],
                                         consts["Nitfull"]-1-ti, substeps=tracc, dir=1)
            # trace backward
            g[ir,ip,:ti+1] = tr.field_line(g[ir,ip,ti], consts["angle_step"],
                                           ti, substeps=tracc, dir=-1)
    return g, consts

def make_outer_boundary(g, consts,
                        pprint=False, plot=False, show=False):
    degtorad = np.pi/180.
    pathtohere = os.path.realpath(__file__)
    pathtofolder = "/".join(pathtohere.split("/")[:-1])
    wall = Kisslinger.Kisslinger(f'{pathtofolder}/W7X-VacuumVessel-Medium.kis')

    if consts["vessel_pdf"] is not None:
        if pprint: print("make fractional Vessel spacing")
        # generate fractional spacings inv_cdf[tht,phi]
        inv_cdf = w7e.convert_rawpdf_to_invcdf(consts["vessel_pdf"], consts["Nip"])
    
    #Interpolate points on outer boundary
    # dphi = phitri/(Nit-1)
    tmpR,tmpz,tmpp = consts["gax"][:,0], consts["gax"][:,1], consts["gax"][:,2]
    lphi = []
    for it in range(consts["Nitfull"]):
        phi = np.round(tmpp[it]/degtorad*1.E3)/1.E3#36.-dphi*it/degtorad
        lphi.append(phi)
        # map angle to half module angle and direction
        mod_phi, needs_flip = min(-phi%72, phi%72), phi%72 > 72/2
        R, Z = wall.get_phi_lineout(mod_phi)
        Z = -Z if needs_flip else Z
        #Convert to SI-unit
        R, Z = R/100., Z/100.
        #Use vessel outline and thetavalues of grid boundary
        #Re-center on magnetic axis, so that the boundary is around 0,0
        R, Z = R-tmpR[it], Z-tmpz[it]
        # calculate the poloidal angle of every point on the boundary
        vessel_deltaR = consts["center_delta_R"][-1]
        vessel_deltaZ = consts["center_delta_Z"][-1]
        thetaves = np.arctan2(Z-vessel_deltaZ,R-vessel_deltaR)
        
        #Sort increasing atan2 for interpolation (ergo start at 180° going anticlockwise)
        idx = np.argsort(thetaves)
        R = R[idx]
        Z = Z[idx]
        thetaves = thetaves[idx]
        # one must not forget to close the theta boundary again after reordering,
        # so that for theta
        # values smaller than the first of the 73 and bigger than the last of the 73
        # numpy knows how to interpolate them
        thetaves = np.append(np.insert(thetaves, 0, thetaves[-1]-2*np.pi), thetaves[0]+2*np.pi)
        R = np.append(np.insert(R, 0, R[-1]), R[0])
        Z = np.append(np.insert(Z, 0, Z[-1]), Z[0])

        if consts["eq_space_vessel"]:
            # in or decrease angle to project first point outwards
            dR, dZ = consts["eq_space_vessel_offset"]
            # breakpoint()
            # make equally spaced points on vessel
            if consts["normal_vessel"]:
                int_c, i0raw = w7e.normal_vessel_int(g[consts["irneut"],:,it],
                                                 (R,Z), 0, (tmpR[it], tmpz[it]))
                # new outer vessel point at this position: p_intersect
                # index of point after new point: relevant_i+1
                R0, Z0, i0 = int_c[0], int_c[1], i0raw+1
            else: # project from center
                # find theta angle of first inner grid point
                theta0 = np.arctan2(g[consts["irneut"],0,it,1]-tmpz[it]-dZ,
                                    g[consts["irneut"],0,it,0]-tmpR[it]-dR)
                # make new outer vessel point at this angle
                R0 = np.interp(theta0, thetaves, R)
                Z0 = np.interp(theta0, thetaves, Z)
                # add this point into R & Z and
                # resort so that the new point is the first one in the list
                bigger_than_theta0 = thetaves>theta0  # false,...,true,... array
                i0 = np.argmax(bigger_than_theta0) # index of point after new point
                assert(np.any(bigger_than_theta0))
            R = np.concatenate(([R0], R[i0:-1], R[1:i0]))
            Z = np.concatenate(([Z0], Z[i0:-1], Z[1:i0]))
            # add first point to end again
            R = np.concatenate((R, R[0:1]))
            Z = np.concatenate((Z, Z[0:1]))
            
            if consts["vessel_pdf"] is None:
                invcdf1d = None # naive equal spacing version 
            else: # get fractional spacing for this phi
                invcdf1d = inv_cdf[:,it]
                
            w7e.equal_spaced_points(R+tmpR[it], Z+tmpz[it], consts["Nip"],
                                g[-1,:,it,0], g[-1,:,it,1],
                                fractional_space=invcdf1d)

        elif consts["normal_vessel"]:
            # smooth boundary
            smooth_coords = w7e.multi_smooth(g[consts["irneut"],:,it],
                                             consts["normal_smooth"])
            # calculate normals
            offset = [tmpR[it], tmpz[it]]
            for ipol in range(consts["Nip"]):
                int_c, _ = w7e.normal_vessel_int(g[consts["irneut"],:,it],
                                (R,Z), ipol, offset, normal_arr=smooth_coords)
                g[-1,ipol,it,:2] = int_c + offset
                g[-1,ipol,it, 2] = g[consts["irneut"],ipol,it,2]

        else:
            #Use theta values from grid boundary (sorted from -pi to pi for all 1001 points)
            thetagrid = np.arctan2(g[consts["irneut"],:,it,1]-tmpz[it]-vessel_deltaZ,
                                   g[consts["irneut"],:,it,0]-tmpR[it]-vessel_deltaR)
            
            # here we have 2 thetalists. one for the vessel shape (thetaves[73]) and one for the 
            # plasmaboundary shape (thetagrid[Nip])
            
            # Get interpolated boundary
            # ergo: one knows the values of R at every point in thetaves,
            # but one would like to know what values R would take (linear approx)
            # at the values in between, specifically thetagrid.
            # so the new calculated R and Z lists list the coordinates of the vessel boundary 
            # at the poloidal angles thetagrid (plus the delta again)
            # RR, ZZ = R.copy(), Z.copy()
            R = np.interp(thetagrid, thetaves, R)+tmpR[it]
            Z = np.interp(thetagrid, thetaves, Z)+tmpz[it]
            # with that, one now has the 1001 edge points and 1001 vessel points that the edge points can connect to.
            
            #Force first and last point to coincide
            R[-1] = R[0]; Z[-1] = Z[0]
            # set outer boundary to the vessel points
            g[-1,:,it,0]=R
            g[-1,:,it,1]=Z

    return g, consts

def make_outer_grid(g, consts, pprint=False, show=False):
    x3=np.linspace(1.0,0.0,consts["NirN"]+1)[1:]
    for it in range(consts["Nitfull"]):
        for ip in range(consts["Nip"]):
            # R,z coords
            for ic in range(2):
                coordv = g[-1,ip,it,ic]+(g[consts["irneut"],ip,it,ic]-g[-1,ip,it,ic])*x3
                g[consts["irneut"]+1:consts["Nir"],ip,it,ic] = coordv
            # phi coords
            g[consts["irneut"]+1:consts["Nir"], ip, it, 2] = g[0, ip, it, 2]
                                    
    #print(g[:,0,0,0])   
    #--------------------------
    if(pprint):
        print('Check grid')
    if np.any(np.isnan(g)):
        raise Exception('invalid grid cells.')
    
    err=gr.grid3d_check(g)
    if(pprint):
        print('grid has %i errors' % (len(np.where(err)[0])))
        #
        #--------------------------
        print('Use vessel exterior for grid boundary')
        '''
        #--------------------------
        print('load vessel exterior')
        import utils.data as data
        ves = data.data()
        ves.load('W7XWall-Simple.pik')
        x = ves.lpR[37]
        y = np.array(ves.lpz[37])
        ax[0].plot(x,y,'+c')
        ax[1].plot(ves.lpR[2],-np.array(ves.lpz[2]),'+c')
        '''
    return g

def target_and_EMC3_compliance(g, consts, reftime=None, save_grid_raw=False,
                               pprint=False, show=False, outputf=None):
    pathtohere = os.path.realpath(__file__)
    pathtofolder = "/".join(pathtohere.split("/")[:-1])

    module_num = consts["module_index"]
    if pprint: print('write grid.')
    timecurr = time.strftime('%Y-%m-%dT%H:%M:%S') if reftime is None else reftime
    params = f"{consts['configuration']}-{timecurr}"
    if outputf is None:
        outputfolder = f"{pathtofolder}/EMC3compliance/{params}/{module_num}/"
    else:
        outputfolder = f"{pathtofolder}/{outputf}/{params}/{module_num}/"
    os.makedirs(os.path.dirname(outputfolder), exist_ok=True)

    # save constants
    if pprint: print('write config.')
    with open(outputfolder+'consts.txt','w') as outfile:
        json.dump(consts, outfile, default=lambda a: a.tolist())

    # save grid
    if pprint: print('write grid.')
    with open(outputfolder+'grid.txt','w') as fg:
        fg.write('%10i %10i %10i\n' % tuple(g.shape[0:3]))
        phi_offset = consts["phi_offset"]
        for it in range(g.shape[2]): #iterate over poloidal planes
            fg.write('%16.8f \n' % ((g[0,0,it,2]+phi_offset)*180./np.pi))  # angle in deg
            np.savetxt(fg,np.array(g[:,:,it,0]*100).reshape(-1,order='F')) # R-coordinate in cm
            np.savetxt(fg,np.array(g[:,:,it,1]*100).reshape(-1,order='F')) # z-coordinate in cm
    if save_grid_raw == True:
        np.savez(outputfolder+'grid', data=g)

    # save magnetic field
    if pprint: print('write field.')
    with open(outputfolder+'field.txt','w') as fb:
        B=gr.get_b_grid(g)
        B[consts["NirC"]+consts["NirS"]+1:,:,:]=-1.0
        np.savetxt(fb,B.reshape(-1,order='F'))

    #--------------------------
    if pprint: print('writing input files')
    Nir, Nip, Nit = consts["Nir"], consts["Nip"], consts["Nit"]
    NirC, NirS = consts["NirC"], consts["NirS"]
    for fninput in ['input.geo','input.n0g']:
        fninputpathtemplate = f"{pathtofolder}/template_{fninput}"
        s=open(fninputpathtemplate).read()
        s=s.replace('#Nir#',str(Nir)).replace('#Nir-1#',str(Nir-1)).replace(
            '#Nir-2#',str(Nir-2)).replace('#Nrplasma#',str(NirC+NirS-2))
        s=s.replace('#Nip#',str(Nip)).replace('#Nip-1#',str(Nip-1)).replace(
            '#Nip-2#',str(Nip-2))
        s=s.replace('#Nit#',str(Nit)).replace('#Nit-1#',str(Nit-1)).replace(
            '#Nit-2#',str(Nit-2))
        open(outputfolder+fninput,'w').write(s)

    #--------------------------
    def write_EMC3_target_file(fn,Rzp,comment=None,scaling_factor=100.0,
                               phi_scaling_factor=1.0,phi_offset=0.0):
        if comment==None:comment=fn
        #print 'phi_scaling_factor',phi_scaling_factor
        f=open(outputfolder+fn,'w')
        f.write(comment+'\n')
        f.write('%i  %i   1      0.00000      0.00000\n'% (Rzp.shape[1],Rzp.shape[0]))
        for it in range(Rzp.shape[1]):
          f.write("%15.5f\n" % ((Rzp[0,it,2]+phi_offset)*phi_scaling_factor))
          for x in Rzp[:,it,0:2]*scaling_factor:
              f.write("%15.5f   %15.5f\n" % (x[0],x[1]))
        f.close()

    if(pprint):
        print('Load target geometry from Kisslinger files')

    mod_i = consts["module_index"]
    hmv = {"half_module": [2*mod_i], "full_module": [2*mod_i, 2*mod_i+1],
           "full_torus": range(10)}
    ltarx, ltarxid = load_targets(pathtofolder, pprint=True,
                                  halfmodule_indices=hmv[consts["module"]])

    if pprint:
        print('search grid intersection with targets and write out target files')
    targetcells=np.zeros((Nir-1,Nip-1,consts["Nitfull"]-1),
                         dtype=np.int32,order='F') # an array to mark cells as inside (=1) or outside (=0) the targets
    #Determine target cells (non-plasma) from extended target boundaries
    if pprint: print("target no.", end="\t")
    for i, tar in enumerate(ltarx):
        tar3D=np.zeros((tar.nRZ,tar.nphi,3))
        tar3D[:,:,0] = tar.R.T/100. ; tar3D[:,:,1] = tar.Z.T/100. ; tar3D[:,:,2] = tar.P.T*np.pi/180.0
        gr.intersect3d(g,targetcells,tar3D)
        if pprint: print(i/len(ltarx), end=", ")

    # count everything farther out radially from a target as hit by a target too
    targetcells = (targetcells.cumsum(axis=0) != 0).astype(targetcells.dtype)

    print('number of targetcells found',len(np.where(targetcells)[0]))
    with open(outputfolder+'intersection.txt','w') as f:
        pm,Npm=gr.plates_mag(targetcells) # prepare the format required by EMC3
        for i in range(Npm): #...and write it into a file
                k=pm[i,2]+3
                f.write('%i  ' % (mod_i))
                f.write((('%i ' * k) % tuple(pm[i,0:k]))+'\n')

    if not show: return
    #--------------------------
    print('plot grid and intersection.')
    itt=34 # ???
    fig, ax = plt.subplots(0, 1, sharex=True, sharey=True)
    for it in [itt]:#[0,itt-2,itt-1,itt,itt+1]:
        iir,iip=np.where(targetcells[:,:,it])
        #Plot target

        for tar in [consts["utar"],consts["ltar"]]:
            ax[1].plot(tar[:,0],tar[:,1],color='red',lw=2)
        for tar in [consts["utars"],consts["ltars"]]:
            ax[1].plot(tar[:,0],tar[:,1],color='green',ls='--',lw=2)

        for tar in ltarx:
            tar.plot_phi_lineout(consts["phitri"]/np.pi*180, # !!!
                                 fig=fig,ax=ax[0],color='k',norm=0.01)
            tar.plot_phi_lineout(consts["phi_tar"]/np.pi*180,
                                 fig=fig,ax=ax[1],color='k',norm=0.01)
            ax[0].plot(tar3D[:,-1,0],tar3D[:,-1,1],color='blue',lw=1) # show the actual target contour
            ax[1].plot(tar3D[:, 0,0],tar3D[:, 0,1],color='blue',lw=1) # show the actual target contour
        ax[1].set_aspect('equal')
        #plt.draw()
        fig.savefig('grid_plane_%03i.png' % it)
        #plt.close(fig)
        #fig.show()
        
    plt.draw()

from combine.quick_g import multi_bump
if __name__ == "__main__":
    # grid parameters, you can add more!
    consts = {
        # in -> outside cell numbers (sum is redial resolution)
        "NirC": 30, "NirS": 40, "NirN": 40,
        # half(-1/2) poloidal and toroidal cell number (per half-module)
        "Niphalf": 150, "Nit": 36,
        # poloidal grid spacing and smoothing options implemented by phg
        "eq_space": True, "zoidpol": False,
        # proj for first (or all in not eq. space) vessel point(s)
        "eq_space_vessel": False,  "normal_vessel": True, "normal_smooth": 25,
        # strating angle in [0,Nit[ and offset list of projection center
        "trace_index": 35, "center_delta_R": .15, # [0,0,0,0.2,.15],
        # path to magnetic field file
        "magnetic_field_file": "./Fields/Field-EIM-FullT-std.dat", # ErrFullT- FullT-
        "configuration": "EIM", # overwrites more specific changes
        "module": "full_module", # or "half_module" or "full_torus"
        "module_index": 1, # from 0 to 4 for starting position
    }

    # size = constants["Niphalf"]*2+1, constants["Nit"]
    # scales_mus_covs = [ [0.9, [0.5,0.5], [[30,0],[0,30]]] ]
    # constants["vessel_pdf"] = multi_bump(size, scales_mus_covs)

    pp = True
    consts = process_consts_load_Bfield(consts, pprint=pp)
    g, consts = make_inner_grid(consts, pprint=pp)
    # g = np.load("Grids/EIM-ErrFullT-test1.npz")["data"]  # ErrFullT-
    g, consts = make_outer_boundary(g, consts, pprint=pp)
    g = make_outer_grid(g, consts, pprint=pp)
    # g = np.load("Grids/EIM-newtest.npz")["data"]
    # np.savez("Grids/EIMorFTM-xyz.npz", data=g)

    print(g.shape)
    quick_g_plot(g, phi=46)
    #quick_g_plot(g, phi=consts["trace_angle"])

    #target_and_EMC3_compliance(g, consts)

if False: sys.exit()

