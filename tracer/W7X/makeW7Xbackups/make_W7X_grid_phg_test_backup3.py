import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
import os
#sys.path.append('../build/.')
#breakpoint()
# from tracer.build.field_routines import baxisym as bas 
from tracer.build.field_routines import trace as tr
from tracer.build.field_routines import bprecalc as bpre 
from tracer.build.field_routines import bfield as bf 
from tracer.build.field_routines import grid as gr 

#sys.path.append('../grid/.')
#from curve import *
import tracer.W7X.Kisslinger as Kisslinger
#import utils.geomshape as gs
#import utils.plot as pu

#import shapely.geometry as sgeo


def load_targets(pprint=False):
  pathtohere = os.path.realpath(__file__)
  pathtofolder = "/".join(pathtohere.split("/")[:-1])
  
  #List of components to use
  pn = f'{pathtofolder}/structures/vacuumvessel/'
  lcomp = [pn+'vessel.medium']

  if True:
    pn = f'{pathtofolder}/structures/components/2020/'
    tmp = [\
    'baf_lower_left_0_21',\
    'baf_lower_left_19_28',\
    'baf_lower_right',\
    'baf_upper_left',\
    'baf_upper_right',\
    'div_hor_lower',\
    'div_hor_upper',\
    'div_ver_upper',\
    'shield_0_21_fix2.wvn',\
    'shield_21_28_fix2.wvn',\
    'shield_28_31p5_fix2.wvn',\
    'shield_31p5_32_fix2.wvn',\
    'shield_32_32p5_fix2.wvn',\
    'shield_32p5_35_fix2.wvn',\
    'shield_35_36_fix2.wvn',\
    'cover_lower_chamber_gap',\
    'cover_lower_gap_0-1.0',\
    'cover_lower_hor_div',\
    'cover_lower_phi=21',\
    'cover_lower_phi=28',\
    'cover_upper_chamber_gap',\
    'cover_upper_gap_0-1.5',\
    'cover_upper_hor_div',\
    'cover_upper_ver_div',\
    'cover_upper_phi=19',\
    'cut_lowerChamber',\
    'cut_upperChamber'\
  ]

  for i,item in enumerate(tmp):
    lcomp.append(pn+item)

  #Load components
  lc = []
  for item in lcomp:
    if(pprint):
        print(item)
    lc.append(Kisslinger.Kisslinger(item, pprint=pprint))
  return lc, lcomp

def onclick(event):
    print( '[%.4f,%.4f],' % (event.xdata,event.ydata))


def make_inner_grid(consts=dict(), pprint=False, plot=False, show=False):
    #--------------------------
    if(pprint):
        print('Define grid parameters & locations')
        
    # radial resolution of the SOL, core and outer neutral regions
    NirS = consts.get("NirS", 100)
    NirC = consts.get("NirC", 25)
    NirN = consts.get("NirN", 5)
    consts["NirS"], consts["NirC"], consts["NirN"] = NirS, NirC, NirN 
    
    
    # poloidal and toroidal resolutions
    Niphalf = consts.get("Niphalf", 500)
    Nit = consts.get("Nit", 37)
    consts["Niphalf"], consts["Nit"] = Niphalf, Nit
    
    # start in bean crossection or triangle?
    is_bean = consts.get("is_bean", False)
    consts["is_bean"] = is_bean
    
    if(not is_bean): # triangle starting slice
        dRcore=0.15;dRneut=0.50;dRSOL=-0.13
    else: # bean starting slice
        dRcore=0.10;dRneut=0.3;dRSOL=-0.085 #  -0.0735
    
    # create poloidal points equally spaced from each other?
    eq_space = consts.get("eq_space", False)
    consts["eq_space"] = eq_space
    
    
    Nir=NirC+NirS+NirN; Nip=2*Niphalf+1
    consts["Nir"], consts["Nip"] = Nir, Nip
    
    #Define magnetic field locations
    dphi_fp=2*np.pi/5.0 # toroidal extension of the field period
    phitri=2*np.pi/10.0 # toroidal coordinate in which the plasma has a triangular shape
    phibean=0.#2*np.pi/5.0 # toroidal coordinate in which the plasma a bean shape
    
    # flux surface radii for tracing for bean and triangle crossection
    Rlcfs_bean=6.2014041;Rax_bean=5.944e+00;zax_bean=0.0
    Rax_tri=(4.2091**2+3.0581**2)**0.5; zax_tri=0.0
    #Rlcfs_tar=6.201
    Rlcfs_tri=6.0166;
    
    
    #

    #--------------------------
    if(pprint):
        print('Define artificial target location')
    #Dont'specify target at mapping plane (bean)
    #phi_tar=phibean+2.0*np.pi/180.;dphi_tar=0.1*np.pi/180. # toroidal plane, where the limiter is located and its extension
    phi_tar=phibean
    #phi_offset=0.0#-phitri  # phi phi_offset for grid and targets
    '''
    #Use targets around grid for intersection tests (cell inside target polygon?)
    #Use targets inside grid for actual target geometry in EMC3-runs to avoid recycling particles
    # outside the plasma domain (mismapping at edges?)
    ltars=np.array([[5.4973,-0.4677],[5.3513,-0.8740],[5.3782,-0.9335],[5.4550,-0.9699],[5.4915,-0.9784],[5.7316,-0.9116],[5.9122,-0.8279],[5.9196,-0.8346],[5.6115,-1.0197],[5.3556,-1.0607],[5.2155,-0.8411],[5.4658,-0.4708],[5.4973,-0.4677]])
    ltar=np.array([[5.4973,-0.4677],[5.3397,-0.8170],[5.3513,-0.8740],[5.3782,-0.9335],[5.4550,-0.9699],[5.4915,-0.9784],[5.7316,-0.9116],[5.9122,-0.8279],[5.9430,-0.8631],[5.5434,-1.2012],[5.2148,-1.2036],[5.1553,-0.9153],[5.1591,-0.7564],[5.4166,-0.4919],[5.4973,-0.4677]])
    utars=np.array([[5.9311,0.7593],[5.6181,0.9549],[5.5479,0.9616],[5.4146,0.9002],[5.4076,0.8456],[5.5564,0.5330],[5.5292,0.5316],[5.3320,0.9436],[5.4206,1.0677],[5.6692,0.9893],[5.8635,0.8640],[5.9549,0.7727],[5.9311,0.7593]])
    utar=np.array([[5.9311,0.7593],[5.6181,0.9549],[5.5479,0.9616],[5.4146,0.9002],[5.4076,0.8456],[5.5564,0.5330],[5.5171,0.5282],[5.2434,0.9817],[5.3066,1.1275],[5.4820,1.1342],[5.7318,1.0268],[5.9928,0.7861],[5.9311,0.7593]])
    '''

    #--------------------------
    if(pprint):
        print('Load target geometry from Kisslinger files')
        
    ltarx, ltarxid = load_targets(pprint)
    
    #--------------------------
    if(pprint):
        print('load/calculate mag. field')
        
        
    pathtohere = os.path.realpath(__file__)
    pathtofolder = "/".join(pathtohere.split("/")[:-1])
    #fn='W7XField-Small-EIM.dat'
    #fn='W7XField-VSmall-EIM-Test.dat'
    #fn='W7XField-Small-EIM-Test.dat'
    #fn='W7XField-VSmall-EIM2.dat'
    fn=f'{pathtofolder}/W7XField-EIM.dat'
    #fn='W7X.dat'
    if False: # set True to re-compute the field
        cc=pickle.load(open(f'{pathtofolder}/W7XCoils.pik','rb'))
        II=pickle.load(open(f'{pathtofolder}/Currents.pik','rb'))['I']
    
        wires=np.zeros((0,4))
    
        for k,c in cc.items():
            #c = np.stack((c.x,c.y,c.z,np.zeros(c.x.shape))).T
            if abs(II[k])>1e-3:
                c[:,3]=II[k]
                c[-1,3]=0.0
                wires=np.vstack((wires,c))
    
        bpre.set_current_wires_xyzi(wires)
        
        epsilon=1e-10 
    
        #NC=3/12 Number of components (3 = Br,z,p ; 12 = Br,z,p, dr,dz,dp,...)
        bpre.prepare_field(4.4,6.3,128,-1.15,1.15,128,-epsilon,2*np.pi/5.+epsilon,72,12)# precompute the field defining radial, vertical and toroidal ranges and resolution of the grid. In toroidal direction the grid is extended by an infinitisimal epsilon to avoid aliasing effects
    
        bpre.save_field(fn) # store the result in a fortran format file

    else:
        bpre.restore_field(fn) # restore the result (faster than computing)

    #turn on the pre-calculated 3D magnetic field and the axi-symmetric Tokamak field off
    #first to components are tokamak background field, third is 3D-field (pertubation for tokamaks)
    bf.scale_components([0,0,1.0])
    #print(bf.b_rzp([6.20,0.,0.]))
    #print(bf.b_rzp([5.95,0.,0.]))
    #print(bf.b_rzp([6.10,0.,0.]))
    
    if(plot):
        fig,ax=plt.subplots(1,2,figsize=(9,6),sharey=True, sharex=True)
        fig.tight_layout()
        ax[0].set_aspect("equal")
        ax[1].set_aspect("equal")

    #Define r,z vectors
    RR=np.linspace(bpre.rapre,bpre.rbpre,bpre.nrpre)
    zz=np.linspace(bpre.zapre,bpre.zbpre,bpre.nzpre)
    #Define grid
    g=np.zeros((Nir,Nip,Nit,3),order='F')*np.nan

    #Plot poloidal field in triangular and bean shaped plane
    if(plot):
        if(is_bean): it_arr = [[1,phitri,Rlcfs_tri],[0,phi_tar,Rlcfs_bean]]
        else: it_arr = [[0,phitri,Rlcfs_tri],[1,phi_tar,Rlcfs_bean]]
        for i,phi,Rlcfs in it_arr:
            #Get index closest to planes
            it=int((phi-bpre.fapre)/(bpre.fbpre-bpre.fapre)*bpre.nfpre) % (bpre.nfpre - 1)
            #Get fields
            Bpol=(bpre.ba[0,it,:,:]**2+bpre.ba[1,it,:,:]**2)**0.5
            #Plot
            ax[i].pcolorfast(RR,zz,Bpol.T,vmin=0,vmax=3.0)
        
            #Trace field lines around LCFS
            for dR in np.linspace(-0.05,0.1,20):
                fl=tr.field_line([Rlcfs+dR,0.0,phi],2*np.pi/5.,200,substeps=360,dir=1)
                ax[i].scatter(fl[:,0],fl[:,1],marker='x',s=1,color='purple', zorder=-200)
    
        if(show):
            plt.show(block=False)
    #--------------------------
    
    #exit
    
    #--------------------------
    if(pprint):
        print('construct flux surfaces in confinement region')
    
    # set relevant consts for bean/triangle field line tracing
    if(is_bean): 
        Rlcfs=Rlcfs_bean;zax=zax_bean;Rax=Rax_bean;rel_phi=phibean
    else:
        Rlcfs=Rlcfs_tri;zax=zax_tri;Rax=Rax_tri;rel_phi=phitri
    
    #Construct a flux surface by field line tracing     
    for ifs,Rfs in [[0,dRneut],[1,dRcore],[NirC-1,0.0],[NirS+NirC-1,dRSOL]]:  #enumerate(dR):#[[0,Rlcfs-0.15],[1,Rlcfs-1e-4]]:
        if(pprint): print(ifs,Rfs)
        # #b=tr.field_line([Rfs,zax,phitri],2*np.pi/5.0, 1,substeps=72*5,dir=-1)
        # #fl=tr.field_line(b[0,:]*1.0,np.pi,Nip,substeps=180*5,dir=1)
        fl=tr.field_line([Rlcfs-Rfs,0,rel_phi],dphi_fp,10*Nip,substeps=72*5,dir=1)
        if(plot):
            ax[0].scatter(fl[:,0],fl[:,1],marker='x',s=1,color="pink")
    
        # #order the points according to the theta coordinate
        thetafl=np.arctan2(fl[:,1]-zax,fl[:,0]-Rax)
        ii=np.argsort(thetafl)
        fl=fl[ii];thetafl=thetafl[ii]
        fl=np.vstack((fl,fl,fl))
        thetafl=np.hstack((thetafl-2*np.pi,thetafl,thetafl+2*np.pi))
        
        if eq_space: # place points an equal distance apart
            # subsample fieldlinepoints (to remove jitter)
            theta=np.linspace(-np.pi,np.pi,5*Nip)
            Rc = np.interp(theta, thetafl, fl[:,0])
            Zc = np.interp(theta, thetafl, fl[:,1])
            equal_spaced_points(Rc, Zc, Nip, g[ifs,:,0,0], g[ifs,:,0,1])
            
        else: # just place points uniformly along polidal angle
            theta=np.linspace(-np.pi,np.pi,Nip)
            g[ifs,:,0,0]=np.interp(theta, thetafl, fl[:,0])
            g[ifs,:,0,1]=np.interp(theta, thetafl, fl[:,1])
    
    
    if(plot and show):
        plt.draw()

    #--------------------------
    if(pprint):
        print('extrapoloate intermediate surfaces linearily') 
    x1=np.linspace(0.0,1.0,NirC-1)[1:-1]
    x2=np.linspace(0.0,1.0,NirS+1)[1:]
    x3=np.linspace(0.0,0.1,NirN+1)[1:] # :-(
    
    irin=1;irsep=NirC-1;irneut=NirC+NirS-1
    vax = [Rax,zax]
    for ip in range(Nip):    
      for ic in range(2):
        g[irin +1:irsep   ,ip,0,ic] = g[irin ,ip,0,ic]+\
                                      (g[irsep,ip,0,ic]-g[irin,ip,0,ic])*x1
        g[irsep+1:irneut+1,ip,0,ic] = g[irsep,ip,0,ic]+\
                                      (g[irneut,ip,0,ic]-g[irsep,ip,0,ic])*x2
        #Rescale last flux-surface
        g[irneut+1:Nir    ,ip,0,ic] = g[irneut,ip,0,ic]+\
                                      (-vax[ic]+g[irneut,ip,0,ic])*x3
    #--------------------------
    if(pprint):
        print('force up/down symmetry in triangular plane.')
    for ir in range(Nir):
        #ir=NirC+NirS+i
        for ip in range(Niphalf+1):
            g[ir,Nip-1-ip,0,0]= g[ir,ip,0,0]
            g[ir,Nip-1-ip,0,1]=-g[ir,ip,0,1]
    
    if(is_bean): g[:,:,0,2]=phibean
    else: g[:,:,0,2]=phitri

    #--------------------------
    if(pprint):
        print('Trace mag. axis')
        
    if(is_bean):
        gax = tr.field_line(np.array([Rax_bean,zax_tri,phibean]), \
                        (phitri-phibean)/(Nit-1),Nit-1,substeps=10,dir=1)
    else:
        gax = tr.field_line(np.array([Rax_tri,zax_tri,phitri]), \
                        (phibean-phitri)/(Nit-1),Nit-1,substeps=10,dir=1)
    #from mayavi import mlab
    #fig = mlab.figure()
    #mlab.plot3d(gax[:,0],gax[:,1],gax[:,2])
    
    
    #--------------------------
    if(pprint):
        print('extend along fieldline')
    for ir in range(Nir):
      for ip in range(Nip):    
        # note that Nit (and not Nit-1) points along the fieldline are given back
        if(is_bean):
            g[ir,ip,:,:]=tr.field_line(g[ir,ip,0,:], \
                            (phitri-phibean)/(Nit-1),Nit-1,substeps=10,dir=1)
        else:
            g[ir,ip,:,:]=tr.field_line(g[ir,ip,0,:], \
                            (phibean-phitri)/(Nit-1),Nit-1,substeps=10,dir=1)
    
    #--------------------------
    if(pprint):
        print('Check grid')
    if len(np.where(np.isnan(g))[0])>0:
        raise Exception('invalid grid cells.')
    
    err=gr.grid3d_check(g)
    if(pprint):
        print('grid has %i errors' % (len(np.where(err)[0])))

        #--------------------------
        print('Use vessel exterior for grid boundary')
        '''
        #--------------------------
        print('load vessel exterior')
        import utils.data as data
        ves = data.data()
        ves.load('W7XWall-Simple.pik')
        x = ves.lpR[37]
        y = np.array(ves.lpz[37])
        ax[0].plot(x,y,'+c')
        ax[1].plot(ves.lpR[2],-np.array(ves.lpz[2]),'+c')
        '''
        
    plotdata = None if not plot else [fig, ax]
    old_globals = [phitri, phi_tar, gax, irneut]
    return g, old_globals, consts, plotdata
    
    
def make_outer_boundary(g, old_globals, consts, plotdata,
                        pprint=False, plot=False, show=False):
    phitri, phi_tar, gax, irneut = old_globals
    if(plot):
        fig, ax = plotdata
    
    degtorad = np.pi/180.
    pathtohere = os.path.realpath(__file__)
    pathtofolder = "/".join(pathtohere.split("/")[:-1])
    wall = Kisslinger.Kisslinger(f'{pathtofolder}/W7X-VacuumVessel-Medium.kis')
    
    #Interpolate points on outer boundary
    # dphi = phitri/(Nit-1)
    tmpR,tmpz,tmpp = gax[:,0],gax[:,1],gax[:,2]
    lphi = []
    for it in range(consts["Nit"]):
        phi = np.round(tmpp[it]/degtorad*1.E3)/1.E3#36.-dphi*it/degtorad
        lphi.append(phi)
        R, Z = wall.get_phi_lineout(phi)
        #Convert to SI-unit
        R, Z = R/100., Z/100.
        #Use vessel outline and thetavalues of grid boundary
        #Re-center on magnetic axis, so that the boundary is around 0,0
        R, Z = R-tmpR[it], Z-tmpz[it]
        # calculate the poloidal angle of every point on the boundary
        thetaves = np.arctan2(Z,R)
        
        #Sort increasing atan2 for interpolation (ergo start at 180° going anticlockwise)
        idx = np.argsort(thetaves)
        R = R[idx]
        Z = Z[idx]
        thetaves = thetaves[idx]
        
        # one must not forget to close the theta boundary again after reordering,
        # so that for theta
        # values smaller than the first of the 73 and bigger than the last of the 73
        # numpy knows how to interpolate them
        thetaves = np.append(np.insert(thetaves, 0, thetaves[-1]-2*np.pi), thetaves[0]+2*np.pi)
        R = np.append(np.insert(R, 0, R[-1]), R[0])
        Z = np.append(np.insert(Z, 0, Z[-1]), Z[0])
        
        if not consts["eq_space"]:
            #Use theta values from grid boundary (sorted from -pi to pi for all 1001 points)
            thetagrid = np.arctan2(g[irneut,:,it,1]-tmpz[it],g[irneut,:,it,0]-tmpR[it])
            
            # here we have 2 thetalists. one for the vessel shape (thetaves[73]) and one for the 
            # plasmaboundary shape (thetagrid[Nip])
            
            #Get interpolated boundary
            # ergo: one knows the values of R at every point in thetaves,
            # but one would like to know what values R would take (linear approx)
            # at the values in between, specifically thetagrid.
            # so the new calculated R and Z lists list the coordinates of the vessel boundary 
            # at the poloidal angles thetagrid (plus the delta again)
            # RR, ZZ = R.copy(), Z.copy()
            R = np.interp(thetagrid, thetaves, R)+tmpR[it]
            Z = np.interp(thetagrid, thetaves, Z)+tmpz[it]
            # with that, one now has the 1001 edge points and 1001 vessel points that the edge points can connect to.
            
            #Force first and last point to coincide
            R[-1] = R[0]; Z[-1] = Z[0]
            # set outer boundary to the vessel points
            g[-1,:,it,0]=R
            g[-1,:,it,1]=Z
            
        else: # make equally spaced points on vessel
            # find theta angle of first inner grid point
            theta0 = np.arctan2(g[irneut,0,it,1]-tmpz[it], g[irneut,0,it,0]-tmpR[it])
            # make new outer vessel point at this angle
            R0 = np.interp(theta0, thetaves, R)
            Z0 = np.interp(theta0, thetaves, Z)
            # add this point into R & Z and
            # resort so that the new point is the first one in the list
            i0 = np.argmax(thetaves>theta0) # index of point after new point
            assert(i0 != 0)
            R = np.concatenate(([R0], R[i0:-1], R[1:i0]))
            Z = np.concatenate(([Z0], Z[i0:-1], Z[1:i0]))
            # add first point to end again
            R = np.concatenate((R, R[0:1]))
            Z = np.concatenate((Z, Z[0:1]))

            equal_spaced_points(R+tmpR[it], Z+tmpz[it], consts["Nip"],
                                g[-1,:,it,0], g[-1,:,it,1])
      
        
    '''
    if it == 35:
      fig = plt.figure()
      plt.plot(thetaves,x+tmpR)
      plt.plot(theta,g[-1,:,it,0])
    '''

    if(plot):
        i = 1 if consts["is_bean"] else 0
        R, Z = wall.get_phi_lineout(phi_tar/degtorad)
        ax[1-i].plot(R/100.,Z/100.,'g')
        R, Z = wall.get_phi_lineout(phitri/degtorad)
        ax[i].plot(R/100.,Z/100.,'g')
        ax[1-i].plot(g[-1,:,0,0],g[-1,:,0,1],'-m')
        ax[i].plot(g[-1,:,-1,0],g[-1,:,-1,1],'-m')
        ax[i].plot(tmpR[0],tmpz[0],'r+',ms=5)
        ax[1-i].plot(tmpR[-1],tmpz[-1],'r+',ms=5)
        #print(phi_tar/degtorad,lphi[-1])
        #print(phitri/degtorad,lphi[0])
        if(show):
            plt.draw()
        #exit
            
    plotdata = None if not plot else [fig, ax]
    old_globals = [phitri, phi_tar, gax, irneut]
    return g, old_globals, consts, plotdata
    
    
def make_outer_grid(g, old_globals, consts, plotdata, pprint=False, plot=False, show=False):
    phitri, phi_tar, gax, irneut = old_globals
    if(plot):
        fig, ax = plotdata

    #--------------------------
    if(pprint):
        print('Replace neutral domain grid with extrapolation to vesel')
    x3=np.linspace(1.0,0.0,consts["NirN"]+1)[1:]
    for it in range(consts["Nit"]):
      for ip in range(consts["Nip"]):
        for ic in range(2):
          g[irneut+1:consts["Nir"],ip,it,ic] = g[-1,ip,it,ic]+\
                                    (g[irneut,ip,it,ic]-g[-1,ip,it,ic])*x3
    
    #--------------------------
    if(pprint):
        print('Plot grid')
    #Plot triangluar plane grid
    if(plot):
        Nir, Nip = consts["Nir"], consts["Nip"]
        for ir in range(Nir):ax[0].plot(g[ir,:,0,0],g[ir,:,0,1],color='blue',lw=0.3)
        for ip in range(Nip):ax[0].plot(g[:,ip,0,0],g[:,ip,0,1],color='red',lw=0.3)
        ax[0].plot([4.25,6.5],[0]*2,color='gray',ls='--',lw=0.5)
        
        #Plot bean shaped plane grid
        for ir in range(Nir):ax[1].plot(g[ir,:,-1,0],g[ir,:,-1,1],color='blue',lw=0.3)
        for ip in range(Nip):ax[1].plot(g[:,ip,-1,0],g[:,ip,-1,1],color='red',lw=0.3)
        ax[1].plot([4.25,6.5],[0]*2,color='gray',ls='--',lw=0.5)
        
        if(show):
            plt.draw()
        #fig.savefig('poincare.png')
            
    plotdata = None if not plot else [fig, ax]
    return g, plotdata


def equal_spaced_points(x_coords, y_coords, n, x_out, y_out, pprint=False):
    # x and y coord differences between points 
    xcd = np.diff(x_coords) # 
    ycd = np.diff(y_coords)
    # calulate length of path and therefore length between points
    point_dists = (xcd**2 + ycd**2)**(1/2)
    arc_len = np.sum(point_dists)
    point_dist = arc_len/(n-1)
    if(pprint): 
        print("\n", arc_len, point_dist)
    # find coord of mth point by going along subsampling points
    # until it would be too ling, and then interpolating
    for i in range(n-1): # [0, n]
        path_len, full_len = 0, i*point_dist
        c = 0
        while True: # go along polypath
            if path_len + point_dists[c] >= full_len:
                # interpolate
                frac = (full_len - path_len)/point_dists[c]
                x_out[i] = x_coords[c] + frac*xcd[c]
                y_out[i] = y_coords[c] + frac*ycd[c]
                if(pprint): print(i, full_len)
                break
            else:
                # go to next path segment
                path_len += point_dists[c]
                c += 1
    # place last point at same coord as first one
    x_out[-1] = x_coords[0]
    y_out[-1] = y_coords[0]
    

if __name__ == "__main__":
    consts = {"NirS": 10, "NirC": 10, "NirN": 10,  # in -> outside cell number 
             "Niphalf": 20, "is_bean": True,  # half pol cell number
             "eq_space": True}
    
    g, glob, consts, plots = make_inner_grid(consts=consts, plot=True)
    g, glob, consts, plots = make_outer_boundary(g, glob, consts, plots, plot=True)
    g, (fig, ax) = make_outer_grid(g, glob, consts, plots, plot=True)
    
    
    fig.show()
    #print(g, g.shape)
    
    
if False: sys.exit()

# #--------------------------
# if False:
#     print('write grid.')
#     fg=open('grid.txt','w')
#     fg.write('%10i %10i %10i\n' % tuple(g.shape[0:3]))
#     for it in range(g.shape[2]): #iterate over poloidal planes
#             fg.write('%16.8f \n' % ((g[0,0,it,2]+phi_offset)*180./np.pi))  # angle in deg
#             np.savetxt(fg,np.array(g[:,:,it,0]*100).reshape(-1,order='F')) # R-coordinate in cm
#             np.savetxt(fg,np.array(g[:,:,it,1]*100).reshape(-1,order='F')) # z-coordinate in cm
#     fg.close()

#     print('write field.')
#     B=gr.get_b_grid(g)
#     B[NirC+NirS+1:,:,:]=-1.0
#     fb=open('field.txt','w')
#     np.savetxt(fb,B.reshape(-1,order='F'))
#     fb.close()

# #--------------------------
# print('writing input files')
# for fninput in ['input.geo','input.n0g']:
#     s=open('template_'+fninput).read()
#     s=s.replace('#Nir#',str(Nir)).replace('#Nir-1#',str(Nir-1)).replace('#Nir-2#',str(Nir-2)).replace('#Nrplasma#',str(NirC+NirS-2))
#     s=s.replace('#Nip#',str(Nip)).replace('#Nip-1#',str(Nip-1)).replace('#Nip-2#',str(Nip-2))
#     s=s.replace('#Nit#',str(Nit)).replace('#Nit-1#',str(Nit-1)).replace('#Nit-2#',str(Nit-2))
#     open(fninput,'w').write(s)

# #--------------------------
# def write_EMC3_target_file(fn,Rzp,comment=None,scaling_factor=100.0,phi_scaling_factor=1.0,phi_offset=0.0):
#     if comment==None:comment=fn
#     #print 'phi_scaling_factor',phi_scaling_factor
#     f=open(fn,'w')
#     f.write(comment+'\n')
#     f.write('%i  %i   1      0.00000      0.00000\n'% (Rzp.shape[1],Rzp.shape[0]))
#     for it in range(Rzp.shape[1]):
#       f.write("%15.5f\n" % ((Rzp[0,it,2]+phi_offset)*phi_scaling_factor))
#       for x in Rzp[:,it,0:2]*scaling_factor:
#           f.write("%15.5f   %15.5f\n" % (x[0],x[1]))
#     f.close()


# '''
# #define a grid aligned target. Its surface is shifted by 1% of the cell size radially outward to avoid aliasing-effect of the intersection routine
# irt=NirC+5;itt=Nit-3;alpha=0.99
# tar3D=np.zeros((Nip*2+1,2,3))

# tar3D[:-1,0,:]=np.vstack((alpha*g[irt,:,itt,:]+(1.0-alpha)*g[irt+1,:,itt,:],g[Nir-1,::-1,itt,:]))
# tar3D[:-1,0,2]-=0.1*np.pi/180.
# tar3D[:-1,1,:]=np.vstack((alpha*g[irt,:,itt,:]+(1.0-alpha)*g[irt+1,:,itt,:],g[Nir-1,::-1,itt,:]))
# tar3D[:-1,1,2]+=0.1*np.pi/180.
# tar3D[-1,:,:]=tar3D[0,:,:] # close the target

# write_EMC3_target_file('bean_target.txt',tar3D,phi_scaling_factor=180./np.pi,phi_offset=phi_offset)
# # you could trivially mark the cells writing
# # targetcells[irt:,:,itt-1:itt+1]=1
# # but let's use the intersection search routine that might be required when defining more general target shapes:

# '''

# #--------------------------

# exit

# #--------------------------
# print('search grid intersection with targets and write out target files')
# targetcells=np.zeros((Nir-1,Nip-1,Nit-1),dtype=np.int32,order='F') # an array to mark cells as inside (=1) or outside (=0) the targets
# #Determine target cells (non-plasma) from extended target boundaries
# for tar in ltarx:
#     tar3D=np.zeros((tar.nRZ,tar.nphi,3))
#     tar3D[:,:,0] = tar.R.T/100. ; tar3D[:,:,1] = tar.Z.T/100. ; tar3D[:,:,2] = tar.P.T/degtorad/100.
#     gr.intersect3d(g,targetcells,tar3D)

# '''
# #Save target geometry of in-grid target boundaries
# for tar,fn in [[ltars,'lower_target.txt'],[utars,'upper_target.txt']]:
#     tar3D=np.zeros((tar.shape[0],2,3))
#     tar3D[:,0,0:2]=tar*1.0;tar3D[:,0,2]=phi_tar-dphi_tar/2.0
#     tar3D[:,1,0:2]=tar*1.0;tar3D[:,1,2]=phi_tar+dphi_tar/2.0
    
#     write_EMC3_target_file(fn,tar3D,phi_scaling_factor=180./np.pi,phi_offset=phi_offset)
# '''
# print('number of targetcells found',len(np.where(targetcells)[0]))

# f=open('intersection.txt','w')
# pm,Npm=gr.plates_mag(targetcells) # prepare the format required by EMC3
# nzone=0
# for i in range(Npm): #...and write it into a file
#         k=pm[i,2]+3
#         f.write('%i  ' % (nzone))
#         f.write((('%i ' * k) % tuple(pm[i,0:k]))+'\n')
# f.close()


# #--------------------------
# print('plot grid and intersection.')
# itt=34
# for it in [itt]:#[0,itt-2,itt-1,itt,itt+1]:
#     #fig,ax=plt.subplots(figsize=(12,8))
#     #fig.suptitle('$\phi=%.1f^o$' % (g[0,0,it,2]*180./np.pi),fontsize=22)
#     #fig.canvas.mpl_connect('button_press_event', onclick)

#     #for ip in range(Nip):ax[1].plot(g[:,ip,it,0],g[:,ip,it,1],color='black',lw=0.3)
#     #for ir in range(Nir):ax[1].plot(g[ir,:,it,0],g[ir,:,it,1],color='black',lw=0.3)

#     iir,iip=np.where(targetcells[:,:,it])

#     if False:
#       for ir,ip in zip(iir,iip):
#           jr=[ir,ir+1,ir+1,ir,ir]
#           jp=[ip,ip,ip+1,ip+1,ip]
#           #ax[1].fill(g[jr,jp,it,0],g[jr,jp,it,1],color='orange',lw=2,zorder=5,alpha=0.2) # color cells that are marked as target cells
#           ax[1].plot(g[jr,jp,it,0],g[jr,jp,it,1],color='orange',lw=0.5)

#     #Plot target
#     '''
#     for tar in [utar,ltar]:ax[1].plot(tar[:,0],tar[:,1],color='red',lw=2)
#     for tar in [utars,ltars]:ax[1].plot(tar[:,0],tar[:,1],color='green',ls='--',lw=2)
#     '''
#     for tar in ltarx:
#         tar.plot_phi_lineout(phitri/degtorad ,fig=fig,ax=ax[0],color='k',norm=0.01)
#         tar.plot_phi_lineout(phi_tar/degtorad,fig=fig,ax=ax[1],color='k',norm=0.01)
#         #ax[0].plot(tar3D[:,-1,0],tar3D[:,-1,1],color='blue',lw=1) # show the actual target contour
#         #ax[1].plot(tar3D[:, 0,0],tar3D[:, 0,1],color='blue',lw=1) # show the actual target contour
#     #ax[1].set_aspect('equal')
#     #plt.draw()
#     fig.savefig('grid_plane_%03i.png' % it)
#     #plt.close(fig)
#     #fig.show()
    
# plt.draw()


# '''
# Rt=5.9;rt=1.5;th=np.linspace(0,2*np.pi,1000)

# v=curve(np.array([Rt+np.cos(th)*rt,np.sin(th)*rt]).T)

# for it in range(Nit):
#     g[:,:,it,2]=g[0,0,it,2]

#     lv=v.totl*1.0
    
#     for ip in range(Nip):
#         c=curve([g[0,ip,it,0:2],g[NirC+NirS-1,ip,it,0:2]])
#         c.extend(5.0)
#         ccc=c.collide(v)
#         #lpp[i]=ccc[3]*1.0
#         for ic in range(2):
#             g[NirC+NirS:,ip,it,ic]=c.Rz[1,ic]+(ccc[ic]-c.Rz[1,ic])*x3
    
# '''

