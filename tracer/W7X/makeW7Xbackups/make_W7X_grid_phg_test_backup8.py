
import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
import os
#sys.path.append('../build/.')
#breakpoint()
# from tracer.build.field_routines import baxisym as bas 
from tracer.build.field_routines import trace as tr
from tracer.build.field_routines import bprecalc as bpre 
from tracer.build.field_routines import bfield as bf 
from tracer.build.field_routines import grid as gr 

from combine.quick_g import quick_g_plot
#sys.path.append('../grid/.')
#from curve import *
import tracer.W7X.Kisslinger as Kisslinger


def make_inner_grid(consts=dict(), pprint=False):
    #--------------------------
    if(pprint):
        print('Define grid parameters & locations')
        
    # radial resolution of the SOL, core and outer neutral regions
    NirS = consts.get("NirS", 100)
    NirC = consts.get("NirC", 25)
    NirN = consts.get("NirN", 5)
    consts["NirS"], consts["NirC"], consts["NirN"] = NirS, NirC, NirN 
    
    
    # poloidal and toroidal resolutions
    Niphalf = consts.get("Niphalf", 500)
    Nit = consts.get("Nit", 37)
    assert(Nit == 37)
    consts["Niphalf"], consts["Nit"] = Niphalf, Nit
    
    # start in bean crossection or triangle or somewhere else?
    # 0 == bean, np.pi/180 * 36 == triangle
    trace_angle = consts.get("trace_angle", 0)
    assert(0 <= trace_angle < Nit) # in first module
    assert(type(trace_angle) == int)
    consts["trace_angle"] = trace_angle * 2*np.pi/Nit/10
    consts["trace_index"] = trace_angle
    
    # bean starting points to trace to any angle
    # flux surface radii for tracing for bean and triangle crossection
    cax_bean = [5.944e+00, 0.0, 0.0]
    Rlcfs_bean = consts.get("Rlcfs_bean", 6.2014041)
    dRcore = consts.get("dRcore", 0.10)
    dRneut = consts.get("dRneut", 0.3)
    dRSOL = consts.get("dRSOL", -0.085)
    
    # create poloidal points equally spaced from each other?
    consts["eq_space"] = consts.get("eq_space", False)
    consts["eq_space_vessel"] = consts.get("eq_space_vessel", False)
    consts["vessel_pdf"] = consts.get("vessel_pdf", None)
    if consts["eq_space_vessel"] and (consts["vessel_pdf"] is not None):
        # fallback_pdf = np.ones((1, consts["Nit"])) 
        # check that one pdf for every phi angle exists
        assert(consts["vessel_pdf"].shape[1] == consts["Nit"])
    
    
    # when parameterizing over the theta angle of the boundary, the vessel
    # shape can in certain circumstances not be monotonically increasing
    # in theta. as we are free to place the vesses grid points, this shifts
    # the midpoint a bit to the outside so that the middle bean shape
    # is monotonically increasing again
    # the shift will vary between machines and even configurations!!!
    consts["center_delta_R"] = consts.get("center_delta_R", 0)
    
    Nir=NirC+NirS+NirN; Nip=2*Niphalf+1
    consts["Nir"], consts["Nip"] = Nir, Nip
    
    #Define magnetic field locations
    dphi_fp=2*np.pi/5.0 # toroidal extension of the field period
    phitri=2*np.pi/10.0 # toroidal coordinate in which the plasma has a triangular shape
    phibean=0.#2*np.pi/5.0 # toroidal coordinate in which the plasma a bean shape
    
    #--------------------------
    if(pprint):
        print('load/calculate mag. field')
           
    pathtohere = os.path.realpath(__file__)
    pathtofolder = "/".join(pathtohere.split("/")[:-1])
    # pathtofolder = os.path.abspath('')
    # fn=f'{pathtofolder}/TEST1.dat'
    # fn=f'{pathtofolder}/W7X.dat'
    fn = f'{pathtofolder}/W7XField-FTM-Test.dat'
    bpre.restore_field(fn) # restore the result (faster than computing)

    #turn on the pre-calculated 3D magnetic field and the axi-symmetric Tokamak field off
    #first to components are tokamak background field, third is 3D-field (pertubation for tokamaks)
    bf.scale_components([0,0,1.0])

    #Define grid
    g=np.zeros((Nir,Nip,Nit,3),order='F')*np.nan
    
    
    #--------------------------
    if(pprint):
        print('construct flux surfaces in confinement region')
        
    # trace from bean to starting surface
    ta, ti = consts["trace_angle"], consts["trace_index"]
    angle_step = (phitri - phibean)/(Nit-1)
    new_coords = [None]*4 # new Rfs's at new plane angle
    r_indices, r_deltas = [0,1,NirC-1,NirC+NirS-1], [dRneut, dRcore, 0, dRSOL]
    for i, (ifs, Rfs) in enumerate(zip(r_indices, r_deltas)):
        fl = tr.field_line([Rlcfs_bean-Rfs,0,phibean],ta,1,substeps=72,dir=1)
        new_coords[i] = fl[-1]
        
    if(pprint):
        print('Trace mag. axis')
        
    # trace axis starting cross section   
    new_cax = tr.field_line(cax_bean,ta,1,substeps=72,dir=1)[-1]
    # trace axis along whole segement
    gax = tr.field_line(cax_bean,angle_step,Nit-1,substeps=72,dir=1)
    
    
    #Construct a flux surface by field line tracing     
    for ifs, nc in zip(r_indices, new_coords):
        if(pprint): print(ifs,Rfs)
        fl=tr.field_line(nc,dphi_fp,10*Nip,substeps=72*5,dir=1)
    
        # #order the points according to the theta coordinate
        thetafl=np.arctan2(fl[:,1]-new_cax[1],fl[:,0]-new_cax[0])
        ii=np.argsort(thetafl)
        fl=fl[ii];thetafl=thetafl[ii]
        fl=np.vstack((fl,fl,fl))
        thetafl=np.hstack((thetafl-2*np.pi,thetafl,thetafl+2*np.pi))
        
        if consts["eq_space"]: # place points an equal distance apart
            # subsample fieldlinepoints (to remove jitter)
            theta=np.linspace(-np.pi,np.pi,5*Nip)
            Rc = np.interp(theta, thetafl, fl[:,0])
            Zc = np.interp(theta, thetafl, fl[:,1])
            equal_spaced_points(Rc, Zc, Nip, g[ifs,:,ti,0], g[ifs,:,ti,1])
            
        else: # just place points uniformly along polidal angle
            theta=np.linspace(-np.pi,np.pi,Nip)
            g[ifs,:,ti,0]=np.interp(theta, thetafl, fl[:,0])
            g[ifs,:,ti,1]=np.interp(theta, thetafl, fl[:,1])

    if(pprint):
        print('extrapoloate intermediate surfaces linearily') 
    x1=np.linspace(0.0,1.0,NirC-1)[1:-1]
    x2=np.linspace(0.0,1.0,NirS+1)[1:]
    
    irin=1;irsep=NirC-1;irneut=NirC+NirS-1
    for ip in range(Nip):    
        for ic in range(2):
            g[irin +1:irsep  ,ip,ti,ic] = g[irin ,ip,ti,ic]+\
                        (g[irsep,ip,ti,ic]-g[irin,ip,ti,ic])*x1
            g[irsep+1:irneut+1,ip,ti,ic] = g[irsep,ip,ti,ic]+\
                        (g[irneut,ip,ti,ic]-g[irsep,ip,ti,ic])*x2
    #--------------------------
    if(pprint):
        print('force up/down symmetry in triangular plane.')
    if(ta == 0 or ta == Nir-1):
        for ir in range(Nir):
            #ir=NirC+NirS+i
            for ip in range(Niphalf+1):
                g[ir,Nip-1-ip,ti,0]= g[ir,ip,ti,0]
                g[ir,Nip-1-ip,ti,1]=-g[ir,ip,ti,1]
        
    g[:,:,ti,2] = ta
    
    #--------------------------
    if(pprint):
        print('extend along fieldline')
    for ir in range(NirC+NirS): # only trace inner points not outside SOL
        for ip in range(Nip):  
            # note that Nit (and not Nit-1) points along the fieldline
            # are given back in the trace routine
    
            # trace forward
            g[ir,ip,ti:] = tr.field_line(g[ir,ip,ti], angle_step, Nit-1-ti, substeps=10, dir=1)
            # trace backward
            g[ir,ip,:ti+1] = tr.field_line(g[ir,ip,ti], angle_step, ti, substeps=10, dir=-1)
    
    old_globals = [phitri, gax, irneut]
    return g, old_globals, consts
    
    
def make_outer_boundary(g, old_globals, consts,
                        pprint=False, plot=False, show=False):
    phitri, gax, irneut = old_globals
    
    degtorad = np.pi/180.
    pathtohere = os.path.realpath(__file__)
    pathtofolder = "/".join(pathtohere.split("/")[:-1])
    wall = Kisslinger.Kisslinger(f'{pathtofolder}/W7X-VacuumVessel-Medium.kis')
    
    if pprint: print("make fractional Vessel spacing")
    if consts["vessel_pdf"] is not None:
        # generate fractional spacings inv_cdf[tht,phi]
        inv_cdf = convert_rawpdf_to_invcdf(consts["vessel_pdf"], consts["Nip"])
    
    #Interpolate points on outer boundary
    # dphi = phitri/(Nit-1)
    tmpR,tmpz,tmpp = gax[:,0],gax[:,1],gax[:,2]
    lphi = []
    for it in range(consts["Nit"]):
        phi = np.round(tmpp[it]/degtorad*1.E3)/1.E3#36.-dphi*it/degtorad
        lphi.append(phi)
        R, Z = wall.get_phi_lineout(phi)
        #Convert to SI-unit
        R, Z = R/100., Z/100.
        #Use vessel outline and thetavalues of grid boundary
        #Re-center on magnetic axis, so that the boundary is around 0,0
        R, Z = R-tmpR[it], Z-tmpz[it]
        # calculate the poloidal angle of every point on the boundary
        thetaves = np.arctan2(Z,R-consts["center_delta_R"])
        
        #Sort increasing atan2 for interpolation (ergo start at 180° going anticlockwise)
        idx = np.argsort(thetaves)
        R = R[idx]
        Z = Z[idx]
        thetaves = thetaves[idx]
        
        # one must not forget to close the theta boundary again after reordering,
        # so that for theta
        # values smaller than the first of the 73 and bigger than the last of the 73
        # numpy knows how to interpolate them
        thetaves = np.append(np.insert(thetaves, 0, thetaves[-1]-2*np.pi), thetaves[0]+2*np.pi)
        R = np.append(np.insert(R, 0, R[-1]), R[0])
        Z = np.append(np.insert(Z, 0, Z[-1]), Z[0])
        
        if not consts["eq_space_vessel"]:
            #Use theta values from grid boundary (sorted from -pi to pi for all 1001 points)
            thetagrid = np.arctan2(g[irneut,:,it,1]-tmpz[it],
                                   g[irneut,:,it,0]-tmpR[it]-consts["center_delta_R"])
            
            # here we have 2 thetalists. one for the vessel shape (thetaves[73]) and one for the 
            # plasmaboundary shape (thetagrid[Nip])
            
            # Get interpolated boundary
            # ergo: one knows the values of R at every point in thetaves,
            # but one would like to know what values R would take (linear approx)
            # at the values in between, specifically thetagrid.
            # so the new calculated R and Z lists list the coordinates of the vessel boundary 
            # at the poloidal angles thetagrid (plus the delta again)
            # RR, ZZ = R.copy(), Z.copy()
            R = np.interp(thetagrid, thetaves, R)+tmpR[it]
            Z = np.interp(thetagrid, thetaves, Z)+tmpz[it]
            # with that, one now has the 1001 edge points and 1001 vessel points that the edge points can connect to.
            
            #Force first and last point to coincide
            R[-1] = R[0]; Z[-1] = Z[0]
            # set outer boundary to the vessel points
            g[-1,:,it,0]=R
            g[-1,:,it,1]=Z
            
        else:
            # breakpoint()
            # make equally spaced points on vessel
            # find theta angle of first inner grid point
            theta0 = np.arctan2(g[irneut,0,it,1]-tmpz[it], g[irneut,0,it,0]-tmpR[it])
            # make new outer vessel point at this angle
            R0 = np.interp(theta0, thetaves, R)
            Z0 = np.interp(theta0, thetaves, Z)
            # add this point into R & Z and
            # resort so that the new point is the first one in the list
            i0 = np.argmax(thetaves>theta0) # index of point after new point
            assert(i0 != 0)
            R = np.concatenate(([R0], R[i0:-1], R[1:i0]))
            Z = np.concatenate(([Z0], Z[i0:-1], Z[1:i0]))
            # add first point to end again
            R = np.concatenate((R, R[0:1]))
            Z = np.concatenate((Z, Z[0:1]))
            
            if consts["vessel_pdf"] is None:
                invcdf1d = None # naive equal spacing version 
            else: # get fractional spacing for this phi
                invcdf1d = inv_cdf[:,it]
                
            equal_spaced_points(R+tmpR[it], Z+tmpz[it], consts["Nip"],
                                g[-1,:,it,0], g[-1,:,it,1],
                                fractional_space=invcdf1d)
                
      
    old_globals = [phitri, gax, irneut]
    return g, old_globals, consts
    
    
def make_outer_grid(g, old_globals, consts, pprint=False, show=False):
    phitri, gax, irneut = old_globals

    x3=np.linspace(1.0,0.0,consts["NirN"]+1)[1:]
    for it in range(consts["Nit"]):
        for ip in range(consts["Nip"]):
            # R,z coords
            for ic in range(2):
                coordv = g[-1,ip,it,ic]+(g[irneut,ip,it,ic]-g[-1,ip,it,ic])*x3
                g[irneut+1:consts["Nir"],ip,it,ic] = coordv
            # phi coords
            g[irneut+1:consts["Nir"], ip, it, 2] = g[0, ip, it, 2]
                                    
    #print(g[:,0,0,0])   
    #--------------------------
    if(pprint):
        print('Check grid')
    if np.any(np.isnan(g)):
        raise Exception('invalid grid cells.')
    
    err=gr.grid3d_check(g)
    if(pprint):
        print('grid has %i errors' % (len(np.where(err)[0])))

        #--------------------------
        print('Use vessel exterior for grid boundary')
        '''
        #--------------------------
        print('load vessel exterior')
        import utils.data as data
        ves = data.data()
        ves.load('W7XWall-Simple.pik')
        x = ves.lpR[37]
        y = np.array(ves.lpz[37])
        ax[0].plot(x,y,'+c')
        ax[1].plot(ves.lpR[2],-np.array(ves.lpz[2]),'+c')
        '''
            

    return g

# takes a thtsize x phisize input raw density function
# it then converts it to an inverse cdf along the tht-axis
# (so changes in other phi coords dont affect the output at the orig. phi coord)
# pdf(tht) = const -> invcdf(x) = x
def convert_rawpdf_to_invcdf(pdf, invcdf_resolution):
    pdf = pdf / pdf.sum(axis=0)
    thtsize, phisize = pdf.shape
    invcdf_x1d = np.linspace(0, 1, invcdf_resolution)
    cdf_x1d = np.linspace(0, 1, thtsize+1)
    cdf_y = np.cumsum(np.insert(pdf, 0, np.zeros(phisize), axis=0), axis=0)
    inv_cdf = np.empty((invcdf_resolution, phisize))
    for i, cdf_y1d in enumerate(cdf_y.T):
        inv_cdf[:,i] = np.interp(invcdf_x1d, cdf_y1d, cdf_x1d)
    return inv_cdf


def equal_spaced_points(x_coords, y_coords, n, x_out, y_out, 
                        fractional_space=None, pprint=False):
    # x and y coord differences between points 
    xcd = np.diff(x_coords) # 
    ycd = np.diff(y_coords)
    # calulate length of path and therefore length between points
    point_dists = (xcd**2 + ycd**2)**(1/2)
    arc_len = np.sum(point_dists)
    point_dist = arc_len/(n-1)
    if(pprint): 
        print("\n", arc_len, point_dist)
        print("equal" if fractional_space is None else "fractional", "spacing")
        
    # find coord of mth point by going along subsampling points
    # until it would be too ling, and then interpolating
    for i in range(n-1): # [0, n]
        if fractional_space is None:
            # naive version, every point has same distance
            full_len = i*point_dist
        else:
            # point distance fractions along the path are given by frac._sp.
            full_len = fractional_space[i]*arc_len
            
        path_len, c = 0, 0
        while True: # go along polypath
            # would you be further than intended at the next point
            if path_len + point_dists[c] >= full_len:
                # YES: interpolate the goal between current and next point
                frac = (full_len - path_len)/point_dists[c]
                x_out[i] = x_coords[c] + frac*xcd[c]
                y_out[i] = y_coords[c] + frac*ycd[c]
                if(pprint): print(i, full_len)
                break
            else:
                # NO: go to next path segment
                path_len += point_dists[c]
                c += 1
    # place last point at same coord as first one
    x_out[-1] = x_coords[0]
    y_out[-1] = y_coords[0]

if __name__ == "__main__":
    # grid parameters, you can add more!
    consts = {"NirS": 5, "NirC": 5, "NirN": 10,  # in -> outside cell number 
         "Niphalf": 50,  # half pol cell number
         "eq_space": True, "eq_space_vessel": True, "Nit": 37,
         "zoidpol": False, "trace_angle": 10, # angle is int in [0,36]
         "center_delta_R": .15, "dRSOL": -0.08, }
    
    # vpdf = np.ones((consts["Niphalf"]*2+1, consts["Nit"]))
    # vpdf[:,0] = [1]*10 + [2]*10 + [3]*10 + [4]*10 + [1]*61
    # consts["vessel_pdf"] = vpdf
    
    g, glob, consts = make_inner_grid(consts=consts)
    g, glob, consts = make_outer_boundary(g, glob, consts)
    g = make_outer_grid(g, glob, consts)
    
    #print(g, g.shape)
    quick_g_plot(g, phi=0)
    
    
if False: sys.exit()

