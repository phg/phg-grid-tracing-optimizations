# Testing
import sys
import numpy as np
sys.path.append('../../build')
sys.path.append('../')
from importlib import reload
from matplotlib import pyplot as plt
# from field_routines import bprecalc as bpc

# sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)

# Felix' spezielle Werte
# IP = [13524, 13145, 12524, 11897, 11526, 0, 0]
# ICC = np.array([531.77449638, 378.90750231, -255.17636576,
#                 -269.13820556, 102.6648061, -1.31131196,
#                 300.85974959, 401.6726024,
#                 -391.2362153, -483.75262189])
# ICC = np.array([round(num, 0) for num in ICC])
# ITC = np.array([81.0, -33, -100, -33, 81])
# coils = mf.load_coils()
# cur = mf.get_currents(IP,ICC,ITC)
# fn = 'Field-EIM-ErrFullT-high.dat'
# mf.make_field(cur,coils,res='full_t_high',fn=fn)
# mf.plot_magnetic_field()

sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)
#coils = mf.get_coils()
coils = mf.load_coils(fn='../W7XCoils.pik')
config = 'FTM' # <- configuration
IP,ICC,ITC = mf.get_config(config)
#ITC = np.array([6950.,0.,-6990.,-4330.,4340.])/48
cur = mf.get_currents(IP,ICC,ITC)
# mf.make_field(cur,coils,res='std',fn=f'Field-{config}-std.dat')
# print("Done high")
# mf.make_field(cur,coils,res='high',fn=f'Field-{config}-high.dat')
print("hi")
mf.make_field(cur,coils,res='full_t_std',fn=f'Field-{config}-FullT-std.dat')
print("Done std")
mf.make_field(cur,coils,res='full_t_high',fn=f'Field-{config}-FullT-high.dat')
print("Done high")