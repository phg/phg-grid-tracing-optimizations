# Testing
import sys
import numpy as np
sys.path.append('../../build')
sys.path.append('../')
from importlib import reload
from matplotlib import pyplot as plt
# from field_routines import bprecalc as bpc

# sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf ; reload(mf)


def local_mfp(phil=[0.,36.*np.pi/180],Rsepl=[6.200,6.0166],
                        xr=[4.5,6.35],yr=[-1.2,1.2],Rs=False):

  nph = len(phil)
  fig,ax=plt.subplots(1,nph,figsize=(6*nph,8),sharey=True, sharex=True)
  fig.canvas.mpl_connect('button_press_event',
                         lambda x: print('[%.4f,%.4f],' % (x.xdata,x.ydata)))
  if nph == 1: ax = [ax]
  #Plot poloidal field in triangular and bean shaped plane
  RR=np.linspace(mf.bpc.rapre,mf.bpc.rbpre,mf.bpc.nrpre) #R-range vector
  zz=np.linspace(mf.bpc.zapre,mf.bpc.zbpre,mf.bpc.nzpre) #z-range vector
  pp=np.linspace(mf.bpc.fapre, mf.bpc.fbpre,mf.bpc.nfpre) #phi-range vector

  #Get field on-axis in bean-shaped plane
  Raxis,zaxis,phiaxis = 5.95, 0.0, 0.0
  idxR = np.argmin((RR-Raxis)**2.)
  idxz = np.argmin((zz-zaxis)**2.)
  idxp = np.argmin((pp-phiaxis)**2)
  Bpolaxis=(mf.bpc.ba[2,idxp,idxR,idxz]**2+mf.bpc.ba[1,idxp,idxR,idxz]**2)**0.5
  Btoraxis=mf.bpc.ba[0,idxp,idxR,idxz]
  print('')
  print('Bfield on-axis:')
  print('phi = %.2f ; B = %.2f;  %.2f; %.2f' %(phiaxis/np.pi/180,mf.bpc.ba[0,idxp,idxR,idxz],
                                               mf.bpc.ba[1,idxp,idxR,idxz],mf.bpc.ba[2,idxp,idxR,idxz]))
  print('phi = %.2f ; Btor = %.2f; Bpol = %.2f' %(phiaxis/np.pi/180,Btoraxis,Bpolaxis))
  print('')

  il = [i for i in range(len(phil))]
  colarray = (["white", "pink", "lime", "coral", "lightskyblue",
         "lavender", "ivory"]+[f"C{k}" for k in range(1000)])
  for i,phi,Rlcfs in zip(il,phil,Rsepl):
      #Get index closest to planes
      it=int((phi-mf.bpc.fapre)/(mf.bpc.fbpre-mf.bpc.fapre)*mf.bpc.nfpre) % (mf.bpc.nfpre - 1)
      #Get fields
      #Initiallz Bp 0+1 und Bt 2
      Bpol=(mf.bpc.ba[2,it,:,:]**2+mf.bpc.ba[1,it,:,:]**2)**0.5
      Btor=mf.bpc.ba[0,it,:,:]
      #Plot
      ax[i].pcolorfast(RR,zz,Bpol.T,vmin=0,vmax=3.0)

      #Trace field lines around LCFS
      nr = 30
      R0 = 5.95
      # Triangle specifics
      # R = [6.1071, 5.9262, 4.7045, 6.2012, 6.0771, 5.6501, 6.0939]
      # z = [0.0084, 0.0061, 0.4169, 0.0039, -0.1315, -0.2931, 0.0103]
      # BEAN (inner, speratrix, outer, [VESSEL comes later])
      if Rs is False:
          R = [6.01, 6.2401, 6.1751]  # , 6.2462] #  6.2464, 6.2467]
          z = [0, 0, 0, 0, 0, 0]
      else:
          R = Rs
          z = np.zeros(len(Rs))
      if i == 0:
          for j, (Rl,zl) in enumerate(zip(R,z)):
              fl=mf.tr.field_line([Rl,zl,phi],2*np.pi,5e3,substeps=2000,dir=1)
              lenofdata = np.argmax(np.isnan(fl[:,0]))
              # if lenofdata == 0:
              print(fl.shape, lenofdata, fl[0,0])
              col = colarray[j]
              ax[i].scatter(fl[:,0],fl[:,1],marker='x',s=1,color=col)

      #for dR in np.linspace(-0.05,0.1,25):
      #    fl=tr.field_line([Rlcfs+dR,0.0,phi],2*np.pi/5.,500,substeps=360*1,dir=-1)
      #    ax[i].scatter(fl[:,0],fl[:,1],marker='x',s=1,color='black')
  for item in ax:
    item.set_xlim(xr)
    item.set_ylim(yr)
  plt.show(block=False)
  return fig

# IP = [13.52E3]*5+[0.]*2
# ICC = 1.*np.array([-531.77,-378.91,255.18,269.14,-102.66,1.31,-300.86,-401.67,391.24,483.75])
# ITC = 1.*np.array([-80.82,32.61,100.09,32.31,-81.02])
# # ITC = [0.]*5
# # ICC = [0.]*10
# # IP = [13600,13600,13600,13600,13600,0,0]
# coils = mf.load_coils()
# cur = mf.get_currents(IP,ICC,ITC)
# fn = 'W7XField-EJM-ErrorField-NOERROR.dat'
# # fn = None
# mf.make_field(cur,coils,res='std',fn=fn)
# mf.plot_magnetic_field()


# Felix' spezielle Werte
# IP = [13524, 13145, 12524, 11897, 11526, 0, 0]
# ICC = np.array([531.77449638, 378.90750231, -255.17636576,
#                 -269.13820556, 102.6648061, -1.31131196,
#                 300.85974959, 401.6726024,
#                 -391.2362153, -483.75262189])
# ICC = np.array([round(num, 0) for num in ICC])
# ITC = np.array([81.0, -33, -100, -33, 81])
# coils = mf.load_coils(fn='../W7XCoils.pik')
# cur = mf.get_currents(IP,ICC,ITC)
# fn = 'W7XField-FELIX-ErrorField-std_fullT.dat'
# mf.make_field(cur,coils,res='full_t_std',fn=fn)
# mf.plot_magnetic_field()

# sys.path.append('/afs/ipp/u/flr/python')
# import make_field as mf ; reload(mf)
# #coils = mf.get_coils()
# coils = mf.load_coils(fn='../W7XCoils.pik')
# config = 'EIM' # <- configuration
# IP,ICC,ITC = mf.get_config(config)
# #ITC = np.array([6950.,0.,-6990.,-4330.,4340.])/48
# cur = mf.get_currents(IP,ICC,ITC)
# mf.make_field(cur,coils,res='small',fn='W7XField-'+config+'-Test.dat')
# #mf.make_field(cur,coils,res='std',fn='W7XField-'+config+'-Test.dat')
##mf.set_magnetic_field('Field-EIM-ErrFullT-std.dat', fplt=False);
# [5.99, 6.09,6.175, ??]
# np.linspace(6.211, 6.215, 500)
# [6.21114428, 6.2112965931, 6.2113126, 6.21229058, ]
# [5.9315, 5.88, 6.095, 6.175333,
##local_mfp(Rs = [5.9315, 5.88, 6.095, 6.175333, 6.246], phil=[0])

"""Uncomment one of the following"""
mf.set_magnetic_field('Field-EIM-ErrFullT-high.dat', fplt=False)
#local_mfp()
# mf.set_magnetic_field('Field-EIM-ErrFullT-high.dat', fplt=False)
# local_mfp()
# mf.set_magnetic_field('Field-EIM-ErrFullT-std.dat', fplt=False)
# local_mfp()
# mf.set_magnetic_field('Field-EIM-high.dat', fplt=True)
# mf.set_magnetic_field('Field-EIM-std.dat', fplt=True)
# mf.set_magnetic_field('Field-FTM-high.dat', fplt=True)
# mf.set_magnetic_field('Field-FTM-std.dat', fplt=True)