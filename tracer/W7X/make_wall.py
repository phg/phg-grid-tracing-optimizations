import geometry.w7x.meshsrv as msh#; reload(msh)
import numpy as np
import matplotlib.pylab as plt
import utils.geom as geom
import utils.data as data

def get_wall(fn='W7XWall-Simple.pik'):
  wc = msh.WALL()
  wc.set_wall([1]) #Simplified Wall
  #wc.compile_components([25,26,27,28,29]) #Cylinder
  npp = 73
  lp = np.linspace(0.,72.,npp)
  fig = plt.figure()
  lrp = []
  for i in range(npp):
      lrp.append(wc.cut_wall_phi(lp[i]))

  #Plot examples
  #fig = plt.figure()
  #for i in range(2):
  #    wc.plot_wall(lrp[i],fig=fig)

  ### Save to file
  lpR = []
  lpz = []
  for i in range(npp):
      v = lrp[i][0].vertices
      #rt, zt, pt = geom.cartesiantocylinder(v.x1[0],v.x2[0],v.x3[0])
      R = []
      z = []
      for item in lrp[i]:
          v =item.vertices
          #print(v)
          rt, zt, pt = geom.cartesiantocylinder(v.x1[:],v.x2[:],v.x3[:])
          R.extend(rt)
          z.extend(zt)
      lpR.append(R)
      lpz.append(z)

  dat = data.data()
  dat.npp = npp
  dat.p = lp
  dat.R = lpR
  dat.z = lpz
  if fn != '':
    dat.save(fn)
  return dat

