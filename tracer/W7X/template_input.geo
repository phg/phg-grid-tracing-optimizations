* input file for EMC3 code (test
*** 1. fein grid mesh representing magnetic coordinates
1                                !zone number
#Nir#  #Nip#  #Nit#
*** 2. non default non transparent, plate surface
*** 2.1 non default surface
* radial
0
* poloidal
* Type of surface=1:  periodic
*                 2:  up/down symmetric
*                 3:  Mapping
2                               !number
0     0     1                   !IP  IZONE  Type of surface
0    #Nir-2#     0    #Nit-2#             !IR1-2, IT1-2
#Nip-1#   0     1                   !IP  IZONE  Type of surface
0    #Nir-2#     0    #Nit-2#             !IR1-2, IT1-2
* toroidal
2                               !number
0     0     3                   !IT  IZONE  IND    
0    #Nir-2#     0    #Nip-2#            !IR1-2, IP1-2
36    0     2                   !IT  IZONE  IND    
0    #Nir-2#     0    #Nip-2#            !IR1-2, IP1-2
*** 2.2 non transparent (Boundary condition must be defined)
* radial
2
1     0     1                   !IR  IZONE  ISIDE 
0    #Nip-2#    0    #Nit-2#             !IP1-2, IT1-2
#Nrplasma#    0    -1                   !IR  IZONE  ISIDE 
0    #Nip-2#    0    #Nit-2#             !IP1-2, IT1-2
* POLOIDAL 
0
* TOROIDAL 
0
*** 2.3 plate surface (Bohm Boundary condition)
* radial
-1
* POLOIDAL 
-1
* TOROIDAL 
-1
*** 3. physical cell       
*<0: user defines cell
* 0: default; 
*>0: read from file   
1   1              
* check cell ?
F      
