sys.path.append('/afs/ipp/u/flr/python')
import make_field as mf; reload(mf)


#cl = mf.get_coils()
cl = mf.load_coils()

#IPP,ICC,ITC = mf.get_config()
#cr = mf.get_currents(IP=IP,ICC=ICC,ITC=ITC)
cr = mf.load_currents()

#wr = mf.make_wires(cr,cl)
#mf.make_field(cr,cl,nr=181,nz=181,nt=481,fn='W7XField-Ref-EIM2.dat')
#mf.make_field(cr,cl,nr=128,nz=128,nt=72,fn='W7XField-EIM2.dat')
#mf.make_field(cr,cl,nr=64,nz=64,nt=36,fn='W7XField-Small-EIM2.dat')
mf.make_field(cr,cl,nr=20,nz=20,nt=36,fn='W7XField-VSmall-EIM2.dat')

