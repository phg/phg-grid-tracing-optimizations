#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 16:34:38 2022

@author: phg
"""
import time
import make_W7X_grid_phg_test as w7xg
pp = True
import numpy as np

# ''' EIM '''
# consts = {
#     # in -> outside cell numbers (sum is redial resolution)
#     "NirC": 30, "NirS": 40, "NirN": 40,
#     # half(-1/2) poloidal and toroidal cell number (per half-module)
#     "Niphalf": 150, "Nit": 36,
#     # poloidal grid spacing and smoothing options implemented by phg
#     "eq_space": True, "zoidpol": False,
#     # proj for first (or all in not eq. space) vessel point(s)
#     "eq_space_vessel": False,  "normal_vessel": True, "normal_smooth": 25,
#     # strating angle in [0,Nit[ and offset list of projection center
#     "trace_index": 35, "center_delta_R": .15, # [0,0,0,0.2,.15],
#     # path to magnetic field file
#     "magnetic_field_file": "./Fields/Field-EIM-FullT-std.dat", # ErrFullT- FullT-
#     "configuration": "EIM", # overwrites more specific changes
#     "module": "full_module", # or "half_module" or "full_torus"
#     # trace from 0 to ta and then rotate instead of the other way around
#     "project_trace_incorrectly": True,
#     # "module_index": 0, # from 0 to 4 for starting position
# }

''' EIM Error '''
consts = {
    # in -> outside cell numbers (sum is redial resolution)
    "NirC": 30, "NirS": 40, "NirN": 40,
    # half(-1/2) poloidal and toroidal cell number (per half-module)
    "Niphalf": 150, "Nit": 36,
    # poloidal grid spacing and smoothing options implemented by phg
    "eq_space": True, "zoidpol": False,
    # proj for first (or all in not eq. space) vessel point(s)
    "eq_space_vessel": False, "normal_vessel": True, "normal_smooth": 50,
    # strating angle in [0,Nit[ and offset list of projection center
    "trace_index": 35, "center_delta_R": .15, # [0,0,0,0.2,.15],
    # path to magnetic field file
    "magnetic_field_file": "./Fields/Field-EIM-ErrFullT-high.dat", # ErrFullT- FullT-
    "configuration": "EIM-ErrFullT", # overwrites more specific changes
    "module": "full_module", # or "half_module" or "full_torus"
    # trace from 0 to ta and then rotate instead of the other way around
    "project_trace_fully": True,
    "module_index": 0, # from 0 to 4 for starting position
}

# reftime = time.strftime('%Y-%m-%dT%H:%M:%S')
# for i in range(5):
#     c = consts.copy()
#     c["module_index"] = i
#     c = w7xg.process_consts_load_Bfield(c, pprint=pp)
#     g, c = w7xg.make_inner_grid(c, pprint=pp)
#     # g = np.load("Grids/EIM-ErrFullT-test1.npz")["data"]  # ErrFullT-
#     g, c = w7xg.make_outer_boundary(g, c, pprint=pp)
#     g = w7xg.make_outer_grid(g, c, pprint=pp)
#     c = w7xg.target_and_EMC3_compliance(g, c, reftime=reftime,
#                                     save_grid_raw=True)
#     print(i)

'''FTM ONE HALF MODULE'''
consts = {
    # in -> outside cell numbers (sum is redial resolution)
    "NirC": 30, "NirS": 40, "NirN": 40,
    # half(-1/2) poloidal and toroidal cell number (per half-module)
    "Niphalf": 150, "Nit": 36,
    # poloidal grid spacing and smoothing options implemented by phg
    "eq_space": True, "zoidpol": False,
    # proj for first (or all in not eq. space) vessel point(s)
    # eq_sp_v + normal_vessel projects first point from normal and others via equal spacing
    "eq_space_vessel": False,  "normal_vessel": True, "normal_smooth": 50,
    # strating angle in [0,Nit[ and offset list of projection center
    "trace_index": 13, "center_delta_R": .15, # [0,0,0,0.2,.15],
    # path to magnetic field file
    "magnetic_field_file": "./Fields/Field-FTM-high.dat", # ErrFullT- FullT-
    #"configuration": "FTM", # overwrites more specific changes
    "module": "half_module", # or "half_module" or "full_torus"
    # trace from 0 to ta and then rotate instead of the other way around
    # "project_trace_incorrectly": False, "project_trace_fully": False,
    "explicit_alpha_plot": True,
    "module_index": 0, # from 0 to 4 for starting position
    "surface_radii": [5.99, 6.09, 6.1751, np.linspace(6.2129, 6.2129001, 10)],
    "Rcax": 5.96957, "tr_acc": 720, 
    "shrink_fit_outer_surface": (30, 0.06, 0.002, 20, False),
}

reftime = time.strftime('%Y-%m-%dT%H:%M:%S')
c = consts.copy()
c["module_index"] = 0
c = w7xg.process_consts_load_Bfield(c, pprint=pp)
g, c = w7xg.make_inner_grid(c, pprint=pp)
# g = np.load("Grids/EIM-ErrFullT-test1.npz")["data"]  # ErrFullT-
g, c = w7xg.make_outer_boundary(g, c, pprint=pp)
g = w7xg.make_outer_grid(g, c, pprint=pp)
c = w7xg.target_and_EMC3_compliance(g, c, reftime=reftime,
                                save_grid_raw=True)
print(i)