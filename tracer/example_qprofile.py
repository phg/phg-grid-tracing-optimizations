#This example works only if install.sh was executed beforehand.

import numpy as np
import matplotlib.pyplot as plt
import sys
import sys
sys.path.append('build/.')
from field_routines import baxisym as bas
from field_routines import trace as tr
from load_magnetics import *
import argparse

#read the command line parameters or define default values
parser=argparse.ArgumentParser()
parser.add_argument("-s","--shot",default=-1,type=int)
parser.add_argument("-t","--time",default=2.0,type=float)
parser.add_argument("-fn","--filename",default='',type=str,nargs='?')
parser.add_argument("-o","--output",default='',type=str,nargs='?')
args=parser.parse_args()


if args.filename!='':
  magxml=read_from_file(args.filename)
elif args.shot<0:
  magxml=read_from_file('AUG/magnetics_AUGD_EQH_29273_04600ms_0.xml')
else:  
  magxml=read_AUG_magnetics(ishot=args.shot,time=args.time)
  #write_to_file(magxml) #activate this line to write the magnetics data to an xml-file (with a default file name)

load_magnetics(magxml)  # passes the magnetic configuration to the fortan modules baxisym,bprecalc, etc...


N=100
tr.set_npol_limit(1.0)

psiNq=np.zeros((N,2))

for i,x in enumerate(np.linspace(0,1,N)):
  p=bas.opoint[1:]+x*(bas.xpoint[1:,0]-bas.opoint[1:])
  fl=tr.field_line([p[0],p[1],0.0],1.0*np.pi/180.,10000,dir=1)
  psiNq[i,0]=bas.psin_at_rz(p)
  psiNq[i,1]=-tr.rzpc[2]/(2*np.pi*tr.npold[0])
    
q95=np.interp(0.95,psiNq[:,0],psiNq[:,1])

fig,ax =plt.subplots()
ax.plot(psiNq[:,0],psiNq[:,1])

ax.set_xlim(0,1.1)
ax.set_ylim(0,q95*1.5)
ax.set_xlabel(r'$\Psi_N$',fontsize=20)
ax.set_ylabel(r'$q$',fontsize=20)
try:
   tit='%s time = %i ms $q_{95}=$%.2f' % (magxml.attrib['discharge'],float(magxml.attrib['time'])*1000,q95)
except:
   tit='unknown discharge'

fig.suptitle(tit,fontsize=22)

if args.output!='':
  fig.savefig(args.output)
  fig.show()
else:
  plt.show()


