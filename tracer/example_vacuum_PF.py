#This example works only if install.sh was executed beforehand.

import numpy as np
import matplotlib.pyplot as plt
import sys
import os
sys.path.append('build/.')
from field_routines import baxisym as bas
from field_routines import trace as tr
from load_magnetics import *
sys.path.append('AUG/.')
from machine_geometry import *
sys.path.append('grid/.')
from curve import *
import argparse
import pickle

parser=argparse.ArgumentParser()
parser.add_argument("-s","--shot",default=-1,type=int)
parser.add_argument("-t","--time",default=30e-3,type=float)
parser.add_argument("-g","--grid",default='0.05,4.0,256,-2.9,2.9,512',type=str)
parser.add_argument("-dA",'--delta_ampere', action='append',default=[])
parser.add_argument("-dAt",'--delta_ampere_turns', action='append',default=[])
parser.add_argument('--all_zero', default=False)
parser.add_argument('--what', default='Bpol',choices=['Bpol','PSI'])
parser.add_argument("-vmin",default=0.0,type=float)
parser.add_argument("-vmax",default=20.0e-3,type=float)
parser.add_argument("-vscale",default=1000.0,type=float,choices=[1.0,1000])
parser.add_argument("-vlabel",default='$B_{pol}$ [mT]',type=str)
parser.add_argument("--fl_RzphidphiN", action='append',default=[])# example: '--fl_RzphidphiN "1.4,1.0,0.0,-360,25"'
parser.add_argument("--machine_geometry",default="AUG/MD_AUG.xml",type=str)
parser.add_argument("--Btheta",default=False,type=bool)
parser.add_argument("--tcurr0",default=-9.5,type=float)
parser.add_argument("--tBthe0",default=-4.0,type=float)
parser.add_argument("--BRzI",default=[],action='append',type=str)
parser.add_argument("--addlabel",default='',type=str)

parser.add_argument("-o","--output",default='',type=str,nargs='?')
args=parser.parse_args()
print args


lims=np.fromstring(args.grid,count=6,dtype=float,sep=',')

print 'grid R: %.1f..%.1f m (%i) z: %.1f..%.1f m (%i)' % tuple(lims)

AUGxml=open_machine(args.machine_geometry)
xml_remove_all(AUGxml,'.//*[@type="RMP"]')

PFcoils={p.attrib['name']:p for p in AUGxml.findall(".//coils/coil[@type='PF']")}


if not os.path.isfile('psiPF.p'):
    print 'precomputing magnetic unit PFields'
    PSIPF={}
    for k,PFxml in PFcoils.iteritems():
        print k
        bas.set_psi(np.zeros((int(lims[2]),int(lims[5]))),lims[0],lims[1],lims[3],lims[4])
        RzI=xml2np(PFxml.find('wires'))
        bas.add_current_wires(RzI)
        PSIPF[k]=bas.psi*1.0
        
    pickle.dump([PSIPF,lims],open('psiPF.p','wb'))
else:
    [PSIPF,limtemp]=pickle.load(open('psiPF.p','rb'))
    if np.any(limtemp!=lims):raise Exception('PFields precomputed on a different grid. Delete psiPF.p to recompute.')

bas.set_psi(np.zeros((int(lims[2]),int(lims[5]))),lims[0],lims[1],lims[3],lims[4])


if args.shot<0:
  magxml=read_from_file('AUG/magnetics_AUGD_EQH_31360_00030ms_0.xml')
else:  
  magxml=read_AUG_magnetics(ishot=args.shot,time=args.time,output='XmlObject',include=['PF','BTF','Btheta'],tcurr0=args.tcurr0,tBthe0=args.tBthe0)
  write_to_file(magxml) #activate this line to write the magnetics data to an xml-file (with a default file name)

load_magnetics(magxml)  # passes the magnetic configuration to the fortan modules baxisym,bprecalc, etc...


IPF={}
bas.set_psi(np.zeros((int(lims[2]),int(lims[5]))),lims[0],lims[1],lims[3],lims[4])
for k in sorted(PFcoils.keys()):
    IPF[k]=curr_old=float(magxml.find('.//*[@name="%s"]' % k).text)
    if args.all_zero:IPF[k]=0.0
    N=float(PFcoils[k].attrib['turns'])
    s='%10s (%3i) %10.2f kA %10.2f MAt' % (k,N,IPF[k]*1e-3,IPF[k]*N*1e-6)
    
    for dA in args.delta_ampere:
        if k+'=' in dA:IPF[k]=float(dA.replace(k+'=',''))
        if k+'+=' in dA:IPF[k]+=float(dA.replace(k+'+=',''))
        
    for dAt in args.delta_ampere_turns:
        if k+'=' in dAt:IPF[k]=float(dAt.replace(k+'=',''))/N
        if k+'+=' in dAt:IPF[k]+=float(dAt.replace(k+'+=',''))/N
        
    if IPF[k]!=curr_old:s+=' new: %10.2f kA %10.2f MAt' % (IPF[k]*1e-3,IPF[k]*N*1e-6)        
    print s
    bas.psi+=PSIPF[k]*IPF[k]



for i,s in enumerate(args.BRzI):
    RzI=np.zeros((1,3))
    RzI[0,:]=np.fromstring(s,dtype=float,sep=',')
    print 'adding current filament',RzI
    bas.add_current_wires(RzI)



fig,ax =plt.subplots(figsize=(12,12))
fig.subplots_adjust(right=0.85,top=0.93,bottom=0.05)

plot_machine(ax,AUGxml.find('components'))
plot_machine(ax,AUGxml.find('coils'))

sing=bas.scan_singularities(0.9,2.5,-1.2,1.2,20,20,10)

ax.scatter(sing[:,1],sing[:,2],marker='x',color='white')

pfcs=xml_get_element(AUGxml,'.//modelling_elements/*[@name="all_pfcs"]/pol_contour')
tr.unset_all_limits()
tr.add_axisym_target_curve(pfcs)

for s in args.fl_RzphidphiN:
    ps=np.fromstring(s,dtype=float,sep=',')
    print 'fieldline: ',ps
    fl=tr.field_line(ps[0:3],ps[3]*np.pi/180,int(ps[4]),substeps=int(abs(ps[3])),dir=1)
    ax.plot(fl[:,0],fl[:,1],color='red',lw=2)

if args.what=='Bpol':
    RR=np.linspace(bas.rmin,bas.rmax,int(bas.nr));Rc=0.5*(RR[1:]+RR[:-1])
    zz=np.linspace(bas.zmin,bas.zmax,int(bas.nz));zc=0.5*(zz[1:]+zz[:-1])
    bpol=bas.bpol_2d(RR,zz)

    cont = ax.pcolorfast(RR,zz,bpol.T*args.vscale,vmin=args.vmin*args.vscale,vmax=args.vmax*args.vscale,zorder=-15)

    b=ax.get_position().bounds
    cbax=fig.add_axes([b[0]+b[2]*1.02,b[1]+0.0*b[3],0.02*b[2],1.0*b[3]])
    cb = plt.colorbar(cont, cax=cbax)	 
    cb.set_label(args.vlabel,fontsize=15)


print 'B_Rz,Do1=%.2f,%.2fT' % tuple(bas.bas_rz([1.460,1.158]))
print 'B_Rz,Do2=%.2f,%.2fT' % tuple(bas.bas_rz([1.645,1.142]))

#print 'B_Rz,Do1=%.2f,%.2fT' % tuple(bas.bas_rz([1.460,1.158+0.05]))
#print 'B_Rz,Do2=%.2f,%.2fT' % tuple(bas.bas_rz([1.645,1.142+0.05]))


ax.set_aspect('equal')
ax.set_xlabel('R [m]')
ax.set_ylabel('z [m]')

try:
   tit='%s @ %i ms%s' % (magxml.attrib['discharge'],float(magxml.attrib['time'])*1000,args.addlabel)
except:
   tit='unknown discharge'

fig.suptitle(tit,fontsize=22)


if args.Btheta:
  plot_machine(ax,AUGxml.find('diagnostics'))

  figB,axB =plt.subplots()
  Bp=[]
  for pxml in AUGxml.findall('diagnostics/Btheta_probes/Btheta_probe'):
    R=float(pxml.attrib['R'])
    z=float(pxml.attrib['z'])
    a=float(pxml.attrib['angle'])
    nam=pxml.attrib['name']
    Brz=bas.bas_rz([R,z])
    Bpol=np.dot(Brz,[np.cos(a),np.sin(a)])
    
    Bmeas=magxml.find('Btheta_probes/Btheta_probe[@name="%s"]' % nam)
    if not Bmeas is None:
    	ip=int(nam.replace('Bthe',''))
        Bp+=[[ip,Bpol,float(Bmeas.text),Brz[0],Brz[1]]]
    
  Bp=np.array(Bp)
  axB.bar(Bp[:,0]-0.1,Bp[:,1]*1000,color='blue',width=0.4,label='calculation')
  axB.bar(Bp[:,0]+0.1,Bp[:,2]*1000,color='red',width=0.4,label='measurement')
  #axB.bar(Bp[:,0]+0.1,Bp[:,4],color='green',width=0.4,label='Bz')
  axB.set_xlabel('# probe')
  axB.set_ylabel(r'$B_\theta$ [mT]')
  axB.set_ylim(-40,100)
  axB.legend(loc='best') 
  figB.suptitle(tit,fontsize=22) 
  



if args.output=='':
    plt.show()
else:
    fig.savefig(args.output)
    fig.show()
    if args.Btheta:
       se=os.path.splitext(args.output)
       figB.savefig(se[0]+'_Btheta'+se[1],fontsize=22) 
       figB.show()
