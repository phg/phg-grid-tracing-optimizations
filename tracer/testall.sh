cd build
./compile.sh
cd ..


mkdir tests
mkdir tests/GEOMETRYSN
mkdir tests/GEOMETRYSF
mkdir tests/GEOMETRYAUGSF

python example_Helmholtz_coils.py -o tests/01_Helmholtz_coils.png
python example_circular_conductor.py > tests/02_example_circular_conductor.txt
python example_simple.py -o tests/03_example_simple.png
python example_fl_tracing.py -o tests/04_example_fl_tracing.png
python example_qprofile.py -o tests/05_example_qprofile.png
rm psiPF.p
python example_vacuum_PF.py --Btheta true -o tests/06_example_vacuum_PF.png
python example_RMP_mode_spectrum.py -o tests/07_example_RMP_mode_spectrum.png
python example_Lc.py -o tests/08_example_Lc.png

#increase the numbers in the arguments to get a better resolution
python example_Lc_RMP.py -refrad 1 -refpol 1  -Nrc 10 -Nrs 15  -Np 200 -o tests/09_example_Lc_RMP.png

cd grid
python make_SN.py -op ../tests/GEOMETRYSN/ -o ../G01_SN.png
python make_SF.py -op ../tests/GEOMETRYSF/ -o ../G02_SF.png
python make_SF_AUG_SOLPS.py -op ../tests/GEOMETRYAUGSF/ -o ../G03_AUGSF.png

cd ..
