import numpy as np
import xml.etree.ElementTree as et
from xml.dom import minidom
import sys
sys.path.append('build/.')

def xml2np(pxml):
    '''
    Interprets the numerical data contained in the xml element text as a numpy array according to the type and dimension meta data stored in the xml attributes.
    '''
    if not ('type' in pxml.attrib.keys()):
        raise Exception('xml-tag must contain numpy-type information.')
    N=np.fromstring(pxml.attrib['dim'],dtype=int,sep=',')
    if np.product(N)>1:
        return np.fromstring(pxml.text,sep=',',dtype=pxml.attrib['type'],count=np.product(N)).reshape(N)
    else:
        return np.fromstring(pxml.text,sep=',',dtype=pxml.attrib['type'],count=1)[0]


def np2xml(pxml,A,fmt=None):
    '''
    Converts a (numerical) numpy array data into the xml element text and stores the meta data in the attributes. 
    The format can be chosen by specifying e.g. fmt='%.5f'. The type is chosen automatically for fmt=None.
    '''
    A=np.array(A);N=A.shape
    if N==():N=[0]
    pxml.attrib['dim']=','.join('%d' % i for i in N)
    pxml.attrib['type']=str(A.dtype)
    if fmt is None:
      if np.issubdtype(A.dtype, np.integer):
        fmt='%d'
      elif np.issubdtype(A.dtype, np.float): 
        fmt='%.8e'
      else:
        raise Exception('Only numerical data types are supported.')     
    pxml.text=', '.join(fmt % i for i in A.reshape(-1))


def load_magnetics(ref,scale_psi=1.0,scale_Bo=1.0,make_rmps=False):
  from field_routines import baxisym as bas 
  from field_routines import bprecalc as bp
  from field_routines import bfield as bf
  
  if isinstance(ref,et.Element):
    magneticsxml=ref
  elif isinstance(ref,str):
    print 'loading magnetics from',ref
    magneticsxml=et.parse(ref).getroot()
  else:
    raise Exception('ref needs to be either a file name (string) or a reference to an xml-object')

  if magneticsxml.attrib['version']=='1.0':
    raise Exception('Old name convention. Rename Coio to CoIo, Coiu to CoIu, PSLu to pslu, PSLo to pslo and version="1.0" to version="1.1" in file %s' % ref)
    
  if magneticsxml.attrib['version']!='1.1':
    raise Exception('This format has not yet been implemented.')


  bas.opoint[:]*=np.nan
  bas.xpoint[:,:]*=np.nan  
    
  eqxml= magneticsxml.find('equilibrium') 
  if eqxml is not None:
     Ri=xml2np(eqxml.find('Ri'))
     zj=xml2np(eqxml.find('zj'))
     PFM=xml2np(eqxml.find('PFM'))


     Nz,NR=PFM.shape 

     if (NR != Ri.shape[0]) or (Nz != zj.shape[0]):
       raise Exception('There is something wrong with the equilibrium')

     bas.set_psi(PFM.T*scale_psi,np.min(Ri),np.max(Ri),np.min(zj),np.max(zj))

     singxml=eqxml.find('singularities')

     if singxml is not None:
       bas.opoint[:]  =xml2np(singxml.find('op'))
       bas.xpoint[:,0]=xml2np(singxml.find('xp'))
       bas.xpoint[:,1]=xml2np(singxml.find('xp2'))
  else:
     bas.set_psi(np.zeros((2,2))*np.nan,0.1,1.0,-1.0,1.0)
  
  
  Bphixml=magneticsxml.find('Bphi')
  
  if Bphixml is not None:
    bas.set_bt(float(Bphixml.attrib['Bo'])*scale_Bo,float(Bphixml.attrib['Ro']))
  else:
    bas.set_bt(np.nan,np.nan)



#  if make_rmps:
#   kk=[k for k in field.keys() if 'Bcoil_' in k]
#   print kk
#   cc=np.zeros((0,4))
#   for k in kk:
#     print field[k].shape
#     cc=np.vstack((cc,field[k]))
#   bp.set_current_wires_xyzi(cc)    

  return magneticsxml    


def make_empty_xml_object(device,experiment,diag,ishot,time,ed):
  return et.Element('magnetics',device=device,discharge='%05d' % ishot, time='%.4f' % time,version='1.1',default_fn='magnetics_%s_%s_%05d_%05dms_%d.xml' % (experiment,diag,ishot,int(time*1000),ed)) 


def read_from_file(fn):
    return et.parse(fn).getroot()

  
def write_to_file(magneticsxml,path='AUG/',fn=None):
    if fn==None:
      fn=path+magneticsxml.attrib['default_fn']
    open(fn,'w').write(minidom.parseString(et.tostring(magneticsxml, 'utf-8')).toprettyxml(indent="  "))
    print fn,' written.'
    return fn


def add_PFM(magxml,ishot,time,diag='EQH',experiment='AUGD',ed=0,diagnostic2=None):
  import dd
  sf = dd.shotfile(diag, ishot,experiment=experiment,edition=ed,diagnostic2=diagnostic2)

  t=sf.getSignal('time')

  it=np.where(t>=time)[0][0]

  if abs(time-t[it])>10e-3:
    raise Exception('No equilibrium found for t=%f, nearest one at t=%f' % (time,t[it]))

  eqxml=et.SubElement(magxml,'equilibrium', time='%.5f' % t[it], diag=diag, edition='%d' % sf.edition, requested_edition='%d' % ed)

  Ri=sf.getObjectData('Ri')[it]
  zj=sf.getObjectData('Zj')[it]
  PFM=sf.getObjectData('PFM')[it]

  

  np2xml(et.SubElement(eqxml,'Ri', unit='m'),Ri,fmt='%.6f')
  np2xml(et.SubElement(eqxml,'zj', unit='m'),zj,fmt='%.6f')
  np2xml(et.SubElement(eqxml,'PFM', unit='m',comment='BR=-1/(2piR)dPFM/dz, Bz=1/(2piR)dPFM/dR'),PFM,fmt='%.8f')
  return eqxml


def add_eq_params(eqxml,ishot,time,dtime=0.005):
  import dd
  sf = dd.shotfile('FPC', ishot,experiment='AUGD')
  t=sf.getObjectData(sf.getTimeBaseInfo('IpiFP').name)
  IpiFP=sf.getSignalCalibrated('IpiFP')[0]
  iit=np.where(abs(t-time)<dtime)[0]
  eqxml.attrib['Ip']=str(np.mean(IpiFP[iit]))
  
  sf = dd.shotfile('GQH', ishot,experiment='AUGD')
  t=sf.getTimeBase(sf.getTimeBaseInfo('betpol').name)
  betapol=sf.getSignal('betpol')
  iit=np.where(abs(t-time)<dtime)[0]
  eqxml.attrib['betapol']=str(np.mean(betapol[iit]))
  
  
  
  
def add_BTF(magxml,ishot,time,diag='MAY',experiment='AUGD',dtime=0.005,ed=-1,tcurr0=-9.5):
  import dd
  try:
    sf = dd.shotfile(diag, ishot,experiment=experiment,edition=ed)
  except:
    sf = dd.shotfile('MAI', ishot,experiment=experiment,edition=ed)
    
  BTF=sf.getSignalCalibrated('BTF')[0]
  tBTF=sf.getTimeBase(sf.getTimeBaseInfo('BTF').name)
  
  iit=np.where(abs(tBTF-time)<dtime)[0]
  iit0=np.where(abs(tBTF-tcurr0)<dtime)[0]
  tcoils=np.mean(tBTF[iit])
  
  Bt=np.mean(BTF[iit])-np.mean(BTF[iit0])
  Bphixml=et.SubElement(magxml,'Bphi', Bo='%.5f' % Bt,Ro='1.65', unit='T',time='%.5f' % tcoils,diag=sf.diagnostic, edition='%d' % sf.edition, requested_edition='%d' % ed)
  
  return Bphixml


def add_Ipol(Bphixml,ishot,time,diag='EQH',experiment='AUGD',ed=0,diagnostic2=None):
  import dd
  sf = dd.shotfile(diag, ishot,experiment=experiment,edition=ed,diagnostic2=diagnostic2)

  t=sf.getSignal('time')

  it=np.where(t>=time)[0][0]

  if abs(time-t[it])>10e-3:
    raise Exception('No equilibrium found for t=%f, nearest one at t=%f' % (time,t[it]))

  PFL=sf.getSignalGroup('PFL')[it,:]
  Jpol=sf.getSignalGroup('Jpol')[it,::2]
  ii=np.where(Jpol!=0.0)
  Jpol=Jpol[ii];PFL=PFL[ii]
  ii=np.argsort(PFL)
  Jpol=Jpol[ii];PFL=PFL[ii]
  mu0=4e-7*np.pi
  BphiR=mu0*Jpol/(2*np.pi)
  np2xml(et.SubElement(Bphixml,'BphiR', comment='PFL,Bphi*R', unit='Tm,Tm',time='%.5f' % t[it],diag=diag),np.vstack((PFL,BphiR)).T)


def add_currents(magxml,ishot,time,experiment='AUGD',diag='MAY',dtime=0.005,ed=-1,include=['PF','TF','RMP'],tcurr0=-9.5):
  import dd
  try:
    sf = dd.shotfile(diag, ishot,experiment=experiment,edition=ed)
  except:
    sf = dd.shotfile('MAI', ishot,experiment=experiment,edition=ed)
    
  t=sf.getTimeBase(sf.getTimeBaseInfo('BTF').name)
  iit=np.where(abs(t-time)<dtime)[0]
  iit0=np.where(abs(t-tcurr0)<dtime)[0]

  tcoils=np.mean(t[iit])

  currentsxml=et.SubElement(magxml,'currents')

  if 'PF' in include:

    namesigs =['V1o','V1u','V2o','V2u','V3o','V3u','CoIo','CoIu']
    
    #namesigs+=[['Coio',[['ICoIo',1.0]]], ['Coiu',[['ICoIu',1.0]]]]
    
    namesigs+=[['OH1',[['IOH',1.0]]], ['OH3o',[['IOH',1.0]]], ['OH3u',[['IOH',1.0]]]]
    
    namesigs+=[['OH2o',[['IOH',1.0],['dIOH2s',1.0]]]]
    
    namesigs+=[['OH2u',[['IOH',1.0],['dIOH2u',1.0],['dIOH2s',1.0]]]]

    namesigs+=[['pslu',[['Ipsluk',1./12.]]], ['pslo',[['Ipslok',1./12.]]]]
    #namesigs+=[['PSLu',[['Ipslun',1./12.]]], ['PSLo',[['Ipslon',1./12.]]]]

    #namesigs+=[['plasma',[['Ipa',1.0]]]]
    
    for namesig in namesigs:
      I=0.0
      if isinstance(namesig,str):
        name=namesig
        signals=[['I'+name,1.0]]
      else:
        name,signals=namesig

      for sn,f in signals:
        #print sn,f
        I+=(np.mean(sf.getSignalCalibrated(sn)[0][iit])-np.mean(sf.getSignalCalibrated(sn)[0][iit0]))*f

      np2xml(et.SubElement(currentsxml,'PFcoil',name=name, unit='A',time='%.5f' % tcoils,diag='MAY'),I)    

    for name in ['Do1','Do2']:
      np2xml(et.SubElement(currentsxml,'PFcoil',name=name, unit='A'),0.0)

    if 'TF' in include:
      np2xml(et.SubElement(currentsxml,'TFcoil',name='TF', unit='A',time='%.5f' % tcoils,diag=sf.diagnostic),np.mean(sf.getSignalCalibrated('ITF')[0][iit]))
  
    if 'RMP' in include:
      sf = dd.shotfile('MAW', ishot,experiment=experiment,edition=-1)

    for j in range(2):
      for i in range(8):
        name='B%s%i' % (['u','l'][j],i+1)
        try:
          tRMP=sf.getTimeBase(sf.getTimeBaseInfo('I'+name).name)
          iit=np.where(abs(tRMP-time)<dtime)[0]
          meantime=np.mean(tRMP[iit])
          I=sf.getSignalCalibrated('I'+name)[0][iit]
        except:
         I=np.nan
         meantime=np.nan
        
    np2xml(et.SubElement(currentsxml,'RMPcoil',name=name, unit='A',time='%.5f' % meantime,diag='MAW'),np.mean(I))


def add_currents_micdu(magxml,ishot,time,experiment='micdu',ed=0,diag='EQB',diagnostic2='EQK'):
  #PFnams=['V1o','V1u','V2o','V2u','V3o','V3u', 'PSLu', 'PSLo', 'Do1',   'Do2', 'dOH2s','dOH2u', 'OH']
  PFtrns=[   93,   93,   86,   86,   28,   28,     12,     12,      4,      4,   2*81,    81,  510] 

  #computed names
  PFnams=['V1o','V1u','V2o','V2u','V3o','V3u', 'pslu', 'pslo', 'Do1',   'Do2', 'OH2o','OH2u', 'OH1']
  
  import dd
  sf = dd.shotfile(diag,ishot,experiment=experiment,edition=ed,diagnostic2=diagnostic2)
  
  tsf=sf.getSignal('time')

  it=np.where(tsf>=time)[0][0]

  if abs(time-tsf[it])>10e-3:raise Exception('No equilibrium found for t=%f, nearest one at t=%f' % (time,tsf[it]))

  CLD=sf.getSignalGroup('CLD')[it]/np.array(PFtrns)
  
  CLD[11]=CLD[12]+CLD[10]+CLD[11] #OH2u=OH+dOH2s+dOH2u
  CLD[10]=CLD[12]+CLD[10]         #OH2o=OH+dOH2s
  
  currentsxml=et.SubElement(magxml,'currents')
  for i in range(13):
    np2xml(et.SubElement(currentsxml,'PFcoil',name=PFnams[i], unit='A',diag=diag),CLD[i])    
  
  for nam in ['CoIo', 'CoIu']:
    np2xml(et.SubElement(currentsxml,'PFcoil',name=nam, unit='A',diag=diag),0.0)    


def search_singularities_LSN(magxml):
    from field_routines import baxisym as bas
    load_magnetics(magxml)
    bas.singularities_lsn()


def search_singularities(magxml,pfcs=None):
    from field_routines import trace as tr
    load_magnetics(magxml)
    if pfcs!=None:
      tr.unset_axisym_targets()
      tr.add_axisym_target_curve(pfcs)
      
    tr.find_confinement()


def add_singularities(eqxml):
    from field_routines import baxisym as bas 

    singxml=et.SubElement(eqxml,'singularities')
    opxml=et.SubElement(singxml,'op');np2xml(opxml,bas.opoint[:]);opxml.attrib['unit']='Tm,m,m'
    xpxml=et.SubElement(singxml,'xp');np2xml(xpxml,bas.xpoint[:,0]);xpxml.attrib['unit']='Tm,m,m'
    xp2xml=et.SubElement(singxml,'xp2');np2xml(xp2xml,bas.xpoint[:,1]);xp2xml.attrib['unit']='Tm,m,m'


def add_Btheta(magxml,ishot,time,experiment='AUGD',diag='MAY',dtime=0.005,tBthe0=-4.0,ed=-1):
  import dd
  try:
    sf = dd.shotfile(diag, ishot,experiment=experiment,edition=ed)
  except:
    sf = dd.shotfile('MAG', ishot,experiment=experiment,edition=ed) 
  
  nams=[nam for nam in sf.getSignalNames() if nam.startswith('Bthe')]

  t=sf.getTimeBase(sf.getTimeBaseInfo(nams[0]).name)
  iit0=np.where(abs(t-tBthe0)<dtime)[0]
  iit=np.where(abs(t-time)<dtime)[0]
  tbthe=np.mean(t[iit])

  Btheta_probesxml=et.SubElement(magxml,'Btheta_probes')

  for nam in nams:
     Btheta=     np.mean(sf.getSignalCalibrated(nam)[0][iit])
     Btheta_offs=np.mean(sf.getSignalCalibrated(nam)[0][iit0])
     np2xml(et.SubElement(Btheta_probesxml,'Btheta_probe',name=nam,unit='T',time='%.5f' % tbthe,diag='MAY'),Btheta-Btheta_offs)    


def read_AUG_magnetics(fn=None,ishot=32383,time=2.85,diag='EQH',experiment='AUGD',ed=0,output=None,diagnostic2=None,dtime=0.005,include=['PFM','PF','TF','BTF','oxp_lsn','RMP','IP_pol','eq_params'],tcurr0=-9.5,tBthe0=-4.0,pfcs=None):
  '''
  writes the data from the AUG shotfile into an XML object or file
  for private shot files from Mike Dunne set for example: diag='EQB',ishot=34632,experiment='micdu', edition=1,  diagnostics2='EQI'
  '''
  
  magxml=make_empty_xml_object('AUG',experiment,diag,ishot,time,ed)
  
  if 'PFM' in include:
      eqxml=add_PFM(magxml,ishot,time,diag=diag,experiment=experiment,ed=ed,diagnostic2=diagnostic2)
      if 'eq_params':add_eq_params(eqxml,ishot,time)

  if 'BTF' in include:
     Bphixml=add_BTF(magxml,ishot=ishot,time=time,dtime=dtime)
     if 'IP_pol' in include:
         add_Ipol(Bphixml,ishot,time,diag=diag,experiment=experiment,ed=ed,diagnostic2=diagnostic2)
  
  if len(set(include).intersection(set(['PF','TF','RMP'])))>0:
    add_currents(magxml,ishot,time,dtime=dtime,include=include,tcurr0=tcurr0)

  if 'PFmicdu' in include:
    add_currents_micdu(magxml,ishot,time,experiment=experiment,diag=diag,ed=ed,diagnostic2=diagnostic2)
    
  if 'oxp_lsn' in include:
    search_singularities_LSN(magxml)
    add_singularities(eqxml)

  if 'oxp' in include:
    #print pfcs    
    search_singularities(magxml,pfcs=pfcs)
    add_singularities(eqxml)


  if 'Btheta' in include:
    add_Btheta(magxml,ishot=ishot,experiment=experiment,time=time,dtime=dtime,tBthe0=tBthe0)


  if (output is None) or (output.lower()=='xmlobject'):
    return magxml
  elif output.lower()=='xmlfile':
    return write_to_file(magxml)
  elif output.endswith('/'):
    return write_to_file(magxml,path=output,fn=None)  
  elif output.endswith('.xml'):
    return write_to_file(magxml,path='',fn=output)  
  else:
    raise Exception("output can only be None, 'XmlFile', 'XmlObject' or end with '.xml'")


'''

def AUG_shotfile_simple(fn=None,path='AUG/',ishot=32383,time=2.85,diag='EQH',experiment='AUGD',exp2=None,ed=0):#old format
#writes the data from the AUG shotfile into an XML object or file

  if fn==None:
    fn='%spsi_%s_%s_%05d_%05dms_%d.txt' % (path,experiment,diag,ishot,int(time*1000),ed)
       
  import dd
  mu0=4e-7*np.pi

  sf = dd.shotfile(diag, ishot,experiment=experiment,edition=ed)

  t=sf.getSignal('time')

  it=np.where(t>=time)[0][0]

  if abs(time-t[it])>10e-3:
    raise Exception('No equilibrium found for t=%f, nearest one at t=%f' % (time,t[it]))
 
  Ri=sf.getObjectData('Ri')[it]
  zj=sf.getObjectData('Zj')[it]
  PFM=sf.getObjectData('PFM')[it].T

  f=open(fn,'w')

  brk=6
       
  f.write('%10d %10d ! Nr Np \n' % (PFM.shape[0],PFM.shape[1]))
  f.write('  %.5f    %.5f    %.5f    %.5f ! Rmin Rmax zmin zmax \n' % (np.min(Ri),np.max(Ri),np.min(zj),np.max(zj))) 
  A=PFM.reshape(-1,order='F')
  N=len(A)
  for i in range(N):
    f.write('%.8e %s'% (A[i],'\n' if (i % brk == (brk-1)) or (i==N-1) else ''))
  
'''


if __name__ == "__main__":
  import argparse

  #read the command line parameters or define default values
  parser=argparse.ArgumentParser()
  parser.add_argument("-s","--shot",default=29273,type=int)
  parser.add_argument("-t","--time",default=4.6,type=float)
  parser.add_argument("-i","--include",default='PFM,PF,TF,BTF,oxp_lsn,RMP,IP_pol',type=str)  
  parser.add_argument("-o","--output",default='AUG/',type=str,nargs='?')
  parser.add_argument("-diag",default='EQH',type=str)
  parser.add_argument("-diag2",default=None,type=str)
  parser.add_argument("-exp",'--experiment',default='AUGD',type=str)
  parser.add_argument("-ed",'--edition',default=0,type=int)
  parser.add_argument('--pfcs',default=None,type=str)
  parser.add_argument("--tolerance",default=None,type=float)  
  args=parser.parse_args()

  if not (args.tolerance is None):
    from field_routines import baxisym as bas 
    bas.set_tolerance(args.tolerance)  

  pfcs=None if args.pfcs is None else np.loadtxt(args.pfcs,skiprows=1)
  
 # ishot=32383,time=2.85,diag='EQH',experiment='AUGD',ed=0,output=None,diagnostic2=None,dtime=0.005,include=['PFM','PF','TF','BTF','oxp_lsn','RMP','IP_pol'],tcurr0=-9.5,tBthe0=-4.0,pfcs=None):
  magxml=read_AUG_magnetics(ishot=args.shot,time=args.time,diag=args.diag,experiment=args.experiment,ed=args.edition,include=args.include.split(','), diagnostic2=args.diag2,output=args.output)
#  write_to_file(magxml,path=args.path)


  #read_AUG_magnetics(ishot=32921,time=3.3,diag='EQH',experiment='AUGD',ed=0,output='XmlFile')
  #read_AUG_magnetics(ishot=29887,time=4.4,diag='EQH',experiment='AUGD',ed=0,output='XmlFile')
  #read_AUG_magnetics(ishot=26081,time=4.0,diag='EQH',experiment='AUGD',ed=0,include=['PFM','BTF','oxp','RMP'],output='XmlFile') 
  #read_AUG_magnetics(ishot=31360,time=30e-3,experiment='AUGD',include=['PF','BTF','Btheta'],tcurr0=-9.5,tBthe0=4.0,output='XmlFile')
  #ref=AUG_shotfile(path='./',ishot=30701,time=3.2,diag='EQH',experiment='AUGD',ed=0)


