#This example works only if ./compile.sh in the build directory was executed beforehand

import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('build/.')
from field_routines import bprecalc as bp
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-o","--output",default='',type=str)
args=parser.parse_args()


#define the two coils with radius rQ, current IQ and distance d
N=678;rQ=0.75;IQ=1e5;d=1.5
c1=np.zeros((N,4))
c1[:,0]=rQ
c1[:,1]=d/2
c1[:,2]=np.linspace(0,2*np.pi,N)
c1[:,3]=IQ
c1[-1,3]=0.0 # if this is not set to 0 the two coils are connected to eachother

c2=c1*1.0
c2[:,1]*=-1
bp.set_current_wires_rzphii(np.vstack((c1,c2)))

if True:#set false to restore the precomputed field (faster)
    bp.prepare_field(0.0,0.2,5,-10,10,256,0,2*np.pi,4,12)
    bp.save_field('Helmholz.dat')
else:
    bp.restore_field('Helmholz.dat')

#plot the field on the axis
NN=200
z=np.linspace(-5,5,NN)
mu0=4*np.pi*1e-7

fig,ax =plt.subplots()

ax.plot(z,IQ*mu0/2*(rQ**2/(rQ**2+(z-d/2)**2)**1.5+rQ**2/(rQ**2+(z+d/2)**2)**1.5),label='analytic formula')

Bz=np.zeros(NN)

for i in range(NN):
    BR,Bz[i],BF=bp.bpre_rzp([0,z[i],0])


ax.plot(z,Bz,ls='--',lw=2,label='Biot-Savart on grid')

for i in range(NN):
    BR,Bz[i],BF=bp.bpre_rzp_direct([0,z[i],0])


ax.plot(z,Bz,ls='--',lw=2,label='Biot-Savart directly')


ax.set_xlabel('$z$ [m]',fontsize=20)
ax.set_ylabel('$B_z (R=0)$ [T]',fontsize=20)
ax.legend(loc='best')
fig.suptitle('Helmholz-coils, $r_Q=%5.2f$ m, $d=%5.2f$ m, $I=%3.1f$ kA' % (rQ,d,IQ/1000),fontsize=20)

if args.output!='':
  fig.savefig(args.output)
  fig.show()
else:
  plt.show()
