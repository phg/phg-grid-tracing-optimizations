
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('build/.')
from field_routines import bprecalc as bp
from field_routines import bfield as bf
from field_routines import baxisym as bas

#conductor position
Rc=2.0;zc=0.0;Ic=1e6

#probe position
Rp=2.0;zp=1.0


print '\n\n\n\nComputing the field analytically according to Feneberg:'
#Computer Physics Communications 31 (1984) 143-148
#This formula is implemented in BAXISYM.f

bas.set_psi(np.zeros((256,256)),Rp-0.5,Rp+0.5,zp-0.5,zp+0.5) 
RzI=np.zeros((1,3))
RzI[0,:]=[Rc,zc,Ic]
bas.add_current_wires(RzI)
Bfen=bas.bas_rz([Rp,zp])
print 'B_Rz=',Bfen,'T'


print 'and now from Biot-Savart integration:'
#a Biot-Savart integrator is implemented in BPRECALC.f

bf.scale_components([0.0,0.0,1.0])#switch-off the Bpol & Bphi components of BAXISYM and switch-on BPRECALC 
#resolution coductor
N=10000

c=np.zeros((N,4))
c[:,0]=Rc
c[:,1]=zc
c[:,2]=np.linspace(0,2*np.pi,N)
c[:,3]=Ic
c[-1,3]=0.0 # end the current

bp.set_current_wires_rzphii(c)


Bbs=bf.b_rzp(np.array([Rp,zp,0]))
print 'B_Rzphi=',Bbs,'T'

print 'ratio: ',Bfen[0]/Bbs[0],Bfen[1]/Bbs[1]
