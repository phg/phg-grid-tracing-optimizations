#This example works only if install.sh was executed beforehand.

import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('build/.')
from field_routines import baxisym as bas
from field_routines import trace as tr
from load_magnetics import *
sys.path.append('AUG/.')
from machine_geometry import *
import argparse
import os

#read the command line parameters or define default values
parser=argparse.ArgumentParser()
parser.add_argument("-fn","--filename",default='',type=str)
parser.add_argument("-s","--shot",default=-1,type=int)
parser.add_argument("-t","--time",default=2.0,type=float)
parser.add_argument("-o","--output",default='',type=str)
args=parser.parse_args()


if args.filename!='':
  magxml=read_from_file(args.filename)
elif args.shot<0:
  magxml=read_from_file('AUG/magnetics_micdu_EQB_34263_40000ms_4.xml')#AUG/magnetics_AUGD_EQH_26081_04000ms_0.xml
else:  
  magxml=read_AUG_magnetics(ishot=args.shot,time=args.time)


load_magnetics(magxml)

fig,ax =plt.subplots(figsize=(8,12))

#load the machine geometry information from the xml file and plot them. 
AUGxml=open_machine('AUG/MD_AUG.xml',state='planning_ud_2017_06')
plot_machine(ax,AUGxml.find('components'))
bas.find_separatrix(400,10,10)#number of poloidal points on separatrix in core, right leg and left leg respectively
ax.plot(bas.sep[:,0],bas.sep[:,1],color='black',lw=3)

pfcs=xml_get_element(AUGxml,'.//modelling_elements/*[@name="all_pfcs"]/pol_contour')

tr.unset_all_limits()
tr.add_axisym_target_curve(pfcs)

bas.find_upstream_pos()

N=1000;lq=3e-3
Lc=np.zeros(N)
dRs=np.linspace(lq*1e-4,lq,N)

for i,dR in enumerate(dRs):
    fl=tr.field_line([bas.rz_u[0]+dR,bas.rz_u[1],0.0],5.0*np.pi/180.,1000,dir=1)
    fl=fl[~np.isnan(fl[:,0]),:]#remove non valid points
    if (i % 100)==1:
        ax.plot(fl[:,0],fl[:,1],color='red') #plot the field line
        ax.scatter([fl[-1,0]],[fl[-1,1]],color='red')#mark the point where the fieldline hits the target
    Lc[i]=tr.lc*1.0 #store the result (the factor 1.0 forces a copy of the result)
    
ax.set_aspect('equal')
ax.set_xlabel('R [m]',fontsize=20)
ax.set_ylabel('z [m]',fontsize=20)
fig.tight_layout()

if args.output!='':
  se=os.path.splitext(args.output)   
  fig.savefig(se[0]+'_Lc2'+se[1])
  fig.show()
  
  
fig,ax =plt.subplots()
ax.plot(dRs*1000,Lc,color='red')
ax.set_xlabel('$R-R_{sep}$ [mm]',fontsize=20)
ax.set_ylabel('$L_c$ [m]',fontsize=20)
ax.set_ylim(0,80)
fig.tight_layout()

if args.output!='':
  se=os.path.splitext(args.output)   
  fig.savefig(se[0]+'_Lc'+se[1])
  fig.show()
else:
  plt.show()

