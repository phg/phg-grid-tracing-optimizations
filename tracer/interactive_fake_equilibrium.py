import sys
from PyQt4 import QtGui,QtCore
from matplotlib.figure import Figure

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar

sys.path.append('build/.')
sys.path.append('AUG/.')
sys.path.append('grid/.')
from field_routines import baxisym as bas
from load_magnetics import *
from field_routines import trace as tr
from machine_geometry import *
from curve import *
import argparse
import datetime


parser=argparse.ArgumentParser()
parser.add_argument("-s","--shot",default=-1,type=int)
parser.add_argument("-t","--time",default=30e-3,type=float)
parser.add_argument("-fn","--filename",default='AUG/magnetics_micdu_EQB_34263_05000ms_10.xml',type=str)
parser.add_argument("-xmin",default=1.0,type=float)
parser.add_argument("-xmax",default=1.9,type=float)
parser.add_argument("-ymin",default=0.7,type=float)
parser.add_argument("-ymax",default=1.3,type=float)
parser.add_argument("-c","--coil",default=[],action='append',type=str)
parser.add_argument("-o","--output",default='',type=str,nargs='?')

parser.add_argument("-i","--include",default='PFM,BTF,PFmicdu,TF,oxp',type=str)  
parser.add_argument("-diag",default='EQH',type=str)
parser.add_argument("-diag2",default=None,type=str)
parser.add_argument("-exp",'--experiment',default='AUGD',type=str)
parser.add_argument("-ed",'--edition',default=0,type=int)
args=parser.parse_args()
if len(args.coil)==0:args.coil=['V1o','Do1','Do2']
print args

print 'example calls:'
print 'python -i interactive_fake_equilibrium.py -s 34263 -t 4.0 -exp micdu -diag EQB -diag2 EQK -ed 10'


geoxml=open_machine(fn='AUG/MD_AUG.xml',state='planning_ud_2018_06')
pfcs=xml_get_element(geoxml,xpath='.//modelling_elements/*[@name="all_pfcs"]/pol_contour')


bas.set_tolerance(1e-8)

if args.shot<0:
  magxml=read_from_file(args.filename)
else:
  magxml=read_AUG_magnetics(ishot=args.shot,time=args.time,diag=args.diag,experiment=args.experiment,ed=args.edition,include=args.include.split(','), diagnostic2=args.diag2,pfcs=pfcs)


limcoil={'Do1':[-60.e3/4,0.0,50.e3/4],'Do2':[0.0,60.e3/4,50.e3/4],'V1o':[0.0,30.e3,23.0e3],'V2o':[-30.0e3,0.0,24.e3]}

class Window(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

	
        #exitAction = QtGui.QAction(QtGui.QIcon(), '&Exit', self)        
        #exitAction.setShortcut('Ctrl+Q')
        #exitAction.setStatusTip('Exit application')
        #exitAction.triggered.connect(QtGui.qApp.quit)
	#self.statusBar()
        #menubar = self.menuBar()
        #fileMenu = menubar.addMenu('&File')
        #fileMenu.addAction(exitAction)



        # set the layout
        layout = QtGui.QVBoxLayout()

        # a figure instance to plot on
        self.figure = Figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)
        self.canvas.setSizePolicy(QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Expanding)
        layout.addWidget(self.canvas)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)
        layout.addWidget(self.toolbar)

        #define buttons
        hbox= QtGui.QHBoxLayout()
        self.xpbtn = QtGui.QPushButton('search X-points')
        self.xpbtn.clicked.connect(self.search_xp)
        hbox.addWidget(self.xpbtn)

        self.resetbtn = QtGui.QPushButton('reset')
        self.resetbtn.clicked.connect(self.reset)
        hbox.addWidget(self.resetbtn)
        
        self.savebtn = QtGui.QPushButton('save')
        self.savebtn.clicked.connect(self.save)
        hbox.addWidget(self.savebtn)
        
        layout.addLayout(hbox)
        
        self.cnames=args.coil
        
        self.slider={};self.sval={}
        for nam in self.cnames:
            self.sval[nam] = QtGui.QLabel(nam)
            self.sval[nam].color ='Black'
            layout.addWidget(self.sval[nam])
            sl=self.slider[nam] = QtGui.QSlider(QtCore.Qt.Horizontal, self)
            sl.setMinimum(limcoil[nam][0])
            sl.setMaximum(limcoil[nam][1])
            self.connect(sl,  QtCore.SIGNAL('valueChanged(int)'),self.plot)
            layout.addWidget(sl)
        
        self.loaded=False
        self.resize(1200,1000)
        self.setLayout(layout)
        self.initplot()
        self.load()
        self.plot()


    def updatelabels(self):
        for nam in self.cnames:
            self.sval[nam].color='Black'
            if abs(self.slider[nam].value())>limcoil[nam][2]:self.sval[nam].color='Red'

        if ('Do1' in self.slider.keys()) and ('Do2' in self.slider.keys()):
            diffDo=self.slider['Do1'].value()+self.slider['Do2'].value()        
            if np.abs(diffDo)>12e3/4.:self.sval['Do1'].color=self.sval['Do2'].color='Orange'
                
        for nam in self.cnames:
            self.sval[nam].setText("<font color='%s'>%s: %.3fkA</font>" % (self.sval[nam].color,nam,self.slider[nam].value()/1000.))
        
        
    def reset(self):
        for nam in self.cnames:
            self.slider[nam].setValue(self.currents[nam])
            
        self.initplot()
        self.plot()


    def load(self):
        self.geoxml=geoxml
        eq=self.eq=load_magnetics(magxml)
        #bas.remesh_psi(bas.rmin,bas.rmax,bas.nr*3,bas.zmin,bas.zmax,bas.nz*3)
        self.wires=wires={}
        self.turns=turns={}
        self.currents=currents={}
        
        bas.find_upstream_pos()
        self.psicoils={'eq':bas.psi*1.0}
        bas.psi*=0.0
        
        for nam in self.cnames:
            wires[nam]=xml_get_element(geoxml,".//coils/coil[@name='%s']/wires" % nam)
            turns[nam]=int(geoxml.find(".//coils/coil[@name='%s']" % nam).attrib['turns'])
            currents[nam]=float(eq.find('.//currents/PFcoil[@name="%s"]' % nam).text)
            #print nam,currents[nam]
            self.slider[nam].setValue(currents[nam])
            self.slider[nam].setSingleStep(10.)
            self.slider[nam].setPageStep(100.)
            bas.psi*=0.0
            bas.add_current_wires(wires[nam])  
            self.psicoils[nam]=bas.psi*1.0
        
        
        self.R=np.linspace(bas.rmin,bas.rmax,bas.nr)
        self.z=np.linspace(bas.zmin,bas.zmax,bas.nz)
        Psi2lq=bas.dpsinu_dru*2*2.7e-3+1
        self.ff=-(np.linspace(Psi2lq,1.0,20))
	if (0.95<bas.psin_x2<1.1):
		self.ff=np.sort(np.hstack((self.ff,-bas.psin_x2)))
		
	
        self.all_pfcs=pfcs*1.0
        self.all_pfcs_ext=self.all_pfcs*1.0
        
        self.all_pfcs_ext[53:55,1]+=0.035
        
        self.plate=self.all_pfcs[53:55,:]
        self.Lpol=np.nan
	self.dx2=np.nan
        #tr.fc_iterations=20
        self.loaded=True


    def search_xp(self):
        #print bas.xpoint[1:,0]
        #print bas.xpoint[1:,1]
        tr.unset_all_limits()
        tr.set_npol_limit(2)
        tr.add_axisym_target_curve(self.all_pfcs_ext)    
        tr.find_confinement([1.6,0.0],[2.5,0.0])
        #print bas.xpoint[1:,0]
        #print bas.xpoint[1:,1]
        self.plot()


    def initplot(self):
        ax = self.figure.add_subplot(111)
        
        # discards the old graph
        ax.clear()

        ax.set_xlim(args.xmin,args.xmax)
        ax.set_ylim(args.ymin,args.ymax)

        ax.set_aspect('equal')


    def plot(self):
        if not self.loaded:return
        self.updatelabels()
        # create an axis

        xl=self.figure.gca().get_xlim()
        yl=self.figure.gca().get_ylim()

        ax = self.figure.add_subplot(111)
        
        # discards the old graph
        ax.clear()
        
        plot_machine(ax,self.geoxml.find('components'))
        
        
        bas.psi[:,:]=self.psicoils['eq'][:,:]
        #print self.psicoils['eq'][0,0]
        for nam in self.cnames:
          bas.psi[:,:]+=self.psicoils[nam][:,:]*(self.slider[nam].value()-self.currents[nam])
        
        if not bas.readjust_oxp(): 
            print 'scanning O-and X-points.'
            tr.find_confinement()

        bas.find_upstream_pos()
        
        for n,x in [['x1',bas.xpoint[:,0]],['x2',bas.xpoint[:,1]]]:
           ax.scatter([x[1]],[x[2]],color='red',marker='x')
        
        tit=r'$r_{u,x2}=%.2f$ mm' % (bas.ru_x2*1000)
        
        if True and not np.isnan(bas.xpoint[0,0]):
            tr.unset_all_limits()  
            tr.add_axisym_target_curve(self.all_pfcs)
            fl=tr.field_line([bas.xpoint[1,0]+1.e-3,bas.xpoint[2,0]+1.e-3,0],5*np.pi/180.,10000,dir=1)
            ax.plot(fl[:,0],fl[:,1],lw=5,color='red')
	    self.Lpol=curve(fl[:,0:2],remove_Nan=True).totl
            tit+=', $L_{pol}=%.1f$ cm' % (self.Lpol*100)

        


        if True and not np.isnan(bas.xpoint[0,1]):
            a,b=self.plate
            x=bas.xpoint[1:,1]
            self.dx2=-np.cross(x-a,(b-a)/np.linalg.norm(b-a))
            #print a,b,x,d
            ax.plot(self.plate[:,0],self.plate[:,1])
            tit+=', $d_\perp=%.1f$ mm' % (self.dx2*1000) 
        
	ax.plot(self.all_pfcs_ext[:,0],self.all_pfcs_ext[:,1],color='red')

	ff=np.linspace(-bas.dpsinu_dru*2.7e-3,0.0,10)+1.0
	ax.contour(self.R,self.z,(bas.psi.T-bas.opoint[0])/(bas.xpoint[0,0]-bas.opoint[0]),ff,colors='grey',zorder=0)
	


        Psi2lq=bas.dpsinu_dru*2*2.7e-3+1
        self.ff=-(np.linspace(Psi2lq,1.0,20))
	#if (0.95<bas.psin_x2<1.1):
		#self.ff=np.sort(np.hstack((self.ff,-bas.psin_x2)))

        cplt=ax.contour(self.R,self.z,-(bas.psi.T-bas.opoint[0])/(bas.xpoint[0,0]-bas.opoint[0]),self.ff,cmap=plt.cm.jet,zorder=0)


        
        ax.set_title(tit,fontsize=23)
        ax.set_xlim(xl)
        ax.set_ylim(yl)        
        # refresh canvas
        self.canvas.draw()
        

    def save(self):
	s='new equilibrium '+datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+'\n'
	for param in ['shot','time','filename','diag','diag2','experiment','edition']:
	  s+=' %s=%s\n' % (param,getattr(args,param))
	
	s+=' ru_x2=%.1f mm\n' % (bas.ru_x2*1000)
	s+=' Lpol=%.1f cm\n' % (self.Lpol*100)
	s+=' dperp=%.1f mm\n' % (self.dx2*1000)
	
	
	s+='coil  original [A]      new [A]     diff [A]   original [At]     new [At]    diff [At]\n' 
	for nam in self.cnames:
	   s+=' %s: %12.3e %12.3e %12.3e' % (nam,self.currents[nam],self.slider[nam].value(),self.slider[nam].value()-self.currents[nam])
	   s+='    %12.3e %12.3e %12.3e\n' % (self.currents[nam]*self.turns[nam],self.slider[nam].value()*self.turns[nam],(self.slider[nam].value()-self.currents[nam])*self.turns[nam])
	
	s+='\n\n\n'
	#open('log_interactive_fake_equilibrium.txt','a').write(s)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())
