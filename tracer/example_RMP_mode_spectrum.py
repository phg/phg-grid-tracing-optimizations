import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage

import sys
flr_path='./'   #git@gitlab.mpcdf.mpg.de:tal/f2py_field_routines.git : 63914d45859135efa8bbe902c463ee17ce9862ba 
sys.path.append(flr_path+'build/')
from field_routines import baxisym as bas
from field_routines import bprecalc as bp
from field_routines import bfield as bf
from field_routines import trace as tr

sys.path.append(flr_path+'AUG/')
from machine_geometry import * 
import time

sys.path.append(flr_path)
from load_magnetics import *
import argparse
import os

#read the command line parameters or define default values
parser=argparse.ArgumentParser()
parser.add_argument("-s","--shot",default=-1,type=int)
parser.add_argument("-t","--time",default=2.0,type=float)
parser.add_argument("-o","--output",default='',type=str,nargs='?')
args=parser.parse_args()


if args.shot<0:
  #Matthias Willensdorfer Plasma Phys. Control. Fusion 58 (2016) 114004
  #Fig.11d where the amplitude is multiplied by 2 to account for positive and negative m
  magxml=read_from_file('AUG/magnetics_AUGD_EQH_30839_03200ms_0.xml')
else:  
  magxml=read_AUG_magnetics(ishot=args.shot,time=args.time)
  #write_to_file(magxml) #activate this line to write the magnetics data to an xml-file (with a default file name)


load_magnetics(magxml)  # passes the magnetic configuration to the fortan modules baxisym,bprecalc, etc...


Nq=100 # resolution of the q-profile
Np=512;Nt=256 # poloidal and toroidal resolutions (needs to be divisible by 2)
qcol=[[-5.,'red']] #q-values of the surfaces on which the spectrum is computed
nRMP=2 #plot the spectrum for this n-value

AUGxml=open_machine(fn=flr_path+'AUG/MD_AUG.xml')

RMPs=xml_get_elements(AUGxml,".//coils/coil[@type='RMP']/wires")

wires=np.zeros((0,4))

for RMPcurr in magxml.find('currents').findall('RMPcoil'):
  nam=RMPcurr.attrib['name']
  IRMP=xml2np(RMPcurr)
  print nam,'%1f A' % IRMP
  if ~np.isnan(IRMP):
    w=RMPs[nam]*1.0;w[:,3]*=IRMP
    wires=np.vstack((wires,w))


bp.set_current_wires_xyzi(wires)

fn='RMP_field_%s_%ims.dat' % (magxml.attrib['discharge'],int(float(magxml.attrib['time'])*1000))

if True:#set false to restore the precomputed field (faster)
  #precompute the magnetic field and its derivatives (i.e.12 components in total) on an equidistant R,z,phi grid 
  bp.prepare_field(1.0,2.25,64, -1.3,1.3,64, 0.0,2*np.pi,128, 12)  
  bp.save_field(fn) # and store it in a file
else:
  bp.restore_field(fn)


bas.find_upstream_pos() #determines the R,z upstream position of the separatrix (usally located near the outboard midplane)

tr.set_npol_limit(1.0)  #follow fieldlines for only one poloidal turn

qprof=np.zeros((Nq,5))  

rr=qprof[:,0]=np.linspace(0.01,1,Nq)

for i,r in enumerate(rr): #compute q-profile
  qprof[i,1:3]=p=bas.opoint[1:]+r*(bas.xpoint[1:,0]-bas.opoint[1:])
  fl=tr.field_line([p[0],p[1],0.0],1.0*np.pi/180.,10000,dir=1)
  qprof[i,3]=bas.psin_at_rz(p)
  qprof[i,4]=-tr.rzpc[2]/(2*np.pi*tr.npold[0])


q95=np.interp(0.95,qprof[:,3],qprof[:,4]) 

   
figq,axq =plt.subplots() # plot for the q-profile
figg,axg =plt.subplots(figsize=(8,12)) # plot for the equilibrium

axq.plot(qprof[:,3],qprof[:,4])
axq.set_xlim(0,1.1)
axq.set_ylim(0,q95*1.5)
axq.set_xlabel(r'$\Psi_N$',fontsize=20)
axq.set_ylabel(r'$q$',fontsize=20)
try:
  tit='%s @ %.1f s %s ed=%s $q_{95}=$%.2f' % (magxml.attrib['discharge'],float(magxml.attrib['time']),magxml.find('equilibrium').attrib['diag'],magxml.find('equilibrium').attrib['edition'],q95)
except:
  tit='unknown equilibrium'
  
axq.set_title(tit,fontsize=24)

plot_machine(axg,AUGxml.find('components'))

RR=np.linspace(bas.rmin,bas.rmax,bas.nr)
zz=np.linspace(bas.zmin,bas.zmax,bas.nz)
psiN=(bas.psi.T-bas.opoint[0])/(bas.xpoint[0,0]-bas.opoint[0])
  
ff=list(np.linspace(0.01,1.0,10))+[1.04]#[bas.psin_x2]

axg.contour(RR,zz,psiN,ff,colors=['black']*10+['blue'],zorder=-1)

for q,col in qcol:
    psiq=np.interp(np.abs(q),np.abs(qprof[:,4]),qprof[:,3])
    axq.plot([0,psiq,psiq],[q,q,0],color=col)
    axg.contour(RR,zz,psiN,[psiq],colors=[col],lws=[2],zorder=-1)
    
    PF=psiq*(bas.xpoint[0,0]-bas.opoint[0])+bas.opoint[0]

    p=bas.ray_flxsf_intersection(bas.opoint[1:]+np.array([0.1,0]),[0.1,0.0],PF)

    Rz=tr.field_line([p[0],p[1],0.0],2.0*np.pi*abs(q)/float(Np),Np,dir=0)[:-1,0:2]
    axg.scatter(Rz[:,0],Rz[:,1],color=col,marker='o')

thetas=np.linspace(-np.pi,np.pi,Np)

axg.set_aspect('equal')

RzpSF=np.zeros((Np,Nt,3))

for it in range(Nt):
  RzpSF[:,it,0:2]=Rz
  RzpSF[:,it,2]=2*np.pi*it/(Nt-1.0)-np.pi

  
Bpol=bf.b_on_sf(RzpSF)[:,:,0:2];lBpol=np.linalg.norm(Bpol,axis=2)
ur=np.zeros((Np,Nt,2))
ur[:,:,0]=-Bpol[:,:,1]/lBpol
ur[:,:,1]= Bpol[:,:,0]/lBpol

bf.scale_components([0,0,1.0])

Bp=bf.b_on_sf(RzpSF)[:,:,0:2]

Br=Bp[:,:,0]*ur[:,:,0] + Bp[:,:,1]*ur[:,:,1] 
BrFFT=np.fft.fft2(Br)/(Np*Nt)

Nth=Nt/2;Nph=Np/2;
nn=np.hstack((np.arange(Nth),-np.arange(Nth)[::-1]-1))
ii=np.argsort(nn);nn=nn[ii];BrFFT=BrFFT[:,ii]

mm=np.hstack((np.arange(Nph),-np.arange(Nph)[::-1]-1))
ii=np.argsort(mm);mm=mm[ii];BrFFT=BrFFT[ii,:]



figs,axs=plt.subplots()
figs.subplots_adjust(right=0.85)

figf,axf=plt.subplots()
figf.subplots_adjust(right=0.85)

figm,axm=plt.subplots()
figm.subplots_adjust(left=0.15,bottom=0.15,right=0.98,top=0.98)


paxs=axs.pcolorfast(RzpSF[0,:,2],thetas,Br[:-1,:-1]*1000,vmin=-12e0,vmax=12e0,cmap='seismic')
xphi=np.linspace(-np.pi,np.pi,1000)
#axs.plot(xphi,xphi/(-q))


axs.set_xlabel(r'$\phi$ [rad]',fontsize=22)
axs.set_ylabel(r'$\theta^*$ [rad]',fontsize=22)

axf.set_xlabel('n',fontsize=22)
axf.set_ylabel('m',fontsize=22)

b=axs.get_position().bounds
cbax=figs.add_axes([b[0]+b[2]*1.02,b[1]+0.0*b[3],0.03*b[2],1.0*b[3]])
cb = plt.colorbar(paxs, cax=cbax)	 
cb.set_label('$B_r$ [mT]',fontsize=15)

paxf=axf.contourf(nn,mm,np.abs(BrFFT)*1e6,np.linspace(0,250,256),vmin=0,vmax=250)
x=np.linspace(-100,0,100)
axf.plot(x,x*q,color='white')

axf.set_xlim(-16,16)
axf.set_ylim(0,25)

i_nm2=np.where(nn==-nRMP)[0][0]
i_np2=np.where(nn==+nRMP)[0][0]

axm.bar(mm-0.25,(np.abs(BrFFT[:,i_nm2])+np.abs(BrFFT[:,i_np2]))*1e3,color='blue',width=0.5)

axm.set_xlabel('poloidal mode number m',fontsize=22)
axm.set_ylabel('$|B_r|$ [mT]',fontsize=22)
axm.set_xlim(-18,18)
axm.set_ylim(0,0.45)


b=axf.get_position().bounds
cbax=figf.add_axes([b[0]+b[2]*1.02,b[1]+0.0*b[3],0.03*b[2],1.0*b[3]])
cb = plt.colorbar(paxf, cax=cbax)	 
cb.set_label(r'$B_r$ [$\mu$T] (amplitude)',fontsize=15)


figg.tight_layout()
figg.show()
figq.show()
figs.show()
figf.show()
figm.show()

if args.output!='':

  se=os.path.splitext(args.output)
    
  figg.savefig(se[0]+'_eq'+se[1])
  figq.savefig(se[0]+'_q'+se[1])
  figs.savefig(se[0]+'_Bpert'+se[1])
  figf.savefig(se[0]+'_spec'+se[1])
  figm.savefig(se[0]+'_mode'+se[1])
