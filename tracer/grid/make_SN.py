import numpy as np
import matplotlib.pyplot as plt
import time

import sys
flr_path='../.'
sys.path.append(flr_path+'build/')
sys.path.append(flr_path+'grid/')
from grid import *
from make_inputfiles_def import *
from field_routines import baxisym as bas

mg_path='../AUG/'
sys.path.append(mg_path)
from machine_geometry import *# open_machine,get_elements,write_EMC3_target_file  
from make_emc3_geometry import *
import os
import argparse

#read the command line parameters or define default values
parser=argparse.ArgumentParser()
parser.add_argument("-o","--output",default='',type=str)
parser.add_argument("-op","--outputpath",default='GEOMETRY/',type=str)
args=parser.parse_args()


g=grid()#,Nithalf=1Npcore=399


AUGxml=open_machine(fn=mg_path+'MD_AUG.xml',ishot=32921,state='actual_state')

# get the geometry with the ICRH limiter frame shifted to the center of S1:
all_pfcs,vessel,gfcs,targets3D=make_EMC3_geometry(AUGxml,phi0=0.0,phi1=22.5,gridpath=args.outputpath,t3Dselect={'icrh_limiter':11.25}) 

fig,ax =plt.subplots(figsize=(10,12))

def onclick(event):print event.xdata,',',event.ydata#event.x,event.y,

fig.canvas.mpl_connect('button_press_event', onclick)

ax.set_aspect('equal')


#comment out the following line to switch off graphical output during the generation process and speed up significantly
#g.figax=[fig,ax]

#comment in the following line to enter a kind of debugging mode
#g.step_mode=True;g.Nskipsteps=0

bas.set_tolerance(1e-8)   #maximum tolerated flux inaccuracy for the X-point search

g.load_magnetics('../AUG/magnetics_AUGD_EQH_32921_03300ms_0.xml')

# bend open the divertor to avoid problems during the farsol grid construction
g.pfcs=curve(all_pfcs)
g.pfcs.rotate(20.*np.pi/180.,i1=7,i2=0) 
g.pfcs.rotate(-30.*np.pi/180.,i1=17,i2=27)

g.xpcurvecpar=[[0.7,0.0]]
g.xpcurve1par=[[0.04,0.0],[0.4,10.0]]
g.xpcurve2par=[[0.04,0],[0.04,-10],[0.025,-10.0],[0.6,0.0]]
g.target_range[1]=[-0.07,0.3,'dsposflx','red']
g.target_range[2]=[-0.1,0.3,'dsposflx','blue']

ax.plot(all_pfcs[:,0],all_pfcs[:,1])

g.prepare()


#PsiNcore=np.hstack((0.7,0.9+0.1*np.linspace(0.0,1.0,15)**0.2))
PsiNcore=np.hstack((0.7,0.9+0.1*np.linspace(0.0,1.0,19)**np.linspace(0.5,0.1,19)))

g.make_zone('CORE',PsiN=PsiNcore[:-1],color='red')
PsiNsol=1.0+0.1236*np.linspace(0.0,1.0,30)[1:]**2
g.make_zone('SOL',PsiN=PsiNsol,color='blue')
g.make_zone('PFR',dr=np.linspace(0,0.15,15)[1:],color='green')

g.wall=curve(vessel)
g.farsolextension(p2=[1.55,-1.03],llist=0.2*np.linspace(0,1,15)[1:])#[0.025,0.05,0.075,0.1,0.125,0.15,0.2]


#g.figax=[fig,ax]
for k in g.zone.keys():g.check_zone(k,lw=5,color='black')


for k,t2D in gfcs.iteritems(): ax.fill(t2D[:,0],t2D[:,1],color='#ff8888',zorder=-15)

cols=['red','blue','green']

for k,t3D in targets3D.iteritems(): 
  for i in range(3):  
    ax.plot(t3D[:,i,0],t3D[:,i,1],color=cols[i],zorder=-15)

#for t in triangles:
    #ax.fill(t[0:3,0],t[0:3,1],color='#ff8888',zorder=-15)

#g.figax=[fig,ax]
#for k in g.zone.keys():g.plotzn(k)

g.figax=[fig,ax]


g.make_3Dgrid(outputpath=args.outputpath)
g.plot3D(it=5)

set_input_file_parameters(g)
write_input_files(g,outputpath=args.outputpath)



#start=time.time()
#g.compute_target_intersection_triangles(triangles,outputpath='GRID/')
#print 'time for compute_target_intersection_triangles',time.time()-start

start=time.time()
g.compute_2Dtarget_intersection(gfcs)
g.compute_3Dtarget_intersection(targets3D)
for nam in ['SOL','PFR']:g.plot_targecells(nam=nam,it=5)
g.writeout_intersection(outputpath=args.outputpath)
print 'time for compute_target_intersection',time.time()-start

if args.output!='':
  se=os.path.splitext(args.outputpath+'/'+args.output)   
  fig.savefig(se[0]+'_grid'+se[1])
  
fig.show()



if True:# plot a toroidal projection of the limiter intersection
    Rzp=g.zone['SOL'].Rzp[0]
    fig,ax=plt.subplots()
    ip=np.argmax(Rzp[0,:,5,0])

    for ir in range(Rzp.shape[0]):ax.plot(Rzp[ir,ip,:,0],Rzp[ir,ip,:,2],color='black')

    for it in range(Rzp.shape[2]):ax.plot(Rzp[:,ip,it,0],Rzp[:,ip,it,2],color='black')

    hit=g.zone['SOL'].targetcells[0]

    iir,iit=np.where(hit[:,ip,:]==1)

    for ir,it in zip(iir,iit):
        x=[Rzp[ir,ip,it,0],Rzp[ir,ip,it+1,0],Rzp[ir+1,ip,it+1,0],Rzp[ir+1,ip,it,0],Rzp[ir,ip,it,0]]
        y=[Rzp[ir,ip,it,2],Rzp[ir,ip,it+1,2],Rzp[ir+1,ip,it+1,2],Rzp[ir+1,ip,it,2],Rzp[ir,ip,it,2]]
        ax.fill(x,y,color='blue')

    zlim=Rzp[35,ip,5,1]

    t3D=targets3D['icrh_limiter']

    lim=np.zeros((t3D.shape[1],2))
    for it in range(t3D.shape[1]):
        lim[it,0]=np.interp(zlim,t3D[1:-1,it,1],t3D[1:-1,it,0])
        lim[it,1]=np.interp(zlim,t3D[1:-1,it,2],t3D[1:-1,it,2])

    ax.plot(lim[:,0],lim[:,1],color='red')
    if args.output!='':
      se=os.path.splitext(args.outputpath+'/'+args.output)   
      fig.savefig(se[0]+'_intersection'+se[1])
  

    fig.show()






