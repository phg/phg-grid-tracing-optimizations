
def set_input_file_parameters(grid):
	#grid.core_bnd=1
	grid.perform_fluxtest='T'
	grid.inner_b_eirene=-3;grid.outer_b_eirene=-2
	grid.NSSIDE=-1
	grid.defaultplasma_isb=[[2.0E+14,200.,200.,0.000]]#ne Te Ti M
	grid.defaultplasma_osb=[1.0E+07,0.1,0.1,0.000]#ne Te Ti M


def write_input_files(grid,outputpath='output/'):
	#input.geo
	s1='';s21r='';s21p='';s21t='';s22r='';s22p=''
	N1=0;N21r=0;N21p=0;N21t=0;N22r=0;N22p=0
	
	
	for isegm in range(grid.Nsegm):
		for nam in grid.zonelist:
			Nir,Nip,Nit,_=grid.zone[nam].Rzp[isegm].shape
			
			s1+='%4d %4d %4d  \n' %  (Nir,Nip,Nit)
			
			s21r+='%4d %4d %4d       !IR  IZONE  Type of surface\n' % (Nir-1 if nam=='CORE' else 0,N1,3)
			s21r+='%4d %4d %4d %4d  !IP1-2, IT1-2\n' % (0,Nip-2,0,Nit-2)

			s21t+='%4d %4d %4d       !IT  IZONE  Type of surface\n' %  (0,N1,3)
			s21t+='%4d %4d %4d %4d  !IR1-2, IP1-2\n' % (0,Nir-2,0,Nip-2)
			s21t+='%4d %4d %4d       !IT  IZONE  Type of surface\n' %  (Nit-1,N1,3)
			s21t+='%4d %4d %4d %4d  !IR1-2, IP1-2\n' % (0,Nir-2,0,Nip-2)

			s22r+='%4d %4d %4d       !IR  IZONE  ISIDE\n' % (len(grid.defaultplasma_isb) if nam=='CORE' else Nir-grid.Nn-1,N1,1 if nam=='CORE' else -1)
			s22r+='%4d %4d %4d %4d  !IP1-2, IT1-2\n' % (0,Nip-2,0,Nit-2)
			
			if nam=='CORE':
				s21p+='%4d %4d %4d       !IP  IZONE  Type of surface\n' %  (0,N1,1)
				s21p+='%4d %4d %4d %4d  !IR1-2, IT1-2\n' % (0,Nir-2,0,Nit-2)
				s21p+='%4d %4d %4d       !IP  IZONE  Type of surface\n' %  (Nip-1,N1,1)
				s21p+='%4d %4d %4d %4d  !IR1-2, IT1-2\n' % (0,Nir-2,0,Nit-2)
				N21p+=2
			elif nam=='SOL':
				s22p+='%4d %4d %4d       !IP  IZONE  ISIDE\n' %  (grid.Nn,N1,1)
				s22p+='%4d %4d %4d %4d  !IR1-2, IT1-2\n' % (0,Nir-2,0,Nit-2)
				s22p+='%4d %4d %4d       !IP  IZONE  ISIDE\n' %  (Nip-1-grid.Nn,N1,-1)
				s22p+='%4d %4d %4d %4d  !IR1-2, IT1-2\n' % (0,Nir-2,0,Nit-2)
				N22p+=2
			elif nam=='PFR':
				s22p+='%4d %4d %4d       !IP  IZONE  ISIDE\n' %  (grid.Nn,N1,1)
				s22p+='%4d %4d %4d %4d  !IR1-2, IT1-2\n' % (0,Nir-2,0,Nit-2)
				s22p+='%4d %4d %4d       !IP  IZONE  ISIDE\n' %  (Nip-1-grid.Nn,N1,-1)
				s22p+='%4d %4d %4d %4d  !IR1-2, IT1-2\n' % (0,Nir-2,0,Nit-2)
				N22p+=2
			
			N1+=1;N21r+=1;N21t+=2;N22r+=1		
	
	s= '* input file for EMC3\n*** 1. grid mesh\n  %5d  !Number of definitions following\n%s' % (N1,s1)
	
	s+='*** 2. surface definitions\n'
	s+='* Type of surface:\n* 1:  periodic\n* 2:  up/down symmetric\n* 3:  Mapping\n'
	s+='*** 2.1 interfaces\n'
	s+='* radial\n* glueing the zones radially together\n %5d\n%s' % (N21r,s21r)
	s+='* poloidal\n* definition of poloidal periodicity of the core grid\n %5d\n%s' % (N21p,s21p)
	s+='* toroidal\n* glueing the zones toroidally together\n %5d\n%s' % (N21t,s21t)
	
	s+='*** 2.2 boundaries (boundary conditions need to be defined)\n'
	s+='* radial\n %5d\n%s' % (N22r,s22r)
	s+='* poloidal\n %5d\n%s' % (N22p,s22p)
	s+='* poloidal\n0\n'
	
	s+='*** 2.3 plate surface (Bohm boundary condition)\n'
	s+='* radial\n-1\n* poloidal\n-1\n* toroidal\n-1\n'
	
	s+='*** 3. physical cell\n*<0: user defines cell\n* 0: default\n*>0: read from file\n1%s\n* check cell ?\n' % (' 1'*len(grid.zone))
	s+=grid.perform_fluxtest
	
	open(outputpath+'input.geo','w').write(s)

	#input.n0g
	s1r='';s1p='';s2=''
	N1r=0;N1p=0;N2=0
	cmts2='* ZONE   R1   R2   DR   P1   P2   DP   T1   T2   DT\n* ne       Te      Ti      M\n'

	N1=0
	for isegm in range(grid.Nsegm):
		for nam in grid.zonelist:
			Nir,Nip,Nit,_=grid.zone[nam].Rzp[isegm].shape

			
			if nam=='CORE':
				s1r+='%4d %4d %4d\n' %  (0,N1,grid.inner_b_eirene)
				s1r+='%4d %4d %4d %4d\n' %  (0,Nip-2,0,Nit-2)
				
				for idp in range(len(grid.defaultplasma_isb)):
					s2+='2  1\n'
					s2+='%s  %4d %4d %4d %4d %4d %4d %4d %4d %4d %4d \n' % (cmts2,N1,idp,idp+1,1,0,Nip-1,30,0,Nit-1,Nit-1)
					s2+='%.3e  %.3f   %.3f   %.3f\n' % tuple(grid.defaultplasma_isb[idp])
					N2+=1
				
				
			elif nam in ['SOL','PFR']:
				s1r+='%4d %4d %4d\n' %  (Nir-1,N1,grid.outer_b_eirene)
				s1r+='%4d %4d %4d %4d\n' %  (0,Nip-2,0,Nit-2)
				
				s1p+='%4d %4d %4d\n' %  (0,N1,grid.outer_b_eirene)
				s1p+='%4d %4d %4d %4d\n' %  (0,Nir-2,0,Nit-2)
				s1p+='%4d %4d %4d\n' %  (Nip-1,N1,grid.outer_b_eirene)
				s1p+='%4d %4d %4d %4d\n' %  (0,Nir-2,0,Nit-2)
				N1p+=2
				
				

				s2+='2  0\n'#outer radial part for neutrals
				s2+='%s  %4d %4d %4d %4d %4d %4d %4d %4d %4d %4d \n' % (cmts2,N1,Nir-1.0-grid.Nn,Nir-1,1,0,Nip-1,1,0,Nit-1,Nit-1)
				s2+='%.3e  %.3f   %.3f   %.3f\n' % tuple(grid.defaultplasma_osb)

				s2+='2  0\n'#right outer poloidal part for neutrals
				s2+='%s  %4d %4d %4d %4d %4d %4d %4d %4d %4d %4d \n' % (cmts2,N1,0,Nir-1,1,0,grid.Nn,1,0,Nit-1,Nit-1)
				s2+='%.3e  %.3f   %.3f   %.3f\n' % tuple(grid.defaultplasma_osb)
				
				s2+='2  0\n'#left outer poloidal part for neutrals
				s2+='%s  %4d %4d %4d %4d %4d %4d %4d %4d %4d %4d \n' % (cmts2,N1,0,Nir-1,1,Nip-grid.Nn-1,Nip-1,1,0,Nit-1,Nit-1)
				s2+='%.3e  %.3f   %.3f   %.3f\n' % tuple(grid.defaultplasma_osb)
				
				s2+='2  0\n'#rest
				s2+='%s  %4d %4d %4d %4d %4d %4d %4d %4d %4d %4d \n' % (cmts2,N1,0,Nir-1,1,0,Nip-1,1,0,Nit-1,Nit-1)
				s2+='%.3e  %.3f   %.3f   %.3f\n' % tuple(grid.defaultplasma_osb)
						
				N2+=4

			N1+=1;N1r+=1
	
	s= '***************** additional geometry and parameters for N0 *********\n*** 1. non-transparent surfaces for neutral\n'
	s+='* link the surface material defined in input.eir sec 3A by using negative indicees\n'
	s+='* radial\n* IR  IZONE  SF.NR. in Eirene\n* IP1-2, IT1-2\n%4d\n%s' % (N1r,s1r)
	s+='* poloidal\n* IP  IZONE  SF.NR. in Eirene\n* IR1-2, IT1-2\n%4d\n%s' % (N1p,s1p)
	s+='* poloidal\n0\n'
	
	s+='*** 2. DEFINE ADDITIONAL PHYSICAL CELLS FOR NEUTRALS\n'
	s+=' %5d   70\n%s' % (N2,s2)
	s+='*** 3 Neutral Source distribution\n'
	s+='*   Neutral source distribution can either be readed from a file\n'
	s+='*   (N0S<0) or from EMC3 Code (N0S=0) \n'
	s+='* NS_PLACE=0: the source points are on the additional surfaces defined in the geometric part\n'
	s+='*          1: the source points are on the additional surfaces defined in the atomic part.\n'
	s+='* NSSIDE=-1,1 (only for NS_PLACE=0)the source points are on the negative/positiside of a surface\n'    
	s+='* N0S NS_PLACE  NSSIDE\n'
	s+='0        0        %d \n' % grid.NSSIDE
	s+='*** 4 Additional surfaces\n../GRID/ADD_SF_N0\n'

	
	s+='*** 5 Neutral diagnostic\n*** 1. Atom & molecule denstiy, atom & molecule energy density\n* Results in NEUTRAL_DENSITY in the consequence given above\n'
	s+='1 1 0 0\n'
	s+='*** 2. atom & molecule flux & energy spectrum across radial surfaces\n'
	s+='*  IR >=0: Radial surface or additional surface depending on IZ\n'
	s+='*  IR < 0: add. surf. nr\n'
	s+='* IZ>=0: Zone where the radial surface is located\n'
	s+='*        irrevant for add surf\n'
	s+='* ID=0: net fluxes\n'
	s+='*   >0: positive fluxes. For IR<0, incident flux on the possive  side\n'
	s+='*   <0: negative fluxes. For IR<0, incident flux on the negative side\n'
	s+='* A>0 : atom flux\n'
	s+='* M>0 : molecule flux\n'
	s+='* A_E>0: atom energy flux\n'
	s+='* NE_P>0: energy spectrum of atoms\n'
	s+='* E_L,_U lower and upper energy boundary\n'
	s+='* At present, only radial, or non-transparent\n'
	s+='%i ! total surfaces for diagnose (only radial, or non-transparent)\n' % (grid.zone['CORE'].Rz.shape[0]-1)
	s+='* IR  IZ  ID   A   M   A_E   NE_P  E_L  E_U\n'
	for ir in range(grid.zone['CORE'].Rz.shape[0]-1):
            s+='%i   0    0    1    1    0        400   1.     1.E4 \n' % ir
	open(outputpath+'input.n0g','w').write(s)

def make_input_files(grid,outputpath='output/'):
	set_input_file_parameters(grid)
	write_input_files(grid,outputpath=outputpath)