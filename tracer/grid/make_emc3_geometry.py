from machine_geometry import *
from IPython import embed
#from IPython.core.debugger import Tracer

def make_EMC3_geometry(machinexml,gridpath='GEOMETRY/',phi0=-180,phi1=180,rot3Dtargets=0.0,t3Dselect=None):
    Ndeg=int(np.ceil(abs(phi1-phi0)))
    phi0*=np.pi/180.;phi1*=np.pi/180.;rot3Dtargets*=np.pi/180.
    
    targets2D=xml_get_elements(machinexml,xpath='.//modelling_elements/*[pol_contour]/pol_contour',filt=[['seen_by','plasma',True]])
    
    nfcs=xml_get_elements(machinexml,xpath='.//modelling_elements/*[pol_contour]/pol_contour',filt=[['seen_by','neutrals',True]])
    
    gfcs=xml_get_elements(machinexml,xpath='.//modelling_elements/*[pol_contour]/pol_contour',filt=[['seen_by','grid',True]])
    
    all_pfcs=xml_get_element(machinexml,xpath='.//modelling_elements/*[@name="all_pfcs"]/pol_contour')
    
    vessel=xml_get_element(machinexml,xpath='.//modelling_elements/*[@name="vessel"]/pol_contour')

    if t3Dselect is None:
        targets3D=xml_get_elements(machinexml,xpath='.//modelling_elements/*[mesh]/mesh',filt=[['seen_by','plasma',True]])
        for k,t3D in targets3D.iteritems():t3D[:,:,2]+=rot3Dtargets
        #select limiters that are within the toroidal interval 
        targets3D={k:t3D for k,t3D in targets3D.iteritems() if np.any((phi0<=t3D[:,:,2])*(t3D[:,:,2]<phi1))}
    else:
        targets3D={}
        for k,a in t3Dselect.iteritems():
          t3D=xml_get_element(machinexml,xpath='.//modelling_elements/*[@name="%s"]/mesh' % k)
          #t3D[:,:,2]*=3
          t3D[:,:,2]+=a*np.pi/180.
          targets3D[k]=t3D
    

    f=open(gridpath+'ADD_SF_N0','w');f.write('%i   !total number of targets\n' % (len(nfcs)+len(targets3D)))
    
    for k,t in nfcs.iteritems():
        write_EMC3_target_file(gridpath+k+'.txt',t)
        f.write('0    -4    1\n../%s/%s.txt\n' % (gridpath,k))
                
    for k,t in targets3D.iteritems():
        write_EMC3_target_file(gridpath+k+'.txt',t,phi_scaling_factor=180./np.pi)
        f.write('0    -4    1\n../%s/%s.txt\n' % (gridpath,k))

    f.close()
    
    
    f=open(gridpath+'targets.txt','w');f.write('%i   !total number of targets\n' % (len(targets2D)+len(targets3D)))
    for k,t in targets2D.iteritems():
        write_EMC3_target_file(gridpath+k+'_deposition.txt',t,np.array([phi0,phi1])*180./np.pi)
        f.write("'../%s/%s_deposition.txt'  %i  %i\n" % (gridpath,k,30,Ndeg))
    
    for k,t in targets3D.iteritems():
#        write_EMC3_target_file(gridpath+k+'_deposition.txt',t,phi_scaling_factor=180./np.pi)
        f.write("'../%s/%s.txt'  1  1\n" % (gridpath,k))
    
    f.close()

    return all_pfcs,vessel,gfcs,targets3D



def write_EMC3_target_file(fn,target,phi=[0.0],comment=None,scaling_factor=100.0,phi_scaling_factor=1.0):
    if comment==None:comment=fn
    phi=np.array(phi)
    if len(target.shape)==2:
        Rzp=np.zeros((target.shape[0],phi.shape[0],3))
        for it,p in enumerate(phi):
          Rzp[:,it,0:2]=target;Rzp[:,it,2]=p
    elif (len(target.shape)==3) and (phi.shape==(1,)):
        Rzp=target
    else:
        raise Exception('wrong format.')

    #print 'phi_scaling_factor',phi_scaling_factor
    f=open(fn,'w')
    f.write(comment+'\n')
    f.write('%i  %i   1      0.00000      0.00000\n'% (Rzp.shape[1],Rzp.shape[0]))
    for it in range(Rzp.shape[1]):
      f.write("%15.5f\n" % (Rzp[0,it,2]*phi_scaling_factor))
      for x in Rzp[:,it,0:2]*scaling_factor:
          f.write("%15.5f   %15.5f\n" % (x[0],x[1]))
    f.close()


    
if __name__ == "__main__":
    import argparse

    parser=argparse.ArgumentParser()
    parser.add_argument("-s","--shot",default=34000,type=int,nargs='?')
    parser.add_argument("-st","--state",default='actual_state',type=str,nargs='?')    
    args=parser.parse_args()
    
    AUGxml=open_machine(ishot=args.shot,state=args.state)
    
    all_pfcs,vessel,triangles,gfcs=make_EMC3_geometry(AUGxml,'GRID/',0.0,np.pi/8.,dphi=2.0*np.pi/180.,limphioffset=0.0)
