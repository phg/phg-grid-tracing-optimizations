from grid2x import *

from field_routines import baxisym as bas
from field_routines import trace as tr
from field_routines import grid as gr
from field_routines import bfield as bf

class grid2xSOLPS(grid2x):
	def __init__(self,**kwargs):
		super(grid2xSOLPS, self).__init__(**kwargs)
		self.gc=0

	
	def make_SOLPS(self,figmap=None,fn='SF-LFS'):
		di1=self.Nithalf*self.rf[1]-self.gc; di2=self.Nithalf*self.rf[2]+1-self.gc; di3=self.Nithalf*self.rf[3]+1-self.gc; di4=self.Nithalf*self.rf[4]-self.gc
		Nir={n:zn.Rz.shape[0] for n,zn in self.zone.iteritems()}
		Nip={n:zn.Rz.shape[1] for n,zn in self.zone.iteritems()}

		NpSOL=Nip['SOL']-di1-di2
		Np=Nip['SOL']-di1-di2+Nip['FARSOL43']-di4-di3
		
		Nr=Nir['CORE']+Nir['SOL']+Nir['FARSOL42']-3

		if Nir['PFR'] != Nir['CORE']:raise Exception('PFR does not have the same radial size as the CORE.')

		if Nir['FARSOL42'] != Nir['FARSOL13']:raise Exception('FARSOL42 does not have the same radial size as the FARSOL13.')

		ids={'CORE':1,'SOL':2,'PFR':3,'FARSOL42':4,'FARSOL43':5,'FARSOL13':6}
		
		b=np.zeros((Nr,Np,6,2))*np.nan		

		pcs=[['PFR',['b',0.8,0.8,-10],Nip['PFR']-di2,self.ixp,-1,0],
	         ['CORE',['',0.3,0.98,10],Nip['CORE']-1,0,1,0],
	         ['PFR',['a',0.8,0.1,-70],self.ixp,di1,-1,1], 	         
	         ['SOL',['',0.5,0.87,0],Nip['SOL']-di2,di1,1,1],
	         ['FARSOL42',['a',0.7,0.98,-100],Nip['FARSOL42']-di2,self.s2leg[4].N-1,1,0],
	         ['FARSOL13',['a',0.7,0.15,30],self.ix2l,di1,1,0],
	         ['FARSOL13',['b',0.83,0.2,-45],Nip['FARSOL13']-di3,Nip['FARSOL13']-self.s2leg[3].N,1,0],
	         ['FARSOL42',['b',0.5,0.8,7],self.s2leg[4].N-1,di4,1,0],
	         ['FARSOL43',['',0.7,0.4,55],Nip['FARSOL43']-di3,di4,-1,0]]

		
		f=open(fn+'.regions','w')
		f.write('# ir0 ir1 ip0 ip1  i_zone      name     color\n') 

		ir0=0;ip0=0;

		for n,lab,ipa,ipe,incr,fir in pcs:
			print 'mapping zone ',n,lab[0],ipa,ipe
			zn=self.zone[n]
			Npp=abs(ipe-ipa)
			#if n=='FARSOL43':ir0,ip0=0,494
			if n=='FARSOL43':ir0,ip0=0,NpSOL

			ir1=ir0+Nir[n]-1
			ip1=ip0+Npp
			incp=np.sign(ipe-ipa)
			irm=int((Nir[n]-1)*lab[1]);
			ipm=int(ipa+(ipe-ipa)*lab[2]);
			
			Rzc=(zn.Rz[:-1,:-1]+zn.Rz[1:,:-1]+zn.Rz[:-1,1:]+zn.Rz[1:,1:])/4
			ipepincp=ipe+incp if ipe>=1 else None
			
			self.plotc(curve(zn.Rz[ 0,ipa:ipepincp:incp]),color=zn.color,lw=2)
			self.plotc(curve(zn.Rz[-1,ipa:ipepincp:incp]),color=zn.color,lw=2)
			self.plotc(curve(zn.Rz[:,ipa]),color=zn.color,lw=2)
			self.plotc(curve(zn.Rz[:,ipe]),color=zn.color,ls='--',lw=4)
			
			#self.plotcc(Rzc[:,min(ipa,ipe):max(ipa,ipe)],color=zn.color)
			
			#if self.figax:self.figax[1].text(zn.Rz[irm,ipm,0],zn.Rz[irm,ipm,1],n+lab[0],color=zn.color,va='center',ha='center',rotation=lab[3],fontsize=22)
			
			b[ir0:ir1,ip0:ip1,5,0]=ids[n]
			
			iir=self.iir=np.arange(Nir[n])[::incr]
			print 'iir',iir
			print ir0,ir1
			b[ir0:ir1,ip0:ip1,1,:]=zn.Rz[iir[:-1],ipa:ipe:incp]
			b[ir0:ir1,ip0:ip1,2,:]=zn.Rz[iir[:-1],ipa+incp:ipepincp:incp]
			b[ir0:ir1,ip0:ip1,3,:]=zn.Rz[iir[1: ],ipa:ipe:incp]
			b[ir0:ir1,ip0:ip1,4,:]=zn.Rz[iir[1: ],ipa+incp:ipepincp:incp]			
			b[ir0:ir1,ip0:ip1,0,:]=(zn.Rz[iir[:-1],ipa:ipe:incp]+zn.Rz[iir[:-1],ipa+incp:ipepincp:incp]+zn.Rz[iir[1: ],ipa:ipe:incp]+zn.Rz[iir[1: ],ipa+incp:ipepincp:incp])/4
			
			f.write(' %4i%4i%4i%4i%8i%10s%10s\n' % (ir0,ir1-1,ip0,ip1-1,ids[n],n+lab[0],zn.color))

			if figmap:
				figmap[1].text((ip0+ip1)/2,(ir0+ir1)/2,n+lab[0],color=self.zone[n].color,fontsize=30,va='center',ha='center',zorder=20,rotation=90 if abs(ipe-ipa)<100 else 0)
				figmap[1].text(ip0+Npp-1,-3,'ip=%i' % (ip0+Npp-1),rotation=90,va='center',ha='right')
				print ip0+Npp-1
				figmap[1].text(Np+5,ir0+Nir[n]-2,'ir=%i' % (ir0+Nir[n]-2),va='center')
			
			ip0+=Npp
			ir0+=(Nir[n]-1)*fir
			if fir==1:ip0=0
		f.close()
		
		f=open(fn+'.B2','w')
		f.write('%i %i\n' % (Np-2,Nr-2))		
		
		for ir in range(Nr):
			for ip in range(Np):
				f.write('%i %i  ' % (ip,ir))
				for i in range(5):
					f.write('%e %e ' % tuple(b[ir,ip,i]))
				
				x=b[ir,ip,0]
				B=bf.b_rzp([x[0],x[1],0.0])
				f.write('%e %e  \n' % ((B[0]**2+B[1]**2)**0.5,B[2]))
		
		f.close()

		self.bb=b
		if figmap:
			#check poloidal connectivity
			ii=np.where(~np.all(b[:,:-1,2,:]==b[:,1:,1,:],axis=2))
			figmap[1].scatter(ii[1],ii[0],color='black',marker='+',s=8)

			ii=np.where(~np.all(b[:,:-1,4,:]==b[:,1:,3,:],axis=2))
			figmap[1].scatter(ii[1],ii[0],color='black',marker='+',s=8)

			#check radidal connectivity
			ii=np.where(~np.all(b[:-1,:,3,:]==b[1:,:,1,:],axis=2))
			figmap[1].scatter(ii[1],ii[0],color='black',marker='o',s=8)

			ii=np.where(~np.all(b[:-1,:,4,:]==b[1:,:,2,:],axis=2))
			figmap[1].scatter(ii[1],ii[0],color='black',marker='o',s=8)

			for k,i in ids.iteritems():
				ii=np.where(b[:,:,5,0]==i)
				figmap[1].scatter(ii[1],ii[0],color=self.zone[k].color,marker='x',s=4)

			#ii=np.where(np.all(b[:-1,:,2,:]!=b[1:,:,1,:],axis=3))
			

	
	def thin_out_core(self,ii):
		zn=self.zone['CORE']
		zn.Rz=np.delete(zn.Rz,ii,axis=1)
		zn=self.zone['SOL']
		zn.Rz=np.delete(zn.Rz,np.array(ii)+self.ixp,axis=1)
		zn=self.zone['FARSOL13']
		zn.Rz=np.delete(zn.Rz,np.array(ii)+self.ixp,axis=1)

		self.Npcore-=len(ii)
		self.ix2l-=len(ii)
	

	def make_guard_cells(self,d=1e-2):
		
		def gczn(zn,ip1,ip2,typ):
			for ir in range(zn.Rz.shape[0]):
				zn.Rz[ir,ip1-1]=zn.Rz[ir,ip1]+(zn.Rz[ir,ip1]-zn.Rz[ir,ip1+1])*d
				zn.Rz[ir,ip2+1]=zn.Rz[ir,ip2]+(zn.Rz[ir,ip2]-zn.Rz[ir,ip2-1])*d
							
			if typ==1:
				for ip in range(zn.Rz.shape[1]):
					x=(zn.Rz[-2,ip]-zn.Rz[-3,ip])
					zn.Rz[-1,ip]=zn.Rz[-2,ip]+x*d
					
			
		self.gc=1# guard cells
		
		for nam,typ in [['SOL',2],['PFR',1],['FARSOL13',1],['FARSOL43',1],['FARSOL42',1]]:
			zn=self.zone[nam]
			gczn(zn,self.Nithalf*self.rf[1],zn.Rz.shape[1]-(self.Nithalf*self.rf[2]+1),typ)
		

	def outer_boundary(self,fn='outer_boundary.txt'):
		di1=self.Nithalf*self.rf[1]; di2=self.Nithalf*self.rf[2]+1; di3=self.Nithalf*self.rf[3]+1; di4=self.Nithalf*self.rf[4]
		print 'di1..4:',di1,di2,di3,di4
		zn=self.zone['SOL']

		bir=[None,-1][self.gc];iir=[-1,-2][self.gc]
		
		c=self.ccc=curve(self.zone['SOL'].Rz[:,di1,:]*1.0)
		c.add(self.zone['FARSOL13'].Rz[:bir,di1,:]*1.0)
		c.add(self.zone['FARSOL13'].Rz[iir,di1:-di3,:]*1.0)
		c.add(self.zone['FARSOL13'].Rz[iir::-1,-di3,:]*1.0)

		c.add(self.zone['FARSOL43'].Rz[:bir,-di3,:]*1.0)
		c.add(self.zone['FARSOL43'].Rz[iir,-di3:di4-1:-1,:]*1.0)
		c.add(self.zone['FARSOL43'].Rz[iir::-1,di4,:]*1.0)

		c.add(self.zone['FARSOL42'].Rz[:bir,di4,:]*1.0)
		c.add(self.zone['FARSOL42'].Rz[iir,di4:-di2,:]*1.0)
		c.add(self.zone['FARSOL42'].Rz[iir::-1,-di2,:]*1.0)
		c.add(self.zone['SOL'].Rz[::-1,-di2,:]*1.0)
		c.add(self.zone['PFR'].Rz[:bir,-di2,:]*1.0)
		c.add(self.zone['PFR'].Rz[iir,-di2:di1:-1,:]*1.0)
		c.add(self.zone['PFR'].Rz[iir::-1,di1,:]*1.0)

		c.remove_zerolength_intervals()
		
		self.plotc(c,lw=3,color='blue')
		c.write_to_file(fn,scaling_factor=1.0)
		return c

	#def plotlegs(self):
		#l=[[1,'SOL',self.Nithalf*self.rf[1],self.ixp+1],[2,'SOL',self.i2xp,self.sep.N-self.Nithalf*self.rf[2]],
		   #[4,'FARSOL42',self.Nithalf*self.rf[4],self.s2leg[4].N],[3,'FARSOL43',self.s2leg[4].N-1,self.sequences['FARSOL43'][0]-self.Nithalf*self.rf[3]],
		   #[11,'FARSOL13',self.Nithalf*self.rf[1],self.ix2l+1]]
		#L={}
		#for n,nam,ip1,ip2 in l:
			#zn=self.zone[nam]
			#self.plotc(curve(zn.Rz[0,ip1:ip2]),color=zn.color,lw=5)
			#L[n]=ip2-ip1
			#print 'leg ',n,'points: ',nam,ip2-ip1
		
		#print 'L4-L11=',L[4]-L[11]
		#print 'L1+Npcore+L2-(2L4+2L3)=',L[1]+self.Npcore+L[2]-(2*L[4]+2*L[3])
		#print 'target L3: ',(L[1]+self.Npcore+L[2]-2*L[4])/2.0

	#def plotcc(self,c,**kwargs):
		#if self.figax:self.figax[1].scatter(c[:,:,0],c[:,:,1],**kwargs)

		

