import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../build/.')
from field_routines import baxisym as bas
from field_routines import trace as tr
from field_routines import grid as gr
from curve import *
from copy import copy
import inspect
import time

def angle(v,aref=0.0):
	return (np.arctan2(v[1],v[0])-aref+5*np.pi) % (2*np.pi)

def norml(v):
	return (v-v[0])/(v[-1]-v[0])


class grid(object):
	def __init__(self,Npcore=400,Nithalf=5,dphi=360./16.,figax=None):
		object.__init__(self)
		self.zone={}
		self.Npcore=Npcore
		self.Nithalf=Nithalf
		self.Nit=Nithalf*2+1
		self.phistart=0.0
		self.Dphi=dphi*np.pi/180.;
		self.dphi=self.Dphi/(self.Nit-1)
		self.Np1=100;self.Np2=100;
		self.fcreg1=0.1;self.fcreg2=0.93;
		self.rf={1:1,2:1} #refinement factors for targets 1 and 2
		self.legrf={1:1.0,2:1.0} #refinement factors for legs 1 and 2
		self.Ntrace=12000;self.dphistep=1.0*np.pi/180.0
		self.step_mode=False;self.istep=0;self.Nskipsteps=0
		
		#give a list of distance-angle values to define the xpoint-curves
		self.xpcurve1par=[[0.4,0.0]]
		self.xpcurve2par=[[0.02,0],[0.02,-10],[0.025,-15.0],[0.07,0.0],[0.2,0.0]]
		self.xpcurveppar=[[0.4,0.0]]
		self.xpcurvecpar=[[0.4,0.0]]
		
		self.oxp_estimates={} #In some cases the O and X-points cannot be found automatically. This helps to find them. E.g. for TCV choose {'op':[0.88,0.4]}.
		
		self.target_range={1:[-0.05,0.1,'dsposflx','red'],2:[-0.05,0.1,'dsposflx','blue']} # positive s direction = positive direction in flux
		
		#alternatively:
		#self.target_range={1:[-0.05,0.1,'ds','red'],2:[-0.05,0.1,'ds','blue']} # positive s direction is given by direction of the pfcs
		#or
		#self.target_range={1:[0.98,1.05,'PsiN','red'],2:[0.98,1.05,'PsiN','blue']} # give range in normalized flux

		self.target={}
		self.sp={}#strike points (determined by construct_basesep)
		self.dsp=0.2
		self.figax=figax
		self.istrxp=3
		self.amaxfactor=1.03
		self.postlimitsteps=15
		self.flt_substeps=5
		self.a_upstream=0.0
		
		self.xp2reg_linearization=False #activate this switch, if the angle between the separatrix and the line connecting O-and X-point is significantly different to +/-45deg (e.g. in TCV)

		#paramters for the 3D extension
		self.Nsegm=1
		self.Nn=3
		self.zonelist=['CORE','SOL','PFR']
		self.wall=None
		


	def load_magnetics(self,ref,scale_psi=1.0,scale_Bo=1.0):
		import xml.etree.ElementTree as et
		def xml2np(pxml):
			'''
			Interprets the numerical data contained in the xml element text as a numpy array according to the type and dimension meta data stored in the xml attributes.
			'''
			if not ('type' in pxml.attrib.keys()):
			    raise Exception('xml-tag must contain numpy-type information.')
			N=np.fromstring(pxml.attrib['dim'],dtype=int,sep=',')
			if np.product(N)>1:
			    return np.fromstring(pxml.text,sep=',',dtype=pxml.attrib['type'],count=np.product(N)).reshape(N)
			else:
			    return np.fromstring(pxml.text,sep=',',dtype=pxml.attrib['type'],count=1)[0]
		

		if isinstance(ref,et.Element):
			magneticsxml=ref
		elif isinstance(ref,str):
			magneticsxml=et.parse(ref).getroot()
		else:
			raise Exception('ref needs to be either a file name (string) or a reference to an xml-object')

		if magneticsxml.attrib['version']=='1.0':
		    raise Exception('Old name convention. Rename Coio to CoIo, Coiu to CoIu, PSLu to pslu, PSLo to pslo and version="1.0" to version="1.1" in file %s' % ref)
    
		if magneticsxml.attrib['version']!='1.1':
		    raise Exception('This format has not yet been implemented.')

		eqxml= magneticsxml.find('equilibrium') 
		Rixml=eqxml.find('Ri');Ri=np.fromstring(Rixml.text,count=int(Rixml.attrib['dim']),sep=',')
		zjxml=eqxml.find('zj');zj=np.fromstring(zjxml.text,count=int(zjxml.attrib['dim']),sep=',')
		PFMxml=eqxml.find('PFM');
		Nz,NR=np.fromstring(PFMxml.attrib['dim'],count=2,sep=',',dtype=int)

		if (NR != Ri.shape[0]) or (Nz != zj.shape[0]):
			raise Exception('There is something wrong with the equilibrium')

		PFM=np.fromstring(PFMxml.text,count=int(NR*Nz),sep=',').reshape(Nz,NR)
		bas.set_psi(PFM.T*scale_psi,np.min(Ri),np.max(Ri),np.min(zj),np.max(zj))

		Bphixml=magneticsxml.find('Bphi')
		bas.set_bt(float(Bphixml.attrib['Bo'])*scale_Bo,float(Bphixml.attrib['Ro']))
		singxml=eqxml.find('singularities')

		if singxml is not None:
			bas.opoint[:]  =xml2np(singxml.find('op'))
			bas.xpoint[:,0]=xml2np(singxml.find('xp'))
			bas.xpoint[:,1]=xml2np(singxml.find('xp2'))
			self.oxp_estimates='magnetics'
			
		return magneticsxml
	
	def load_equilibrium(self,fn,scale_psi=1.0,Bo=None,Ro=None):
		print 'warning: this routine will be deprecated in the near future.'
		f=open(fn)
		s=f.readline().split()
		Nr=int(s[0]);Nz=int(s[1])
		if Bo==None and Ro==None:
			Bo=float(s[2]);Ro=float(s[3])
		self.Bo=Bo;self.Ro=Ro
		#Nr,Nz=np.fromstring(f.readline(),dtype=int,sep=' ',count=2)
		rmn,rmx,zmn,zmx=np.fromstring(f.readline(),dtype=float,sep=' ',count=4)
		self.psi=np.fromfile(f,dtype=float,sep=' ',count=Nr*Nz).reshape(Nz,Nr)*scale_psi
		f.close()
		self.Req=np.linspace(rmn,rmx,Nr)
		self.zeq=np.linspace(zmn,zmx,Nz)
		bas.set_psi(self.psi.T,rmn,rmx,zmn,zmx)
		bas.set_bt(Bo,Ro)


	def add_pfcs(self,fn,scaling_factor=1.0):
		self.pfcs=pfcs=curve(fn,scaling_factor=scaling_factor)
		self.plotc(pfcs,color='black',lw=2)


	def find_singularities(self):
		oxpe=self.oxp_estimates
		if oxpe=='magnetics':
			print 'singularities loaded from magnetics file.' 
		
		elif not 'xp' in oxpe:
			tr.unset_axisym_targets()
			tr.add_axisym_target_curve(self.pfcs.Rz)
			tr.find_confinement(p1=oxpe['op'] if 'op' in oxpe.keys() else [-1,-1])
			tr.unset_axisym_targets()
		else:
			for k,p in oxpe.iteritems():
				bas.search_oxp(p,{'op':0,'xp':1,'xp2':2}[k])
				
		self.op=op=bas.opoint[1:3];self.xp=xp=bas.xpoint[1:3,0];self.xp2=xp2=bas.xpoint[1:3,1]
		
		
		self.thetaxp=angle(xp-op)
		bas.find_upstream_pos(self.a_upstream)
		self.dirfield=np.sign(bas.theta_pitchu)
		self.omp=bas.rz_u
		self.ompcurve=curve([self.omp,self.omp+np.array([0.3,0.0])])
		
		self.scatter([op,xp])
		if xp2[0]>0:self.scatter([xp2])


	def construct_basesep(self):
		bas.find_separatrix(self.Npcore,self.Np1,self.Np2,lr=0.2,ll=0.2)#number of poloidal points on separatrix in core, right leg and left leg respectively. lr and ll are refinement factors for the right and left Divertor legs.
		
		if np.any(np.isnan(bas.sep)):
			self.plotc(curve(bas.sep,remove_Nan=True),marker='+')
			raise Exception('Divertor legs are leaving computational domain. Decrease Np1 and/or Np2.')

		self.rawsep=s=curve(bas.sep)
		self.plotc(s)
		
		c=s.collide_all(self.pfcs)
		
		self.sp[1]=c[0]
		self.scatter([self.sp[1]],color='red')
		
		self.sp[2]=c[-1]
		self.scatter([self.sp[2]],color='blue')
		
		if len(c)<2:
			raise Exception('at least two intersections of the separatrix with the PFC contour expected.')

		self.thetacor=np.array([angle(p-self.op,aref=self.thetaxp) for p in s.Rz[self.Np1:self.Np1+self.Npcore]])
		self.thetacor[0] =0.0
		self.thetacor[-1]=2*np.pi
		
		
		#self.target1=self.pfcs.extract_interval(l0=self.sp1[3]-self.dsp,l1=self.sp1[3]+self.dsp)
		#self.plotc(self.target1,color='red',lw=2)
		#self.target2=self.pfcs.extract_interval(l0=self.sp2[3]-self.dsp,l1=self.sp2[3]+self.dsp)
		#self.plotc(self.target2,color='blue',lw=2)


	def make_targets(self,ii=[1,2],Ntest=5,dtest=0.2,dstest=0.01,lw=4,ls='-'):
		for i in ii:
			if not i in self.sp.keys():
				raise Exception('Strike point %i not known.' % i)
			
			a,b,typ,color=self.target_range[i]
			print a,b,typ,color
			if typ.lower()=='psin':
				ltest=self.sp[i][3]+np.linspace(-dtest,dtest,2*Ntest+1)
				psitar=np.array([bas.psin_at_rz(self.pfcs.l2Rz(l)) for l in ltest])
				jj=np.argsort(psitar)
				a=np.interp(a,psitar[jj],ltest[jj])
				b=np.interp(b,psitar[jj],ltest[jj])
			elif typ.lower()=='dsposflx' or typ.lower()=='ds':
				if typ.lower()=='dsposflx':
					if bas.psin_at_rz(self.pfcs.l2Rz(self.sp[i][3]+dstest))<1.0:a,b=-a,-b
				a+=self.sp[i][3];b+=self.sp[i][3];
			else:
				raise Exception('unknown type for SP %i' % i)
			
			if b<a:a,b=b,a
			#print a,b
			self.target[i]=self.pfcs.extract_interval(l0=a,l1=b)
			self.plotc(self.target[i],color=color,lw=lw,ls=ls)


	def compute_target_intersection_triangles(self,triangles,outputpath='./GEOMETRY/',shift=1e-5):
		xp=self.xp
		
		if abs(shift)>1.e-9:
			for i in range(triangles.shape[0]):
				for j in range(3):
					triangles[i,j]=triangles[i,j]+(triangles[i,j]-xp)*shift
		
		f=open(outputpath+'intersection.txt','w')

		nzone=0
		for nam in self.zonelist:
				zn=self.zone[nam]
				zn.hit=[]
		
		start=time.time()
		for isegm in range(self.Nsegm):
			for nam in self.zonelist:
				zn=self.zone[nam]
				Nir,Nip,Nit,_=zn.Rzp[isegm].shape
				hit=np.zeros((Nir-1,Nip-1,Nit-1),dtype=np.int32,order='F')
				gr.intersect(zn.Rzp[isegm],hit,triangles)
				pm,Npm=gr.plates_mag(hit)
				
				for i in range(Npm):
					k=pm[i,2]+3
					f.write('%i  ' % (nzone))
					f.write((('%i ' * k) % tuple(pm[i,0:k]))+'\n')
					
				nzone+=1
				zn.hit+=[hit]
				
			print 'segment %i from %i finished, %i s elapsed.' % (isegm+1,self.Nsegm,int(time.time()-start))
		print ' done.'


	def allocate_targetcells(self):
		for nam,zn in self.zone.iteritems():
			zn.targetcells=[]
			for isegm in range(self.Nsegm):
				Nir,Nip,Nit,_=zn.Rzp[isegm].shape
				zn.targetcells.append(np.zeros((Nir-1,Nip-1,Nit-1),dtype=np.int32,order='F'))


	def compute_2Dtarget_intersection(self,targets,zonelist=['SOL','PFR'],shift=1e-5):
		xp=self.xp
		
		kk=targets.keys() if isinstance(targets,dict) else range(len(targets))
		
		print '2D target intersection:'
		
		for nam in zonelist:
			print nam
			zn=self.zone[nam]
			for isegm in range(self.Nsegm):
				for k in kk:
					tar=targets[k]*1.0
					
					if abs(shift)>1.e-9:tar=tar+(tar-xp)*shift
					
					#print zn.Rzp[isegm].shape,hit.shape,tar.shape
					gr.intersect2d(zn.Rzp[isegm],zn.targetcells[isegm],tar)
		
		print ' done.'


	def compute_3Dtarget_intersection(self,targets,zonelist=['SOL']):
		
		kk=targets.keys() if isinstance(targets,dict) else range(len(targets))
		
		print '3D target intersection:'
		
		for nam in zonelist:
			print nam
			for isegm in range(self.Nsegm):
				for k in kk: 
					print k
					gr.intersect3d(self.zone[nam].Rzp[isegm],self.zone[nam].targetcells[isegm],targets[k])
		
		print ' done.'



	def plot_targecells(self,nam='SOL',isegm=0,it=-1,**kwargs):
		if self.figax:
			Rzp=self.zone[nam].Rzp[isegm]
			hit=self.zone[nam].targetcells[isegm]
			#print 'targetcells'
			#print Rzp.shape,hit.shape
			#from IPython import embed;embed()
			iit=[it] if it>=0 else range(Rzp.shape[2]-1)
			for i in iit:
				ii=np.where((hit[:,:,i]==1))
				self.figax[1].scatter(0.5*(Rzp[ii[0],ii[1],i,0]+Rzp[ii[0]+1,ii[1]+1,i+1,0]),0.5*(Rzp[ii[0],ii[1],i,1]+Rzp[ii[0]+1,ii[1]+1,i+1,1]),**kwargs)


	def writeout_intersection(self,outputpath='./GEOMETRY/'):
		f=open(outputpath+'intersection.txt','w')
		nzone=0
		for isegm in range(self.Nsegm):
			for nam in self.zonelist:
					
				pm,Npm=gr.plates_mag(self.zone[nam].targetcells[isegm])
				
				for i in range(Npm):
					k=pm[i,2]+3
					f.write('%i  ' % (nzone))
					f.write((('%i ' * k) % tuple(pm[i,0:k]))+'\n')
					
				nzone+=1
				
		f.close()

	def linearize_xp2reg_regions(self):
		#instead of an equal distance in angular space the poloidal points are set equidistant in real-space
		i1=int(round(self.fcreg1*self.Npcore))
		i2=int(round(self.fcreg2*self.Npcore))

		c=curve(self.rawsep.Rz[self.Np1:self.Np1+i1])
		ll=np.linspace(0,c.totl,i1)
		self.thetacor[:i1]=np.array([angle(p-self.op,aref=self.thetaxp) for p in c.l2Rz(ll)])
		
		c=curve(self.rawsep.Rz[self.Np1+i2:self.Np1+self.Npcore])
		ll=np.linspace(0,c.totl,self.Npcore-i2)
		self.thetacor[i2:]=np.array([angle(p-self.op,aref=self.thetaxp) for p in c.l2Rz(ll)])

		self.thetacor[-1]=2*np.pi


	def thetacor_correction(self):
		#this routine may be overwritten to change the poloidal distribution of the grid points in the core
		if self.xp2reg_linearization:
                    self.linearize_xp2reg_regions()
                else:
                    pass


	def construct_regular_region(self):
		#construct curves limiting the regular SOL region
		op=self.op;tc=self.thetacor

		i1=int(round(self.fcreg1*self.Npcore))
		i2=int(round(self.fcreg2*self.Npcore))

		amax=np.max([np.linalg.norm(Rz-op) for Rz in self.rawsep.Rz[self.Np1:self.Np1+self.Npcore]])
		self.raylist=[curve([op,op+np.array([np.cos(t),np.sin(t)])*amax*self.amaxfactor]) for t in tc[:]+self.thetaxp-np.pi]
		
		self.reglist=self.raylist[i1:i2+1]
		
		self.reg1=self.reglist[0]
		self.reg2=self.reglist[-1]
		self.plotc(self.reg1,color='red',ls='--')
		self.plotc(self.reg2,color='blue',ls='--')


	def construct_xp_curves(self):
		#construct reference curves touching the X-point
		op=self.op;xp=self.xp
		v=xp-op;vp=np.array([-v[1],v[0]])#/np.linalg.norm(v)
		
		cols=['red','blue','black','orange']
		
		self.xpcurve1=length_angle_list2curve(xp,vp,self.xpcurve1par)
		self.xpcurve2=length_angle_list2curve(xp,-vp,self.xpcurve2par)
		self.xpcurvep= length_angle_list2curve(xp,v,self.xpcurveppar)
		self.xpcurvec=  length_angle_list2curve(xp,-v,self.xpcurvecpar)
		for c,col in zip([self.xpcurve1,self.xpcurve2,self.xpcurvep,self.xpcurvec],cols):
			self.plotc(c,color=col,marker='x',ls='--')
			
		self.xpcurvep.refine(20)


	def construct_separatrix(self):
		s=self.rawsep
	
		#create a list of length values and add the points from the core
		
		ths=[angle(rz-self.op,aref=self.thetaxp) for rz in s.Rz[self.Np1:self.Np1+self.Npcore]];ths[0]=0.0; ths[-1]=2*np.pi
		ll=np.interp(self.thetacor,ths,s.l[self.Np1:self.Np1+self.Npcore])
		
		#note that if thetacor is regular the two lines above simplify to
		#self.ll=ll=s.l[self.Np1:self.Np1+self.Npcore]*1.0 
		
		#add the points around the SPs
		
		for sp,rf in [[self.sp[1],self.rf[1]],[self.sp[2],self.rf[2]]]:
			fl=curve(tr.field_line([sp[0],sp[1],0],self.dphi/rf,(self.Nit-1)*rf,dir=0)[:,0:2])
			ll=np.append(ll,fl.l-fl.l[self.Nithalf*rf]+sp[2])
		
		
		ll.sort()
		
		self.scatter(s.l2Rz(ll,output_format='curve'))
		
		#fill the separatrix intervals between the target aligned region and the X-point
		self.ll=ll
		i=self.ispr1=(self.Nit-1)*self.rf[1]

		L=stuff_interval(ll[i]-ll[i-1],ll[i+1]-ll[i],(ll[i+2]-ll[i+1])/self.legrf[1])+ll[i]
		ixp=self.ixp=i+len(L)+1
		ll=np.append(ll,L)
		ll.sort()
		self.i2xp=ixp+self.Npcore-1
		
		i=self.i2xp
		ll=np.append(ll,stuff_interval((ll[i]-ll[i-1])/self.legrf[2],ll[i+1]-ll[i],(ll[i+2]-ll[i+1]))+ll[i])
		ll.sort()
		self.ispr2=len(ll)-(self.Nit-1)*self.rf[2]-1

		self.sep=sep=s.l2Rz(ll,output_format='curve')

		self.scatter(sep)

		self.isp1=self.Nithalf*self.rf[1]
		self.isp2=len(ll)-self.Nithalf*self.rf[2]-1
		
		self.ireg1=self.ixp+int(round(self.fcreg1*self.Npcore))
		self.ireg2=self.ixp+int(round(self.fcreg2*self.Npcore))
		

	def make_sequences(self):
		self.sequences={};l=self.sep.l

		seq=[[1,         0,self.target[1],[self.dphi/self.rf[1],(self.Nit-1)*self.rf[1]]],
	         [1,self.ispr2,self.target[2],[self.dphi/self.rf[2],(self.Nit-1)*self.rf[2]]],
	         [2,self.ixp  ,self.xpcurve1,None],
	         [2,self.i2xp ,self.xpcurve2,None]]+[[2,i+self.ireg1,r,None] for i,r in enumerate(self.reglist)]
		
		self.sequences['SOL']=[self.sep.Rzl,0,0,4,self.ompcurve,seq]
		
		Np=self.ixp+self.sep.N-self.i2xp

		ispr2pfr=Np-(self.Nit-1)*self.rf[2]-1

		seq=[[1,0,self.target[1],[self.dphi/self.rf[1],(self.Nit-1)*self.rf[1]]],
	         [1,ispr2pfr,self.target[2],[self.dphi/self.rf[2],(self.Nit-1)*self.rf[2]]],
	         [2,self.ixp,self.xpcurvep,None]]
	     
		seppfr=curve(np.vstack((self.sep.Rz[:self.ixp,:],self.sep.Rz[self.i2xp:]))) 
		self.sequences['PFR']=[seppfr.Rzl,0,0,4,self.xpcurvep,seq]
	
		Nregr=14
		seq=[[3,0,self.xpcurvec,0.0],[2,self.Npcore-1,self.xpcurvec,0.01]]+\
			[[2,i+Nregr,r,None] for i,r in enumerate(self.raylist[Nregr:-Nregr+1])]
		sepcr=curve(self.sep.Rz[self.ixp:self.i2xp+1])
		self.sequences['CORE']=[sepcr.Rzl,-1,1,3,self.xpcurvec,seq]


	def construct_poloidal_lines_xpregion(self):
		pllist=[[self.ixp-self.istrxp,self.ixp-self.istrxp,self.xpcurvep.Rz[-1],'PFR'],
				[self.i2xp+self.istrxp,self.ixp+self.istrxp,self.xpcurvep.Rz[-1],'PFR'],
				[self.ixp-self.istrxp,self.ixp-self.istrxp,self.xpcurve1.Rz[-1],'SOL'],
				[self.ixp+self.istrxp,self.ixp+self.istrxp,self.xpcurve1.Rz[-1],'SOL'],
				[self.i2xp-self.istrxp,self.i2xp-self.istrxp,self.xpcurve2.Rz[-1],'SOL'],
				[self.i2xp+self.istrxp,self.i2xp+self.istrxp,self.xpcurve2.Rz[-1],'SOL']]
		
		for ip1,ip2,p,snam in pllist:
			crv=curve([self.sep.Rz[ip1],p])
			self.plotc(crv,color='green',ls='--')
			self.sequences[snam][5]+=[[2,ip2,crv,None]]


	def prepare(self):
		self.find_singularities()
		self.construct_basesep()
		self.make_targets()
		self.thetacor_correction()
		self.construct_regular_region()
		self.construct_xp_curves()
		self.construct_separatrix()
		self.make_sequences()
		self.construct_poloidal_lines_xpregion()


	def make_zone(self,nam,dr=None,drn=None,dru=None,PsiN=None,color='blue'):
		op=bas.opoint[1:3]
		print 'making',nam
		Rzl,ipedge,fldir,limtype,start,seq=self.sequences[nam]
		Nip=Rzl.shape[0];Ls=Rzl[:,2]
		
		if dr is not None:
			dll=np.array(dr)
		elif drn is not None:
			if not hasattr(start,'reflength'):
				raise Exception('curve has no reference length. Define this quantity or call specifying dr.')
			dll=np.array(drn*start.reflength)			
		elif dru is not None:
			drustart=np.array([bas.psin_at_rz(rz)-1.0 for rz in start.Rz])/bas.dpsinu_dru
			ii=np.argsort(drustart)
			dll=np.interp(dru,drustart[ii],start.l[ii])
			
		elif PsiN is not None:
			
			srtref=curve(start.Rz,refinement=100)

			pn=np.array([bas.psin_at_rz(rz) for rz in srtref.Rz])
			self.plotc(srtref)#,marker='o')
			ii=np.argsort(pn)
			dll=np.interp(PsiN,pn[ii],srtref.l[ii])
		else:
			raise Exception('you must either specify dr, drn, dru or PsiN.')
		
		Nir=dll.shape[0]
		
		self.zone[nam]=zn=zone(Nir,Nip,color=color)
		
		tr.unset_all_limits()
		
		
		for ir,dl in enumerate(dll):
			#print 'making',dl,limtype
			
			if limtype==4:
				tr.add_axisym_target_curve(self.pfcs.Rz) 
				tr.set_endpointcorrection(False)
				tr.set_postlimitsteps(self.postlimitsteps) # fl is continued for self.postlimitsteps after having reached the target
			elif limtype==3:
				tr.set_npol_limit(1.01)
			else:
				raise Exception('unknown limit type.')
			
			sf=curve(tr.field_line(start.l2Rz(dl,'Rz0'),self.dirfield*self.dphistep,self.Ntrace,dir=fldir,substeps=self.flt_substeps)[:,0:2],remove_Nan=True)
			
			tr.unset_all_limits()
			
			self.plotc(sf,color='grey')
			
			zn.Rz[ir,:]=self.sequence2Rzl(Nip,Ls,sf,seq)[:,0:2]*1.0
		
		zn.insert_sf(i=ipedge)
		zn.Rz[ipedge,:]=Rzl[:,0:2]*1.0
		self.plotzn(nam)#,marker='x'			


	def sequence2Rzl(self,Nip,Ls,sf,seq):
		Rzl=np.zeros((Nip,3))*np.nan
		for typ,ip,crv,params in seq:
			#print 'making type',typ,ip
			if typ==1:
				c=sf.collide(crv)
				tr.unset_all_limits()
				fl=self.fl=curve(tr.field_line([c[0],c[1],0],params[0],params[1],dir=0,substeps=self.flt_substeps)[:,0:2])
				#self.plotc(fl,color='green',lw=2)
				L=fl.l-fl.l[fl.Nhalf]+c[2]
				L.sort()
				Rzl[ip:ip+fl.N,:]=np.hstack((sf.l2Rz(L),L.reshape(-1,1)))
			elif typ==2:
				Rzl[ip]=sf.collide(crv,l1min=params if (params is not None) else 0.0)[0:3]
			elif typ==3:
				Rzl[ip]=np.hstack((sf.l2Rz(params),params))
			elif typ==4:
				Rzl[ip]=np.hstack((sf.l2Rz(params*sf.totl),params*sf.totl))
		
		ii=np.arange(Nip)[np.isnan(Rzl[:,2])]

		edi=np.where((ii[1:]-ii[:-1])>1)[0]
		
		ii1=[ii[0]-1]+[ii[ei+1]-1 for ei in edi]
		ii2=[ii[ei]+1 for ei in edi]+[ii[-1]+1]
		
		for i1,i2 in zip(ii1,ii2):
			#self.scatter([Rzl[i1,0:2],Rzl[i2,0:2]],color='red',s=200)
			L=norml(Ls[i1:i2+1])[1:-1]*(Rzl[i2,2]-Rzl[i1,2])+Rzl[i1,2]
			Rzl[i1+1:i2,:]=np.hstack((sf.l2Rz(L),L.reshape(-1,1)))
		
		return Rzl


	def make_core(self,psil,color='red'):
		op=bas.opoint[1:3]
		psil=np.array(psil)
		Nir=psil.shape[0];
		self.zone['CORE']=zn=zone(Nir,self.Npcore,color=color)
		#from IPython import embed;embed()
		for ir,p in enumerate(psil):
			pabs=bas.opoint[0]+p*(bas.xpoint[0,0]-bas.opoint[0])
			for ip in range(self.Npcore):
				x=self.sep.Rz[ip+self.ixp]*1.0
				zn.Rz[ir,ip,:]=bas.ray_flxsf_intersection(x+(op-x)*0.05,(op-x)*0.05,pabs)
		zn.insert_sf(i='end')
		zn.Rz[-1,:]=self.sep.Rz[self.ixp:self.i2xp+1]*1.0
		self.plotzn('CORE')


	def farsolextension(self,p1=None,p2=None,name='SOL',llist=[0.05,0.1]):
		llist=np.array(llist)
		ir0=self.zone[name].Rz.shape[0]-1
		sol=self.zone[name].Rz=np.insert(self.zone[name].Rz,[ir0+1]*llist.shape[0],np.nan,axis=0)
		ir1=sol.shape[0]-1
		p1=np.array(p1);p2=np.array(p2)
		if len(p1.shape)!=1 or p1.shape[0]!=2:p1=self.op
		if len(p2.shape)!=1 or p2.shape[0]!=2:p2=self.xp
		print 'p1,p2=',p1,p2
		v=(p2-p1)/np.linalg.norm(p2-p1)
		for ip in range(sol.shape[1]):
			p=sol[ir0,ip,0:2]
			pp=p1+v*np.clip(np.dot(p-p1,v),0.0,np.linalg.norm(p2-p1))
			ray=curve(np.array([p,p+(p-pp)*10]))
			col=ray.collide(self.wall)
			for i,l in enumerate(llist):
				sol[ir0+1+i,ip,0:2]=p+(col[0:2]-p)*l

	
	#def glue_zones(self):
		#zn=self.zone['CORE']
		#zn.insert_sf(i='end')
		#zn.Rz[-1,:]=self.sep.Rz[self.ixp:self.i2xp+1]*1.0
		#self.plotzn('CORE')
		
		#zn=self.zone['SOL']
		#zn.insert_sf(i=0)
		#zn.Rz[0,:]=self.sep.Rz*1.0
		#self.plotzn('SOL')
		
		#zn=self.zone['PFR']
		#zn.insert_sf(i=0)
		#zn.Rz[0,0:self.ixp]=self.sep.Rz[0:self.ixp]*1.0
		#zn.Rz[0,self.ixp:]=self.sep.Rz[self.i2xp:]*1.0
		#self.plotzn('PFR')
		
		##self.plotc(self.sep,color='black',marker='x')


	def expand3d(self):
		for nam,zn in self.zone.iteritems():
			Nir,Nip,_=zn.Rz.shape
			zn.Rzp=[]
#			zn.targetcells=[]
			for isegm in range(self.Nsegm):
				Rzp=np.zeros((Nir,Nip,self.Nit,3),order='F')*np.nan
				Rzp[:,:,self.Nithalf,0:2]=zn.Rz
				Rzp[:,:,self.Nithalf,2]=(self.Nithalf + isegm * (self.Nit-1))*self.dphi+self.phistart
				
				gr.expand3d(Rzp,self.dphi)				
				zn.Rzp.append(Rzp)
#				zn.targetcells.append(np.zeros((Nir-1,Nip-1,self.Nit-1),dtype=np.int32,order='F'))


	def set_wallexpansion_parameters(self,so=[1.0,5.0,5.0,   1.0,5.0,5.0,   0.001,0.001]):
		'''
			|--3-----4--|-----------
			|     
			2
			|     
			|     x---ipc1----------
			|     |
			1     |
			|     |
			------------------------
			the factors in 'so' stretch the intervals 1,2,3 left and 1,2,3 right followed by factors that move the left and right corners out. The intervals 4 are determined by the condition to fit the required length
		'''
            
		Nn=self.Nn
		
		self.zone['CORE'].ntype=0
		
		zn=self.zone['SOL']
		zn.ntype=1
		zn.regn0interval=[self.ixp+Nn+1,self.i2xp+Nn+1]
		zn.pc0=self.op*1.0;zn.pc1=self.xp*1.0;zn.plr=[self.xp*1.0,self.xp*1.0,[0,0],[0,0]];zn.so=np.array(so)*1.0
		
		zn=self.zone['PFR']
		zn.ntype=1
		zn.regn0interval=[self.ixp+Nn+1,self.ixp+Nn+1]
		zn.pc0=self.op*1.0;zn.pc1=self.xp*1.0;zn.plr=[self.xp*1.0,self.xp*1.0,[0,0],[0,0]];zn.so=np.array(so)*1.0



	def expand2wall(self):
		Nn=self.Nn
		
		self.plotc(self.wall,color='black',lw=2)
		
		for nam in self.zonelist:
			zn=self.zone[nam]
			NN=self.wall.N-1 if self.wall.isclosed() else self.wall.N
			ii=(np.arange(NN)+zn.nrotwall) % NN
			
			gr.set_wall(np.asfortranarray(self.wall.Rz[ii])*1.0)
			
			for isegm in range(self.Nsegm):
				Nir,Nip,Nit,_=zn.Rzp[isegm].shape
				if zn.ntype==1:
					zn.Rzp[isegm]=np.insert(zn.Rzp[isegm],[Nir]*Nn,np.nan,axis=0)
				if zn.ntype in [1,2]:#typ==2 is foreseen for x2 grids
					zn.Rzp[isegm]=np.insert(zn.Rzp[isegm],[0]*Nn+[Nip]*Nn,np.nan,axis=1)
					
					gr.expand2wall(zn.Rzp[isegm],zn.ntype,Nn,zn.regn0interval,zn.pc0,zn.pc1,zn.plr,zn.so)


	def glue_neutral_zones(self):
		pass


	def check3Dgrid(self):
		for nam,zn in self.zone.iteritems():
			zn.error3D=[]
			for isegm in range(self.Nsegm):
				err=gr.grid3d_check(zn.Rzp[isegm])
				zn.error3D.append(err)
				print 'zone %s segment %i has %i errors' % (nam,isegm,len(np.where(err)[0]))


	def get_Bgrid(self):
		for nam,zn in self.zone.iteritems():
			zn.B=[]
			for isegm in range(self.Nsegm):
				B=gr.get_b_grid(zn.Rzp[isegm])
				zn.B.append(B)


	def writeout_grid3d(self,outputpath='GEOMETRY/'):
		fg=open(outputpath+'grid.txt','w')
		fb=open(outputpath+'field.txt','w')
		
		for isegm in range(self.Nsegm):
			for nam in self.zonelist:
				Rzp=self.zone[nam].Rzp[isegm]
				#gr.writeg(Rzp)
				fg.write('%10i %10i %10i\n' % tuple(Rzp.shape[0:3]))
				for it in range(Rzp.shape[2]):
					fg.write('%16.8f \n' % (Rzp[0,0,it,2]*180./np.pi))
					np.savetxt(fg,np.array(Rzp[:,:,it,0]*100).reshape(-1,order='F'))
					np.savetxt(fg,np.array(Rzp[:,:,it,1]*100).reshape(-1,order='F'))
				np.savetxt(fb,self.zone[nam].B[isegm].reshape(-1,order='F'))
		fg.close()
		fb.close()

	
	def make_3Dgrid(self,outputpath='GEOMETRY/'):
		#bf.scale_components([1.0,1.0,1.0])
		self.expand3d()
		self.set_wallexpansion_parameters()
		#bf.scale_components([1.0,1.0,0.0])
		self.expand2wall()
		self.glue_neutral_zones()
		self.check3Dgrid()
		self.get_Bgrid()
		self.writeout_grid3d(outputpath=outputpath)
		self.allocate_targetcells()
		#self.plot3D(it=0)


	def plotc(self,c,**kwargs):
		if self.figax:
			self.figax[1].plot(c.R,c.z,**kwargs)
			self.figax[0].show()
			plt.pause(1e-6)
			self.step()


	def scatter(self,pp,**kwargs):
		if self.figax:
			if isinstance(pp,curve):
				self.figax[1].scatter(pp.R,pp.z,**kwargs)
			else:
				for p in pp:				
					self.figax[1].scatter([p[0]],[p[1]],**kwargs)
			self.figax[0].show()
			plt.pause(1e-6)
			self.step()


	def plotzn(self,nam,**kwargs):
		if self.figax:
			zn=self.zone[nam]
			Rz=zn.Rz
			if not 'color' in kwargs:kwargs['color']=zn.color
			#if not 'marker' in kwargs:kwargs['marker']='+'
			
			if not 'lw' in kwargs:kwargs['lw']=0.1
			for ir in range(Rz.shape[0]):
				self.figax[1].plot(Rz[ir,:,0],Rz[ir,:,1],**kwargs)
				
				#p=Rz[ir,self.Nithalf,:]
				#fl=tr.field_line([p[0],p[1],0],self.dphi,self.Nithalf,dir=1)
				#self.figax[1].plot(fl[:,0],fl[:,1],marker='o',ls=' ',**kwargs)

				#p=Rz[ir,-self.Nithalf-1,:]
				#fl=tr.field_line([p[0],p[1],0],self.dphi,self.Nithalf,dir=1)
				#self.figax[1].plot(fl[:,0],fl[:,1],marker='o',ls=' ',**kwargs)
				
			for ip in range(Rz.shape[1]):
				self.figax[1].plot(Rz[:,ip,0],Rz[:,ip,1],**kwargs)
			
			self.figax[0].show()
			plt.pause(1e-6)
			self.step()


	def plot3D(self,it=None):
		for nam in self.zonelist:
			self.plotzn3D(nam,it=it if it!=None else (self.Nit-1)/2,showerr=True,color=self.zone[nam].color)#,marker='x')


	def plotzn3D(self,nam,it=0,isegm=0,showerr=False,**kwargs):
		if self.figax:
			zn=self.zone[nam]
			Rz=zn.Rzp[isegm][:,:,it,0:2]
			if not 'color' in kwargs:kwargs['color']=zn.color
			if not 'lw' in kwargs:kwargs['lw']=0.1
			for ir in range(Rz.shape[0]):
				self.figax[1].plot(Rz[ir,:,0],Rz[ir,:,1],**kwargs)
			for ip in range(Rz.shape[1]):
				self.figax[1].plot(Rz[:,ip,0],Rz[:,ip,1],**kwargs)

			if showerr:
				indx=np.where(zn.error3D[isegm][:,:,it])
				if self.figax:
					for ir,ip in zip(indx[0],indx[1]):			
						jr=[ir,ir+1,ir+1,ir,ir]
						jp=[ip,ip,ip+1,ip+1,ip]
						self.figax[1].plot(zn.Rzp[isegm][jr,jp,it,0],zn.Rzp[isegm][jr,jp,it,1],color='black',lw=2)
			
			self.figax[0].show()
			plt.pause(1e-6)
			self.step()

	def ploterr3D(self,nam,its=None,isegms=[0],showerr=False,**kwargs):
		if self.figax:
			if its==None:its=range(self.Nit)
			for isegm in isegms:
				zn=self.zone[nam]
				for it in its:
					indx=np.where(zn.error3D[isegm][:,:,it])
					for ir,ip in zip(indx[0],indx[1]):			
						jr=[ir,ir+1,ir+1,ir,ir]
						jp=[ip,ip,ip+1,ip+1,ip]
						self.figax[1].plot(zn.Rzp[isegm][jr,jp,it,0],zn.Rzp[isegm][jr,jp,it,1],color='black',lw=2)
			
			self.figax[0].show()
			plt.pause(1e-6)
			self.step()

	def step(self):
		if self.step_mode:
			print 'Step '+str(self.istep)+' called from '+ inspect.stack()[2][3]
			if (self.istep>=self.Nskipsteps):
				raw_input(' Press Enter to continue.')
				
		#if self.figax:
			#self.figax[0].savefig('output/fig%003i.png' % self.istep)
			
		self.istep+=1


	def check_zone(self,nam,**args):
		#print 'checking zone ',nam
		zn=self.zone[nam]
		Rz=zn.Rz
		zn.error=gr.grid2d_check(Rz)
		
		zn.errorindx=indx=np.where(zn.error)
		print 'zone ',nam,' has ',len(indx[0]),' errors'
		if self.figax:
			for ir,ip in zip(indx[0],indx[1]):			
				jr=[ir,ir+1,ir+1,ir,ir]
				jp=[ip,ip,ip+1,ip+1,ip]
				self.figax[1].plot(Rz[jr,jp,0],Rz[jr,jp,1],**args)


	def plot_erroroutput(self,fn,col='red'):
		if self.figax:
			for e in open(fn,'r').read().split('X-like mesh or overlapping cell detected\n')[1:]:
				ll=e.split('\n')
				x=np.fromstring(ll[2],dtype=float,sep=' ')/100
				y=np.fromstring(ll[3],dtype=float,sep=' ')/100
				self.figax[1].plot(x,y,color=col,lw=4)
				
			self.figax[0].show()
			plt.pause(1e-6)
			self.step()




class zone(object):
	def __init__(self,Nir,Nip,color='blue'):
		object.__init__(self)
		self.Rz=np.zeros((Nir,Nip,2))*np.nan
		self.ntype=0 # defines the way how to expand the grid for the neutrals
		self.nrotwall=0
		self.color=color
		self.update()
		
	
	def update(self):
		self.R=self.Rz[:,:,0]
		self.z=self.Rz[:,:,1]		
	
	def interpol_rad_sf(self,ir,p):
		if isinstance(p,int):
			pp=np.linspace(0,1,p+2)[1:-1]
		elif isinstance(p,np.ndarray):
			pp=p
		else:
			raise RuntimeError('call this routine either with an integer value or alist 0..values..1')
		
		N=len(pp)
		self.Rz=np.insert(self.Rz,[ir]*N,0,axis=0)
		ir1=ir-1;ir2=ir+N
		for iir,x in enumerate(pp):
			self.Rz[iir+ir1+1,:,:]=self.Rz[ir1,:,:]*(1-x)+self.Rz[ir2,:,:]*x

	#def remove_sf(self,i,axis=0):
		#self.Rz=np.delete(self.Rz,i,axis=axis)

	def insert_sf(self,i='end',N=1,axis=0,val=np.nan):
		if i in ['end',-1]:
			i=self.Rz.shape[axis]
		self.Rz=np.insert(self.Rz,[i]*N,val,axis=axis)
				
	#def writeout(self,fn,scaling_factor=100.0):
		#f=open(fn,'w')
		#Nir,Nip,_=self.Rz.shape
		#f.write('%i  %i\n' % (Nir,Nip))
		#np.savetxt(f,self.Rz.reshape(-1,order='F')*scaling_factor,fmt='%.10e')
		#f.close()
		
	def reverse(self,axis=0):
		if axis==0:
			self.Rz=self.Rz[::-1,:,:]
		elif axis==1:
			self.Rz=self.Rz[:,::-1,:]
		else:
			raise RuntimeError('undefined value for axis')
		self.R=self.Rz[:,:,0]
		self.z=self.Rz[:,:,1]
		
