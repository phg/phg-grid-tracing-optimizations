import numpy as np
import matplotlib.pyplot as plt
import time
from grid2xSOLPS import *
from field_routines import baxisym as bas
import argparse
import os
sys.path.append('../AUG/')
from machine_geometry import *

AUGxml=open_machine(fn='../AUG/MD_AUG.xml',ishot=35840,state='planning_ud_2018_06')

all_pfcs=xml_get_element(AUGxml,xpath='.//modelling_elements/*[@name="all_pfcs"]/pol_contour')


#read the command line parameters or define default values
parser=argparse.ArgumentParser()
parser.add_argument("-o","--output",default='',type=str)
parser.add_argument("-op","--outputpath",default='GEOMETRY/',type=str)
parser.add_argument("-prefix",default='AUG_upper_SF-LFS',type=str)
args=parser.parse_args()


fig,ax =plt.subplots(figsize=(22,16))

ax.plot(all_pfcs[:,0],all_pfcs[:,1])
ax.set_xlim(1.2,1.7)
ax.set_ylim(0.8,1.15)

ax.set_aspect('equal')

g=grid2xSOLPS(Nithalf=1)#,Nithalf=1Npcore=399

#comment out the following line to switch off graphical output during the generation process and speed up significantly
g.figax=[fig,ax]

#comment in the following line to enter a kind of debugging mode
#g.step_mode=True;g.Nskipsteps=165

bas.set_tolerance(1e-8)   #maximum tolerated flux inaccuracy for the X-point search

g.load_magnetics('../AUG/pyequil_35840_02500ms_Do1-12000A_Do2+12500_V3+4180A.xml')
bas.readjust_oxp()

g.add_pfcs(all_pfcs)

g.xpcurve1par=[[0.04,0.0],[0.04,15.0],[0.3,15.0]]
g.xpcurve2par=[[0.04,0.0],[0.04,-15.0],[0.3,-20.0]]

g.xp2curve3par=[[0.03,0.0],[0.4,20]]

g.xpcurveppar=[[0.15,5]]

g.legrf[4]=3.0

g.Np1=7*5;g.Np2=8*5

g.target_range={1:[-0.05,0.25,'dsposflx','red'],2:[-0.05,0.05,'dsposflx','blue'],3:[-0.15,0.05,'ds','magenta'],4:[-0.05,0.03,'ds','cyan']}

g.rf[1]=2;g.rf[2]=2
g.rf[3]=2;g.rf[4]=2
g.legrf[1]=1.5
g.legrf[2]=2.0
g.legrf[3]=2.0

g.fcreg1=0.06;g.fcreg2=0.93;

g.prepare()
		
g.sequences['FARSOL13'][5]+=[[2,g.i2xp,g.xpcurve2,None]]

g.make_core(np.linspace(0.97,1,10)[:-1]**0.5,color='red')
#g.make_zone('CORE',dr=np.linspace(1.5,0,10)[:-1]*5e-2,color='red')#PsiN=[0.9,0.95,0.98])

g.make_zone('SOL',drn=np.linspace(0.0,1.0,10)[1:-1],color='blue')

g.make_zone('PFR',dr=np.linspace(0,0.11,10)[1:],color='green')
g.make_zone('FARSOL42',dr=np.linspace(0,0.04,10)[1:],color='magenta')

dr=np.linspace(0,0.17,10)[1:]
dr[-1]=dr[-2]+0.001
dr[-3]+=0.002
g.make_zone('FARSOL13',dr=dr,color='brown')
g.make_zone('FARSOL43',dr=np.linspace(0,0.04,19)[1:],color='black')




#g.step_mode=True;g.Nskipsteps=0

g.connectSOL2sep2()

g.figax=[fig,ax]

jj=range(g.Npcore)
ii=[2, 4, 6,7, 9,10, 12,13,14, 16,17,18]+jj[20:200:5]+jj[21:200:5]+jj[22:200:5]+jj[23:200:5]

g.thin_out_core(ii+list(g.Npcore-1-np.array(ii)))

g.make_guard_cells()

g.plotc(g.pfcs,lw=2,color='black')
for k in g.zone.keys():g.plotzn(k)



figmap=plt.subplots(figsize=(30,10))


g.make_SOLPS(figmap=figmap,fn=args.outputpath+args.prefix)


'''
if False: #make the triangles for the neutrals
	v=curve('vessel.txt',scaling_factor=1e-2)
	b=g.outer_boundary(fn=prefix+'outer_boundary.txt')
	import pickle
	components=pickle.load(open('components.p','r'))


	del(components['PFCs']['Gefaess_eng'])
	del(components['PFCs']['ICRHa'])

	#b.remove_linear_points(tolerance=1e-4)

	exclude=[b]

	for k,st in components['PFCs'].iteritems():
		news=not (('TPRT' in k) or ('TPLT' in k))		
		for Rc,zc,Rw,zw,Iw in st['elements']:
			if news:# and (N<2):#k in ['cryo']:
				exclude.append(curve(np.vstack((Rc,zc)).T,close=True))

	tt=triangulate_area(include=[v],exclude=exclude,holes=[[1.6,0]])

	f=open(prefix+'triangles.txt','w')
	for t in tt:
		#t=np.hstack((t,t[0]))
		ax.fill(t[:,0],t[:,1],color='lightgrey')
		ax.plot(t[:,0],t[:,1],color='grey',zorder=30)
		f.write(('%20.9f  '*9+'\n') % tuple(t.reshape(-1)))
	fig.show()
	f.close()
'''


ax.set_xlabel('R [m]')
ax.set_ylabel('z [m]')

fig.tight_layout()	
if args.output!='':
  se=os.path.splitext(args.outputpath+'/'+args.output)   
  fig.savefig(se[0]+'_grid'+se[1])

fig,ax=figmap
#ax.set_xlim(-3,620)
ax.set_xlim(-3,160)

ax.set_ylim(27,-5)
ax.set_xlabel('poloidal index')
ax.set_ylabel('radial index')

#g.map2block()
fig.tight_layout()	

if args.output!='':
  se=os.path.splitext(args.outputpath+'/'+args.output)   
  fig.savefig(se[0]+'_map'+se[1])
  
fig.show()
