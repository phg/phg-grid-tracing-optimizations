import numpy as np
import matplotlib.pyplot as plt
import time
from gridSOLPS import *
from field_routines import baxisym as bas
#from field_routines import trace as tr

fig,ax =plt.subplots(figsize=(15,13))
#fig,ax =plt.subplots(figsize=(22,16))

ax.set_xlim(1.2,1.7)
ax.set_ylim(0.8,1.15)

ax.set_aspect('equal')

g=gridSOLPS(Nithalf=1)#,Nithalf=1Npcore=399

#comment out the following line to switch off graphical output during the generation process and speed up significantly
#g.figax=[fig,ax]

#comment in the following line to enter a kind of debugging mode
#g.step_mode=True;g.Nskipsteps=5

bas.set_tolerance(1e-8)   #maximum tolerated flux inaccuracy for the X-point search


g.load_equilibrium('../upper_divertor/psi_SN.txt')#,Bo=-1.4388,Ro=0.88

g.oxp_estimates={'op':[1.6900536,0.02988712],'xp':[1.41188521,  0.95110978],'xp2':[ 1.5163847 , -1.08389697]}	
g.add_pfcs('pfcs_ud.txt')
g.xpcurve1par=[[0.05,0.0],[0.35,22.0]]
g.xpcurve2par=[[0.4,0.0]]
g.xpcurveppar=[[0.15,-5.0]]

g.Np1=7*5;g.Np2=13*5

#g.dsp=0.07
g.target_range={1:[-0.05,0.2,'dsposflx','red'],2:[-0.05,0.15,'dsposflx','blue']}

g.rf[1]=2;g.rf[2]=2
g.fcreg1=0.06

b=time.time()
g.prepare()
print time.time()-b,'s'


g.make_core(np.linspace(0.97,1,10)[:-1]**0.5,color='red')
g.make_zone('SOL',dru=np.linspace(0.0,0.019,20)[1:-1],color='blue')
g.make_zone('PFR',dr=np.linspace(0,0.11,10)[1:],color='green')


g.figax=[fig,ax]

jj=range(g.Npcore)
ii=[2, 4, 6,7, 9,10, 12,13,14, 16,17,18]+jj[20:200:5]+jj[21:200:5]+jj[22:200:5]+jj[23:200:5]

g.thin_out_core(ii+list(g.Npcore-1-np.array(ii)))

g.make_guard_cells()


g.plotc(g.pfcs,lw=2,color='black')
for k in g.zone.keys():g.plotzn(k)


figmap=plt.subplots(figsize=(30,10))


prefix='AUG_upper_SN'
g.make_SOLPS(figmap=figmap,fn=prefix)

if False: #make the triangles for the neutrals
	v=curve('vessel.txt',scaling_factor=1e-2)
	b=g.outer_boundary(fn=prefix+'outer_boundary.txt')
	import pickle
	components=pickle.load(open('components.p','r'))


	del(components['PFCs']['Gefaess_eng'])
	del(components['PFCs']['ICRHa'])

	#b.remove_linear_points(tolerance=1e-4)

	exclude=[b]

	for k,st in components['PFCs'].iteritems():
		news=not (('TPRT' in k) or ('TPLT' in k))		
		for Rc,zc,Rw,zw,Iw in st['elements']:
			if news:# and (N<2):#k in ['cryo']:
				exclude.append(curve(np.vstack((Rc,zc)).T,close=True))

	tt=triangulate_area(include=[v],exclude=exclude,holes=[[1.6,0],[1.414,1.141]])

	f=open(prefix+'triangles.txt','w')
	for t in tt:
		#t=np.hstack((t,t[0]))
		ax.fill(t[:,0],t[:,1],color='lightgrey')
		ax.plot(t[:,0],t[:,1],color='grey',zorder=30)
		f.write(('%20.4f  '*9+'\n') % tuple(t.reshape(-1)))
	fig.show()
	f.close()

ax.set_xlabel('R [m]')
ax.set_ylabel('z [m]')

fig.tight_layout()	
fig.savefig('grid.pdf')
fig,ax=figmap
#ax.set_xlim(-3,620)
ax.set_xlim(-3,126)

ax.set_ylim(27,-5)
ax.set_xlabel('poloidal index')
ax.set_ylabel('radial index')

fig.tight_layout()	

fig.savefig('map.pdf')

fig.show()
