import numpy as np
import matplotlib.pyplot as plt
import time
from grid2x import *
from field_routines import baxisym as bas
from make_inputfiles_def import *
import argparse
import os

#read the command line parameters or define default values
parser=argparse.ArgumentParser()
parser.add_argument("-o","--output",default='',type=str)
parser.add_argument("-op","--outputpath",default='GEOMETRY/',type=str)
args=parser.parse_args()


fig,ax =plt.subplots(figsize=(8,13))

ax.set_aspect('equal')

g=grid2x()

#comment out the following line to switch off graphical output during the generation process and speed up significantly
#g.figax=[fig,ax]

#comment in the following line to enter a kind of debugging mode
#g.step_mode=True;g.Nskipsteps=0

bas.set_tolerance(1e-8)   #maximum tolerated flux inaccuracy for the X-point search

g.oxp_estimates={'op':[0.88,0.4]}
g.load_equilibrium('../TCV/psi_44826t0.8000_sigma_0.65_theta_010n.txt',scale_psi=-2*np.pi,Bo=-1.4388,Ro=0.88)

g.add_pfcs('../TCV/pfcs_TCV.txt',scaling_factor=1.0e-2)
g.xpcurve1par=[[0.3,-30]]
g.xpcurve2par=[[0.3,-30]]
g.xp2curve3par=[[0.2,20]]
g.xp2curve4par=[[0.2,10]]
g.xpcurveppar=[[0.3,-45]]
g.xp2curveppar=[[0.2,10]]
g.xpcurvecpar=[[0.03,-40],[0.15,40]]
g.Np1=15*5;g.Np2=25*5
g.xp2reg_linearization=True # good for TCV


g.dsp=0.05
g.rf1=1;g.rf2=1
g.rf3=1;g.rf4=1
g.legrf4=1.8
g.legrf3=1.81


b=time.time()
g.prepare()
print time.time()-b,'s'


g.make_zone('CORE',dr=np.linspace(1.5,0,10)[:-1]*5e-2,color='red')#PsiN=[0.9,0.95,0.98])

g.make_zone('SOL',drn=np.linspace(0.0,1.0,10)[1:-1],color='blue')
g.make_zone('PFR',dr=np.linspace(0,0.09,10)[1:],color='green')
g.make_zone('FARSOL42',dr=np.linspace(0,0.1,10)[1:],color='magenta')
g.make_zone('FARSOL13',dr=np.linspace(0,0.1,10)[1:],color='brown')
g.make_zone('FARSOL43',dr=np.linspace(0,0.1,19)[1:],color='black')


#g.figax=[fig,ax]
#g.step_mode=True;g.Nskipsteps=0



#g.glue_zones()
g.connectSOL2sep2()

for k in g.zone.keys():g.plotzn(k)

for k in g.zone.keys():g.check_zone(k,lw=5,color='black')


#g.check_zone('CORE',lw=5,color='black')
#g.check_zone('SOL',lw=5,color='black')
#g.check_zone('PFR',lw=5,color='black')
#g.check_zone('FARSOL42',lw=5,color='black')
#g.check_zone('FARSOL13',lw=5,color='black')
#g.check_zone('FARSOL43',lw=5,color='black')



#g.step_mode=True;g.Nskipsteps= 24;#11
g.figax=[fig,ax]

ax.plot(g.pfcs.R,g.pfcs.z,color='black')

#ax.set_xlim(-3,620)
#ax.set_ylim(-0.5,0)


g.wall=curve('../TCV/pfcs_TCV.txt',scaling_factor=1.e-2)
ax.scatter([g.wall.R[0]],[g.wall.z[0]])
g.zone['FARSOL42'].nrotwall=10 # rotates the indexing of the wall points when making the neutral part of this zone in order to avoid a discontinuity.

#g.wall.rotate_beginning(10)


g.wall.Rzl[:,0]=((g.wall.Rzl[:,0]-0.8)*1.5+0.8)
g.wall.Rzl[:,1]*=1.2

g.wall.update()

g.make_3Dgrid(outputpath=args.outputpath)

g.plot3D(it=5)

set_input_file_parameters(g)
write_input_files(g,outputpath=args.outputpath)

targets=np.vstack((g.pfcs.Rz,g.wall.Rz[::-1]))

#ax.fill(targets[:,0],targets[:,1],color='yellow',zorder=-10)

start=time.time()
zl=['SOL','PFR','FARSOL13', 'FARSOL43', 'FARSOL42']
g.compute_2Dtarget_intersection({'targets':targets},zonelist=zl)
#for nam in zl:g.plot_targecells(nam=nam,it=5)
g.writeout_intersection(outputpath=args.outputpath)
print 'time for compute_target_intersection',time.time()-start

if args.output!='':
  se=os.path.splitext(args.outputpath+'/'+args.output)   
  fig.savefig(se[0]+'_grid'+se[1])


fig.show()

