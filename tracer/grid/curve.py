import numpy as np
from scipy.ndimage import zoom
from scipy.spatial import Delaunay
from scipy.interpolate import splev, splrep
import sys
sys.path.append('../build/.')
from field_routines import tools as tls

def length_angle_list2curve(p,v,la):
	v/=np.linalg.norm(v)
	c=curve([p,p+v*la[0][0]])
	c.rotate(la[0][1]*np.pi/180.,i1=-2)
	for da in la[1:]:
		c.extend(da[0])
		c.rotate(da[1]*np.pi/180.,i1=-2)
				
	return c


class curve(object):
	def __init__(self,D,scaling_factor=1.0,refinement=1,close=False,remove_Nan=False):
		"""initiate the pyField class"""
		object.__init__(self)
		
		if isinstance(D,basestring):
			f=open(D,'r')
			Np=np.fromstring(f.readline(),dtype=int,count=1,sep=' ')[0]
			self.Rzl=np.zeros((Np,3))
			#scale=args['scale'] if 'scale' in args.keys() else 1.0
			self.Rzl[:,0:2]=np.fromfile(f,dtype=float,count=2*Np,sep=' ').reshape(Np,2)*scaling_factor
			f.close()
			
		elif isinstance(D,np.ndarray):
			self.Rzl=np.zeros((len(D),3))		
			self.Rzl[:,0:2]=D[:,:]			
		elif isinstance(D,list):
			self.Rzl=np.zeros((len(D),3))
			self.Rzl[:,0:2]=np.array(D)
		elif isinstance(D,int):
			self.Rzl=np.zeros((D,3))			
		else:
			raise RuntimeError('Cannot initiate curve object.')
		
		if remove_Nan:self.Rzl = self.Rzl[~np.isnan(self.Rzl[:,0]),:]
		self.update()
		if close:self.close()
		if refinement>1:self.refine(refinement)
		self.collisions=[]

	def add(self,D,scaling_factor=1.0,refinement=1,remove_Nan=False,close=False):
			
		if isinstance(D,np.ndarray):
			Rzl=np.zeros((len(D),3))		
			Rzl[:,0:2]=D[:,:]			
		else:
			raise Exception('So far only numpy arrays are implemented.')
		if remove_Nan:Rzl = Rzl[~np.isnan(Rzl[:,0]),:]
		self.Rzl=np.vstack((self.Rzl,Rzl))
		self.update()
		if close:self.close()		
		
            
	def refine(self,factor):
		self.Rzl=zoom(self.Rzl,[factor,1],order=1)
		self.update()

	def update(self):
		self.Rz=self.Rzl[:,0:2]
		self.R=self.Rzl[:,0]
		self.z=self.Rzl[:,1]
		self.l=self.Rzl[:,2]				
		self.dl=dl=np.linalg.norm(self.Rz[1:]-self.Rz[:-1],axis=1)
		self.l[:]=np.insert(np.cumsum(dl),0,0.0)
		self.totl=self.l[-1]
		#self.reflength=self.totl
		self.N=self.Rzl.shape[0];self.Nhalf=(self.N-1)/2

	def l2Rz(self,l,output_format='Rz'):
		R=np.interp(l,self.l,self.Rz[:,0],left=np.nan)
		z=np.interp(l,self.l,self.Rz[:,1],left=np.nan)
		if output_format=='Rz':
			return np.array([R,z]).T
		if output_format=='Rz0':
			return np.array([R,z,R*0.0]).T
		elif output_format=='curve':
			return curve(np.array([R,z]).T)
		else:
			raise RuntimeError('Unknown output_format.')
		
	#def ln2Rz(self,ln,**kwargs):
		#return l2Rz(ln*self.reflength)
		  
	def Rz2i(self,Rz,dist=0.01):
		d=np.linalg.norm(self.Rz-Rz,axis=1)		
		return np.where(d<dist)[0]

	def l2i(self,l):
		return np.interp(l,self.l,np.arange(self.N))
	
	def attractive_point(self,x,dist=0.01):
		ii=self.Rz2i(x,dist=dist)
		for i in ii:self.Rz[i]=x
		self.update()
		



	def collide(self,c,l1min=0.0,l2min=0.0):
		return tls.curve_curve_coll_rzl(self.Rzl,c.Rzl,l1min=l1min,l2min=l2min)
		#c1=self.Rz;c2=c.Rz;dl1=self.l[1:]-self.l[:-1];dl2=c.l[1:]-c.l[:-1];
		#for i1 in range(len(c1)-1):
			#if dl1[i1]>0:
				#for i2 in range(len(c2)-1):
					#if dl2[i2]>0:
						#params = np.linalg.solve(np.column_stack((c1[i1+1]-c1[i1], c2[i2]-c2[i2+1])),c2[i2]-c1[i1])
						#if np.all((params >= 0) & (params <= 1)):
							#Rzl1=self.Rzl[i1] + params[0]*(self.Rzl[i1+1] - self.Rzl[i1])
							#l2=c.l[i2] + params[1]*(c.l[i2+1] - c.l[i2])                                                    
							#self.collisions=np.hstack((Rzl1,l2))
							#return self.collisions
		#return np.array([])

	def collide_all(self,c):
		c1=self.Rz;c2=c.Rz;dl1=self.l[1:]-self.l[:-1];dl2=c.l[1:]-c.l[:-1];self.collisions=[]
		
		for i1 in range(len(c1)-1):
			if dl1[i1]>0:
				for i2 in range(len(c2)-1):
					if dl2[i2]>0:
						params = np.linalg.solve(np.column_stack((c1[i1+1]-c1[i1], c2[i2]-c2[i2+1])),c2[i2]-c1[i1])
						if np.all((params >= 0) & (params <= 1)):
							Rzl1=self.Rzl[i1] + params[0]*(self.Rzl[i1+1] - self.Rzl[i1])
							l2=c.l[i2] + params[1]*(c.l[i2+1] - c.l[i2])    							
							self.collisions.append(np.hstack((Rzl1,l2)))
				
		self.collisions=np.array(self.collisions)
		return self.collisions

	def reverse(self):
		self.Rzl[:,:]=self.Rzl[::-1,:]
		self.update()
	
	def isclosed(self):
		return np.all(self.Rzl[0,0:2]==self.Rzl[-1,0:2])
	
	def close(self):
		if not self.isclosed():
			self.Rzl=np.insert(self.Rzl,len(self.Rzl),self.Rzl[0]*1.0,axis=0)
			self.update()			
		
	def rotate_beginning(self,ip0,close=False):
		if close:self.close()
		Rzl=self.Rzl
		if not self.isclosed():
			raise RuntimeError('This function can only be called if the curve is closed.')
		Nip,_=Rzl.shape
		ip=np.append((np.arange(Nip-1)+ip0) % (Nip-1),ip0)
		Rzl[:,0:2]=Rzl[ip,0:2]
		self.update()
	
	def insert_point(self,l=None,ray=None):#, Rzi=None
		if ray!=None:
			l=self.collide(ray)[2]
#		if Rzi==None:
		R=np.interp(l,self.l,self.Rz[:,0],left=np.nan)
		z=np.interp(l,self.l,self.Rz[:,1],left=np.nan)
		i=np.interp(l,self.l,np.arange(len(self.l)),left=np.nan)
		#else:
			#R,z,i=Rzi
			#if i<0:
				#i+=Rzl.shape[0]
		self.Rzl=np.insert(self.Rzl,int(i)+1,[R,z,l],axis=0)
		self.update()
		return i+1

	def extract_interval(self,l0=None,l1=None,ray0=None,ray1=None):
		if ray0!=None:
			l0=self.collide(ray0)[2]
		if ray1!=None:
			l1=self.collide(ray1)[2]
		
		swapped=(l0>l1)
		if swapped:
			l1,l0=l0,l1
			
		ii=np.where((self.l>=l0)*(self.l<=l1))[0]
		if len(ii)==0:
			return self.l2Rz([l0,l1],output_format='curve')
		res=curve(self.Rz[ii])
		if self.l[ii[0 ]]!=l0:
			res.extend(self.l2Rz(l0),side='beginning')
		if self.l[ii[-1]]!=l1:
			res.extend(self.l2Rz(l1),side='end')
		if swapped:res.reverse()
		return res
	
	def extend(self,d,side='end'):#,typ='relative'):
		if isinstance(d,np.ndarray):
			if d.shape!=(2,):
				raise RuntimeError('d must either be a float or a 2D point (pair of floats).')
			p=d
		elif isinstance(d,float):
			if side=='end':
				#if typ=='absolute':d/=self.l[-1]-self.l[-2]
				p=self.Rz[-1]+(self.Rz[-1]-self.Rz[-2])/np.linalg.norm((self.Rz[-1]-self.Rz[-2]))*d
			else:
				#if typ=='absolute':d/=self.l[1]-self.l[0]
				p=self.Rz[0]+(self.Rz[0]-self.Rz[1])/np.linalg.norm((self.Rz[0]-self.Rz[1]))*d
		else:	
			raise RuntimeError('d must either be a float or a 2D point (pair of floats).')
		
		p=np.hstack((p,0))
		i=len(self.Rzl) if side=='end' else 0
		self.Rzl=np.insert(self.Rzl,i,p,axis=0)
		self.update()
		
	def isinside(self,p):
		'''
		Detects if y point p is inside a closed curve. 
		'''
		if not self.isclosed():
			return false
		x=p[0];y=p[1]
		n = len(self.Rz)
		inside = False
		p1x,p1y = self.Rz[0]
		for i in range(n+1):
			p2x,p2y = self.Rz[i % n]
			if y > min(p1y,p2y):
				if y <= max(p1y,p2y):
					if x <= max(p1x,p2x):
						if p1y != p2y:
							xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
						if p1x == p2x or x <= xinters:
							inside = not inside
			p1x,p1y = p2x,p2y
		return inside

	def construct_point_inside(self,d=1e-6,i=0):
		if self.N<2:
			raise Exception('number of points must be at least 2')
		if self.Rzl[1,2]==0:
			raise Exception('The function only works if the second point is not equal to the first one.')
		
		v=self.Rz[i+1]-self.Rz[i]
		s=v/np.linalg.norm(v);s=np.array([-s[1],s[0]])
		p=self.Rz[i]+0.5*v
		if self.isinside(p+s*d):
			return p+s*d
		elif self.isinside(p-s*d):
			return p-s*d
		else:
			raise Exception('No point found.')

	def triangulate(self):
		def point_in_poly(x,y,poly):
			n = len(poly)
			inside = False
			p1x,p1y = poly[0]
			for i in range(n+1):
				p2x,p2y = poly[i % n]
				if y > min(p1y,p2y):
					if y <= max(p1y,p2y):
						if x <= max(p1x,p2x):
							if p1y != p2y:
								xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
							if p1x == p2x or x <= xinters:
								inside = not inside
				p1x,p1y = p2x,p2y
			return inside

		p=self.Rz.copy()
		if not ((p[0,0]==p[-1,0]) and (p[0,1]==p[-1,1])):
			p=np.vstack((p,p[0,:]))  #close the polygon if necessary
		
		alltri = Delaunay(p).simplices
		
		indexlist=[];trilist=[]
		for tr in alltri:
			xm=np.mean(p[tr,0]);ym=np.mean(p[tr,1])
			if point_in_poly(xm,ym,p):
				indexlist.append(tr)
				trilist.append([p[tr,0],p[tr,1]])
			
		return trilist,indexlist

	def chamfer_corner(self,i,l):
		p=self.Rz[i]
		v1=self.Rz[i+1]-p;v1/=np.linalg.norm(v1)
		v2=self.Rz[i-1]-p;v2/=np.linalg.norm(v2)
		a2=np.dot(v1,v2)
		d=l/(2.0*np.cos(a2/2.0))
		self.Rzl=np.insert(self.Rzl,int(i+1),[0,0,0],axis=0)
		self.Rzl[i,0:2]=p+v2*d
		self.Rzl[i+1,0:2]=p+v1*d
		self.update()
	
	
	def remove_linear_points(self,tolerance=1e-5):
		#ii=[];Rz=self.Rz
		#for i in range(self.N-3):
			#v=Rz[i+2]-Rz[i]
			#p=np.linalg.norm(np.cross(v,Rz[i+1]-Rz[i]))/np.linalg.norm(v)
			#if p<tolerance:ii.append(i+1)
		#self.Rzl=np.delete(self.Rzl,ii,axis=0)
		
		Rz=self.Rz;Rzln=self.Rzl[0,:]*1.0
		i=0;j=1
		while (i<self.N-2):
			v=Rz[i+1]-Rz[i];v/=np.linalg.norm(v)
			p=0.0;j=i+1
			while (p<tolerance) and (j<self.N-1):
				j+=1
				p=np.linalg.norm(np.cross(Rz[j]-Rz[i],v))
			Rzln=np.vstack((Rzln,self.Rzl[j-1,:]))
			i=j-1
		Rzln=np.vstack((Rzln,self.Rzl[-1,:]))	
		self.Rzl=Rzln
		self.update()
		
	
	def spline_smooth_interval(self,i1,i2,N):
		if i1<0:i1+=self.Rzl.shape[0]
		if i2<0:i2+=self.Rzl.shape[0]		
		if i1>i2:i1,i2 = i2,i1

		#print i1,i2
		#print self.l[i1:i2+1], self.R[i1:i2+1]
		splR = splrep(self.l[i1:i2+1], self.R[i1:i2+1])
		splz = splrep(self.l[i1:i2+1], self.z[i1:i2+1])
		ll = np.linspace(self.l[i1], self.l[i2], N)
		Rzlnew=np.zeros((N,3))
		Rzlnew[:,0]=splev(ll, splR)
		Rzlnew[:,1]=splev(ll, splz)
		
		if i1>=2:
			Rzlnew=np.vstack((self.Rzl[0:i1,:],Rzlnew))
		if i2<=self.Rzl.shape[0]-2:
			Rzlnew=np.vstack((Rzlnew,self.Rzl[i2+1:,:]))
		#print Rznew	
		self.Rzl=Rzlnew
		self.update()
		
	def rotate(self,angle,i1=0,i2=-1,prot=None,irot=None):
		#print 'i1,i2',i1,i2
		if i1<0:i1+=self.Rzl.shape[0]
		if i2<0:i2+=self.Rzl.shape[0]		
		
		#print 'i1,i2',i1,i2
		
		if irot!=None:
			prot=self.Rz[irot,:]*1.0
		
		if prot==None:
			prot=self.Rz[i1,:]*1.0
		
		if i1>i2:i1,i2 = i2,i1
		prot=np.array(prot)
		#print self.Rzl.shape,prot.shape
		
		##matmul is not available before numpy version 1.11.1
		#rotMatrix = np.matrix([[np.cos(angle), -np.sin(angle)],[np.sin(angle),  np.cos(angle)]])
		#for i in range(i1,i2+1):
			#self.Rzl[i,0:2]=np.matmul(rotMatrix,self.Rz[i]-prot)+prot
		
		for i in range(i1,i2+1):
			v=self.Rzl[i,0:2]-prot
			self.Rzl[i,0]=v[0]*np.cos(angle) - v[1]*np.sin(angle) + prot[0]
			self.Rzl[i,1]=v[0]*np.sin(angle) + v[1]*np.cos(angle) + prot[1]
			
			
		self.update()
	
	def write_to_file(self,fn,scaling_factor=1.0):
		f=open(fn,'w')
		f.write('%i\n' % len(self.Rz))
		for rz in self.Rz*scaling_factor:
			f.write('%f  %f\n' % (rz[0],rz[1]))
		f.close()
		
	def write_out_as_target(self,fn,phi=[0.0],scaling_factor=100.0,comment=None):
		if comment==None:comment=fn
		f=open(fn,'w')
		f.write(comment+'\n')
		f.write('%i  %i   1      0.00000      0.00000\n'% (len(phi),self.Rz.shape[0]))
		for p in phi:
			f.write("%15.5f\n" % p)
			for cc in self.Rz*scaling_factor:f.write("%15.5f   %15.5f\n" % (cc[0],cc[1]))
		f.close()
		
	def remove_zerolength_intervals(self,tolerance=1e-10):
		dl=np.diff(self.Rzl[:,2])
		ii=np.hstack(([0],np.where(dl>tolerance)[0]+1))
		self.Rzl=self.Rzl[ii,:]
		self.update()

def stuff_interval(dl1,L,dl2, intermediate_only=True):
	''' returns a sequence of values l_i=-dl1,0,...,L,L+dl2 such that (l[i+2]-l[i+1])/(l[i+1]-l[i])=const (approximately), i.e. a geometric series.
	If intermediate_only=True only the values inbetween 0..L excluding the end values are given back
	'''
	r=1.0 + dl2/L - dl1/L

	print r

	N=int(round(np.log(dl2/dl1)/np.log(r)))

	r=(dl2/dl1)**(1.0/N)

	l=dl1*(1.0-r**np.arange(N+2))/(1.0-r)

	l=(l-l[1])/(l[N]-l[1])*L

	#print 'stuff_interval',N
	if intermediate_only:
		return l[2:-2]
	else:
		return l

def area_intersection(include=[],exclude=[]):
		def point_in_poly(x,y,poly):
			n = len(poly)
			inside = False
			p1x,p1y = poly[0]
			for i in range(n+1):
				p2x,p2y = poly[i % n]
				if y > min(p1y,p2y):
					if y <= max(p1y,p2y):
						if x <= max(p1x,p2x):
							if p1y != p2y:
								xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
							if p1x == p2x or x <= xinters:
								inside = not inside
				p1x,p1y = p2x,p2y
			return inside

		
		p=np.zeros((0,2))
		for pp in include+exclude:
			if not pp.isclosed(): raise Exception('you can call this function only with closed polygons')
			p=np.vstack((p,pp.Rz))
		
		alltri = Delaunay(p).simplices
		
		indexlist=[];trilist=[]
		for tr in alltri:
			xm=np.mean(p[tr,0]);ym=np.mean(p[tr,1])
			fnd=False
			
			for pp in include:fnd=fnd or point_in_poly(xm,ym,list(pp.Rz))
			for pp in exclude:fnd=fnd and not point_in_poly(xm,ym,list(pp.Rz))
			
			if fnd:
				trilist.append([p[tr,0],p[tr,1]])
			
		return trilist,indexlist	

def make_circle(N,x0,r):
	p=np.zeros((N-1,2))
	a=np.linspace(0,np.pi*2,N)[:-1]
	p[:,0]=np.cos(a)*r
	p[:,1]=np.sin(a)*r
	return curve(p+np.array(x0),close=True)

def triangulate_area(include=[],exclude=[],holes=[]):
	#p=np.zeros((0,2))
	i=1;k=-1
	
	s1='';s2='';s3=''
	for pp in include+exclude:
		if not pp.isclosed(): raise Exception('you can call this function only with closed polygons')
		
		i0=i
		for R,z in pp.Rz[:-2]:
			s1+='%4i %15.7f %15.7f \n' % (i,R,z)
			s2+='%4i %4i %4i \n' % (i,i,i+1)
			i+=1;
		s1+='%4i %15.7f %15.7f \n' % (i,pp.Rz[-2,0],pp.Rz[-2,1])
		s2+='%4i %4i %4i \n' % (i,i,i0)
		i+=1
		
	for k,pp in enumerate(exclude):
		p=pp.construct_point_inside()
		s3+='%4i %15.7f %15.7f \n' % (k+1,p[0],p[1])
		
	for p in holes:
		k+=1
		s3+='%4i %15.7f %15.7f \n' % (k+1,p[0],p[1])
		
		
	#print i,k
	open('optri.poly','w').write('#points\n%i   2    0    0\n#connections\n%s%i 0\n%s#holes\n%i\n%s' % (i-1,s1,i-1,s2,k+1,s3))

	import os
	os.system('/home/tal/Projects/EMC3/f2py_field_routines/build/triangle  -p  -a0.01 optri')
	f=open('optri.1.node')
	h1=np.fromfile(f,count=4,sep=' ',dtype=int)
	v=np.fromfile(f,count=4*h1[0],sep=' ',dtype=float).reshape(-1,4)[:,1:]
	f.close()

	f=open('optri.1.ele')
	h3=np.fromfile(f,count=3,sep=' ',dtype=int)
	tt=np.fromfile(f,count=4*h3[0],sep=' ',dtype=int).reshape(-1,4)[:,1:]-1
	f.close()

	os.system('rm optri*')
	return [v[t] for t in tt]
	
	
	
	



#def intervals(dlbefore,dlafter,L,Nmax=200,beta=1.0):
	#'''
	#returns an array of intervals dl_i with sum(dl_i)=L and i=0..N-1 such that dl_-1=dlbefore and dl_N=dlafter
	#'''
	#LL=np.zeros(Nmax)
	#for N in range(1,Nmax):
		#alpha=(dlafter-dlbefore)/(N+1)**beta
		#dl=dlbefore+(np.arange(N)+1)**beta*alpha
		#LL[N]=np.sum(dl)
	#N=np.argmin((LL-L)**2)
	##print N
	#alpha=(dlafter-dlbefore)/(N+1)**beta
	#dl=dlbefore+(np.arange(N)+1)**beta*alpha
	#return dl*L/np.sum(dl)



#def make_trianglefile(fn,clist,iz0=0,iz1=3,it0=0,it1=15,scaling_factor=1.0):
	#s='';n=0
	#for c in clist:
		#tris,_=c.triangulate()		
		#for x,y in tris:
			##plt.fill(x,y)
			#x*=scaling_factor;y*=scaling_factor			
			#n+=1
			#s+='    %i    %i     %i    %i\n' % (iz0,iz1,it0,it1)
			#s+='%14.4f  %14.4f  %14.4f\n'% (x[0],x[1],x[2])
			#s+='%14.4f  %14.4f  %14.4f\n'% (y[0],y[1],y[2])
	#open('triangles.txt','w').write(str(n)+'\n'+s)
	



#def stuff_holes(ll,threshold=3,beta=1.0):
	#'''
	#on a series of points ll, large intervals dl are refined
	#'''
	#dl=ll[1:]-ll[:-1]
	#ii=np.where(dl>(np.mean(dl)*threshold))[0]
	#L=np.array([])
	#for i in ii:
		#L=np.append(L,np.cumsum(intervals(dl[i-1],dl[i+1],dl[i],beta=beta))[:-1]+ll[i])
	#L=np.append(L,ll)
	#L.sort()
	#return L
	
