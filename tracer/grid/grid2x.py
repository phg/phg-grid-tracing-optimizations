from grid import *
from field_routines import baxisym as bas
from field_routines import trace as tr


class grid2x(grid):
	def __init__(self,**kwargs):
		super(grid2x, self).__init__(**kwargs)
		self.rf[3]=1;self.rf[4]=1
		self.legrf[3]=1.0;self.legrf[4]=1.0
		self.xp2curve3par=[[0.4,0.0]]
		self.xp2curve4par=[[0.4,0.0]]
		self.xp2curveppar=[[0.4,0.0]]
		self.xp2dd=3e-3
		self.target_range[3]=[-0.05,0.1,'ds','magenta']
		self.target_range[4]=[-0.05,0.1,'ds','cyan']
		self.zonelist=['CORE','SOL','PFR','FARSOL42','FARSOL43','FARSOL13']
		
	def construct_2nd_separatrix_legs(self):
		xp2=self.xp2
		
		vv,ddr=bas.find_xp_legs(bas.xpoint[:,1],dd=self.xp2dd)
		print ddr
		
		tr.unset_axisym_targets()
		#tr.add_axisym_target_curve(self.target1.Rz) 
		#tr.add_axisym_target_curve(self.target2.Rz)
		tr.add_axisym_target_curve(self.pfcs.Rz)
		tr.set_endpointcorrection(False)
		tr.set_postlimitsteps(self.postlimitsteps)

		self.raws2leg=raws2leg={}
		
		for i,v,dr in zip([11,12,13,14],vv,ddr):
			x=xp2+v
			raws2leg[i]=sf=curve(tr.field_line([x[0],x[1],0],dr*self.dphistep,self.Ntrace,dir=1,substeps=self.flt_substeps)[:,0:2],remove_Nan=True)
			colo='grey'
			
			if sf.collide(self.target[1])[2]>-1.0:
				raws2leg[1]=sf;id1=i;
			elif sf.collide(self.target[2])[2]>-1.0:
				raws2leg[2]=sf;id2=i;
			self.plotc(sf,color=colo)
		
		
		
		rawsep2=self.rawsep2=curve(np.vstack((raws2leg[1].Rz[::-1]*1.0,xp2*1.0,raws2leg[2].Rz*1.0)))
		self.lx2=rawsep2.l[raws2leg[1].N]*1.0
		
		#self.scatter([rawsep2.l2Rz(self.lx2)],marker='*',s=50)
		
		nxt=((id1-11+1) % 4)+11;prv=((id1-11-1) % 4)+11;lst=((id1-11+2) % 4)+11
		print 'ids:',id1,prv,nxt,lst,id2
		if nxt==id2:
			raws2leg[3]=raws2leg[prv];raws2leg[4]=raws2leg[lst]
			
		elif prv==id2:
			raws2leg[3]=raws2leg[nxt];raws2leg[4]=raws2leg[lst]
			#print 'self.xp2curve3par',self.xp2curve3par
			self.xp2curve3=length_angle_list2curve(xp2,vv[id1-11]+vv[nxt-11],self.xp2curve3par)	
			self.plotc(self.xp2curve3,color='red',ls='--')
			self.xp2curve4=length_angle_list2curve(xp2,vv[id2-11]+vv[lst-11],self.xp2curve4par)	
			self.plotc(self.xp2curve4,color='blue',ls='--')
			self.xp2curvep=length_angle_list2curve(xp2,vv[nxt-11]+vv[lst-11],self.xp2curveppar)	
			self.plotc(self.xp2curvep,color='magenta',ls='--')
			
		else:
			raise Exception('Leg 3 & 4 of secondary separatrix not found.')
		
		
		self.sp[3]=raws2leg[3].collide(self.pfcs)
		#self.target[3]=self.pfcs.extract_interval(l0=self.sp[3][3]-self.dsp,l1=self.sp[3][3]+self.dsp)
		#self.plotc(self.target3,color='magenta',lw=2)
		
		self.sp[4]=raws2leg[4].collide(self.pfcs)
		#self.target4=self.pfcs.extract_interval(l0=self.sp4[3]-self.dsp,l1=self.sp4[3]+self.dsp)
		#self.plotc(self.target4,color='cyan',lw=2)
		
		for i,c in [[1,'red'],[2,'blue'],[3,'magenta'],[4,'cyan']]:
			self.plotc(raws2leg[i],color=c)

	def construct_x2limit(self):
		lx2=self.lx2;sep=self.sep
		
		crvs=[self.target[1],self.xpcurve1,self.reg1,self.reg2,self.xpcurve2,self.target[2]]
		idx= [self.isp1,self.ixp,self.ireg1,self.ireg2,self.i2xp,self.isp2]
		ll=[self.rawsep2.collide(c)[2] for c in crvs]
		
		#print ll
		#print self.lx2
		seq=self.sequences['SOL']
		self.x2segment=-1
		for i in range(len(ll)-1):
			print 'case: ',i,ll[i],ll[i+1],lx2
			if (lx2>ll[i]) and (lx2<ll[i+1]):
				self.x2segment=i
				
				x=(lx2-ll[i])/(ll[i+1]-ll[i])
				l=sep.l[idx[i]]+(sep.l[idx[i+1]]-sep.l[idx[i]])*x
				print 'found: ',i,l,sep.l[idx[i]],sep.l[idx[i+1]]
			
		if self.x2segment!=2:
			self.ix2l=int(sep.l2i(l))
			self.x2limit=curve([sep.Rz[self.ix2l],self.xp2])
			self.x2limit.extend(0.01)
			self.plotc(self.x2limit,color='red')
			seq[5]+=[[2,self.ix2l,self.x2limit,None]]
			
	def construct_poloidal_lines_xpregion(self):
		print 'new construct_poloidal_lines_xpregion'
		
		pllist=[[self.ixp-self.istrxp,self.ixp-self.istrxp,self.xpcurvep.Rz[-1],'PFR'],
				[self.i2xp+self.istrxp,self.ixp+self.istrxp,self.xpcurvep.Rz[-1],'PFR']]
		if self.x2segment!=0 :
			pllist+=[[self.ixp-self.istrxp,self.ixp-self.istrxp,self.xpcurve1.Rz[-1],'SOL']]
		if self.x2segment!=1 :
			pllist+=[[self.ixp+self.istrxp,self.ixp+self.istrxp,self.xpcurve1.Rz[-1],'SOL']]
		if self.x2segment!=3 :
			pllist+=[[self.i2xp-self.istrxp,self.i2xp-self.istrxp,self.xpcurve2.Rz[-1],'SOL']]
		if self.x2segment!=4 :
			pllist+=[[self.i2xp+self.istrxp,self.i2xp+self.istrxp,self.xpcurve2.Rz[-1],'SOL']]

		#if self.x2segment in [3,4] :
			#pllist+=[[self.ixp+self.istrxp,self.ixp+self.istrxp,self.xpcurve1.Rz[-1],'FARSOL13']]
			#pllist+=[[self.ixp-self.istrxp,self.ixp-self.istrxp,self.xpcurve1.Rz[-1],'FARSOL13']]
		
		for ip1,ip2,p,snam in pllist:
			crv=curve([self.sep.Rz[ip1],p])
			self.plotc(crv,color='green',ls='--')
			self.sequences[snam][5]+=[[2,ip2,crv,None]]

	def construct_2nd_separatrix(self):
		seq=self.sequences['SOL']
		self.sep2=curve(self.sequence2Rzl(seq[0].shape[0],seq[0][:,2],self.rawsep2,seq[5])[:,0:2])
		self.plotc(self.sep2,marker='x')
		self.plotc(self.ompcurve)
		c=self.ompcurve.collide(self.sep2)
		self.ompcurve.reflength=c[2]
		self.scatter([c[0:2]])
		#self.rf3=rf3;self.rf4=rf4
		dl=(self.sep2.l[self.ix2l+1]-self.sep2.l[self.ix2l-1])/2
		
		self.s2leg={}
		for j,sp,rf,dlrf,sgn,col in [[3,self.sp[3],self.rf[3],dl/self.legrf[3],+1.0,'magenta'],[4,self.sp[4],self.rf[4],dl/self.legrf[4],-1.0,'black']]:
			s=self.raws2leg[j]
			tr.unset_axisym_targets()
			fl=curve(tr.field_line([sp[0],sp[1],0],self.dphi/rf,(self.Nit-1)*rf,dir=0,substeps=self.flt_substeps)[:,0:2])
			ll=sgn*(fl.l-fl.l[self.Nithalf*rf])+sp[2];
						
			#self.plotc(s.l2Rz(fl.l[self.Nithalf*rf]-fl.l+sp[2],output_format='curve'),marker='o',color=col,ls=' ')
			ll.sort()
			#i=len(ll)-1
			ll=np.append([0.0,dlrf],ll)
			#print 'll',ll
			i=1
			ll=np.append(ll,stuff_interval(ll[i]-ll[i-1],ll[i+1]-ll[i],ll[i+2]-ll[i+1])+ll[i])
			ll.sort()
			self.s2leg[j]=crv=s.l2Rz(ll,output_format='curve')
			crv.Rzl[0,0:2]=self.xp2;crv.reverse();
			#self.plotc(crv,marker='+',color=col,ls=' ')
		
		#self.plotc(curve(self.sequence2Rzl(s[0],self.sep2,seq)[:,0:2]),marker='o')

	def add_sequences(self):
		#l=list(self.sep2.l[:self.ix2l])+list(self.s2leg[3].l+self.sep2.l[self.ix2l])
		l=list(self.sep2.l[:self.ix2l])+list(curve(self.s2leg[3].Rz[::-1]).l+self.sep2.l[self.ix2l])
		
		sep13=curve(np.vstack((self.sep2.Rz[:self.ix2l,:],self.s2leg[3].Rz[::-1])))
		Np=sep13.N;ispr3=Np-(self.Nit-1)*self.rf[3]-1
		seq=[[1,         0,self.target[1],[self.dphi/self.rf[1],(self.Nit-1)*self.rf[1]]],
	         [1,ispr3,self.target[3],[self.dphi/self.rf[3],(self.Nit-1)*self.rf[3]]],
	         [2,self.ix2l,self.xp2curve3,None]]

		if self.x2segment in [3,4]:
			seq+=[[2,i+self.ireg1,r,None] for i,r in enumerate(self.reglist)]

		if self.x2segment in [3,4]:
			seq+=[[2,self.ixp,self.xpcurve1,None]]
			
		#if self.x2segment in [4]:
			#seq+=[[2,self.i2xp,self.xpcurve2,None]]
		
		self.sequences['FARSOL13']=[sep13.Rzl,0,0,4,self.xp2curve3,seq]

		sep42=curve(np.vstack((self.s2leg[4].Rz,self.sep2.Rz[self.ix2l+1:,:])))
		
		#l=list(self.s2leg[4].l)+list(self.sep2.l[self.ix2l+1:]-self.sep2.l[self.ix2l]+self.s2leg[4].totl)
		Np=sep42.N;ispr2=Np-(self.Nit-1)*self.rf[2]-1
		ix2=self.s2leg[4].N-1
		seq=[[1,         0,self.target[4],[self.dphi/self.rf[4],(self.Nit-1)*self.rf[4]]],
	         [1,ispr2,self.target[2],[self.dphi/self.rf[2],(self.Nit-1)*self.rf[2]]],
	         [2,ix2,self.xp2curve4,None]]
		
		if self.x2segment in [0,1]:
			seq+=[[2,i+self.ireg1-self.ix2l+ix2,r,None] for i,r in enumerate(self.reglist)]
		
		if self.x2segment in [0,1]:
			self.ipx42=self.Npcore+ix2-self.ix2l+self.ixp
			seq+=[[2,self.ipx42-1,self.xpcurve2,None]]
		
		self.sequences['FARSOL42']=[sep42.Rzl,0,0,4,self.xp2curve4,seq]		

		
		#l=list(self.s2leg[4].l[:-1])+list(curve(self.s2leg[3].Rz[::-1]).l+self.s2leg[4].totl)
		sep43=curve(np.vstack((self.s2leg[4].Rz[:-1],self.s2leg[3].Rz[::-1])))
		Np=sep43.N;ispr3=Np-(self.Nit-1)*self.rf[3]-1
		ix2=self.s2leg[4].N-1
		seq=[[1,         0,self.target[4],[self.dphi/self.rf[4],(self.Nit-1)*self.rf[4]]],
	         [1,ispr3,self.target[3],[self.dphi/self.rf[3],(self.Nit-1)*self.rf[3]]],
	         [2,ix2,self.xp2curvep,None]]
		
		self.sequences['FARSOL43']=[sep43.Rzl,0,0,4,self.xp2curvep,seq]		

	def connectSOL2sep2(self):
		#super(grid2x,self).glue_zones()
		
		zn=self.zone['SOL']
		zn.insert_sf(i='end')
		zn.Rz[-1,:]=self.sep2.Rz*1.0
		
		
	def set_wallexpansion_parameters(self,so=[1.0,5.0,5.0,   1.0,5.0,5.0,   0.001,0.001]):
		'''
			|--3-----4--|-----------
			|     
			2
			|     
			|     x---ipc1----------
			|     |
			1     |
			|     |
			------------------------
			the factors in 'so' stretch the intervals 1,2,3 left and 1,2,3 right followed by factors that move the left and right corners out. The intervals 4 are determined by the condition to fit the required length
		'''
	
		Nn=self.Nn
		
		zn=self.zone['CORE'].ntype=0
		
		zn=self.zone['SOL']
		zn.ntype=2
		zn.regn0interval=[0,0]#undefined here
		zn.pc0=[0,0];zn.pc1=[0,0];zn.plr=[zn.Rz[0,1,:],zn.Rz[0,-2,:],zn.Rz[-1,1,:],zn.Rz[-1,-2,:]];zn.so=np.array(so)*1.0#[0.0]*6
		
		zn=self.zone['FARSOL42']
		zn.ntype=1
		ii=np.where(zn.Rz[0,:,1]>self.op[1])[0]
		zn.regn0interval=[min(ii)+Nn,max(ii)+Nn]
		zn.pc0=self.op;zn.pc1=self.xp;zn.plr=[zn.Rz[0,1,:],zn.Rz[0,-2,:],[0,0],[0,0]];zn.so=np.array(so)*1.0

		zn=self.zone['PFR']
		zn.ntype=1
		zn.regn0interval=[self.ixp+Nn+1,self.ixp+Nn+1]
		zn.pc0=self.op;zn.pc1=self.xp;zn.plr=[zn.Rz[0,1,:],zn.Rz[0,-2,:],[0,0],[0,0]];zn.so=np.array(so)*1.0
				
		ip=self.sequences['FARSOL43'][-1][2][1]
		zn=self.zone['FARSOL43']
		zn.ntype=1
		zn.regn0interval=[ip+Nn,ip+Nn]
		zn.pc0=self.xp2;zn.pc1=self.xp2;zn.plr=[zn.Rz[0,1,:],zn.Rz[0,-2,:],[0,0],[0,0]];zn.so=np.array(so)*1.0

		ip=self.sequences['FARSOL13'][5][2][1]+1
		zn=self.zone['FARSOL13']
		zn.ntype=1
		zn.regn0interval=[ip+Nn,ip+Nn]
		print 'xp2',self.xp2
		zn.pc0=self.xp2;zn.pc1=self.xp2;zn.plr=[zn.Rz[0,1,:],zn.Rz[0,-2,:],[0,0],[0,0]];zn.so=np.array(so)*1.0
		
		self.zonelist=['CORE','SOL','PFR','FARSOL42','FARSOL43','FARSOL13']

#g.make_zone('FARSOL42',dr=np.linspace(0,0.1,10)[1:],color='magenta')
#g.make_zone('FARSOL13',dr=np.linspace(0,0.1,10)[1:],color='brown')
#g.make_zone('FARSOL43',dr=np.linspace(0,0.1,19)[1:],color='black')

	def glue_neutral_zones(self):
		Nn=self.Nn
		for isegm in range(self.Nsegm):
			fs42=self.zone['FARSOL42'].Rzp[isegm]
			fs43=self.zone['FARSOL43'].Rzp[isegm]
			fs13=self.zone['FARSOL13'].Rzp[isegm]
			sol=self.zone['SOL'].Rzp[isegm]
			pfr=self.zone['PFR'].Rzp[isegm]
			
			#print 'fs43'
			#print fs43[0,0:self.Nn,:,:]-fs42[0,0:self.Nn,:,:]
			
			fs43[0,0:Nn,:,:]  =fs42[0,0:Nn,:,:]
			fs43[0,-Nn-1:,:,:]=fs13[0,-Nn-1:,:,:]
			sol[-1,0:Nn,:,:]=fs13[0,0:Nn,:,:]
			sol[0,0:Nn,:,:]=pfr[0,0:Nn,:,:]
			sol[0,-Nn-1:,:,:]=pfr[0,-Nn-1:,:,:]
			sol[-1,-Nn-1:,:,:]=fs42[0,-Nn-1:,:,:]
			
			
		
	def prepare(self):
		self.find_singularities()
		self.construct_basesep()
		self.make_targets()
		self.thetacor_correction()
		self.construct_regular_region()
		self.construct_xp_curves()
		self.construct_separatrix()
		self.make_sequences()
		self.construct_2nd_separatrix_legs()
		self.make_targets(ii=[3,4])
		self.construct_x2limit()
		self.construct_poloidal_lines_xpregion()
		self.construct_2nd_separatrix()
		self.add_sequences()
		#self.construct_poloidal_lines_xpregion()
		
