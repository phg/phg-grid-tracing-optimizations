import numpy as np
import matplotlib.pyplot as plt
import time
from grid2xSOLPS import *
from field_routines import baxisym as bas
#from field_routines import trace as tr

#fig,ax =plt.subplots(figsize=(10,16))
fig,ax =plt.subplots(figsize=(16,16))

ax.set_aspect('equal')

g=grid2xSOLPS()#,Nithalf=1Npcore=399

#comment out the following line to switch off graphical output during the generation process and speed up significantly
g.figax=[fig,ax]

#comment in the following line to enter a kind of debugging mode
#g.step_mode=True;g.Nskipsteps=0

bas.set_tolerance(1e-8)   #maximum tolerated flux inaccuracy for the X-point search

g.oxp_estimates={'op':[0.88,0.4]}
g.xp2reg_linearization=True
g.load_equilibrium('../TCV/psi_44826t0.8000_sigma_0.65_theta_010n.txt',scale_psi=2*np.pi,Bo=-1.4388,Ro=0.88)
g.add_pfcs('../TCV/pfcs_TCV.txt',scaling_factor=1.0e-2)
g.xpcurve1par=[[0.3,-30]]
g.xpcurve2par=[[0.3,-30]]
g.xp2curve3par=[[0.2,20]]
g.xp2curve4par=[[0.2,10]]
g.xpcurveppar=[[0.3,-45]]
g.xp2curveppar=[[0.2,10]]
g.xpcurvecpar=[[0.03,-40],[0.15,40]]
g.Np1=15*5;g.Np2=25*5
g.dsp=0.05
g.rf1=4;g.rf2=4
g.rf3=4;g.rf4=4


b=time.time()
g.prepare()
print time.time()-b,'s'


#g.figax=[fig,ax]

g.make_zone('CORE',dr=np.linspace(1.5,0,10)[:-1]*5e-2,color='red')#PsiN=[0.9,0.95,0.98])
g.make_zone('SOL',drn=np.linspace(0.0,1.0,10)[1:-1],color='blue')
g.make_zone('PFR',dr=np.linspace(0,0.09,10)[1:],color='green')
g.make_zone('FARSOL42',dr=np.linspace(0,0.1,10)[1:],color='magenta')
g.make_zone('FARSOL13',dr=np.linspace(0,0.1,10)[1:],color='cyan')
g.make_zone('FARSOL43',dr=np.linspace(0,0.1,19)[1:],color='black')



#g.step_mode=True;g.Nskipsteps=0

g.connectSOL2sep2()
g.figax=[fig,ax]
for k in g.zone.keys():
    g.plotzn(k)
    g.check_zone(k,lw=5,color='black')


figmap=plt.subplots(figsize=(30,10))
ax.set_ylim(-0.5,0)

g.make_SOLPS(figmap=figmap)



fig.savefig('grid.png')
fig.show()

fig,ax=figmap
ax.set_xlim(-3,620)
ax.set_ylim(27,-5)
ax.set_xlabel('poloidal index')
ax.set_ylabel('radial index')

fig.savefig('map.pdf')

fig.show()

