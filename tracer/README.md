Technical pre-requisits:
------------------------
+ fortran compiler (e.g. gfortran)
+ python 2.7 with libraries numpy,matplotlib,xml,sys
+ f2py
+ access to git@gitlab.mpcdf.mpg.de:tal/AUG_geometry.git

Usage:
------
+ ./install.sh
+ (type password)
+ python example_simple.py