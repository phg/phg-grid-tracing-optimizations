#This example works only if install.sh was executed beforehand.

import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('build/.')
from field_routines import baxisym as bas
from field_routines import trace as tr
from load_magnetics import *
sys.path.append('AUG/.')
from machine_geometry import *
import argparse

#read the command line parameters or define default values
parser=argparse.ArgumentParser()
parser.add_argument("-fn","--filename",default='',type=str)
parser.add_argument("-s","--shot",default=-1,type=int)
parser.add_argument("-t","--time",default=2.0,type=float)
parser.add_argument("-o","--output",default='',type=str,nargs='?')
args=parser.parse_args()


if args.filename!='':
  magxml=read_from_file(args.filename)
elif args.shot<0:
  magxml=read_from_file('AUG/magnetics_AUGD_EQH_29273_04600ms_0.xml')
else:  
  magxml=read_AUG_magnetics(ishot=args.shot,time=args.time)


load_magnetics(magxml)


fig,ax=plt.subplots(1,2,figsize=(20,12))
fig.subplots_adjust(wspace=0.02,left=0.05,right=0.98)

#load the machine geometry information from the xml file and plot them. 
AUGxml=open_machine('AUG/MD_AUG.xml')

plot_machine(ax[0],AUGxml.find('components'),projection='pol')
plot_machine(ax[1],AUGxml.find('components'),projection='tor')
plot_machine(ax[1],AUGxml.find('modelling_elements'),projection='tor')

bas.find_separatrix(400,13,10)#number of poloidal points on separatrix in core, right leg and left leg respectively
ax[0].plot(bas.sep[:,0],bas.sep[:,1],color='black',lw=3)

pfcs=xml_get_element(AUGxml,'.//modelling_elements/*[@name="all_pfcs"]/pol_contour')

pfcs=np.vstack((pfcs[49:],pfcs[:29])) # exclude the region of the limiters

tr.unset_all_limits()

tr.add_axisym_target_curve(pfcs)

tr.set_lc_limit(100.)

tr.set_npol_limit(2)

limiters=xml_get_elements(AUGxml,xpath='.//modelling_elements/*[mesh]/mesh')

for k,Rzphi in limiters.iteritems():
    xyz=Rzphi*0.0 # make a copy
    xyz[:,:,0]=Rzphi[:,:,0]*np.cos(Rzphi[:,:,2])
    xyz[:,:,1]=Rzphi[:,:,0]*np.sin(Rzphi[:,:,2])
    xyz[:,:,2]=Rzphi[:,:,1]
    #ax.plot(xyz[:,:,0],xyz[:,:,1],color='blue')
    tr.add_3d_mesh(xyz)
    
      
limtype={1:'computational domain',2:'Lc',3:'pol.turns',4:'2D target',5:'3D target',6:'Lc_pol'}

for R,col in [[1.9,'red'],[2.128,'green'],[2.16,'blue'],[2.26,'cyan']]:#[[2.28,'red']]:
    fl=tr.field_line([R,0.0,-0.12],-1*np.pi/180.,10000,dir=1)
    fl=fl[~np.isnan(fl[:,0])]
    ax[0].plot(fl[:,0],fl[:,1],color=col)
    ax[0].scatter([fl[0,0],fl[-1,0]],[fl[0,1],fl[-1,1]],color=col)
    ax[1].plot(fl[:,0]*np.cos(fl[:,2]),fl[:,0]*np.sin(fl[:,2]),color=col)
    print 'Start point R=%5.3f m, z=0.0, phi=-0.12 rad' % R
    print 'End point R=%5.3f m, z=%5.3f m, phi=%.3f rad' % tuple(tr.rzpc) 
    print 'limit=%i (%s), Lc=%5.3f m, poloidal turns %5.3f \n\n' % (int(tr.limit),limtype[int(tr.limit)],tr.lc,tr.npol)
    
ax[0].set_aspect('equal')
ax[1].set_aspect('equal')

if args.output!='':
  fig.savefig(args.output)
  fig.show()
else:  
  plt.show()
