rm *.pyf *.o *.mod *.so

f2py ../src/INTERPOLATION.f90 ../src/TOOLS.f90 ../src/BAXISYM.f90 ../src/BPRECALC.f90 ../src/BFIELD.f90  ../src/TRACE.f90 ../src/GRID.f90 -m field_routines -h field_routines.pyf 
#--no-lower #this flag produces errors for capital letters in module names

#--f90exec=/u/system/SLES11/soft/intel16.3/16.0/linux/bin/intel64/bin/ifort 
f2py -c field_routines.pyf --f90flags="-fdefault-real-8 -fdefault-double-8 -O2" ../src/INTERPOLATION.f90 ../src/TOOLS.f90 ../src/BAXISYM.f90 ../src/BPRECALC.f90 ../src/BFIELD.f90 ../src/TRACE.f90 ../src/GRID.f90 

ls -ltr *.so
