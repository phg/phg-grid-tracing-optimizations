#This example works only if install.sh was executed beforehand.

import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('build/.')
from field_routines import baxisym as bas
from load_magnetics import *
sys.path.append('AUG/.')
from machine_geometry import *
import argparse

#read the command line parameters or define default values
parser=argparse.ArgumentParser()
parser.add_argument("-s","--shot",default=-1,type=int)
parser.add_argument("-t","--time",default=2.0,type=float)
parser.add_argument("-fn","--filename",default='',type=str)
parser.add_argument("-o","--output",default='',type=str,nargs='?')
args=parser.parse_args()

if args.filename!='':
  magxml=read_from_file(args.filename)
elif args.shot<0:
  magxml=read_from_file('AUG/magnetics_AUGD_EQH_29273_04600ms_0.xml')
else:  
  magxml=read_AUG_magnetics(ishot=args.shot,time=args.time)

load_magnetics(magxml)  # passes the magnetic configuration to the fortan modules baxisym,bprecalc, etc...

fig,ax =plt.subplots(figsize=(8,12))

#load the machine geometry information from the xml file and plot them. 
AUGxml=open_machine('AUG/MD_AUG.xml')
plot_machine(ax,AUGxml.find('components'))

oxp=np.vstack((bas.opoint,bas.xpoint.T))
ax.scatter(oxp[:,1],oxp[:,2])
    
bas.find_separatrix(400,15,13)#number of poloidal points on separatrix in core, right leg and left leg respectively
bas.find_upstream_pos()

psiNmax=bas.psin_x2 if (1.01<bas.psin_x2<1.06) else 1.06

for psin in np.hstack((np.linspace(0.1,1.0,10),np.linspace(1.0,psiNmax,10),np.linspace(-1.0,-0.98,4))):
    bas.find_flxsurface(psin)
    ax.plot(bas.sf[:,0],bas.sf[:,1],color='red')

ax.set_aspect('equal')
ax.set_xlabel('R [m]')
ax.set_ylabel('z [m]')

if args.output!='':
  fig.savefig(args.output)
  fig.show()
else:  
  plt.show()

