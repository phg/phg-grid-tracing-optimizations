      MODULE INTERPOLATION 
      IMPLICIT NONE

      private
C Cubic interpolation coefficients
      real*8,dimension(3),save ::
     .      a1=(/ 3.0D0,-1.5D0,  1.0D0/3.0D0 /)
     .,     a2=(/-2.5D0, 2.0D0, -0.5D0       /)
     .,     a3=(/ 0.5D0,-0.5D0,  1.0D0/6.0D0 /)
        
      public INTERPOL_SINGU,POLO_FL,INTERPOL_PFL
      CONTAINS
C-----------------------------------------------------------------------
      FUNCTION POLO_FL(PL,X,Y)
      REAL*8 :: POLO_FL
      REAL*8,intent(in) :: X,Y
      real*8,dimension(0:3,0:3),intent(in) :: PL

      real*8,dimension(0:3) :: F
      real*8::X2,X3,Y2,Y3,b1,b2,b3    
      integer :: i

      X2= X*X; X3=X2*X
      Y2= Y*Y; Y3=Y2*Y

      do i=0,3
        b1 = sum( a1(1:3)*(PL(i,1:3)-PL(i,0)) )
        b2 = sum( a2(1:3)*(PL(i,1:3)-PL(i,0)) )
        b3 = sum( a3(1:3)*(PL(i,1:3)-PL(i,0)) )
        F(i) = PL(i,0) + b1*Y+b2*Y2+b3*Y3
      enddo

      b1 = sum( a1(1:3)*(F(1:3)-F(0)) )
      b2 = sum( a2(1:3)*(F(1:3)-F(0)) )
      b3 = sum( a3(1:3)*(F(1:3)-F(0)) )
      POLO_FL = F(0) + b1*X+b2*X2+b3*X3
      RETURN 
      END FUNCTION POLO_FL
C-----------------------------------------------------------------------
      SUBROUTINE INTERPOL_PFL(Z,XI,YI,FF)
C Input: Z(4,4)
       implicit none
C Z(X,Y): X=0,1,2,3
C         Y=0,1,2,3
C Xi,Yi: (X,Y) for evaluation
C FF(0) = Z(XI,YI)           
C FF(1) = DZDX; FF(2) = DZDY
       real*8,dimension(0:3,0:3),intent(in) :: Z
       real*8,intent(in ) :: XI,YI
       real*8,dimension(0:2),intent(out) :: FF

       real*8,dimension(0:3) :: F,DZDY
       real :: Y2,Y3,X2,X3,b1,b2,b3
       integer :: i

       X2= XI*XI;   X3= X2*XI
       Y2= YI*YI;   Y3= Y2*YI

       do i=0,3
       b1 = sum( a1(1:3)*(Z(i,1:3)-Z(i,0)) )
       b2 = sum( a2(1:3)*(Z(i,1:3)-Z(i,0)) )
       b3 = sum( a3(1:3)*(Z(i,1:3)-Z(i,0)) )
       F(i) = Z(i,0) + b1*YI+b2*Y2+b3*Y3
       DZDY(i)= b1+2.D0*b2*YI+3.D0*b3*Y2
       enddo

       b1 = sum( a1(1:3)*(F(1:3)-F(0)) )
       b2 = sum( a2(1:3)*(F(1:3)-F(0)) )
       b3 = sum( a3(1:3)*(F(1:3)-F(0)) )

       FF(0) = F(0) + b1*XI+b2*X2+b3*X3
       FF(1) = b1   +2.D0*b2*XI + 3.D0*b3*X2

       b1 = sum( a1(1:3)*(DZDY(1:3)-DZDY(0)) )
       b2 = sum( a2(1:3)*(DZDY(1:3)-DZDY(0)) )
       b3 = sum( a3(1:3)*(DZDY(1:3)-DZDY(0)) )
       FF(2) = DZDY(0)  + b1*XI+b2*X2 + b3*X3

      END SUBROUTINE INTERPOL_PFL
C-----------------------------------------------------------------------
      SUBROUTINE INTERPOL_SINGU(Z,XI,YI,D,DDDR,DDDZ)
C Input: Z(4,4)
       implicit none
C Z(X,Y): X=0,1,2,3
C         Y=0,1,2,3
C Xi,Yi: (X,Y) for evaluation
C D=0.5*(DFDR**2 + DFDZ**2)
C DDDR,DDDZ
       real*8,dimension(0:3,0:3),intent(in) :: Z
       real*8,intent(in ) :: XI,YI
       real*8,intent(out) :: D,DDDR,DDDZ

       real*8,dimension(0:3) :: F,DFDY1,DFDY2
       real :: Y2,Y3,X2,X3,b1,b2,b3,df2dr2,df2dz2,df2drdz,DFDR,DFDZ
       integer :: i

       X2= XI*XI;   X3= X2*XI
       Y2= YI*YI;   Y3= Y2*YI

       do i=0,3
       b1 = sum( a1(1:3)*(Z(i,1:3)-Z(i,0)) )
       b2 = sum( a2(1:3)*(Z(i,1:3)-Z(i,0)) )
       b3 = sum( a3(1:3)*(Z(i,1:3)-Z(i,0)) )
       F(i) = Z(i,0) + b1*YI+b2*Y2+b3*Y3
       DFDY1(i)= b1+2.D0*b2*YI+3.D0*b3*Y2
       DFDY2(i)=    2.D0*b2   +6.D0*b3*YI
       enddo

       b1 = sum( a1(1:3)*(F(1:3)-F(0)) )
       b2 = sum( a2(1:3)*(F(1:3)-F(0)) )
       b3 = sum( a3(1:3)*(F(1:3)-F(0)) )

       DFDR     =  b1   +2.D0*b2*XI + 3.D0*b3*X2
       DF2DR2   =       +2.D0*b2    + 6.D0*b3*XI

       b1 = sum( a1(1:3)*(DFDY1(1:3)-DFDY1(0)) )
       b2 = sum( a2(1:3)*(DFDY1(1:3)-DFDY1(0)) )
       b3 = sum( a3(1:3)*(DFDY1(1:3)-DFDY1(0)) )
       DFDZ     = DFDY1(0) + b1*XI+b2*X2 + b3*X3
       DF2DRDZ  =      b1   +2.D0*b2*XI + 3.D0*b3*X2

       b1 = sum( a1(1:3)*(DFDY2(1:3)-DFDY2(0)) )
       b2 = sum( a2(1:3)*(DFDY2(1:3)-DFDY2(0)) )
       b3 = sum( a3(1:3)*(DFDY2(1:3)-DFDY2(0)) )
       DF2DZ2    = DFDY2(0) + b1*XI+b2*X2 + b3*X3

       D = 0.5D0*(DFDR**2 + DFDZ**2)
       DDDR = DFDZ*DF2DRDZ + DFDR*DF2DR2
       DDDZ = DFDZ*DF2DZ2  + DFDR*DF2DRDZ
       END SUBROUTINE INTERPOL_SINGU

      END MODULE INTERPOLATION
