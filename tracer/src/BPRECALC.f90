      module BPRECALC

!---------------------------------------------------------------------------
! the following routines were taken from the GOURDON code 
! and were written by E.Strumberger and modified T.Lunt
!
! the magnetic field is pre-calculated on a grid ba which can be stored
! and recovered from a file 
!
! ba has the size ba(Ncomppre,Nfpre-1,Nrpre,Nzpre) the components meaning
!
! ba( 1,i,j,k)=Bphi,        ba( 2,i,j,k)=Br,        ba( 3,i,j,k)=Bz,       |
! ba( 4,i,j,k)=dBphi/dphi,  ba( 5,i,j,k)=dBphi/dr,  ba( 6,i,j,k)=dbphi/dz  |
! ba( 7,i,j,k)=dBr/dphi,    ba( 8,i,j,k)=dBr/dr,    ba( 9,i,j,k)=dbr/dz    |
! ba(10,i,j,k)=dBz/dphi,    ba(11,i,j,k)=dBz/dr,    ba(12,i,j,k)=dbz/dz    |
!
! if Ncomppre=3 only the first three terms are stored
!
! f_i = fapre + df * (i-1), i=1..Nfpre-1, df = (fbpre - fapre) / (Nfpre-1), f_Nfpre = f_1
! R_j = Rapre + dR * (j-1), j=1..NRpre,   dR = (Rbpre - Rapre) / (NRpre-1)
! z_k = zapre + dz * (k-1), k=1..Nzpre,   dz = (zbpre - zapre) / (Nzpre-1)
!
!---------------------------------------------------------------------------
     
      
      
      implicit none
      real*8,parameter :: pi=3.14159265358979312d0
      
      real*8,parameter :: bs_factor=1.d-7  !=mu0/(4pi)
      
      integer :: Nrpre=0,Nzpre=0,Nfpre=0, Ncomppre=0
      real*8  :: Rapre=0.d0,Rbpre=0.d0, 
     .           zapre=0.d0,zbpre=0.d0, 
     .           fapre=0.d0,fbpre=0.d0
      
      real*8,allocatable :: ba(:,:,:,:)
      integer :: ncel=0
      real*8,allocatable :: xcel(:),ycel(:),zcel(:),Icel(:)
      integer,save::ierr_bpre=0
!      private pi
      
      contains
      
C----.|-----------------------------------------------------------------      
      
      subroutine set_current_wires_xyzI(N,xyzI)
      integer,INTENT(IN) :: N 
      REAL*8,INTENT(IN) :: xyzI(N,4)
      
Cf2py intent(in) N 
Cf2py intent(hide),depend(N) xyzI

      if (allocated(xcel)) deallocate(xcel)
      if (allocated(ycel)) deallocate(ycel)
      if (allocated(zcel)) deallocate(zcel)      
      if (allocated(Icel)) deallocate(Icel)      
      ncel=N
      allocate(xcel(ncel),ycel(ncel),zcel(ncel),Icel(ncel))
      xcel=xyzi(:,1);ycel=xyzi(:,2);zcel=xyzi(:,3);Icel=xyzi(:,4)
      end subroutine
      
C----.|-----------------------------------------------------------------      
      
      subroutine set_current_wires_RzphiI(N,RzphiI)
      integer,INTENT(IN) :: N 
      REAL*8,INTENT(IN) :: RzphiI(N,4)
      
Cf2py intent(in) RzphiI 
Cf2py intent(hide),depend(N) RzphiI

      integer :: i

      if (allocated(xcel)) deallocate(xcel)
      if (allocated(ycel)) deallocate(ycel)
      if (allocated(zcel)) deallocate(zcel)      
      if (allocated(Icel)) deallocate(Icel)      
      ncel=N
      allocate(xcel(ncel),ycel(ncel),zcel(ncel),Icel(ncel))
      do i=1,N
       !write(*,*) RzphiI(i,:)
       xcel(i)=RzphiI(i,1)*cos(RzphiI(i,3))
       ycel(i)=RzphiI(i,1)*sin(RzphiI(i,3))
       zcel(i)=RzphiI(i,2)
       Icel(i)=RzphiI(i,4)
      enddo
      end subroutine
      
C----.|-----------------------------------------------------------------      

      subroutine prepare_field(Ra,Rb,Nr,za,zb,Nz,fa,fb,Nf,Nc)
!-----------------------------------------------------------------------------!
! transformation of the magnetic field and its first derivatives              !
! into cylindrical coordinates                                                !
!                                                                             !
! written by E. Strumberger                                          IPP 2000 !
!                                                      last change 09/03/2010 !
!-----------------------------------------------------------------------------!

      implicit none
      
      integer,intent(in) :: Nr,Nz,Nf,Nc
      real*8,intent(in)  :: Ra,Rb,za,zb,fa,fb
      integer :: i,ir,iz
      real*8    :: xac,yac,zac,rac
      
! --- magnetic field components in cartesian coordinates
      real*8    :: bx,by,bz
      real*8    :: dbxx,dbxy,dbxz,dbyx,dbyy,dbyz,dbzx,dbzy,dbzz
      
      real*8,allocatable :: cosphi(:),sinphi(:)
      real*8,allocatable :: cosi(:),cos2(:),sin2(:)
     
      real*8  :: df,dR,dz
      
      Nfpre=Nf;Nrpre=Nr;Nzpre=Nz;Ncomppre=Nc
      Rapre=Ra;Rbpre=Rb
      zapre=za;zbpre=zb      
      fapre=fa;fbpre=fb
      
     
      write (*,*) 'precomputing:'

      write (*,*) Nfpre,Nrpre,Nzpre
      write (*,*) Rapre,Rbpre
      write (*,*) zapre,zbpre      
      write (*,*) fapre,fbpre
      write (*,*) Ncomppre

      df=(fbpre-fapre)/(Nfpre-1)
      dR=(Rbpre-Rapre)/(Nrpre-1)
      dz=(zbpre-zapre)/(Nzpre-1)
   
      if (allocated(ba)) deallocate(ba)           
      
      allocate (ba(Ncomppre,Nfpre-1,Nrpre,Nzpre))
      
      allocate(sinphi(Nfpre-1),cosphi(Nfpre-1),cosi(Nfpre-1),
     .         cos2(Nfpre-1),sin2(Nfpre-1)) 
      
      do i=1,Nfpre-1
       sinphi(i)=dsin(fapre+(i-1)*df)
       cosphi(i)=dcos(fapre+(i-1)*df)
       cosi(i)  =cosphi(i)*sinphi(i)
       cos2(i)  =cosphi(i)*cosphi(i) 
       sin2(i)  =sinphi(i)*sinphi(i)            
      enddo
      

      write (*,*) 'start precalculating field'
        do iz=1,Nzpre
	  write (*,*) 'iz=',iz,Nzpre
          zac = zapre + (iz-1)*dz
          do ir = 1,Nrpre
            rac = Rapre + (ir-1)*dr
            do i=1,Nfpre-1
              xac = rac * cosphi(i)
              yac = rac * sinphi(i)
              call biotsavd(xac,yac,zac,bx,by,bz,  
     .                   dbxx,dbxy,dbxz,dbyx,dbyy,dbyz,dbzx,dbzy,dbzz)
              ba (1,i,ir,iz) = ba (1,i,ir,iz) 
     .                        -bx*sinphi(i) +by*cosphi(i)
              ba (2,i,ir,iz) = ba (2,i,ir,iz) 
     .                        +bx*cosphi(i) +by*sinphi(i)
              ba (3,i,ir,iz) = ba (3,i,ir,iz) + bz
              ba (4,i,ir,iz) = ba (4,i,ir,iz) 
     .                        -(dbyx+dbxy)*rac*cosi(i) 
     .                        +dbxx*rac*sin2(i)+dbyy*rac*cos2(i) 
     .                        -by*sinphi(i)-bx*cosphi(i)
              ba (5,i,ir,iz) = ba (5,i,ir,iz) 
     .                        +dbyx*cos2(i)-dbxy*sin2(i) 
     .                        +(dbyy-dbxx)*cosi(i)
              ba (6,i,ir,iz) = ba (6,i,ir,iz) 
     .                        +dbyz*cosphi(i)-dbxz*sinphi(i)
              ba (7,i,ir,iz) = ba (7,i,ir,iz) 
     .                        +(dbyy-dbxx)*rac*cosi(i) 
     .                        -dbyx*rac*sin2(i)+dbxy*rac*cos2(i) 
     .                        -bx*sinphi(i)+by*cosphi(i)
              ba (8,i,ir,iz) = ba (8,i,ir,iz) 
     .                        +dbxx*cos2(i)+dbyy*sin2(i) 
     .                        +(dbxy+dbyx)*cosi(i)
              ba (9,i,ir,iz) = ba (9,i,ir,iz) 
     .                        +dbxz*cosphi(i)+dbyz*sinphi(i)
              ba(10,i,ir,iz) = ba(10,i,ir,iz) 
     .                        -dbzx*rac*sinphi(i)+dbzy*rac*cosphi(i)
              ba(11,i,ir,iz) = ba(11,i,ir,iz) 
     .                        +dbzx*cosphi(i)+dbzy*sinphi(i)
              ba(12,i,ir,iz) = ba(12,i,ir,iz)+dbzz
            enddo
          enddo
        enddo

      deallocate(sinphi,cosphi,cosi,cos2,sin2) 
      write(*,*) 'field precalculated'
!      Ncomppre=3
      end subroutine 

C----.|-----------------------------------------------------------------      

      subroutine save_field(fn)
      character(*),intent(in) :: fn
Cf2py intent(in) fn      
      integer,parameter :: iun=2
      open(iun,file=fn,access='stream')
      write (iun) Nfpre,Nrpre,Nzpre,Ncomppre
      write (iun) Rapre,Rbpre
      write (iun) zapre,zbpre 
      write (iun) fapre,fbpre       
      write (iun) ba
      close(iun)      
      end subroutine

C----.|-----------------------------------------------------------------      

      subroutine restore_field(fn)
      character(*),intent(in) :: fn
      integer,parameter :: iun=2
Cf2py intent(in) fn      
      open(iun,file=fn,access='stream')
      read (iun) Nfpre,Nrpre,Nzpre,Ncomppre
      ! one could comment out those prints or use an if, idk how fortran works
      write (*,*) 'one could comment out those prints or use an if,'
      write (*,*) ' idk how fortran works'
      write (*,*) 'see .../src/BPRECALC.f90 for the origin'
      write (*,*) 'of theese prints'
      write (*,*) 'grid restored from',fn
      write (*,*) 'Nf,Nr,Nz=',Nfpre,Nrpre,Nzpre
      read (iun) Rapre,Rbpre
      read (iun) zapre,zbpre
      read (iun) fapre,fbpre      
      write (*,*) 'valid R domain ',Rapre,'..' ,Rbpre
      write (*,*) 'valid z domain ',zapre,'..' ,zbpre
      write (*,*) 'valid phi domain ',fapre,'..' ,fbpre
      if (allocated(ba)) deallocate(ba)      
      
      allocate (ba(Ncomppre,Nfpre-1,Nrpre,Nzpre))
      read (iun) ba
      close(iun) 
  
      end subroutine

C----.|-----------------------------------------------------------------      

      subroutine initialize_ba(Ra,Rb,Nr, za,zb,Nz, fa,fb,Nf, Nc)
      real*8,intent(in) :: Ra,Rb,za,zb,fa,fb
      integer,intent(in) :: Nr,Nz,Nf, Nc
Cf2py intent(in) Ra,Rb,Nr, za,zb,Nz, fa,fb,Nf, Nc    
      Ncomppre=Nc;Nfpre=Nf;Nrpre=Nr;Nzpre=Nz
      Rapre=Ra;Rbpre=Rb
      zapre=za;zbpre=zb
      fapre=fa;fbpre=fb
      if (allocated(ba)) deallocate(ba)      
      allocate (ba(Ncomppre,Nfpre-1,Nrpre,Nzpre))
      ba=0.d0
      end subroutine
      
C----.|-----------------------------------------------------------------      

      subroutine add_restored_field(fn,factor)
      character(*),intent(in) :: fn
      real*8 :: factor
      integer,parameter :: iun=2
      real*8,allocatable :: bb(:,:,:,:)
Cf2py intent(in) fn      
      open(iun,file=fn,access='stream')
      read (iun) Nfpre,Nrpre,Nzpre,Ncomppre
      write (*,*) 'grid restored from',fn
      write (*,*) 'Nf,Nr,Nz=',Nfpre,Nrpre,Nzpre
      read (iun) Rapre,Rbpre
      read (iun) zapre,zbpre
      read (iun) fapre,fbpre      
      write (*,*) 'valid R domain ',Rapre,'..' ,Rbpre
      write (*,*) 'valid z domain ',zapre,'..' ,zbpre
      write (*,*) 'valid phi domain ',fapre,'..' ,fbpre
      if (.not.allocated(ba)) allocate(ba(Ncomppre,Nfpre-1,Nrpre,Nzpre))
      allocate(bb(Ncomppre,Nfpre-1,Nrpre,Nzpre))
      read (iun) bb
      close(iun) 
      ba=ba+bb*factor
      deallocate(bb)
      end subroutine

C----.|-----------------------------------------------------------------      


      subroutine biotsavd(xac,yac,zac,bx,by,bz,  
     .                    dbxx,dbxy,dbxz,dbyx,dbyy,dbyz,dbzx,dbzy,dbzz)
!-----------------------------------------------------------------------------!
! Biot-Savart's law:                                                          !
! magnetic field and first derivatives in cartesian coordinates on a grid     !
! point                                                                       !
! written by E. Strumberger                                         IPP >1997 !
!                                                      last change 09/06/2010 !
!-----------------------------------------------------------------------------!

      implicit none 
     
      integer :: i,nall

      real*8    :: dx1,dy1,dz1,dx2,dy2,dz2,rw1,rw2,r12,ri1,ri2
      real*8    :: ak,sk,fk,som,dfkx,dfky,dfkz,rotx,roty,rotz
      real*8    :: curr
      real*8    :: xac,yac,zac,bx,by,bz 
      real*8    :: dbxx,dbxy,dbxz,dbyx,dbyy,dbyz,dbzx,dbzy,dbzz
       
       
      nall=ncel 
      bx=0.d0
      by=0.d0
      bz=0.d0
      dbxx=0.d0
      dbxy=0.d0
      dbxz=0.d0
      dbyx=0.d0
      dbyy=0.d0
      dbyz=0.d0
      dbzx=0.d0
      dbzy=0.d0
      dbzz=0.d0

      dx1  =xac-xcel(1)
      dx2  =xac-xcel(2)
      dy1  =yac-ycel(1)
      dy2  =yac-ycel(2)
      dz1  =zac-zcel(1)
      dz2  =zac-zcel(2)
      rw1  =sqrt(dx1*dx1+dy1*dy1+dz1*dz1) 
      rw2  =sqrt(dx2*dx2+dy2*dy2+dz2*dz2)
      ri1  =1.d0/(rw1*rw1)
      ri2  =1.d0/(rw2*rw2)
      r12  =rw1*rw2
      som  =rw1+rw2
      sk   =1.d0/(r12*(r12+dx1*dx2+dy1*dy2+dz1*dz2))
      fk   =som*sk                                  
      curr =Icel(1)*bs_factor
      dfkx =-sk*(dx1*rw2*ri1+dx2*rw1*ri2) 
     .      -som*som*sk*sk*(rw2*dx1+rw1*dx2)
      dfky =-sk*(dy1*rw2*ri1+dy2*rw1*ri2) 
     .      -som*som*sk*sk*(rw2*dy1+rw1*dy2)
      dfkz =-sk*(dz1*rw2*ri1+dz2*rw1*ri2) 
     .      -som*som*sk*sk*(rw2*dz1+rw1*dz2)
      ak   =curr*fk                                            
      rotx =dy1*dz2-dz1*dy2
      roty =dz1*dx2-dx1*dz2
      rotz =dx1*dy2-dy1*dx2
      bx   =bx + ak*rotx               
      by   =by + ak*roty             
      bz   =bz + ak*rotz                
      dbxx =dbxx+curr*rotx*dfkx
      dbxy =dbxy+curr*((dz2-dz1)*fk+rotx*dfky)
      dbxz =dbxz+curr*((dy1-dy2)*fk+rotx*dfkz)
      dbyx =dbyx+curr*((dz1-dz2)*fk+roty*dfkx)
      dbyy =dbyy+curr*roty*dfky
      dbyz =dbyz+curr*((dx2-dx1)*fk+roty*dfkz)
      dbzx =dbzx+curr*((dy2-dy1)*fk+rotz*dfkx)
      dbzy =dbzy+curr*((dx1-dx2)*fk+rotz*dfky)
      dbzz =dbzz+curr*rotz*dfkz

      do i=3,nall
        dx1  =dx2
        dx2  =xac-xcel(i)
        dy1  =dy2
        dy2  =yac-ycel(i)
        dz1  =dz2
        dz2  =zac-zcel(i)
        rw1  =rw2
        rw2  =sqrt(dx2*dx2+dy2*dy2+dz2*dz2)
        ri1  =ri2            
        ri2  =1.d0/(rw2*rw2)
        r12  =rw1*rw2
        som  =rw1+rw2
        sk   =1.d0/(r12*(r12+dx1*dx2+dy1*dy2+dz1*dz2))
        fk   =som*sk
        curr =Icel(i-1)*bs_factor
        dfkx =-sk*(dx1*rw2*ri1+dx2*rw1*ri2) 
     .        -som*som*sk*sk*(rw2*dx1+rw1*dx2)
        dfky =-sk*(dy1*rw2*ri1+dy2*rw1*ri2) 
     .        -som*som*sk*sk*(rw2*dy1+rw1*dy2)
        dfkz =-sk*(dz1*rw2*ri1+dz2*rw1*ri2) 
     .        -som*som*sk*sk*(rw2*dz1+rw1*dz2)
        rotx =dy1*dz2-dz1*dy2
        roty =dz1*dx2-dx1*dz2
        rotz =dx1*dy2-dy1*dx2
        ak =curr*fk
        bx =bx + ak*rotx                
        by =by + ak*roty                  
        bz =bz + ak*rotz               
        dbxx =dbxx+curr*rotx*dfkx
        dbxy =dbxy+curr*((dz2-dz1)*fk+rotx*dfky)
        dbxz =dbxz+curr*((dy1-dy2)*fk+rotx*dfkz)
        dbyx =dbyx+curr*((dz1-dz2)*fk+roty*dfkx)
        dbyy =dbyy+curr*roty*dfky
        dbyz =dbyz+curr*((dx2-dx1)*fk+roty*dfkz)
        dbzx =dbzx+curr*((dy2-dy1)*fk+rotz*dfkx)
        dbzy =dbzy+curr*((dx1-dx2)*fk+rotz*dfky)
        dbzz =dbzz+curr*rotz*dfkz
      end do

      end subroutine biotsavd

C----.|-----------------------------------------------------------------      
         
      FUNCTION BPRE_RZP(RZP)
      USE TOOLS
      implicit none

      real*8,intent(in)   :: RZP(3)
      REAL*8              :: BPRE_RZP(3)
!      INTEGER,INTENT(OUT) :: IDIV
      integer :: i,lf1,lf2,lr1,lr2,lz1,lz2

      real*8,dimension(3) :: b01,b02,b03,b04,b05,b06,b07,b08,b09,b10
      real*8,dimension(3) :: b11,b12,b13,b14,b15,b16,b17,b18,b19,b20
      real*8,dimension(3) :: bi
      real*8            :: pp1,pp2,pr1,pr2,pz1,pz2
      real*8  :: df,dR,dz,diprz3,f

      BPRE_RZP=NaN()
      ierr_bpre = 1
      if ((Nfpre.eq.0).or.(NRpre.eq.0).or.
     .    (Nzpre.eq.0).or.(Ncomppre.eq.0)) then
       write (*,*) 'ba is not initialized.'
       return
      end if

      b01 = 0.d0
      b02 = 0.d0
      b03 = 0.d0
      b04 = 0.d0
      b05 = 0.d0
      b06 = 0.d0
      b07 = 0.d0
      b08 = 0.d0
      b09 = 0.d0
      b10 = 0.d0
      b11 = 0.d0
      b12 = 0.d0
      b13 = 0.d0
      b14 = 0.d0
      b15 = 0.d0
      b16 = 0.d0
      b17 = 0.d0
      b18 = 0.d0
      b19 = 0.d0
      b20 = 0.d0

      bi     = 0.d0

      df=(fbpre-fapre)/(Nfpre-1)
      dR=(Rbpre-Rapre)/(Nrpre-1)
      dz=(zbpre-zapre)/(Nzpre-1)   
      diprz3  = 1.d0/(df*dr*dz)

      f=modulo(RZP(3)-fapre,fbpre-fapre) + fapre

      lf1    = int((f-fapre)/df) +1
      lr1    = int((RZP(1)-Rapre)/dr) +1
      lz1    = int((RZP(2)-zapre)/dz) +1
      

      if((lf1.ge.1).and.(lf1.lt.Nfpre).and.
     .   (lr1.ge.1).and.(lr1.lt.Nrpre).and.
     .   (lz1.ge.1).and.(lz1.lt.Nzpre)) then
        
	
        lf2    = lf1+1
        lr2    = lr1+1
        lz2    = lz1+1

        pp1    = f-(fapre+(lf1-1)*df)
        pp2    = df-pp1
        pr1    = RZP(1)-(Rapre+(lr1-1)*dr)
        pr2    = dr-pr1
        pz1    = RZP(2)-(zapre+(lz1-1)*dz)
        pz2    = dz-pz1


        if (lf2.eq.Nfpre) lf2=1

        do i=1,3
          b01(i)  =  ba(i,lf1,lr1,lz1)
          b02(i)  =  ba(i,lf2,lr1,lz1)
          b03(i)  =  ba(i,lf1,lr2,lz1)
          b04(i)  =  ba(i,lf2,lr2,lz1)
          b05(i)  =  ba(i,lf1,lr1,lz2)
          b06(i)  =  ba(i,lf2,lr1,lz2)
          b07(i)  =  ba(i,lf1,lr2,lz2)
          b08(i)  =  ba(i,lf2,lr2,lz2)

          if (Ncomppre.ge.4) then
           b09(i)  =  ba(i*3+1,lf1,lr1,lz1)-ba(i*3+1,lf2,lr1,lz1)
           b10(i)  =  ba(i*3+2,lf1,lr1,lz1)-ba(i*3+2,lf1,lr2,lz1)
           b11(i)  =  ba(i*3+3,lf1,lr1,lz1)-ba(i*3+3,lf1,lr1,lz2) 

           b12(i)  =  ba(i*3+1,lf1,lr2,lz1)-ba(i*3+1,lf2,lr2,lz1)
           b13(i)  =  ba(i*3+2,lf2,lr1,lz1)-ba(i*3+2,lf2,lr2,lz1)
           b14(i)  =  ba(i*3+3,lf2,lr1,lz1)-ba(i*3+3,lf2,lr1,lz2)

           b15(i)  =  ba(i*3+1,lf1,lr1,lz2)-ba(i*3+1,lf2,lr1,lz2)
           b16(i)  =  ba(i*3+2,lf1,lr1,lz2)-ba(i*3+2,lf1,lr2,lz2)
           b17(i)  =  ba(i*3+3,lf1,lr2,lz1)-ba(i*3+3,lf1,lr2,lz2)

           b18(i)  =  ba(i*3+1,lf1,lr2,lz2)-ba(i*3+1,lf2,lr2,lz2)
           b19(i)  =  ba(i*3+2,lf2,lr1,lz2)-ba(i*3+2,lf2,lr2,lz2)
           b20(i)  =  ba(i*3+3,lf2,lr2,lz1)-ba(i*3+3,lf2,lr2,lz2)
	  end if 
        enddo



        do i=1,3
          bi(i)=diprz3*(pp2*pr2*pz2*b01(i)+ 
     .                  pp1*pr2*pz2*b02(i)+ 
     .                  pp2*pr1*pz2*b03(i)+ 
     .                  pp1*pr1*pz2*b04(i)+ 
     .                  pp2*pr2*pz1*b05(i)+ 
     .                  pp1*pr2*pz1*b06(i)+ 
     .                  pp2*pr1*pz1*b07(i)+ 
     .                  pp1*pr1*pz1*b08(i)+ 
     .         0.5d0*(pp1*pp2*pr2*pz2*b09(i)+ 
     .              pp2*pr1*pr2*pz2*b10(i)+ 
     .              pp2*pr2*pz1*pz2*b11(i)+ 
     .              pp1*pp2*pr1*pz2*b12(i)+ 
     .              pp1*pr1*pr2*pz2*b13(i)+ 
     .              pp1*pr2*pz1*pz2*b14(i)+ 
     .              pp1*pp2*pr2*pz1*b15(i)+ 
     .              pp2*pr1*pr2*pz1*b16(i)+ 
     .              pp2*pr1*pz1*pz2*b17(i)+ 
     .              pp1*pp2*pr1*pz1*b18(i)+ 
     .              pp1*pr1*pr2*pz1*b19(i)+ 
     .              pp1*pr1*pz1*pz2*b20(i)))
        enddo
       BPRE_RZP=(/bi(2),bi(3),bi(1)/)
       ierr_bpre=0
      else
       ierr_bpre=1
      endif

      end FUNCTION

C----.|-----------------------------------------------------------------      
      
      FUNCTION BPRE_RZP_direct(RZP)
!      subroutine BPRE_RZF_online(R,z,phi,BF,BR,BZ,IDIV)
      implicit none
      real*8,intent(in)   :: RZP(3)
      real*8 :: BPRE_RZP_direct(3)
!       real*8,intent(in)   :: R,z,phi
!       REAL*8, INTENT(OUT) :: BF,BR,BZ
!       INTEGER,INTENT(OUT) :: IDIV
      real*8    :: x,y,bx,by
      real*8    :: dbxx,dbxy,dbxz,dbyx,dbyy,dbyz,dbzx,dbzy,dbzz

      x=RZP(1)*cos(RZP(3));y=RZP(1)*sin(RZP(3));
      call biotsavd(x,y,RZP(2),bx,by,BPRE_RZP_direct(2),  
     .              dbxx,dbxy,dbxz,dbyx,dbyy,dbyz,dbzx,dbzy,dbzz)
      BPRE_RZP_direct(3) = -bx*sin(RZP(3)) +by*cos(RZP(3))
      BPRE_RZP_direct(1) = +bx*cos(RZP(3)) +by*sin(RZP(3))
      ierr_bpre=0      
      end function BPRE_RZP_direct

C----.|-----------------------------------------------------------------      

      subroutine circular_coil
      integer :: i
      if (allocated(xcel)) deallocate(xcel)
      if (allocated(ycel)) deallocate(ycel)
      if (allocated(zcel)) deallocate(zcel)      
      if (allocated(Icel)) deallocate(Icel)      
      
      ncel=1000
      allocate(xcel(ncel),ycel(ncel),zcel(ncel),Icel(ncel))
      write (*,*) 'defining circular current loop'

      do i=1,ncel
       xcel(i)=1.d0*cos(2*pi*(i-1)/(ncel-1))
       ycel(i)=1.d0*sin(2*pi*(i-1)/(ncel-1))
       zcel(i)=0.d0
      enddo
      Icel=1.d0  
      end subroutine

C----.|-----------------------------------------------------------------      


      subroutine long_wire
      integer :: i
      real*8,parameter :: dz=2.d2
      if (allocated(xcel)) deallocate(xcel)
      if (allocated(ycel)) deallocate(ycel)
      if (allocated(zcel)) deallocate(zcel)      
      if (allocated(Icel)) deallocate(Icel)      
      
      ncel=10
      allocate(xcel(ncel),ycel(ncel),zcel(ncel),Icel(ncel))
      write (*,*) 'defining long linear current filament'
      xcel(:)=0.d0;ycel(:)=0.d0
      
      do i=1,ncel
       zcel(i)=(2.d0*i/float(ncel)-1)*dz
      enddo
      Icel=1.d0  
      end subroutine

C----.|-----------------------------------------------------------------      

      end module BPRECALC
    
