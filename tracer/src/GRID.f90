      module GRID
C     routines for grids for EMC3 and other codes
      
      implicit none
      REAL*8,DIMENSION(:,:),allocatable  :: wall
      real*8 :: wrl=-1.d0
      integer :: Nwall      
      
      contains
!-----|----------------------------------------------------------------|           
      SUBROUTINE expand3d(G,Nir,Nip,Nit,dphi)
      USE TRACE      
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: Nir,Nip,Nit
      REAL*8,DIMENSION(Nir,Nip,Nit,3),INTENT(INOUT) :: G
      REAL*8,INTENT(IN) :: dphi
      integer :: Nith,ir,ip
      Nith=(Nit-1)/2
      !write(*,*) 'Nir,Nip,Nit',Nir,Nip,Nit!G(:,1,Nith+1,1)
      do ir=1,Nir
       do ip=1,Nip
       CALL FIELD_LINE(G(ir,ip,1+Nith,:),dphi,Nit-1,3,G(ir,ip,:,:),15,0)
       enddo
      enddo 
      RETURN
      END SUBROUTINE expand3d
      
!-----|----------------------------------------------------------------|           
      SUBROUTINE expandSF(SF,Nip,Nit,dphi,NSUBS)
      USE TRACE      
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: Nip,Nit,NSUBS
      REAL*8,DIMENSION(Nip,0:Nit,3),INTENT(INOUT) :: SF
      REAL*8,INTENT(IN) :: dphi
Cf2py integer,intent(in), optional :: NSUBS=5      
      integer :: ip
      do ip=1,Nip
       CALL FIELD_LINE(SF(ip,0,:),dphi,Nit,3,SF(ip,:,:),NSUBS,1)
      enddo 
      RETURN
      END SUBROUTINE expandSF
      
      
!-----|----------------------------------------------------------------|           
      SUBROUTINE set_wall(c,N)
      USE TOOLS
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: N
      REAL*8,DIMENSION(N,2),INTENT(INOUT) :: c
      integer :: i
      if (allocated(wall)) deallocate(wall)
      allocate(wall(N+1,3))
      wall(1:N,1:2)=c(1:N,:)
      wall(N+1,1:2)=c(1,:) ! close the wall structure     
      Nwall=N+1;wall(:,3)=0.d0
      do i=2,Nwall
       wall(i,3)=wall(i-1,3) + length(wall(i,1:2)-wall(i-1,1:2))
      enddo
      END SUBROUTINE set_wall
      
!-----|----------------------------------------------------------------|           
!-----|----------------------------------------------------------------|           
      function wallray3(p,a)  
      use BAXISYM
      use TOOLS
      IMPLICIT NONE
      real*8,  intent(in) :: p(2),a
      real*8              :: wallray3(2),q(2),x2(2),y2(2),ccc(4)
      real*8              :: r(2),rq(2,3)
      integer             :: i1,i2
       q=pspnt(p,opoint(1:2),xpoint(1:2,1))
       q=(q-opoint(1:2))*a+opoint(1:2)
       r=(p-q)*30.+q
      rq(1,1:2)=q
      rq(2,1:2)=normv2(p-q)*20.+q ! for a machine with a larger minor radius than 20 m increase this number.
      rq(2,3)=1.d0
       ccc=curve_curve_coll_Rzl(rq,wall,2,Nwall,0.d0,0.d0)
       wallray3=ccc(1:2)
       
      end function


!-----|----------------------------------------------------------------|           
      function pspnt(p,op,xp)
      real*8, intent(in)  :: p(2),op(2),xp(2)
      real*8 :: a(2),b(2),proj,pspnt(2),l
      a=xp-op; l=sqrt(sum(a**2));a=a/l
      b=p-op
      proj=dot_product(a,b)
      if (proj.lt.0.d0) proj=0.d0
      if (proj.gt.l)    proj=l     
      pspnt=proj*a+op
      end function 

 
      function wallintersection(p,po,px)  
      use BAXISYM
      use TOOLS
      IMPLICIT NONE
      real*8,  intent(in) :: p(2),po(2),px(2)
      real*8 :: wallintersection(4),proj,q(2),rq(2,3)=0.d0,ccc(4)
      proj=dot(p-po,px-po)/max(length(px-po)**2,1d-6)
      proj=max(min(proj,1.d0),0.d0)
      !proj=max(proj,0.d0)
      q=po+(px-po)*proj
      rq(1,1:2)=q
      rq(2,1:2)=normv2(p-q)*20.+q ! for a machine with a larger minor radius than 20 m increase this number.
      rq(2,3)=1.d0
      wallintersection=curve_curve_coll_Rzl(rq,wall,2,Nwall,0.d0,0.d0)
      end function

      function wallray(p,po,px)
      real*8,  intent(in) :: p(2),po(2),px(2)  
      real*8 :: wallray(2),wi(4)
      wi=wallintersection(p,po,px)  
      wallray=wi(1:2)
      end function
      
      function wallc(p,po,px)  
      real*8,  intent(in) :: p(2),po(2),px(2)  
      real*8 :: wallc,wi(4)
      wi=wallintersection(p,po,px)  
      wallc=wi(4)
      end function
      
!-----|----------------------------------------------------------------|           

      SUBROUTINE expand2wall(G,Nir,Nip,Nit,typ,Nn,ipc,pc0,pc1,plr,so)
      
!      |--3-----4--|-----------
!      |     
!      2
!      |     
!      |     x---ipc1----------
!      |     |
!      1     |
!      |     |
!      ------------------------
!     the factors in 'so' stretch the intervals 1,2,3 left and 1,2,3 right followed by factors that move the left and right corners out. The intervals 4 are determined by the condition to fit the required length
      
      USE TRACE 
      USE TOOLS
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: Nir,Nip,Nit,typ,Nn,ipc(2)
      REAL*8,DIMENSION(Nir,Nip,Nit,3),INTENT(INOUT) :: G
      REAL*8,INTENT(IN) :: pc0(2),pc1(2),so(8),plr(4,2)
      REAL*8 :: pxx(2),a(2),b(2),c(2),d(2),x,lw1,lw2
      REAL*8,DIMENSION(:),ALLOCATABLE :: ll
      integer :: ir,iip1,iip2,ip,it,it0,Nl(2),il,iip(2),di(2),ii(2)
            
      iip1=1+Nn;iip2=Nip-Nn;it0=(Nit-1)/2+1
      !write(*,*) Nir,Nip,Nit,Nn
      
      
      iip=(/iip1,iip2/)
      
      !#################################################################      
      !extend the grid on both sides poloidally and on the outer radial side
      !#################################################################      
      if (typ.eq.1) then

      !project last radial sf to the wall
      !write(*,*) 'pc0,pc1',pc0,pc1
      do ip=ipc(1),ipc(2)
       G(Nir,ip,it0,1:2)=wallray(G(Nir-Nn,ip,it0,1:2),pc0,pc1)
!       write(*,*) 'wallray',ip,G(Nir-Nn,ip,it0,1:2),G(Nir,ip,it0,1:2)
      end do !ip 
      
      
      Nl=(/ipc(1),Nip-ipc(2)/)
      di=(/1,-1/)
      
      do il=1,2
      
       lw1=wallc(G(1,iip(il),it0,1:2),plr(il,:),plr(il,:))
       lw2=wallc(G(Nir-Nn,ipc(il),it0,1:2),pc0,pc1)
      
      
       allocate(ll(Nir+Nl(il)));ll(:)=0.d0
       do ir=2,Nir-Nn
        ll(ir)=length(G(ir,iip(il),it0,1:2)-G(ir-1,iip(il),it0,1:2))
     .         *so(3*il-2)
       enddo
      
       do ip=Nn+2,Nl(il)
        ii=(/ip,Nip-ip/)
        ll(Nir+ip)=
     .  length(G(Nir-Nn,ii(il),it0,1:2)-G(Nir-Nn,ii(il)-di(il),it0,1:2))
       enddo

       ll(Nir-Nn+1:Nir)=mean(ll(2:Nir-Nn))*so(3*il-1)
       ll(Nir+1:Nir+Nn+1)=mean(ll(Nir+Nn+2:Nir+Nl(il)))*so(3*il)

       do ir=2,Nir+Nl(il)
        ll(ir)=ll(ir-1)+ll(ir)
       enddo
      
       ll=lw1+ll/ll(Nir+Nl(il))*(lw2-lw1)
       
       !write(*,*) 'll',ll/ll(Nir+Nl(il))

       ii=(/1,Nip/)
       do ir=1,Nir
        G(ir,ii(il),it0,1:2)=l2Rz(ll(ir),wall,Nwall)
       enddo
      
       do ip=2,Nl(il)
        ii=(/ip,Nip-ip/)
        G(Nir,ii(il),it0,1:2)=l2Rz(ll(Nir+ip),wall,Nwall)
       enddo
      
       deallocate(ll)
      enddo !il
      
       
      !interpolate linearly in radial direction
      do it=1,Nit
       do ip=iip1,iip2 
	a=G(Nir-Nn,ip, it,1:2)
	b=G(Nir   ,ip,it0,1:2)	 
        do ir=Nir-Nn+1,Nir
	 G(ir,ip,it,1:2)=a+(b-a)*(ir-Nir+Nn)/float(Nn)
	end do !ir
       end do !ip
      end do !it        

      
      endif ! typ=1
      
      
      
      !#################################################################
      !extend only he poloidal edges (e.g. in the near-SOL region of a SF)
      !#################################################################      
      if (typ.eq.2) then
      do il=1,2
       lw1=wallc(G(  1,iip(il),it0,1:2),plr(il,:),plr(il,:))
       lw2=wallc(G(Nir,iip(il),it0,1:2),plr(il+2,:),plr(il+2,:))
      
       allocate(ll(Nir));ll(:)=0.d0
       do ir=2,Nir
        ll(ir)=ll(ir-1)+
     .         length(G(ir,iip(il),it0,1:2)-G(ir-1,iip(il),it0,1:2))
       enddo
       write(*,*) 'll',ll     
       ll=lw1+ll/ll(Nir)*(lw2-lw1)

       ii=(/1,Nip/)
       do ir=1,Nir
        G(ir,ii(il),it0,1:2)=l2Rz(ll(ir),wall,Nwall)
       enddo
            
       deallocate(ll)
      enddo !il
       
      endif ! typ=2
      
      !--------------------------------------------
      
       !interpolate linearly in poloidal direction
       do it=1,Nit
        do ir=1,Nir
	 a=G(ir,iip1, it,1:2)
	 b=G(ir,   1,it0,1:2)	 
         do ip=1,iip1-1
	  G(ir,ip,it,1:2)=b+(a-b)*(ip-1)/float(Nn)
	  G(ir,ip,it,3)=G(ir,iip1, it,3)
	 end do
	 a=G(ir,iip2, it,1:2)
	 b=G(ir, Nip,it0,1:2)	 
         do ip=iip2+1,Nip
	  G(ir,ip,it,1:2)=a+(b-a)*(ip-iip2)/float(Nn)
	 end do !ip
        end do !ir
       end do !it 

      if (typ.eq.1) then 
      !move the corners a little further out
      a=G(Nir-1,1,it0,1:2)-G(Nir-1,2,it0,1:2)
      b=G(Nir  ,2,it0,1:2)-G(Nir-1,2,it0,1:2)
      c=G(Nir-1,Nip  ,it0,1:2)-G(Nir-1,Nip-1,it0,1:2)
      d=G(Nir  ,Nip-1,it0,1:2)-G(Nir-1,Nip-1,it0,1:2)
      
      do it=1,Nit
       G(Nir,  1,it,1:2)=G(Nir,  1,it0,1:2)+normv2(a+b)*so(7)
       G(Nir,Nip,it,1:2)=G(Nir,Nip,it0,1:2)+normv2(c+d)*so(8)
      enddo   
      endif !typ==1

      !set the phi coordinate whereever it is not set
      do it=1,Nit;do ip=1,Nip;do ir=1,Nir
       if (isnan(G(ir,ip,it,3))) G(ir,ip,it,3)=G(1,iip1,it,3) 
      enddo;enddo;enddo
      
      
      RETURN
      END SUBROUTINE expand2wall
!-----|----------------------------------------------------------------|           

      SUBROUTINE get_B_grid(G,Nir,Nip,Nit,B)
      USE BFIELD
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: Nir,Nip,Nit
      REAL*8,DIMENSION(Nir,Nip,Nit,3),INTENT(IN) :: G
      REAL*8,DIMENSION(Nir,Nip,Nit),INTENT(OUT)  :: B
      REAL*8 :: BB(3)      
      integer :: ir,ip,it
      do it=1,Nit
       do ip=1,Nip
        do ir=1,Nir
         BB=B_RZP(G(ir,ip,it,:))
         if (count(isnan(BB)).ne.0) BB=0.d0
         B(ir,ip,it)=sqrt(BB(1)**2+BB(2)**2+BB(3)**2)
        end do
       end do
      end do
      END SUBROUTINE

!-----|----------------------------------------------------------------|           

      SUBROUTINE writeG(G,Nir,Nip,Nit)
      USE BFIELD
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: Nir,Nip,Nit
      REAL*8,DIMENSION(Nir,Nip,Nit,3),INTENT(IN) :: G
      integer :: ir,ip,it,iun=10
      open(iun,file='output/grid.txt',position='append')
      
      write (iun,*) Nir,Nip,Nit
      do it=1,Nit 
       write (iun,'(F12.6)') G(1,1,it,3)*180.d0/3.1415
       write (iun,'(F12.6)') G(:,:,it,1)*100
       write (iun,'(F12.6)') G(:,:,it,2)*100
      end do      
      close(iun)      
     
      END SUBROUTINE
      
!-----|----------------------------------------------------------------|           
      SUBROUTINE GRID3D_CHECK(RZP,N1,N2,N3,N4,FAILURE)
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: N1,N2,N3,N4
      REAL*8,DIMENSION(N1,N2,N3,N4),INTENT(IN) :: RZP
      LOGICAL,DIMENSION(N1-1,N2-1,N3),INTENT(OUT) :: FAILURE
      integer :: it
      do it=1,N3
       call GRID2D_CHECK(RZP(:,:,it,1:2),N1,N2,2,FAILURE(:,:,it))
      enddo
      END SUBROUTINE
      
!-----|----------------------------------------------------------------|           
      SUBROUTINE GRID2D_CHECK(RZ,N1,N2,N3,FAILURE)
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: N1,N2,N3
      REAL*8,DIMENSION(N1,N2,N3),INTENT(IN) :: RZ
      LOGICAL,DIMENSION(N1-1,N2-1),INTENT(OUT) :: FAILURE
      REAL*8,DIMENSION(N1,N2) :: RG,ZG
      
      INTEGER :: I,J
      REAL*8  :: A1,A2,DIRECT
      

      RG=RZ(:,:,1);ZG=RZ(:,:,2)
!       write(*,*) N1,N2,N3
!       write(*,*) 'RG',RG(:,1)
!       write(*,*) 'ZG',ZG(:,1)
      
      DIRECT = 0.
      I = 1
      DO J=1,N2-1
        A1=(RG(I+1,J+1)-RG(I  ,J+1))*(ZG(I  ,J+1)-ZG(I  ,J  ))
     .-    (RG(I  ,J+1)-RG(I  ,J  ))*(ZG(I+1,J+1)-ZG(I  ,J+1))
        A2=(RG(I+1,J  )-RG(I  ,J  ))*(ZG(I+1,J+1)-ZG(I+1,J  ))
     .-    (RG(I+1,J+1)-RG(I+1,J  ))*(ZG(I+1,J  )-ZG(I  ,J  ))
        DIRECT = DIRECT+A1+A2
      ENDDO
      FAILURE=.FALSE.
      DO 10 I =1,N1-1
      DO 10 J =1,N2-1
        A1=(RG(I+1,J+1)-RG(I  ,J+1))*(ZG(I  ,J+1)-ZG(I  ,J  ))
     .-    (RG(I  ,J+1)-RG(I  ,J  ))*(ZG(I+1,J+1)-ZG(I  ,J+1))
        A2=(RG(I+1,J  )-RG(I  ,J  ))*(ZG(I+1,J+1)-ZG(I+1,J  ))
     .-    (RG(I+1,J+1)-RG(I+1,J  ))*(ZG(I+1,J  )-ZG(I  ,J  ))
        IF( A1*DIRECT<0..or.A2*DIRECT<0.) THEN
!           WRITE(6,*)'X-like mesh or overlapping cell detected'
!           write(6,*)'I,J=',I,J
!           write(6,*)'Areas=',A1*DIRECT,A2*DIRECT
!           write(6,'(4f12.4)') RG(I,J),RG(I,J+1)
!      .,                      RG(I+1,J+1),RG(I+1,J)
!           write(6,'(4f12.4)') ZG(I,J),ZG(I,J+1)
!      .,                      ZG(I+1,J+1),ZG(I+1,J)
          FAILURE(I,J) = .TRUE.
!	  stop
        ENDIF
10    CONTINUE
!      IF( .NOT.FAILURE) WRITE(6,*)' Regular mesh! ',N1,N2
      
      RETURN
      END SUBROUTINE GRID2D_CHECK
!-----|----------------------------------------------------------------|  

      SUBROUTINE intersect2D(G,Nir,Nip,Nit,H,poly,Npoly)
      USE TOOLS
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: Nir,Nip,Nit,Npoly
      REAL*8,DIMENSION(Nir,Nip,Nit,3),INTENT(IN) :: G
      REAL*8,DIMENSION(Npoly,2),INTENT(IN) :: poly
      LOGICAL,DIMENSION(Nir-1,Nip-1,Nit-1),INTENT(INOUT) :: H
      integer :: ir,ip,it
      real*8  :: p(2),cell(4,2)
      write(*,'(a)',advance="no") 'computing 2D intersection'
      DO it=1,Nit
       write(*,'(a)',advance="no") '.'
       DO ir=1,Nir-1;DO ip=1,Nip-1
         cell(1,:)=G(ir  ,ip  ,it,1:2)
         cell(2,:)=G(ir+1,ip  ,it,1:2)
         cell(3,:)=G(ir+1,ip+1,it,1:2)
         cell(4,:)=G(ir  ,ip+1,it,1:2)
         if (polygons_intersect(cell,4,poly,Npoly)) then
          H(ir,ip,max(it-1,1))  =.true.
          H(ir,ip,min(it,Nit-1))=.true.
         endif
       ENDDO;ENDDO
      ENDDO
      
      END SUBROUTINE
!-----|----------------------------------------------------------------|  

      SUBROUTINE intersect3D(G,Nir,Nip,Nit,H,tar,Npt,Ntt)
      USE TOOLS
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: Nir,Nip,Nit,Npt,Ntt
      REAL*8,DIMENSION(Nir,Nip,Nit,3),INTENT(IN) :: G
      LOGICAL,DIMENSION(Nir-1,Nip-1,Nit-1),INTENT(INOUT) :: H
      REAL*8,INTENT(IN) :: tar(Npt,Ntt,3)
      integer :: ir,ip,it,itt
      real*8  :: mint,maxt,x
      REAL*8  :: t(Npt,2),cell(4,2)
      
      
      mint=minval(tar(1,:,3));maxt=maxval(tar(1,:,3))
      
      do it=1,Nit
       if ((mint.le.G(1,1,it,3)).and.(G(1,1,it,3).lt.maxt)) then
         do itt=1,Ntt-1
          x=(G(1,1,it,3)-tar(1,itt,3))/(tar(1,itt+1,3)-tar(1,itt,3))
          if ((0.d0.le.x).and.(x.lt.1.d0)) exit
         enddo !itt
         !get the target shape in the plane it
         t(:,:)=tar(:,itt,1:2)*(1.d0-x)+tar(:,itt+1,1:2)*x
         
         DO ir=1,Nir-1;DO ip=1,Nip-1         
          cell(1,:)=G(ir  ,ip  ,it,1:2)
          cell(2,:)=G(ir+1,ip  ,it,1:2)
          cell(3,:)=G(ir+1,ip+1,it,1:2)
          cell(4,:)=G(ir  ,ip+1,it,1:2)
          if (polygons_intersect(cell,4,t,Npt)) then
           H(ir,ip,max(it-1,1))  =.true.
           H(ir,ip,min(it,Nit-1))=.true.
          endif
         enddo;enddo  
        !write(*,*) 'found',it,itt,x
       endif
      enddo !it
      END SUBROUTINE

!-----|----------------------------------------------------------------|  

      SUBROUTINE intersect(G,Nir,Nip,Nit,H,TR,Ntr)
      USE TOOLS
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: Nir,Nip,Nit,Ntr
      REAL*8,DIMENSION(Nir,Nip,Nit,3),INTENT(IN) :: G
      REAL*8,DIMENSION(Ntr,4,2),INTENT(IN) :: TR
      LOGICAL,DIMENSION(Nir-1,Nip-1,Nit-1),INTENT(INOUT) :: H
      logical :: coll
      integer :: ir,ip,it,its,itr
      REAL*8  :: P(2),Rz(3,2),phi
      
      write(*,'(a)',advance="no") 'computing intersection'
      DO its=1,Nit-1
       write(*,'(a)',advance="no") '.'
       DO ir=1,Nir-1;DO ip=1,Nip-1
        DO it=its,min(its+1,Nit-1)
        phi=G(ir,ip,it,3)
        Rz(1,:)=G(ir,ip,it,1:2)
        Rz(2,:)=G(ir+1,ip,it,1:2);Rz(3,:)=G(ir,ip+1,it,1:2)
                      
        DO itr=1,Ntr
         coll=.false.
         if ((tr(itr,4,1).le.phi).and.(phi.lt.tr(itr,4,2)))
     .     coll=triangles_collision(Rz,tr(itr,1:3,:))
         H(ir,ip,it)=H(ir,ip,it).or.coll
        ENDDO!itr

        Rz(1,:)=G(ir+1,ip+1,it,1:2)
!       Rz(2,:)=G(ir+1,ip,it,1:2);Rz(3,:)=G(ir,ip+1,it,1:2) !already assigned
        DO itr=1,Ntr 
         coll=.false.
         if ((tr(itr,4,1).le.phi).and.(phi.lt.tr(itr,4,2)))
     .     coll=triangles_collision(Rz,tr(itr,1:3,:))
         H(ir,ip,it)=H(ir,ip,it).or.coll
        ENDDO!itr
        
        ENDDO !it
       ENDDO;ENDDO !ir,ip
      ENDDO !its
      END SUBROUTINE
!-----|----------------------------------------------------------------|           

      SUBROUTINE plates_mag(H,Nr,Np,Nt,PM,NPM)
      USE TOOLS
      IMPLICIT NONE
      INTEGER,INTENT(IN) :: Nr,Np,Nt
      LOGICAL,DIMENSION(Nr,Np,Nt),INTENT(IN) :: H
      INTEGER,DIMENSION(Nr*Np,3+2*Nt),INTENT(OUT) :: PM
      INTEGER,INTENT(OUT) :: NPM
      logical :: state,found
      integer :: ir,ip,it,k,l
      
      l=1
      do ir=1,Nr;do ip=1,Np
       k=0;state=.FALSE.;found=.FALSE.
       
       do it=1,Nt
        if (H(ir,ip,it).neqv.state) then
         !write(*,*) 'ir,ip,it',ir,ip,it,state
         if (.not.state) then
          PM(l,4+k)=it-1
         else
          PM(l,5+k)=it-2
          k=k+2
         endif 
         
         state=H(ir,ip,it) 
         found=.TRUE.
         endif ! state changes
       enddo !it
       
       if (found) then
        if (state) then
            PM(l,5+k)=it-2
            k=k+2
        endif !state
        PM(l,1)=ir-1;PM(l,2)=ip-1;PM(l,3)=k
        l=l+1 
       endif !found
       
      enddo;enddo !ir,ip
      NPM=l-1
      END SUBROUTINE
!-----|----------------------------------------------------------------|      

      end module GRID
