      MODULE BAXISYM    
      IMPLICIT NONE
      !public
C radial, poloidal field
      INTEGER,SAVE :: NR=0,NZ=0,NPER=1
      REAL*8,SAVE  :: DR=-1.d0,DZ=-1.d0
      REAL*8,SAVE  ::RMIN=0.d0,ZMIN=0.d0,RMAX=0.d0,ZMAX=0.d0
      REAL*8,DIMENSION(:,:),ALLOCATABLE :: PSI
      
      REAL*8,SAVE  :: tolerance=1.d-12 ! defines the accuracy of the singularity- and flux_surface search, change this parameter calling set_tolerance
      
C Singlarities 
      REAL*8, DIMENSION(0:2) :: OPOINT=-1.d0
      INTEGER :: NXP=0
      REAL*8, DIMENSION(0:2,2):: XPOINT=-1.d0

C Toroidal Field
      REAL*8                  :: R0=-1.d0,B0=0.d0 !T         
C Separatrix contour
      REAL*8,DIMENSION(:,:),ALLOCATABLE :: sep,sf ! Separatrix and some flux surface requested by find_flxsurface
      INTEGER,SAVE :: SEPNC=0,SEPNL=0,SEPNR=0 ! number of poloidal points on the Separatrix in left leg, core and right leg intervals
      REAL*8,SAVE :: pfr_refpt(2)=0.d0
      integer :: ierr_bas = 0
      character(100) :: last_error=''

C Upsream position
      REAL*8,SAVE :: Rz_u(2),dpsiu_dru,dpsiNu_dru,psi_u(-100:100)
      REAL*8,SAVE :: deltaru,theta_pitchu,ru_x2,PsiN_x2=-1.d0
!private 
      REAL*8,PARAMETER,PRIVATE:: PI=3.14159265358979312D0,PI2=PI*2.D0
      REAL*8,SAVE             :: DR1,DZ1

      CONTAINS
      
C----.|-----------------------------------------------------------------      

       subroutine set_psi(psipsi,nrnr,nznz,rmn,rmx,zmn,zmx)       
C
C      Set to poloidal flux matrix PSI=numpy.array(NR,NZ) defined by
C
C      BR =- 1/(2*pi*R) * dPSI/dz
C      Bz =  1/(2*pi*R) * dPSI/dR
C
C      Note the factor of 2*pi with respect to definition 3.2.2
C      in Wesson's book ('Tokamaks',3rd edition). PSI(R,z) is the 
C      poloidal magnetic flux through a(n arbitrary) surface with the 
C      border R=const,z=const,phi=0..2pi.
C      While the equilibrium code CLISTE uses the same definition as
C      here, EFIT uses that of Wesson's book, i.e.
C
C      PSI=PSI_CLISTE=2*pi*PSI_EFIT.
C

       implicit none
       integer :: nrnr,nznz
       real*8 :: psipsi(nrnr,nznz),rmn,rmx,zmn,zmx

Cf2py intent(in,out,copy) psipsi,rmn,rmx,zmn,zmx
Cf2py integer intent(hide),depend(psipsi) :: nrnr=shape(psipsi,0), nznz=shape(psipsi,1)

       NR=nrnr;NZ=nznz
       if (allocated(PSI)) deallocate(PSI)
       ALLOCATE(PSI(0:NR-1,0:NZ-1))
       PSI=psipsi
       RMIN=rmn;ZMIN=zmn;RMAX=rmx;ZMAX=zmx

       DR  = (RMAX-RMIN)/FLOAT(NR-1)
       DZ  = (ZMAX-ZMIN)/FLOAT(NZ-1)

       DR1 = 1.D0/DR
       DZ1 = 1.D0/DZ
      end subroutine set_psi
      
C----.|-----------------------------------------------------------------      

       subroutine set_Bt(bB0,rR0)       
       implicit none
       real*8 :: bB0,rR0
       B0=bB0;R0=rR0
       end subroutine set_Bt

C----.|-----------------------------------------------------------------      

      subroutine add_current_wires(RzI,N)
      implicit none
      real*8,intent(in) :: RzI(N,3) !R,z in m, I in A
      integer,intent(in) :: N
      real*8 :: R,z
      integer :: ir,iz,i
Cf2py intent(in) RzI,N
Cf2py depend(N) RzIl

!       write(*,*) 'add current wires'
!       do i=1,N
!        write(*,*) RzI(i,1),RzI(i,2),RzI(i,3)
!       enddo
      
      do ir=0,NR-1
      do iz=0,NZ-1
       R=RMIN+ir*DR;Z=ZMIN+iz*DZ
       do i=1,N
        psi(ir,iz)=psi(ir,iz)+
     .             psi_current_wire(R,z,RzI(i,1),RzI(i,2),RzI(i,3))
       enddo 
      enddo;enddo
      end subroutine add_current_wires


C----.|-----------------------------------------------------------------      

      SUBROUTINE RZ_TO_XY(R,Z,JR,JZ,X,Y,INSIDE)
      implicit none

      REAL*8 ,INTENT(IN   ) :: R,Z
      REAL*8 ,INTENT(  OUT) :: X,Y
      INTEGER,INTENT(  OUT) :: JR,JZ
      LOGICAL,INTENT(  OUT) :: INSIDE

      INSIDE = R>=RMIN.AND.R<=RMAX.AND.Z>=ZMIN.AND.Z<=ZMAX
      IF(.NOT.INSIDE) RETURN

      X   = (R-RMIN)*DR1
      JR  = X
      JR  = MIN(MAX(0,JR-1),NR-4)
      X   = X- DFLOAT(JR)

      Y   = (Z-ZMIN)*DZ1
      JZ  = Y
      JZ  = MIN(MAX(0,JZ-1),NZ-4)
      Y   = Y - DFLOAT(JZ)
      END SUBROUTINE RZ_TO_XY
      
C----.|-----------------------------------------------------------------      

      FUNCTION PSI_AT_RZ(Rz)
      USE INTERPOLATION
      USE TOOLS
      REAL*8 :: PSI_AT_RZ
      REAL*8, INTENT(IN ) :: Rz(2) 
      logical :: INSIDE
      integer :: JR,JZ
      real*8 :: X,Y

      call RZ_TO_XY(Rz(1),Rz(2),JR,JZ,X,Y,INSIDE)
      if( .not.INSIDE) then
        write(6,*)' outside the box: stopped in PSI_AT_RZ'  
        PSI_AT_RZ=NaN()
        return
!        stop
      endif
      PSI_AT_RZ=POLO_FL(PSI(JR:JR+3,JZ:JZ+3),X,Y) 
      END FUNCTION PSI_AT_RZ

C----.|-----------------------------------------------------------------      

      FUNCTION PSIN_AT_RZ(Rz)
      REAL*8 :: PSIN_AT_RZ
      REAL*8, INTENT(IN ) :: Rz(2) 
      PSIN_AT_RZ=(PSI_AT_RZ(Rz)-OPOINT(0))/(XPOINT(0,1)-OPOINT(0))
      END FUNCTION PSIN_AT_RZ

C----.|-----------------------------------------------------------------      

      SUBROUTINE set_tolerance(t) 
      REAL*8, INTENT(IN) :: t
      tolerance=t
      END SUBROUTINE set_tolerance
      
C----.|-----------------------------------------------------------------      


      SUBROUTINE search_singularity(Rz,PF,FOUND)      
      USE INTERPOLATION
      USE TOOLS
      
      REAL*8,INTENT(INOUT) :: Rz(2),PF
      logical,intent(out)  :: FOUND
!      REAL*8,INTENT(IN) :: tolerance

Cf2py intent(in,out) Rz(2),PF
Cf2py intent(out) FOUND
Cf2py logical intent(in), optional :: FOUND = 0
Cf2py real*8 intent(in), optional :: PF=0.0
!,tolerance=1e-12

      REAL*8 :: X,Y,DL,DELTA_X,DELTA_Y,F,FY
      REAL*8 :: D,DDDR,DDDZ
      INTEGER:: JR,JZ,CASE,ITER

      X = (Rz(1)-RMIN)*DR1; JR= X; X = X-DFLOAT(JR) + 1.D0
      Y = (Rz(2)-ZMIN)*DZ1; JZ= Y; Y = Y-DFLOAT(JZ) + 1.D0
      PF=NaN()

      ITER = 0
      ITERATION: DO
       ITER = ITER + 1
       CALL INTERPOL_SINGU(PSI(JR-1:JR+2,JZ-1:JZ+2),X,Y,D,DDDR,DDDZ)

       DL = -D/(DDDR**2+DDDZ**2)
       DELTA_X = dl*DDDR
       DELTA_Y = dl*DDDZ
       !write(*,*) iter
       FOUND = (DELTA_X*DR)**2+(DELTA_Y*DZ)**2 < tolerance
              
       IF( FOUND ) THEN
         Rz(1)  = (X + DELTA_X - 1.D0 + float(JR))*DR + RMIN
         Rz(2)  = (Y + DELTA_Y - 1.D0 + float(JZ))*DZ + ZMIN
         EXIT ITERATION
       ENDIF
        CASE = 0
        IF    (X+DELTA_X <1.D0 )  THEN
          F    = (1.D0 - X)/DELTA_X; CASE = 1
        ELSEIF(X+DELTA_X >2.D0 )  THEN
          F = (2.D0 - X)/DELTA_X;    CASE = 2
        ENDIF

        IF   (Y+DELTA_Y <1.D0 )  THEN
          FY = (1.D0 - Y)/DELTA_Y
          IF    ( CASE==0 .OR. FY<F ) THEN
            F    = FY;               CASE = 3
          ENDIF
        ELSEIF(Y+DELTA_Y >2.D0 )  THEN
          FY = (2.D0 - Y)/DELTA_Y
          IF    ( CASE==0 .OR. FY<F ) THEN
            F    = FY;               CASE = 4
          ENDIF
        ENDIF

        select case (case)
        case(0)
         X  = X + DELTA_X; Y  = Y + DELTA_Y
        case(1)
         JR = JR - 1 ; IF(JR<1) EXIT ITERATION
         X  = 2.D0   ; Y  = Y + F*DELTA_Y
        case(2)
         JR = JR + 1 ; IF(JR>NR-3) EXIT ITERATION
         X  = 1.D0;    Y  = Y + F*DELTA_Y
        case(3)
         JZ = JZ - 1 ; IF(JZ<1) EXIT ITERATION
         Y  = 2.D0;    X  = X + F*DELTA_X
        case(4)
         JZ = JZ + 1 ; IF(JZ>NZ-3) EXIT ITERATION
         Y  = 1.D0;    X  = X + F*DELTA_X
        end select
        
        if (ITER.gt.10000) then
         found=.FALSE.
         RETURN
        endif

      ENDDO ITERATION
      
      !PF=POLO_FL(PSI(JR:JR+3,JZ:JZ+3),X,Y) 
      if (found) PF=PSI_AT_RZ(Rz)
      RETURN
      END SUBROUTINE search_singularity
      
C----.|-----------------------------------------------------------------      
      SUBROUTINE search_oxp(p,i)
      integer,intent(in) :: i
      real*8,intent(in) :: p(2)
      logical :: FOUND
      real*8 :: PF,v(2)
      v=p
      call search_singularity(v,PF,FOUND)
      !write(*,*) 'found',p,found
      if (.not.FOUND) v=-1.d0
      if (i.eq.0) then
       OPOINT(0)=PF;OPOINT(1:2)=v
      else if ((i.ge.1).and.(i.le.2)) then
       XPOINT(0,i)=PF;XPOINT(1:2,i)=v
      else
       write(*,*) 'i=0: O-Point'
       write(*,*) 'i=1: 1st X-Point'
       write(*,*) 'i=2: 2nd X-Point'
      endif      
      
      END SUBROUTINE search_oxp
      
C----.|-----------------------------------------------------------------      
      SUBROUTINE set_oxp(p,i)
      integer,intent(in) :: i
      real*8,intent(in) :: p(2)    
      if (i.eq.0) then
       OPOINT(0)=PSI_AT_RZ(p);OPOINT(1:2)=p
      else if ((i.ge.1).and.(i.le.2)) then
       XPOINT(0,i)=PSI_AT_RZ(p);XPOINT(1:2,i)=p
      else
       write(*,*) 'i=0: O-Point'
       write(*,*) 'i=1: 1st X-Point'
       write(*,*) 'i=2: 2nd X-Point'
      endif      
      END SUBROUTINE set_oxp
      
C----.|-----------------------------------------------------------------      
      function readjust_oxp()
      !searches O-and X-point in the proximity of the positions where they were found previously. 
      !this might be necessary after changing the flux matrix (by a small perturbation)
      !returns true if previously found OXP are found again
      logical :: readjust_oxp
      readjust_oxp=.True.
      if (OPOINT(0).eq.OPOINT(0)) then !is not nan
       call search_oxp(OPOINT(1:2),0)
       readjust_oxp=readjust_oxp.and.(OPOINT(1).gt.0.d0)
      endif

      if (XPOINT(0,1).eq.XPOINT(0,1)) then !is not nan
       call search_oxp(XPOINT(1:2,1),1)
       readjust_oxp=readjust_oxp.and.(XPOINT(1,1).gt.0.d0)
      endif

      if (XPOINT(0,2).eq.XPOINT(0,2)) then !is not nan
       call search_oxp(XPOINT(1:2,2),2)
       readjust_oxp=readjust_oxp.and.(XPOINT(1,2).gt.0.d0)
      endif
      END function readjust_oxp
      
      
C----.|-----------------------------------------------------------------      
      SUBROUTINE singularities_LSN()
      !integer :: i,incr
      !logical :: FOUND
      call search_oxp((/0.5*RMIN+0.5*RMAX,0.5*ZMIN+0.5*ZMAX/),0)
      call search_oxp((/0.4*RMIN+0.6*RMAX,0.8*ZMIN+0.2*ZMAX/),1)
      call search_oxp((/0.4*RMIN+0.6*RMAX,0.2*ZMIN+0.8*ZMAX/),2)
      NXP=0
      if (XPOINT(1,1).gt.0.d0) then
       NXP=1
       if (XPOINT(1,2).gt.0.d0) NXP=2
      endif
      END SUBROUTINE singularities_LSN
      
C----.|-----------------------------------------------------------------      
      SUBROUTINE scan_singularities(R1,R2,z1,z2,NNR,NNz,Ns,sout,dl)
      USE TOOLS
      INTEGER, INTENT(IN) :: NNR,NNz,Ns
      REAL*8,INTENT(IN):: R1,R2,z1,z2,dl
      REAL*8,INTENT(OUT):: sout(Ns,3)
      logical :: FOUND
      integer :: ir,iz,i,ising
      real*8 :: PF,v(2)
Cf2py integer,intent(in), optional :: dl=1.e-2      
      sout=NaN()
      ising=0
      do ir=0,NNR-1;do iz=0,NNz-1
       v=(/R1+(R2-R1)*ir/(NNR-1.d0),z1+(z2-z1)*iz/(NNz-1.d0)/)
       call search_singularity(v,PF,FOUND)
       FOUND=FOUND.and.(v(1).ge.R1).and.(v(1).le.R2).and.
     .                 (v(2).ge.z1).and.(v(2).le.z2)
       if (FOUND) then
        do i=1,ising
         if (length(sout(i,2:3)-v).lt.1e-2) FOUND=.FALSE.
        enddo
       endif
       if (FOUND) then
        ising=ising+1
        if (ising.gt.Ns) return
        sout(ising,:)=(/PF,v(1),v(2)/)
       endif
      enddo;enddo
      END SUBROUTINE scan_singularities
      
      
C----.|-----------------------------------------------------------------      

      SUBROUTINE singularities(o,x1,x2)
      real*8,intent(in) :: o(2),x1(2),x2(2)
      call search_oxp(o,0)
      call search_oxp(x1,1)
      call search_oxp(x2,2)
      NXP=0
      if (XPOINT(1,1).gt.0.d0) then
       NXP=1
       if (XPOINT(1,2).gt.0.d0) NXP=2
      endif
      
!       integer :: i,incr
!       logical :: FOUND
!       OPOINT(1) = o(1); OPOINT(2)=o(2)
!       call search_singularity(OPOINT(1:2),OPOINT(0),FOUND,1.d-12)      
!       if (.not.FOUND) OPOINT(1)=-1.d0
!       
!       NXP=0;incr=1
!       
!       XPOINT(1,1) = x1(1); XPOINT(2,1)=x1(2)
!       XPOINT(1,2) = x2(1); XPOINT(2,2)=x2(2)
!       
!       do i=1,2
!        call search_singularity(XPOINT(1:2,i),XPOINT(0,i),FOUND,1.d-12)      
!        if (FOUND) then 
!         NXP=NXP+incr
!        else
!         incr=0
!         XPOINT(1,1)=-1.d0
!        endif
!       enddo      
      END SUBROUTINE singularities


C----.|-----------------------------------------------------------------      

      FUNCTION BAS_RZ(RZ)
      USE INTERPOLATION
      USE TOOLS

      REAL*8, INTENT(IN ) :: RZ(2)
      REAL*8              :: BAS_RZ(2)
Cf2py intent(in) RZ
Cf2py intent(out) BAS_RZ

      REAL*8 :: X,Y
      REAL*8,DIMENSION(0:2) :: PF
      INTEGER:: JR,JZ
      LOGICAL:: INSIDE

      ierr_bas = 1
      BAS_RZ=NaN()
      CALL RZ_TO_XY(RZ(1),RZ(2),JR,JZ,X,Y,INSIDE)
      IF(.NOT.INSIDE) RETURN

      CALL INTERPOL_PFL
     .(PSI(JR:JR+3,JZ:JZ+3),X,Y,PF)

      BAS_RZ(1) =-(PF(2)*DZ1/(PI2*RZ(1)))
      BAS_RZ(2) = (PF(1)*DR1/(PI2*RZ(1))) 

      ierr_bas=0
      END FUNCTION BAS_RZ
C----.|-----------------------------------------------------------------      

      SUBROUTINE BPOL_2D(RG,ZG,NNR,NNZ,BOUT)
      REAL*8, INTENT(IN) :: RG(NNR),ZG(NNZ)      
      INTEGER, INTENT(IN) :: NNR,NNZ
      REAL*8,INTENT(OUT):: BOUT(NNR,NNZ)
      REAL*8 :: BRZ(2)
       integer :: ir,iz
       do ir=1,NNR;do iz=1,NNZ
        BRZ=BAS_RZ((/RG(ir),ZG(iz)/))
        BOUT(ir,iz)=(BRZ(1)**2+BRZ(2)**2)**0.5
       enddo;enddo
      END SUBROUTINE BPOL_2D

C----.|-----------------------------------------------------------------      

      FUNCTION BAS_P(R)
      REAL*8, INTENT(IN ) :: R
      REAL*8              :: BAS_P    

Cf2py intent(in) R
Cf2py intent(out) BAS_P
      
      BAS_P = B0*R0/R
      ierr_bas=0
      if (R.le.0.d0) ierr_bas=1
      RETURN
      END FUNCTION

C----.|-----------------------------------------------------------------      

      SUBROUTINE ray_flxsf_intersection(p,v,PFL)!,tolerance)
      USE INTERPOLATION
      USE TOOLS

      REAL*8,INTENT(IN   ):: v(2),PFL!,tolerance
      REAL*8,INTENT(INOUT):: p(2) 
Cf2py intent(in,out) p(2) 
Cf2py intent(in) v(2),PFL      
Cf#2py real*8 intent(in), optional :: tolerance=1e-7

      REAL*8 :: X,Y,DL,DELTA_X,DELTA_Y,F,FY,CX,CY
      REAL*8,DIMENSION(0:2) :: PF
      INTEGER:: JR,JZ,CASE,ITER

      ierr_bas=1
      if (isnan(p(1)).or.isnan(p(2))) return
      
      ierr_bas=1
      if (isnan(p(1)).or.isnan(p(2))) return
      
      X = (p(1)-RMIN)*DR1; JR= X; X = X-DFLOAT(JR) + 1.D0
      Y = (p(2)-ZMIN)*DZ1; JZ= Y; Y = Y-DFLOAT(JZ) + 1.D0

      CX= v(1)*DR1;  CY=v(2)*DZ1
!      write(6,*) 'Vr,Vz=',VR,VZ
      ITER = 0
      ITERATION: DO
!      write(6,*) 'R,z=',R,z
      
       ITER = ITER + 1
       CALL INTERPOL_PFL(PSI(JR-1:JR+2,JZ-1:JZ+2),X,Y,PF)

       DL = (PFL-PF(0))/(PF(1)*CX+PF(2)*CY)
     
       DELTA_X = dl*CX 
       DELTA_Y = dl*CY

       iF(ITER>400) then
         write(6,*) 'dl,PF,..=',dl,PF,PF(1)*CX+PF(2)*CY
         p=NaN()
         if(iter>405) return
       endif
       IF( ABS(DL) < tolerance ) THEN
         p(1)  = (X + DELTA_X - 1.D0 + float(JR))*DR + RMIN
         p(2)  = (Y + DELTA_Y - 1.D0 + float(JZ))*DZ + ZMIN
!         write(6,*) 'xR,z=',R,z
         EXIT ITERATION
        ENDIF

        CASE = 0
        IF    (X+DELTA_X <1.D0 )  THEN
          F    = (1.D0 - X)/DELTA_X; CASE = 1
        ELSEIF(X+DELTA_X >2.D0 )  THEN
          F = (2.D0 - X)/DELTA_X;    CASE = 2
        ENDIF

        IF   (Y+DELTA_Y <1.D0 )  THEN
          FY = (1.D0 - Y)/DELTA_Y
          IF    ( CASE==0 .OR. FY<F ) THEN
            F    = FY;               CASE = 3
          ENDIF
        ELSEIF(Y+DELTA_Y >2.D0 )  THEN
          FY = (2.D0 - Y)/DELTA_Y
          IF    ( CASE==0 .OR. FY<F ) THEN
            F    = FY;               CASE = 4
          ENDIF
        ENDIF

        select case (case)
        case(0)
         X  = X + DELTA_X; Y  = Y + DELTA_Y
        case(1)
         JR = JR - 1 ; IF(JR<1) GOTO 98
         X  = 2.D0   ; Y  = Y + F*DELTA_Y
        case(2)
         JR = JR + 1 ; IF(JR>NR-3) GOTO 98
         X  = 1.D0;    Y  = Y + F*DELTA_Y
        case(3)
         JZ = JZ - 1 ; IF(JZ<1) GOTO 98
         Y  = 2.D0;    X  = X + F*DELTA_X
        case(4)
         JZ = JZ + 1 ; IF(JZ>NZ-3) GOTO 98
         Y  = 1.D0;    X  = X + F*DELTA_X
        end select

      ENDDO ITERATION
      
      ierr_bas=0
      
      RETURN
98    last_error=' PFL not reached! Stopped in FLUX_SURFCE'
      ierr_bas=1
!       write(6,*)' PFL not reached! Stopped in FLUX_SURFCE'
!       write(6,*)'jr,jz,x,y=',jr,jz,x,y
      p=NaN()      

      END SUBROUTINE ray_flxsf_intersection
      
C----.|-----------------------------------------------------------------      

      SUBROUTINE core_surface(PSI_N,N,SF)
      USE TOOLS
      IMPLICIT NONE
      REAL*8,INTENT(IN)  :: PSI_N
      integer,INTENT(IN) :: N 
      REAL*8,INTENT(OUT) :: SF(N,2)
Cf2py intent(in) PSIN,N 
Cf2py intent(out) SF
Cf2py depend(N) SF
      real*8 :: v(2),T,THETAx,PF
      integer:: i
      !write(*,*) 'PSI_N',PSI_N
      SF=NaN()
      THETAx = ATAN2( XPOINT(2,1)-OPOINT(2),XPOINT(1,1)-OPOINT(1))
      DO i=1,N
        T = (i-1.d0)/(N-1.d0)*PI2+THETAx
        v=(/cos(T),sin(T)/)*0.05
        PF=PSI_N*(XPOINT(0,1)-OPOINT(0))+OPOINT(0)
        SF(i,:)=OPOINT(1:2)+v
        !write(6,*) i,PF,SF(i,:)
        CALL ray_flxsf_intersection(SF(i,:),v,PF)         
      ENDDO
      if (PSI_N.eq.1.d0) then
       SF(1,:)=XPOINT(1:2,1);SF(N,:)=XPOINT(1:2,1)
      endif
            
      END SUBROUTINE core_surface
      
C----.|-----------------------------------------------------------------      

      SUBROUTINE follow_surface(p,v,N,SF)
      USE TOOLS
      IMPLICIT NONE      
      REAL*8,INTENT(IN)  :: p(2),v(2)
      integer,INTENT(IN) :: N 
      REAL*8,INTENT(OUT) :: SF(N,2)
Cf2py intent(in) PSIN,N 
Cf2py intent(out) SF
Cf2py depend(N) SF
      real*8 :: PF,vv(2),vp(2),l
      integer:: i
      SF=NaN()
      PF=PSI_AT_RZ(p)
      
      SF(1,:)=p;l=length(v);vv=v
      
      DO i=2,N
       SF(i,:)=SF(i-1,:)+vv; vp=(/-vv(2),vv(1)/)*0.2
       CALL ray_flxsf_intersection(SF(i,:),vp,PF)
       if (isnan(SF(i,1))) return
       vv=normv2(SF(i,:)-SF(i-1,:))*l
      ENDDO
            
      END SUBROUTINE follow_surface
      
C----.|-----------------------------------------------------------------      

      SUBROUTINE find_xp_legs(xp,dd,v,dir)
      USE TOOLS
      IMPLICIT NONE      
      REAL*8,INTENT(IN)  :: xp(0:2),dd
      REAL*8,INTENT(OUT) :: v(4,2),dir(4)
Cf2py real*8,intent(in) xp
Cf2py real*8,intent(in), optional :: dd=1.e-2
Cf2py real*8,intent(out) v,dir
      real*8 :: vp(2),p(2),B(2)
      integer:: i
 
      p=xp(1:2)+(/dd,0.d0/);vp=(/0.d0,dd/)
      DO i=0,4
       CALL ray_flxsf_intersection(p,vp,xp(0))
       vp=normv2(p-xp(1:2))*dd
       B=BAS_RZ(p)
       if (i.ge.1) then
        v(i,:)=vp
        dir(i)=sign(1.d0,dot(vp,B))*sign(1.d0,BAS_P(p(1)))
       endif 
       p=xp(1:2)+(/-vp(2),vp(1)/)       
      ENDDO
            
      END SUBROUTINE find_xp_legs
      
C----.|-----------------------------------------------------------------      

      SUBROUTINE find_separatrix(NC,NR,NL,ll,lr)
      USE TOOLS
      IMPLICIT NONE      
      integer,INTENT(IN) :: NC,NR,NL
      REAL*8,INTENT(IN)  :: ll,lr
Cf2py intent(in) NC,NR,NL
Cf2py logical intent(in), optional :: ll=1.0,lr=1.0
      REAL*8 :: x(2),v(2)
      integer :: i
      
      if (allocated(sep)) deallocate(sep)
      allocate(sep(NR+NC+NL,2))
      SEPNC=NC;SEPNL=NL;SEPNR=NR
      if (allocated(sf)) deallocate(sf)
      allocate(sf(NR+NC+NL,2))
            
      sep(:,:)=NaN();sf(:,:)=NaN()
      call core_surface(1.d0,NC,sep(NR+1:NC+NR,1:2))

      !the right divertor leg
      x=sep(NR+1,:);v=x-sep(NR+NC-1,:)
      !stop
      !write(*,*) 'lr',length(v),lr
      if (lr.gt.0.d0) then
       v=v*lr
      else
       v=-normv2(v)*lr
      endif 
      call follow_surface(x,v,NR+1,sep(NR+1:1:-1,:))

      !the left divertor leg
      v=x-sep(NR+2,:)
      !write(*,*) 'll',length(v),ll
      if (ll.gt.0.d0) then
       v=v*ll
      else
       v=-normv2(v)*ll      
      endif
      !write(*,*) 'x,v',x,v
      call follow_surface(x,v,NL+1,sep(NR+NC:NL+NC+NR,:))

      v=NaN()
      do i=NR,1,-1
       if (.not.isnan(sep(i,1))) v=sep(i,:)
      enddo
      
      x=NaN()
      do i=NR+NC,NR+NC+NL
       if (.not.isnan(sep(i,1))) x=sep(i,:)
      enddo
      pfr_refpt=(v+x)*0.5d0
      
      sf=sep
      
      !write(*,*) sep(NL+1:NL+NC+1,:)
      END SUBROUTINE find_separatrix
C----.|-----------------------------------------------------------------      


      SUBROUTINE find_flxsurface(PSI_N)
C     the result is written into the public module variable sf      
      USE TOOLS
      IMPLICIT NONE      
      REAL*8,INTENT(IN)  :: PSI_N
Cf2py logical intent(in) :: PSI_N
      real*8 :: PF,v(2),vv(2)
      integer :: i,ist
      
      sf(:,:)=NaN()
      ierr_bas=10
      if ((OPOINT(1).le.0.d0).or.(XPOINT(1,1).le.0.d0)) then
       write(*,*) 'O- and X-points unknown. call find_separatrix first.'
       return
      endif
      
      PF=abs(PSI_N)*(XPOINT(0,1)-OPOINT(0))+OPOINT(0)
      if (PSI_n.eq.1.d0) then !give back Separatrix
       sf=sep
      else if ((PSI_n.gt.0.d0).and.(PSI_N.lt.1.d0)) then ! give back a core surface
       call core_surface(PSI_N,SEPNC,sf(SEPNR+1:SEPNC+SEPNR,1:2))
      else if (PSI_N.gt.1.d0) then ! give back a SOL surface
       ist=SEPNR+SEPNC/2
       do i=ist,SEPNR+SEPNC+SEPNL
        v=sep(i,:)-OPOINT(1:2)
        if (dot(v,OPOINT(1:2)-XPOINT(1:2,1)).ge.0.d0) vv=v
        sf(i,:)=sep(i,:)+vv*(PSI_N-1.D0);
        CALL ray_flxsf_intersection(sf(i,:),vv*0.1d0,PF)
       enddo
       do i=ist,1,-1
        v=sep(i,:)-OPOINT(1:2)
        if (dot(v,OPOINT(1:2)-XPOINT(1:2,1)).ge.0.d0) vv=v
        sf(i,:)=sep(i,:)+vv*(PSI_N-1.D0);
        CALL ray_flxsf_intersection(sf(i,:),vv*0.1d0,PF)
       enddo
      else if (PSI_N.lt.0.d0) then ! give back a surface in the private flux region
       do i=1,SEPNR+1
        v=pfr_refpt-sep(i,:)
        sf(i,:)=sep(i,:)+v*0.1;
        CALL ray_flxsf_intersection(sf(i,:),v*0.1d0,PF)
       enddo 
       do i=SEPNR+SEPNC,SEPNR+SEPNC+SEPNL
        v=pfr_refpt-sep(i,:)
        sf(i,:)=sep(i,:)+v*0.1;
        CALL ray_flxsf_intersection(sf(i,:),v*0.1d0,PF)
       enddo        
      end if
      ierr_bas=0
      END SUBROUTINE find_flxsurface
C----.|-----------------------------------------------------------------      

      SUBROUTINE find_upstream_pos(a,deltaruin)
      USE TOOLS
      IMPLICIT NONE
      REAL*8,INTENT(IN)  :: a,deltaruin
Cf2py real*8,intent(in),optional :: a=0.0,deltaruin=0.03
      real*8 :: v(2),p(2),BRZ(2)
      integer :: i
      ierr_bas = 1

      v=(/cos(a),sin(a)/)
      Rz_u=OPOINT(1:2)+v*0.1
      CALL ray_flxsf_intersection(Rz_u,v*0.01,XPOINT(0,1))         

      BRZ=BAS_RZ(Rz_u)
      

      dpsiu_dru=BRZ(2)*PI2*Rz_u(1)*v(1)-BRZ(1)*PI2*Rz_u(1)*v(2)
      dpsiNu_dru=dpsiu_dru/(XPOINT(0,1)-OPOINT(0))
      ru_x2=nan()
      if (XPOINT(1,2).gt.0.d0) then
       PsiN_x2=(XPOINT(0,2)-OPOINT(0))/(XPOINT(0,1)-OPOINT(0))
       ru_x2=(XPOINT(0,2)-XPOINT(0,1))/dpsiu_dru
      endif
      
      v=(/-v(2),v(1)/)
      
      theta_pitchu=atan(dot(BRZ,v)/BAS_P(Rz_u(1)))
      !write(*,*) 'Bpol,Bt,theta',BRZ,BAS_P(Rz_u(1)),theta_pitchu
      
      if (abs(abs(theta_pitchu)-0.17d0).gt.0.07) then
       write(*,*) 'WARNING: The upstream pitch angle ',theta_pitchu
       write(*,*) 'deviates strongly from the AUG typical value 0.17'
       write(*,*) '(i.e. 10deg). Note that'
       write(*,*) 'BR =- 1/(2*pi*R) * dPSI/dz'
       write(*,*) 'Bz =  1/(2*pi*R) * dPSI/dR'
       write(*,*) 'Did you possibly forget the factor 2pi?'     
      endif
      
      deltaru=deltaruin
      do i=-100,100
       p=Rz_u+v*deltaru*i/100.d0
       psi_u(i)=PSI_AT_RZ(p)
      enddo
      
      ierr_bas=0
            
      END SUBROUTINE find_upstream_pos

C----.|-----------------------------------------------------------------      

      subroutine remesh_psi(rmn,rmx,nrnr, zmn,zmx,nznz)       
      implicit none
      integer,intent(in) :: nrnr,nznz
      real*8,intent(in) :: rmn,rmx,zmn,zmx
      integer :: ir,iz
      real*8, dimension(:,:),allocatable :: newpsi

      ALLOCATE(newpsi(0:nrnr-1,0:nznz-1))
      do ir=0,nrnr-1;do iz=0,nznz-1
       newpsi(ir,iz)=PSI_AT_RZ((/rmn+(rmx-rmn)*ir/(nrnr-1.d0),
     .                         zmn+(zmx-zmn)*iz/(nznz-1.d0)/))
      enddo;enddo

      NR=nrnr;NZ=nznz
      deallocate(PSI)
      ALLOCATE(PSI(0:NR-1,0:NZ-1))
      PSI(:,:)=newpsi(:,:)
      RMIN=rmn;ZMIN=zmn;RMAX=rmx;ZMAX=zmx

      DR  = (RMAX-RMIN)/FLOAT(NR-1)
      DZ  = (ZMAX-ZMIN)/FLOAT(NZ-1)

      DR1 = 1.D0/DR
      DZ1 = 1.D0/DZ      
      deallocate(newpsi)
      end subroutine remesh_psi
      
C----.|-----------------------------------------------------------------      
      function fx(RZ,n)
      USE TOOLS
      implicit none
      real*8,intent(in) :: RZ(2),n(2)
      real*8 :: fx,Bu(2),Bt(2)
Cf2py real*8,intent(in),optional,dimension(2) :: n=0.0
      Bu=BAS_RZ(Rz_u)
      Bt=BAS_RZ(RZ)
      if (length(n).gt.1.d-6) Bt=n*dot(Bt,n)
      fx=Rz_u(1)/RZ(1)*length(Bu)/length(Bt)
      end function fx

C----.|-----------------------------------------------------------------      

      function ellk(k)
      implicit none
      real*8,intent(in) :: k
      real*8 :: m1,a0,a1,a2,a3,a4,b0,b1,b2,b3,b4,ek1,ek2,ellk
      !polynomial approximation for the complete elliptic
      !integral of the first kind (Hasting's approximation)
      m1=1.d0-k**2
      a0=1.38629436112d0;a1=0.09666344259d0;a2=0.03590092383d0;
      a3=0.03742563713d0;a4=0.01451196212d0
      b0=0.5d0;b1=0.12498593597d0;b2=0.06880248576d0;
      b3=0.03328355346d0;b4=0.00441787012d0
      ek1=a0+m1*(a1+m1*(a2+m1*(a3+m1*a4)))
      ek2=(b0+m1*(b1+m1*(b2+m1*(b3+m1*b4))))*log(m1)
      ellk=ek1-ek2
      end function ellk
      
      
      function ellec(k)
      implicit none
      real*8,intent(in) :: k
      real*8 :: m1,a1,a2,a3,a4,b1,b2,b3,b4,ee1,ee2,ellec
      !Computes polynomial approximation for the complete elliptic
      !Integral of the second kind (Hasting's approximation):   
      m1=1.d0-k**2
      a1=0.44325141463d0;a2=0.06260601220d0
      a3=0.04757383546d0;a4=0.01736506451d0
      b1=0.24998368310d0;b2=0.09200180037d0
      b3=0.04069697526d0;b4=0.00526449639d0
      ee1=1.e0+m1*(a1+m1*(a2+m1*(a3+m1*a4)))
      ee2=m1*(b1+m1*(b2+m1*(b3+m1*b4)))*log(1.d0/m1)
      ellec=ee1+ee2
      end function ellec

      function psi_current_wire(R,z,Ro,zo,Io)
      !computes the flux at the position Rz for an axisymmetric current wire located at Ro,zo
      !according to formula 1 in [Feneberg Computer Physics Communications 31(1984)143—148]
      !multiplied by a factor 2 pi due to the definition of psi used here: 
      !BR =- 1/(2*pi*R) * dPSI/dz
      !Bz =  1/(2*pi*R) * dPSI/dR
      implicit none
      real*8,intent(in) :: R,z,Ro,zo,Io
      real*8 :: psi_current_wire,k,A
      !mu=4*PI*1e-7

      k=(4*R*Ro/((R+Ro)**2+(z-zo)**2))**0.5d0
      A=((R*Ro)**0.5d0)/k*((1.d0-0.5d0*k**2)*ellk(k)-ellec(k))
      
      psi_current_wire=PI2*4.d-7*Io*A
      end function psi_current_wire

C----.|-----------------------------------------------------------------      

      END MODULE BAXISYM

