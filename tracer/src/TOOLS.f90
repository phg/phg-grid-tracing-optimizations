      module TOOLS
      implicit none
      real*8,save ::p_ray_tri_coll(3)=-1.d0 ! this variable is set by the ray_triangle_collision function
      real*8,save ::x_ray_tri_coll(3)=-1.d0 ! this variable is set by the ray_triangle_collision function
      REAL*8,PARAMETER:: PI=3.14159265358979312D0,PI2=PI*2.D0
      contains
      
C----.|-----------------------------------------------------------------      
     
      function NaN()
      real*8 :: NaN
      NaN=0.d0;NaN=NaN/NaN
      end function NaN      

!-----|----------------------------------------------------------------|           

      function cross(a,b)
      REAL*8,INTENT(IN) :: a(3),b(3)
      real*8 :: cross(3)
      cross(1)=a(2)*b(3)-a(3)*b(2)
      cross(2)=a(3)*b(1)-a(1)*b(3)
      cross(3)=a(1)*b(2)-a(2)*b(1)      
      END function cross   

!-----|----------------------------------------------------------------|           

      function dot(a,b)
      REAL*8,INTENT(IN) :: a(:),b(:)
      real*8 :: dot
      dot=sum(a*b)
      END function dot   

!-----|----------------------------------------------------------------|           

      function R_z_p2xyz(R,Z,p)
      REAL*8 ,INTENT(IN   ) :: R,Z,p
      real*8 :: R_z_p2xyz(3)
      R_z_p2xyz(1)=R*cos(p)
      R_z_p2xyz(2)=R*sin(p)
      R_z_p2xyz(3)=z
      END function R_z_p2xyz   
!-----|----------------------------------------------------------------|           

      function Rzp2xyz(Rzp)
      REAL*8 ,INTENT(IN) :: Rzp(3)
      real*8 :: Rzp2xyz(3)
      Rzp2xyz(1)=Rzp(1)*cos(Rzp(3))
      Rzp2xyz(2)=Rzp(1)*sin(Rzp(3))
      Rzp2xyz(3)=Rzp(2)
      END function Rzp2xyz
      
!-----|----------------------------------------------------------------|           

      function mean(v)
      REAL*8 ,INTENT(IN),dimension(:) :: v
      real*8 :: mean
      mean = sum(v)/max(1,size(v))
      END function mean

!-----|----------------------------------------------------------------|           
! 
!       function vRzp2xyz(R,Z,p,VR,Vz,Vp)
!       REAL*8 ,INTENT(IN   ) :: R,Z,p,VR,Vz,Vp
!       real*8 :: vRzp2xyz(3)
!       
!       vRzp2xyz(1)=cos(p)*VR-sin(p)*Vp
!       vRzp2xyz(2)=sin(p)*VR+cos(p)*Vp
!       vRzp2xyz(3)=Vz
!       END function vRzp2xyz   
!       
!-----|----------------------------------------------------------------|           

      function xyz2Rzp(xyz)
      REAL*8 ,INTENT(IN) :: xyz(3)
      real*8 :: xyz2Rzp(3)
      xyz2Rzp(1)=(xyz(1)**2+xyz(2)**2)**0.5d0
      xyz2Rzp(2)=xyz(3)
      xyz2Rzp(3)=atan2(xyz(2),xyz(1))
      END function xyz2Rzp   

      
!-----|----------------------------------------------------------------|           
! 
!       function length2(v)
!       REAL*8 ,INTENT(IN) :: v(2)
!       real*8 :: length2
!       length=(v(1)*v(1)+v(2)*v(2))**0.5d0
!       END function length2
!-----|----------------------------------------------------------------|           

      function length(v)
      REAL*8 ,INTENT(IN),dimension(:) :: v
      real*8 :: length      
      length=sqrt(sum(v**2))
      END function length
!-----|----------------------------------------------------------------|           

      function normv(v)
      REAL*8 ,INTENT(IN) :: v(3)
      real*8 :: normv(3)
      normv=v/sqrt(sum(v**2))
      END function normv
      
!-----|----------------------------------------------------------------|           

      function normv2(v)
      REAL*8 ,INTENT(IN) :: v(2)
      real*8 :: normv2(2)
      normv2=v/sqrt(sum(v**2))
      END function normv2
      
!-----|----------------------------------------------------------------|           

      function maphi(p,p1,p2)
      REAL*8 ,INTENT(IN) :: p,p1,p2
      real*8 :: maphi
      maphi=modulo(p-p1,p2-p1)+p1
      END function maphi
          

!-----|----------------------------------------------------------------|           

      function line_line_coll(a1,a2,b1,b2)
      ! finds the point of intersection between two lines passing from 
      ! a1 to a2 and from b1 to b2. The result is returned in normalized
      ! coordinates, i.e. 
      ! x = a1+(a2-a1)*line_line_coll(1) = b1+(b2-b1)*line_line_coll(2) 
      real*8, intent(in)  :: a1(2),a2(2),b1(2),b2(2)
      real*8 :: line_line_coll(2)
      real*8 :: M(2,2),Ms(2,2),d      
      M(:,1)=a2-a1;M(:,2)=b1-b2
      line_line_coll=(/-1.d0,-1.d0/)
      d=M(1,1)*M(2,2)-M(1,2)*M(2,1)
      if (d.ne.0.d0) then
       Ms(1,1)= M(2,2)/d; Ms(1,2)=-M(1,2)/d;
       Ms(2,1)=-M(2,1)/d; Ms(2,2)= M(1,1)/d;     
       line_line_coll(1)=Ms(1,1)*(b1(1)-a1(1))+Ms(1,2)*(b1(2)-a1(2))
       line_line_coll(2)=Ms(2,1)*(b1(1)-a1(1))+Ms(2,2)*(b1(2)-a1(2)) 
!       else
!        write(*,*) 'determinante 0.'
      endif
      return
      end function line_line_coll
!-----|----------------------------------------------------------------|           

      function curve_curve_coll_Rzl(Rzl1,Rzl2,N1,N2,l1min,l2min)
      ! finds the first intersection between the two curves and returns 
      ! the point and the line coordinates
      real*8, intent(in)  :: Rzl1(N1,3),Rzl2(N2,3),l1min,l2min
      integer,intent(in)  :: N1,N2
      real*8 :: curve_curve_coll_Rzl(4),x(2),l1,l2
      real*8,parameter :: about0=-1.d-13
      integer :: i1,i2
Cf2py real*8,intent(in),optional :: l1min=0.0,l2min=0.0      
      curve_curve_coll_Rzl(3:4)=-1.d0
      !write(*,*) 'lmin',l1min,l2min
      do i1=1,N1-1
       do i2=1,N2-1
        x=line_line_coll(Rzl1(i1,1:2),Rzl1(i1+1,1:2),
     .                   Rzl2(i2,1:2),Rzl2(i2+1,1:2))
        if ((x(1).ge.about0).and.(x(1).le.1.d0).and.
     .      (x(2).ge.about0).and.(x(2).le.1.d0)) then 
         l1=Rzl1(i1,3)+x(1)*(Rzl1(i1+1,3)-Rzl1(i1,3))
         l2=Rzl2(i2,3)+x(2)*(Rzl2(i2+1,3)-Rzl2(i2,3)) 
         if ((l1.ge.l1min).and.(l2.ge.l2min)) then
          curve_curve_coll_Rzl(1:2)=
     .         Rzl1(i1,1:2)+x(1)*(Rzl1(i1+1,1:2)-Rzl1(i1,1:2))
          curve_curve_coll_Rzl(3)=l1
          curve_curve_coll_Rzl(4)=l2
          return
!          else
!           write(*,*) 'intersection ignored',l1,l2,l1min,l2min
         endif     
        endif
       enddo      
      enddo
      return
      end function curve_curve_coll_Rzl
      
!-----|----------------------------------------------------------------|           

      function l2Rz(l,Rzl,N)
      ! returns the Rz-position for a given length coordinate l
      real*8, intent(in)  :: Rzl(N,3),l
      integer,intent(in)  :: N
      real*8 :: l2Rz(2),x
      integer :: i
      l2Rz=(/-1.d0,0.d0/)
      if (l.lt.Rzl(1,3)) return
      
      do i=1,N-1
       if (l.le.Rzl(i+1,3)) then
        x=(l-Rzl(i,3))/(Rzl(i+1,3)-Rzl(i,3))
        l2Rz=Rzl(i,1:2)+(Rzl(i+1,1:2)-Rzl(i,1:2))*x
        return 
       endif
      enddo
      end function

!-----|----------------------------------------------------------------|           
      function det3x3(m)
      REAL*8,INTENT(IN) :: m(3,3)
      REAL*8 :: det3x3,D
      D= m(1,1)*m(2,2)*m(3,3)+m(1,2)*m(2,3)*m(3,1)+m(1,3)*m(2,1)*m(3,2)
     .  -m(1,3)*m(2,2)*m(3,1)-m(1,2)*m(2,1)*m(3,3)-m(1,1)*m(2,3)*m(3,2)
      det3x3=D
      end function det3x3

      function inv3x3(m)
      REAL*8,INTENT(IN) :: m(3,3)
      REAL*8 :: inv3x3(3,3)
      
      inv3x3(1,1)=m(2,2)*m(3,3)-m(2,3)*m(3,2)
      inv3x3(1,2)=m(1,3)*m(3,2)-m(1,2)*m(3,3)
      inv3x3(1,3)=m(1,2)*m(2,3)-m(1,3)*m(2,2)
      
      inv3x3(2,1)=m(2,3)*m(3,1)-m(2,1)*m(3,3)
      inv3x3(2,2)=m(1,1)*m(3,3)-m(1,3)*m(3,1)
      inv3x3(2,3)=m(1,3)*m(2,1)-m(1,1)*m(2,3)
      
      inv3x3(3,1)=m(2,1)*m(3,2)-m(2,2)*m(3,1)
      inv3x3(3,2)=m(1,2)*m(3,1)-m(1,1)*m(3,2)
      inv3x3(3,3)=m(1,1)*m(2,2)-m(1,2)*m(2,1)
      inv3x3=inv3x3/det3x3(m)
      end function inv3x3
!-----|----------------------------------------------------------------|           

      function ray_triangle_collision(t1,t2,t3,v1,v2)
      REAL*8,INTENT(IN)   :: t1(3),t2(3),t3(3),v1(3),v2(3)
      !REAL*8,INTENT(OUT)  :: p(3)     
      logical :: ray_triangle_collision
      real*8 :: a(3),b(3),c(3),x(3),y(3),M(3,3),Mi(3,3)
      a=t2-t1;b=t3-t1;c=v1-v2;y=v1-t1
      M(:,1)=a;M(:,2)=b;M(:,3)=c
      ray_triangle_collision=.FALSE.
      if (det3x3(M).ne.0.d0) then
       Mi=inv3x3(M)
       x=Mi(:,1)*y(1)+Mi(:,2)*y(2)+Mi(:,3)*y(3)       
       p_ray_tri_coll=v1+(v2-v1)*x(3)
       x_ray_tri_coll=x
       ray_triangle_collision=(x(3).ge.0.d0).and.(x(3).lt.1.d0).and.
     .     (x(1).ge.0.d0).and.(x(2).ge.0.d0).and.(x(1)+x(2).le.1.d0)
!        write(*,*) 'check matrix inversion' 
!        do i=1,3;do j=1,3
!         write(*,*) i,j,sum(M(i,:)*Mi(:,j))
!        enddo;enddo      
!        write(*,*) 'check collison' 
!        write(*,*) v1+(v2-v1)*x(3)
!        write(*,*) t1+(t2-t1)*x(1)+(t3-t1)*x(2)
!        write(*,*) x
!        stop
      endif
      END function ray_triangle_collision   
!       
!       function test_triangle()
!       real*8 :: test_triangle
!       REAL*8 :: t1(3),t2(3),t3(3),v1(3),v2(3)
!       logical:: l
!       write(*,*)'test collision'
!         t1=(/-0.951776,0.281700,0.121518/)
!         t2=(/-0.770514,0.478482,1.085064/)
!         t3=(/-0.704258,1.220807,-0.116836/)
!         v1=(/-1.860675,0.676990,0.315555/)
!         v2=(/0.095274,0.224361,0.143823/)
! !      
! !       
! !       
! !       t1=(/0.d0,0.d0,1.d0/)
! !       t2=(/1.d0,0.d0,1.d0/)
! !       t3=(/0.d0,1.d0,1.d0/)
! !       v1=(/0.1d0,0.1d0,2.d0/)
! !       v2=(/0.2d0,0.2d0,-0.01d0/)
!       !l=ray_triangle_collision(t1,t2,t3,v1,v2)
!       write(*,*) l
!       !stop
!       end function test_triangle

!-----|----------------------------------------------------------------|           
      logical function in_triangle(p,xy)
      real*8,intent(in) :: p(2),xy(3,2)
      real*8 :: M(2,2),MI(2,2),d,q(2)
      in_triangle=.FALSE.
      M(:,1)=(/xy(2,1)-xy(1,1),xy(2,2)-xy(1,2)/)
      M(:,2)=(/xy(3,1)-xy(1,1),xy(3,2)-xy(1,2)/)      
      q=(/p(1)-xy(1,1),p(2)-xy(1,2)/)
      d=M(1,1)*M(2,2)-M(2,1)*M(1,2)
      if (d.eq.0.d0) return
      MI(:,1)=(/M(2,2),-M(2,1)/)/d
      MI(:,2)=(/-M(1,2),M(1,1)/)/d
      q=matmul(MI,q)
!      write (*,*) q
      in_triangle=(q(1).ge.0.d0).and.(q(2).ge.0.d0).and.
     .            ((q(1)+q(2)).lt.1.d0)
      end function in_triangle
      
!-----|----------------------------------------------------------------|           
      logical function triangles_collision(xy1,xy2)
!     this function checks if there is an intersection between two triangles in the 2D plane. This is given exactly when one triangle is fully inside the other or if the edges intersect.      
      real*8,intent(in) :: xy1(3,2),xy2(3,2) 
      real*8  :: x(2)
      integer :: i,j,ii,jj
      logical :: c
      triangles_collision=.FALSE.
      triangles_collision=triangles_collision.or.
     .                    in_triangle(xy1(1,:),xy2)! some point of triangle 1 is inside triangle 2
      triangles_collision=triangles_collision.or.
     .                    in_triangle(xy2(1,:),xy1)! some point of triangle 2 is inside triangle 1
      
      
      do i=1,3; do j=1,3
       ii=mod(i,3)+1;jj=mod(j,3)+1
        x=line_line_coll(xy1(i,:),xy1(ii,:),xy2(j,:),xy2(j,:))
        
        triangles_collision=triangles_collision.or.
     .                      ((x(1).ge.0.d0).and.(x(1).le.1.d0).and.
     .                       (x(2).ge.0.d0).and.(x(2).le.1.d0))
      enddo; enddo
      end function triangles_collision


!-----|----------------------------------------------------------------|   

      logical function point_in_poly(p,poly,N)
      !this function checks if a point p is inside a closed polygon consisting of N vertices
      !the algorithm is based on the so called the Jordan curve theorem, according to which
      !a semi-infinite ray is inside the polygon if the number of edge crossings is odd
      !the polygon is automatically closed
      real*8,intent(in) :: p(2),poly(0:N-1,2)
      integer,intent(in) :: N
      logical :: c1,c2
      integer :: i,j
      
      j=N-1
      point_in_poly=.false.
      do i=0,N-1
       c1=((poly(i,2)>p(2)).neqv.(poly(j,2)>p(2)))
       c2=(p(1)<((poly(j,1)-poly(i,1))*(p(2)-poly(i,2))/
     .           (poly(j,2)-poly(i,2))+poly(i,1)))
       if (c1.and.c2) point_in_poly=.not.point_in_poly
       j=i
      enddo
      end function point_in_poly

!-----|----------------------------------------------------------------|   

      logical function polygons_intersect(p1,N1,p2,N2)
      !returns true if polygons p1 and p2 share some common area
      !this is exactly the case if one is surrounded entirely by the other
      !or if some edges intersect
      !note that the polygons are automatically assumed to be closed (i.e. point 1 and N are assumed to be connected)
      real*8,intent(in) :: p1(N1,2),p2(N2,2)
      integer,intent(in) :: N1,N2
      integer :: i,j,i2,j2
      real*8  :: llc(2)
      polygons_intersect=.true.
      if (point_in_poly(p1(1,:),p2,N2)) return
      if (point_in_poly(p2(1,:),p1,N1)) return
      
      i2=N1
      do i=1,N1
       j2=N2
       do j=1,N2
        llc=line_line_coll(p1(i,:),p1(i2,:),p2(j,:),p2(j2,:))
        if ((llc(1).ge.0.d0).and.(llc(1).lt.1.d0).and.
     .      (llc(2).ge.0.d0).and.(llc(2).lt.1.d0)) return
        j2=j
       enddo
       i2=i
      enddo
      polygons_intersect=.false.
      end function polygons_intersect
      
!-----|----------------------------------------------------------------|   

      logical function point_in_polyhedron(p,tri,N)
      !this function checks if a point p is inside a closed polyhedron for which the surfaces are given as triangles
      !the algorithm is based on the idea that a point is inside the polyhedron exactly if a semi-infinite ray intersects with its surfaces
      !an odd number of times
      !note that the algorithm only works if the polyhedron is entirely closed (!)
      real*8,intent(in) :: p(3),tri(N,3,3)
      integer,intent(in) :: N
      integer :: i,Ninter
      logical :: dummy
      real*8  :: x(3)
      Ninter=0
      do i=1,N
       dummy=ray_triangle_collision(tri(i,1,:),tri(i,2,:),tri(i,3,:),
     .                              p,p+(/3.d0,0.d0,0.d0/))
       x=x_ray_tri_coll
       if ((x(3).ge.0.d0).and.(x(1).ge.0.d0).and.(x(2).ge.0.d0).
     .     and.(x(1)+x(2).lt.1.d0)) Ninter=Ninter+1
       write(*,*) 'x=',x
      enddo
      write(*,*) 'Ninter=',Ninter
      point_in_polyhedron=(mod(Ninter,2).eq.1)
      end function point_in_polyhedron

!-----|----------------------------------------------------------------|           
!       logical function point_in_poly(p,poly,N)
!       !this function checks if a point is inside a closed polygon consisting of N vertices
!       !the algorithm is based on the so called the Jordan curve theorem, according to which
!       !a semi-infinite ray is inside the polygon if the number of edge crossings is odd
!       real*8,intent(in) :: p(2),poly(N,2)
!       integer,intent(in) :: N
!       real*8 :: x(N,2),f
!       integer :: i,j,inout
!       logical :: mx,my,nx,ny,c1
!       
!       do i=1,N
!        x(i,:)=poly(i,:)-p(:)
!       enddo
!       inout=-1        
!       do i=1,N
!        j=1+MOD(i,N)                                                      
!        mx=x(i,1).GE.0.d0                                                    
!        nx=x(j,1).GE.0.d0                                                    
!        my=x(i,2).GE.0.d0                                                    
!        ny=x(j,2).GE.0.d0
!        c1=(.NOT.((MY.OR.NY).AND.(MX.OR.NX)).OR.(MX.AND.NX))
!        if (.not.c1) then
! 
!         if(.NOT.(MY.AND.NY.AND.(MX.OR.NX).AND..NOT.(MX.AND.NX))) then
!          f=(x(i,2)*x(j,1)-x(i,1)*x(j,2))/(x(j,1)-x(i,1))
!          if (f.eq.0.d0) inout=0
!          if (f.gt.0.d0) inout=-inout
!         else
!          inout=-inout
!         endif
! 
!        endif
!       enddo
!       point_in_poly=inout.ge.0
!       end function point_in_poly
! 
! 
! 
      end module TOOLS
