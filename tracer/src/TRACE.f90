      module TRACE
C     RUNGE-KUTTA-INTEGRATOR 5th order with constant DELTAPHI (DP=const) 
      
      implicit none
      REAL*8,PARAMETER     :: PI2 = 6.283185307179586D0 
      REAL*8,DIMENSION(5),SAVE  :: DR,DZ
      REAL*8,SAVE :: RZPC(3),DP,LC,LCd(2),dLC,Nd(2),Lcpol,Lcpold(2)
      REAL*8,SAVE :: Rzpold(3),xyzold(3),xyzc(3),dNpol,Npold(2)
      REAL*8,SAVE :: theta,thetaold,dtheta
      
      INTEGER,SAVE :: NSUB,IBEGIN=-1

      INTEGER,SAVE :: LIMIT=0
      integer,save :: limd(2)=0,hit_lined(2)=-1
      logical,save :: endpointcorrection=.true.

      !integer :: iterations_find_conf=30
      
      !limit #1 - field line runs into undefined region
      
      !limit #2 - connection length reached
      real*8,save :: Lclimit=-1.d0

      !limit #3 - number of poloidal turns 
      real*8,save :: Npol=0.d0,Npollimit=0.d0
      
      !limit #4 - toroidally symmetric targets defined by lines
      !note that a number of postlimitsteps>0 can be set
      INTEGER,SAVE :: Nlines=0
      real*8,DIMENSION(:,:),allocatable :: lines
      integer,save :: hit_line=-1

      !limit #5 - 3D targets defined by triangles
      INTEGER,SAVE :: Ntriangles=0
      real*8,DIMENSION(:,:,:),allocatable :: triangles
      integer,save :: hit_triangle=-1

      !limit #6 - poloidal connection length reached
      real*8,save :: Lcpollimit=-1.d0

!       
!       !limit #7 - 3D targets defined by enclosing surfaces
!       real*8,save :: Nvolumes=-1.d0

      !limit #10 - a number of steps after reaching limit 4 was arrived
      integer :: dplctr=0,plctr=0,postlimitsteps=0
      
      !periodicity settings
      REAL*8,SAVE :: phimin=0.d0,phimax=PI2
      logical,save :: apply_peridicity=.FALSE. !if this parameter is true phi is mapped to the interval phimin...phimax

      !parameters for find_confinement routine
      integer,save :: fc_iterations=30
      real*8,save :: fc_dphi=1.d0*PI2/360.d0,fc_p0=0.d0
      real*8,save :: fc_tolerace=1.d-4,fc_psiN2est=1.03d0
      real*8,save :: fc_lam1=1.d-4,fc_lam2=1.d-2
      real*8,save :: pLcpolmin(2)
      integer,save :: fc_NSOL=10
      contains


C----.|-----------------------------------------------------------------      
      
      SUBROUTINE RUNGE_KUTTA_5_START()
      use BFIELD
      implicit none
      
      REAL*8,DIMENSION(4),PARAMETER :: 
     .  C =(/ 1.D0/3.D0,2.D0/3.D0  , 1.D0/3.D0  ,1.D0/6.D0  /)
     ., A =(/0.5D0     ,0.5D0      , 1.D0       ,1.D0/6.D0  /)

      REAL*8,DIMENSION(4)       :: QR,QZ,FA
      REAL*8                    :: BRZP(3),BF1,PHI,R,Z
      INTEGER                   :: J,I
      IBEGIN=-1

      PHI = RZPC(3)
      DO I=1,MIN(3,NSUB)
       R=RZPC(1)                 ; Z=RZPC(2)
       DO J=1,3
        BRZP=B_RZP((/R,Z,PHI/)) 
        IF (ierr_bf.ne.0) RETURN
!        CALL B_RZF(R,Z,PHI,BF,BR,BZ,IDIV);IF(IDIV.GE.1) RETURN
        FA(J) = R*DP*A(J)/BRZP(3)
        QR(J) = BRZP(1)*FA(J)    ;QZ(J) = BRZP(2)*FA(J)
        R     = RZPC(1)+QR(J)    ;Z     = RZPC(2)+QZ(J)
        PHI   = RZPC(3)+A(J)*DP 
       ENDDO ! J=1,3
C
       BRZP=B_RZP((/R,Z,PHI/)) 
       IF (ierr_bf.ne.0) RETURN
!       CALL B_RZF(R,Z,PHI,BF,BR,BZ,IDIV);IF(IDIV.GE.1) RETURN
       DR(I)= QR(1) + QR(1)     ; DZ(I)= QZ(1) + QZ(1)
       FA(4)= R*A(4)*DP/BRZP(3)
       RZPC(1) = RZPC(1)+BRZP(1)*FA(4)+SUM(QR(1:3)*C(1:3))
       RZPC(2) = RZPC(2)+BRZP(2)*FA(4)+SUM(QZ(1:3)*C(1:3))
       RZPC(3) = PHI
      END DO !I=1,min(3,NSUB)

      BRZP=B_RZP((/RZPC(1),RZPC(2),PHI/)) 
      IF (ierr_bf.ne.0) RETURN
      
!      CALL B_RZF(RZPC(1),RZPC(2),PHI,BF,BR,BZ,IDIV);
!      IF(IDIV.GE.1) RETURN
      BF1   = RZPC(1)*DP/BRZP(3)
      DR(4) = BRZP(1)*BF1                ;DZ(4) = BRZP(2)*BF1

      IBEGIN=4
      !CALL RUNGE_KUTTA_5_STEP()
      RETURN
      END SUBROUTINE RUNGE_KUTTA_5_START

C----.|-----------------------------------------------------------------      

      SUBROUTINE  RUNGE_KUTTA_5_STEP()  
      
      USE BFIELD   
      IMPLICIT NONE               
      

      REAL*8,DIMENSION(4),PARAMETER :: 
     .     CM=(/-3.D0/8.D0,3.7D1/2.4D1,-5.9D1/2.4D1,5.5D1/2.4D1/)
      REAL*8,DIMENSION(5),PARAMETER :: FM=(/-1.9D0/7.2D1
     .,      5.3D0/3.6D1,-1.1D0/3.D0,1.615D1/1.8D1,2.51D1/7.2D1/)

      REAL*8                    :: BRZP(3),BF1,R,Z
      INTEGER                   :: I
      if (IBEGIN.lt.0) then
       ierr_bf=1
       return
      endif
      DO I=IBEGIN,NSUB 
       R = RZPC(1)+SUM(CM(1:4)*DR(1:4));Z=RZPC(2)+SUM(CM(1:4)*DZ(1:4))
       RZPC(3) = RZPC(3)+DP
       BRZP=B_RZP((/R,Z,RZPC(3)/))
       IF (ierr_bf.ne.0) RETURN
!       CALL B_RZF(R,Z,RZPC(3),BF,BR,BZ,IDIV);IF(IDIV.GE.1) RETURN
       BF1    = DP*R/BRZP(3)
       DR(5)  = BRZP(1)*BF1          ;DZ(5)  = BRZP(2)*BF1
       RZPC(1)= RZPC(1) +SUM(FM*DR)  ;RZPC(2)= RZPC(2)+SUM(FM*DZ)
       DR(1:4)= DR(2:5)              ;DZ(1:4)= DZ(2:5)
      ENDDO  ! I     
      BRZP=B_RZP(RZPC);IF (ierr_bf.ne.0) RETURN
!      CALL B_RZF(RZPC(1),RZPC(2),RZPC(3),BF,BR,BZ,IDIV);
!      IF(IDIV.GE.1) RETURN
      IBEGIN=1

      RETURN
      END SUBROUTINE RUNGE_KUTTA_5_STEP

C----.|-----------------------------------------------------------------      

      SUBROUTINE FIELD_LINE(RZPS,dphi,N,M,FL,SUBSTEPS,dir)
      USE TOOLS
      USE BAXISYM
      USE BFIELD
      IMPLICIT NONE
      
      REAL*8,INTENT(IN) :: RZPS(3),dphi
      INTEGER, INTENT(IN) :: SUBSTEPS,N,M,dir
      REAL*8,INTENT(OUT) :: FL(0:N,M)
      REAL*8 :: dLcpol,dLcpolmin
Cf2py intent(in) RS,ZS,PS,dphi,N
Cf2py intent(out) FL
Cf2py integer,intent(in), optional :: SUBSTEPS=5,dir=1,M=3
Cf2py depend(N,M) FL

      INTEGER :: i,j,I0,I1(2),I2(2),dI(2),Nj
      
      NSUB=SUBSTEPS;
      LCd=NaN();Npold=0;Nd=0;limd=0;hit_lined=0
      
      FL=NaN()
      IF( NSUB<4) THEN
       write (*,*) 'NSUB has to be larger or equal 4 (TRACE.f90)'
       return
      ENDIF 

      IF (dir.eq.1) THEN !forward direction
       Nj=1;I0=0;I1(1)=1;I2(1)=N;dI(1)=1
      ELSE IF (dir.eq.0) THEN !back- and forth directions
       Nj=2;I0=N/2;I1=(/I0+1,I0-1/);I2=(/N,0/);dI=(/1,-1/)
      ELSE IF (dir.eq.-1) THEN !backward direction
       Nj=1;I0=N;I1(1)=N-1;I2(1)=0;dI(1)=-1
      ELSE 
       write(*,*) 'dir has to be -1,0 or +1.'
       return
      END IF 
      
      dLcpolmin=1.d10
      DO J=1,Nj
       LC=0.d0;Npol=0.d0;Lcpol=0.d0
       plctr=0;dplctr=0
       
       !write(*,*) 'J=',J
       RZPC=RZPS
       DP=dI(j)*dphi/NSUB
      
       FL(I0,1:3)=RZPC
       Rzpold=RZPC       
       xyzold=Rzp2xyz(Rzpold)
       thetaold=atan2(RZPC(2)-OPOINT(2),RZPC(1)-OPOINT(1))
       CALL RUNGE_KUTTA_5_START()
       
       DO i=I1(J),I2(J),dI(J)
        CALL RUNGE_KUTTA_5_STEP()
        theta=atan2(RZPC(2)-OPOINT(2),RZPC(1)-OPOINT(1))
        dtheta=theta-thetaold

        if (abs(dtheta).gt.( 1.5d0*PI)) dtheta=dtheta-sign(PI2,dtheta)

        xyzc=Rzp2xyz(RZPC)
        dLc=length(xyzc-xyzold)
        
        CALL CHECK_LIMITS()

        plctr=plctr+dplctr !increase post limit counter if dplctr>0
        
        FL(i,1:3)=RZPC
        
        IF (M.ge.4) FL(i,4)=PSIN_AT_RZ(RZPC(1:2))
        IF (M.ge.5) FL(i,5)=Lc

        Npol=Npol+dtheta/PI2
        LC=LC+dLC
        dLcpol=length(RZPC(1:2)-Rzpold(1:2))
        Lcpol=Lcpol+dLcpol
        if (dLcpol.lt.dLcpolmin) then
         dLcpolmin=dLcpol
         pLcpolmin=RZPC(1:2) ! an X-point might be near this point, so remember it
        endif
        
        Rzpold=RZPC
        xyzold=Rzp2xyz(Rzpold)
        thetaold=theta
        IF (limit.ne.0) EXIT        
       ENDDO
!       write(*,*) 'limit',limit
       LCd(j)=Lc;Npold(j)=Npol;Nd(j)=i;limd(j)=limit
       hit_lined(j)=hit_line
      ENDDO
      
      if (apply_peridicity) then
       do i=0,N
        FL(i,3)=maphi(FL(i,3),phimin,phimax)
       enddo
      endif
      
      END SUBROUTINE FIELD_LINE
C----.|-----------------------------------------------------------------      

      SUBROUTINE CHECK_LIMITS()
      USE TOOLS
      USE BFIELD
      integer :: i
      real*8 :: ll(2),x,p1,p2,x1(3),x2(3)
      logical :: coll,phi_bnd_reached
      !the endpointcorrection might move RZPC away from a flux surface, in particular for large DP. Set endpointcorrection=.false. to avoid this
      phi_bnd_reached=.false.      
      hit_line=0
      hit_triangle=0
      
      limit=1 ! field line runs into undefined region
      if (ierr_bf.eq.1) return 

      limit=2 ! Lc limit arrived
      if ((Lclimit.gt.0.d0).and.((Lc+dLc).gt.Lclimit)) then
       
       if (endpointcorrection) then
        x=(Lclimit-Lc)/dLc
        dLc=dLc*x
        RZPC=Rzpold+x*(RZPC-Rzpold)
       endif 
       return
      endif

      limit=3 ! number of poloidal turns arrived
      if ((Npollimit.ne.0.d0).and.
     .    (abs(Npol+dtheta/PI2).ge.abs(Npollimit))) then 
       if (endpointcorrection) then
        x=(abs(Npollimit)-abs(Npol))/abs(dtheta/PI2)
        dtheta=dtheta*x
        RZPC=Rzpold+x*(RZPC-Rzpold)   
        !write(*,*) 'limit 3 hit'
       endif
       return
       
      endif 
      
      limit=4 ! one of the toroidally symmetric structures is hit
      do i=1,Nlines
       
       ll=line_line_coll(Rzpold(1:2),(/RZPC(1),RZPC(2)/),
     .                   lines(i,1:2),lines(i,3:4))
       if ((ll(1).ge.0).and.(ll(1).lt.1).and.
     .     (ll(2).ge.0).and.(ll(2).lt.1)) then 
        if (endpointcorrection) RZPC=Rzpold+ll(1)*(RZPC-Rzpold)
	hit_line=i

        if (postlimitsteps.le.0) then
         return
        else
         dplctr=1
        endif 

       endif 
      enddo

      
      limit=5 ! a 3D target is hit
      if (Ntriangles.gt.0) then
      p1=maphi(Rzpold(3),phimin,phimax)
      p2=maphi(Rzpc(3)  ,phimin,phimax)
!       p1=Rzpold(3)
!       p2=Rzpc(3)
      x1=Rzp2xyz((/Rzpold(1),Rzpold(2),p1/))
      x2=Rzp2xyz((/Rzpc(1),  Rzpc(2),  p2/))
      phi_bnd_reached=(abs(p2-p1-dp*NSUB).gt.1.d-6)
      
      do i=1,Ntriangles
        coll=ray_triangle_collision(
     .  triangles(i,1,:),triangles(i,2,:),triangles(i,3,:),x1,x2)
         if (coll.and.(.not.phi_bnd_reached)) then          
          if (endpointcorrection) RZPC=xyz2Rzp(p_ray_tri_coll)
	  hit_triangle=i
          return
         endif
      enddo
      endif
      
      limit=10
      if (plctr.gt.postlimitsteps) return
      
      limit=0
      
      end subroutine CHECK_LIMITS
C----.|-----------------------------------------------------------------      

      SUBROUTINE LC_PSI_NMIN(RZPS,Nr,Nz,dphi,N,SUBSTEPS,dir,LcPn)
      USE TOOLS
      
      IMPLICIT NONE
      
      REAL*8,INTENT(IN) :: RZPS(Nr,Nz,3),dphi
      INTEGER, INTENT(IN) :: SUBSTEPS,N,Nr,Nz,dir
      REAL*8,INTENT(OUT) :: LcPn(Nr,Nz,2)
Cf2py intent(in) RZPS,dphi,N,Nr,Nz
Cf2py intent(out) LcPn
Cf2py integer,intent(in), optional :: SUBSTEPS=5,dir=0
      integer :: i,j
      real*8  :: FL(0:N,4)
      do i=1,Nr
      write(*,*) i,Nr
      do j=1,Nz
       call FIELD_LINE(RZPS(i,j,:),dphi,N,4,FL,SUBSTEPS,dir)
       LcPn(i,j,1)=sum(LCd)
       LcPn(i,j,2)=minval(FL(:,4))
      enddo
      enddo
      END SUBROUTINE LC_PSI_NMIN
      
C----.|-----------------------------------------------------------------      

      subroutine unset_all_limits()
      call unset_Lc_limit()
      call unset_Npol_limit()
      call unset_axisym_targets()
      call unset_3D_targets()      
      end subroutine unset_all_limits

C----.|-----------------------------------------------------------------      

      subroutine unset_Lc_limit()       
      implicit none
      Lclimit=-1.d0
      end subroutine unset_Lc_limit

C----.|-----------------------------------------------------------------      

      subroutine set_Lc_limit(Lc)       
      implicit none
      real*8,intent(in) :: Lc
      Lclimit=Lc
      end subroutine set_Lc_limit
C----.|-----------------------------------------------------------------      

      subroutine unset_Npol_limit()       
      implicit none
      Npollimit=0.d0
      end subroutine unset_Npol_limit

C----.|-----------------------------------------------------------------      

      subroutine set_Npol_limit(Npl)       
      use BAXISYM
      implicit none
      real*8,intent(in) :: Npl
      if (OPOINT(1).lt.0) then
       write(*,*) 'WARNING: O-point position must be determined before'
       write(*,*) 'calling set_Npol_limit()'
      endif
      Npollimit=Npl
      end subroutine set_Npol_limit
            
C----.|-----------------------------------------------------------------      

      subroutine unset_axisym_targets()       
      implicit none
      if (allocated(lines)) deallocate(lines)
      Nlines=0
      end subroutine unset_axisym_targets
      
C----.|-----------------------------------------------------------------      

      subroutine set_axisym_targets(ll,N)       
      implicit none
      integer :: N
      real*8 :: ll(N,4)
Cf2py intent(in) ll
Cf2py integer intent(hide),depend(ll)
      if (allocated(lines)) deallocate(lines)
      allocate(lines(N,4))
      lines=ll
      Nlines=N
      end subroutine set_axisym_targets
C----.|-----------------------------------------------------------------      

      subroutine add_axisym_target_curve(c,N)       
      implicit none
      integer :: N,Nold,i
      real*8 :: c(N,2)
      real*8,allocatable,dimension(:,:) :: temp 
Cf2py intent(in) c
Cf2py integer intent(hide),depend(c)
      Nold=0
      !write(*,*) 'Nlines=',Nlines
      if (allocated(lines)) then 
       Nold=Nlines
       allocate(temp(Nold,4))
       temp(:,:)=lines(:,:)
       deallocate(lines)
      endif 
      Nlines=Nold+N-1
      allocate(lines(Nlines,4))
      lines(1:Nold,:)=temp(:,:)
      do i=1,N-1
       lines(Nold+i,1:2)=c(i  ,:)
       lines(Nold+i,3:4)=c(i+1,:)
      enddo
      if (allocated(temp)) deallocate(temp) 
      end subroutine add_axisym_target_curve
      
C----.|-----------------------------------------------------------------      

      subroutine unset_3D_targets()       
      implicit none
      if (allocated(triangles)) deallocate(triangles)
      Ntriangles=0
      end subroutine unset_3D_targets

C----.|-----------------------------------------------------------------      

      subroutine set_3D_targets(tri,N)       
      implicit none
      integer :: N
      real*8 :: tri(N,3,3)
Cf2py intent(in) tri
Cf2py integer intent(hide),depend(tri)
      if (allocated(triangles)) deallocate(triangles)
      allocate(triangles(N,3,3))
      triangles=tri
      Ntriangles=N
      end subroutine set_3D_targets
C----.|-----------------------------------------------------------------      

      subroutine add_3D_mesh(c,N,M)       
      implicit none
      integer :: N,M,Nold,i,j
      real*8 :: c(N,M,3)
      real*8,allocatable,dimension(:,:,:) :: temp 
Cf2py intent(in) c
Cf2py integer intent(hide),depend(c)
      Nold=0      
      if (allocated(triangles)) then 
       Nold=Ntriangles
       allocate(temp(Nold,3,3))
       temp(:,:,:)=triangles(:,:,:)
       deallocate(triangles)
      endif 
      Ntriangles=Nold+(N-1)*(M-1)*2
      allocate(triangles(Ntriangles,3,3))
      triangles(1:Nold,:,:)=temp(:,:,:)
      
      do i=1,N-1;do j=1,M-1
       Nold=Nold+1
       triangles(Nold,1,:)=c(i,j,:)
       triangles(Nold,2,:)=c(i,j+1,:)
       triangles(Nold,3,:)=c(i+1,j+1,:)
       Nold=Nold+1
       triangles(Nold,1,:)=c(i,j,:)
       triangles(Nold,2,:)=c(i+1,j,:)
       triangles(Nold,3,:)=c(i+1,j+1,:)
      enddo;enddo
      if (allocated(temp)) deallocate(temp) 
      end subroutine add_3D_mesh
      
      
C----.|-----------------------------------------------------------------      

      subroutine unset_Lcpol_limit()       
      implicit none
      Lcpollimit=-1.d0
      end subroutine unset_Lcpol_limit

C----.|-----------------------------------------------------------------      

      subroutine set_Lcpol_limit(Lcpol)       
      implicit none
      real*8,intent(in) :: Lcpol
      Lcpollimit=Lcpol
      end subroutine set_Lcpol_limit
C----.|-----------------------------------------------------------------      

      subroutine unset_postlimitsteps()       
      implicit none
      postlimitsteps=0
      end subroutine unset_postlimitsteps

C----.|-----------------------------------------------------------------      

      subroutine set_postlimitsteps(Npls)       
      implicit none
      integer,intent(in) :: Npls
      postlimitsteps=Npls
      !write(*,*) 'postlimitsteps',postlimitsteps
      end subroutine set_postlimitsteps

C----.|-----------------------------------------------------------------      

      subroutine set_endpointcorrection(epc)       
      implicit none
      logical,intent(in) :: epc
      endpointcorrection=epc
      end subroutine set_endpointcorrection
      
C----.|-----------------------------------------------------------------      


      subroutine set_periodicity(pmin,pmax,iapply_peridicity)       
      implicit none
      real*8,intent(in) :: pmin,pmax
      logical :: iapply_peridicity
Cf2py real*8,intent(in) :: pmin,pmax
      phimin=pmin;phimax=pmax
      apply_peridicity=iapply_peridicity
      end subroutine set_periodicity
      
C----.|-----------------------------------------------------------------      

      subroutine find_confinement(p1,p2,psio,psix)
C     This routine does a similar job than singularities_LSN() in BAXISYM
C     with the difference that the primary X-point (i.e. the limit of the 
C     confinement region) is identified as such also in an USN,DN or SF.
C     p1 is an estimate for the magnetic axis (i.e. a point clearly in-
C     side) and p2 a point clearly outside the confinement region
      
      use tools
      use bfield
      use BAXISYM
      implicit none
      REAL*8,INTENT(IN) :: p1(2),p2(2)
      REAL*8,INTENT(OUT) :: psio,psix
Cf2py real*8,intent(in),optional,dimension(2) :: p1=-1.0,p2=-1.0
Cf2py real*8,intent(out) psio,psix
      integer,PARAMETER :: N=100000,Np=400
      integer :: i,j
      real*8 :: FL(0:N,3),remember_Npollimit,pp1(2),pp2(2),p(2),T
      real*8 :: dL,c(2),v(2),PF,best2,psiN2,omp(2),vout(2),BRZ(2)
      logical:: FND
      pp1=p1;pp2=p2
      best2=1.d5
      if (pp1(1).le.0.0) pp1=(/(rmin+rmax)/2.d0,(zmin+zmax)/2.d0/)
      if (pp2(1).le.0.0) pp2=(/rmax            ,(zmin+zmax)/2.d0/)
      call search_oxp(pp1,0)
      
      if (OPOINT(1).le.0.d0) return

      pp1=OPOINT(1:2)
      vout=(pp2-pp1)/length(pp2-pp1)

      remember_Npollimit=Npollimit
      call set_Npol_limit(2.d0) !only in the confinement region a fieldline can make two full poloidal turns
            
      do i=1,fc_iterations
       omp=(pp1+pp2)/2.d0
       write(*,*) i,omp
       call FIELD_LINE((/omp(1),omp(2),fc_p0/),fc_dphi,N,3,FL,5,1)
       if (limit.eq.3) then
        pp1=omp
       else
        pp2=omp
       endif       
      enddo 

      psix=PSI_AT_RZ(omp)
      
      
!       XPOINT(0,1)=PSI_AT_RZ(pp)
!       psio=OPOINT(0);psix=XPOINT(0,1)
      Npollimit=remember_Npollimit
      
      DO i=0,NP
        T = i/(Np-1.d0)*PI2
        v=(/cos(T),sin(T)/)*0.05
        p=OPOINT(1:2)+v        
        CALL ray_flxsf_intersection(p,v,psix)
        
        call search_singularity(p,PF,FND)  
!        write(*,*) i,pp,PF-psix,FND
        
        if (FND.AND.(abs(PF-psix).lt.fc_tolerace)) EXIT
      ENDDO
      XPOINT(1:2,1)=p
      XPOINT(0,1)=PF

      !upstream properties
      Rz_u=omp
      BRZ=BAS_RZ(Rz_u)
      dpsiu_dru=BRZ(2)*PI2*Rz_u(1)*vout(1)-BRZ(1)*PI2*Rz_u(1)*vout(2)
      dpsiNu_dru=dpsiu_dru/(XPOINT(0,1)-OPOINT(0))
      theta_pitchu=atan(dot(BRZ,(/-vout(2),vout(1)/))/BAS_P(Rz_u(1)))
!       write(*,*) pp1
!       write(*,*) pp2
!       write(*,*) length(pp2-pp1)
!       write(*,*) vout
!       write(*,*) Rz_u,theta_pitchu,dpsiNu_dru,BAS_P(Rz_u(1))
!       stop

      
      do i=0,fc_NSOL
       p=omp+vout*i/float(fc_NSOL)*(fc_lam2-fc_lam1)
       
       call FIELD_LINE((/p(1),p(2),fc_p0/),fc_dphi,N,3,FL,5,0)
       p=pLcpolmin
       !write(*,*) 'trying',p
       if (.not.isnan(p(1))) call search_singularity(p,PF,FND)

       if (FND) then
        !write(*,*) 'candidate:',p,best2
	psiN2=(PF-OPOINT(0))/(XPOINT(0,1)-OPOINT(0))
        if ((abs(psiN2-fc_psiN2est).lt.abs(best2-fc_psiN2est)).and.
     .      (abs(psiN2).gt.fc_tolerace).and.
     .      (abs(psiN2-1.d0).gt.fc_tolerace).and.
     .      (length(p-pLcpolmin).lt.1.d+1))then
         !write(*,*) 'used',psiN2,pLcpolmin
         XPOINT(:,2)=(/PF,p(1),p(2)/)
         best2=psiN2
        endif
       endif     
      enddo
      
      end subroutine find_confinement

C----.|-----------------------------------------------------------------      

      END MODULE TRACE
