      MODULE BFIELD    
      IMPLICIT NONE

      real*8,save  :: scale_B(3)=(/1.d0,1.d0,0.d0/)
      integer,save :: ierr_bf=0
      real*8,save  :: last_B_RZP(3)
      contains

      FUNCTION B_RZP(RZP)
      USE BAXISYM
      USE BPRECALC
      IMPLICIT NONE

      REAL*8, INTENT(IN ) :: RZP(3)
      REAL*8              :: B_RZP(3)
      
      ierr_bf=1
      
      B_RZP=0.d0
      if (abs(scale_B(1)).gt.1.D-100) then
       B_RZP(1:2)=B_RZP(1:2)+BAS_RZ(RZP(1:2))*scale_B(1)
       if (ierr_bas.ne.0) return
      end if

      if (abs(scale_B(2)).gt.1.D-100) then
       B_RZP(3)=B_RZP(3)+BAS_P(RZP(1))*scale_B(2)
       if (ierr_bas.ne.0) return
      end if
  
      if (abs(scale_B(3)).gt.1.D-100) then
       if (allocated(ba)) then
        B_RZP=B_RZP+BPRE_RZP(RZP)*scale_B(3)
       else
        B_RZP=B_RZP+BPRE_RZP_direct(RZP)*scale_B(3)
       endif
       if (ierr_bpre.ne.0) return
      end if 
      last_B_RZP=B_RZP
      ierr_bf=0
      END FUNCTION
      
      SUBROUTINE scale_components(f)
      real*8,intent(in) :: f(3)
      scale_B=f
      END SUBROUTINE scale_components


      SUBROUTINE B_on_sf(RZP,N,M,BRZP)
      IMPLICIT NONE
      INTEGER,INTENT(IN) ::N,M
      REAL*8,INTENT(IN) :: RZP(N,M,3)
      REAL*8,INTENT(OUT) :: BRZP(N,M,3)      
      
      integer :: i,j
      do i=1,N; do j=1,M
       BRZP(i,j,:)=B_RZP(RZP(i,j,:))
      enddo;enddo
      END SUBROUTINE B_on_sf
      
      END MODULE BFIELD
