from osa import Client
#import urllib
import numpy as np
import matplotlib.pyplot as plt

import utils.plot as pu

degtorad = 2.*np.pi/360.
class WALL():

    def __init__(self,cID = [2,5,6,7,9,13,14,15,16,17]):
        #cID = [2,5,6,7, 9,15,16,17,14]
        self.client = Client("http://esb.ipp-hgw.mpg.de:8280/services/MeshSrv?wsdl")
        self.set_wall(cID)
        return

    def get_halfmodule(self,hm):
        comp = {'divertor':[[165],[166],[167],[168],[169]],\
        'baffle':[[320],[321],[322],[323],[324]],\
        'shield':[[330],[331],[332],[333],[334]],\
        'tclosure':[[325],[326],[327],[328],[329]],\
        'vessel':[[340],[341],[342],[343],[344]],\
        'panel':[[435,436],[437,438],[439,440],[441,442],[443,444]],\
        'pclosure':[[445],[446],[447],[448],[449]],\
        'pumpslit':[[450],[451],[452],[453],[454]]}
        cID = []
        for key in comp.keys():
            for i in hm:
              cID.extend(comp[key][i])
        return cID

    def set_wall(self,cID,cflag='Set'):
        if cflag == 'Set':
            mset = self.client.types.SurfaceMeshSet()
       #elif cflag == 'Component':
            #mset = self.client.types.SurfaceMeshWrap()  
            #mset = self.client.types.SurfaceMeshSet()
        else:
            raise('cflag not an implmented option!')

        mset.references = []
        for i in  cID:
            ref = self.client.types.DataReference()
            ref.dataId = "%d" %i
            ##ref.server = 'http://esb.ipp-hgw.mpg.de:8280/services/ComponentsDbRest'
            ##ref.protocol = 'REST'
            mset.references.append(ref)
        self.mset = mset
        return

    def rotate_ID(self):
        return

    def translate_ID(self):
        return

    def compile_components(self,cid):
        MeshSet = self.client.types.SurfaceMeshSet()
        MeshSet.meshes = []
        for item in cid:
            MeshWrap = self.client.types.SurfaceMeshWrap()
            DataRef = self.client.types.DataReference()
            DataRef.dataId = item
            MeshWrap.reference = DataRef
            MeshSet.meshes.append(MeshWrap)
        self.mset = MeshSet
        return MeshSet

    def cut_wall_phi(self,phi=0.):
        phi = degtorad*phi
        results = self.client.service.intersectMeshPhiPlane(phi,self.mset)
        self.wc = results
        return results

    def cut_wall_plane(self,pt,normal):
        vpt = self.client.types.Vector()
        vpt.x,vpt.y,vpt.z = pt[0],pt[1],pt[2] 
        vn = self.client.types.Vector()
        vn.x,vn.y,vn.z = normal[0],normal[1],normal[2] 
        plane = self.client.types.Plane()
        plane.point = vpt
        plane.normal = vn
        results = self.client.service.intersectMeshPlane(plane, self.mset)
        self.wc = results
        return results

    def cut_wall_segment(self,pt1,pt2):
        pts = self.client.types.Points3D()
        pts.x1,pts.x2,pts.x3 = pt1[0],pt1[1],pt1[2] 
        pte = self.client.types.Points3D()
        pte.x1,pte.x2,pte.x3 = pt2[0],pt2[1],pt2[2] 
        results = self.client.service.intersectSegmentMesh(pts,pte,self.mset)
        self.wc = results
        return results

    def distance_point(self,pt):
        pts = self.client.types.Points3D()
        pts.x1,pts.x2,pts.x3 = pt[0],pt[1],pt[2] 
        results = self.client.service.getPointMeshDistances(pts,self.mset)
        self.wc = results
        return results

    def save_wall(self,wc,fn=None):
        import utils.data as data
        dat = data.data()
        dat.wc = []
        for s in wc:
            ss = data.data()
            vertex = data.data()
            vertex.x1 = np.array(s.vertices.x1)
            vertex.x2 = np.array(s.vertices.x2)
            vertex.x3 = np.array(s.vertices.x3)
            ss.vertices = vertex
            dat.wc.append(ss)
        if not(fn is None):
            dat.save(fn)
        return dat.wc

    def load_wall(self,fn='Default.pik'):
        import utils.data as data
        dat = data.data()
        dat.load(fn)
        return dat['wc']

    def plot_wall(self,wc,fig=None,ax=None,color=None,lw=1.0,phi=None,shw=False):
        fig,axs,col = pu.setax(fig=fig,figsize=(10.3,10.3))
        if ax is None: ax = axs[0]
        if color is None: color = 'k'

        if not(phi is None):
            #phi = [0.,360.]
            phi = np.array(phi)*degtorad
            twopi = 2.*np.pi
            i = 0
        for s in wc:
                x = np.array(s.vertices.x1)
                y = np.array(s.vertices.x2)
                z = np.array(s.vertices.x3)
                R = np.sqrt(x**2 + y**2)
                #Restrict tor. boundaries
                if not(phi is None):
                    i+=1
                    p = np.arctan2(y,x)
                    #Map to [0,2pi]
                    if any(p < 0.): p = p+twopi
                    #Check boundaries
                    if any(p < phi[0]) or any(p > phi[1]):
                        continue
                ax.plot(R, z, '-',color=color, lw = lw)#, picker=5)
                
        if shw:
            plt.show(block=False)
        return fig