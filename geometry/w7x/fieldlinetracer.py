from osa import Client
from netCDF4 import Dataset
import urllib
import numpy as np
import copy
import matplotlib.pyplot as plt
from mayavi import mlab
import scipy.spatial as spatial
import scipy.signal as signal

#from mayavi.core.api import PipelineBase
#from mayavi.core.ui.api import MayaviScene, SceneEditor, \
#                MlabSceneModel
#from traits.api import HasTraits, Range, Instance, \
#        on_trait_change
#from traitsui.api import View, Item, Group

import utils.colors as colors
import utils.geom as geom
import utils.plot as pu
import utils.data as data
import utils.utils as utils


geoinput = 'geometry/w7x/input/' 

degtorad = np.pi/180.
twopi = 2.*np.pi

def get_w7x():
    w7x = w7xgeo.W7X_data()
    return w7x

def plot_3d(W7X, phi_arr=[0.05,0.21] ):
    u"""
    Make Poincare plot in 3D
    """

    fig=mlab.figure(size = (1024,768),bgcolor = (1,1,1), fgcolor = (0.5, 0.5, 0.5))    
    
    phi_arr = [2.*pi/5.*phi for phi in phi_arr]

    for i in range(len(phi_arr)):
        result=poincare( copy(phi_arr[i]) )
        j_fac=255.0/len(result.surfs)
        for j in range(0,len(result.surfs)):
#            mlab.points3d( asarray( result.surfs[j].points.x1 )*cos(phi_arr[i]), asarray( result.surfs[j].points.x1 )*sin(phi_arr[i]),\
#                           asarray( result.surfs[j].points.x3 ) , scale_factor=0.02, color=( (j_fac*j)/255.0, (255- j_fac*j)/255.0,(255- j_fac*j)/255.0)   )
            mlab.points3d( asarray( result.surfs[j].points.x1 ), asarray( result.surfs[j].points.x2 ),\
                           asarray( result.surfs[j].points.x3 ) , scale_factor=0.02, \
                           color=( (j_fac*j)/255.0, (255- j_fac*j)/255.0,(255- j_fac*j)/255.0), \
                           transparent=True, opacity=0.3)        

            
#------------plot W7X plasma-------------------------------------------------
#    woutfile = 'wout_w7x.nc'  #VMEC nc file            
#    s1=Flux_surface_VMEC(woutfile=woutfile, s=0.99, v_range=[0, 2.0*pi], Nyquist=1)        
#    mlab.mesh( s1.xmesh, s1.ymesh, s1.zmesh, color=(1, 0, 1) ,transparent=True, opacity=0.1)        
#------------plot divertor -------------------------------------------------
    mlab.triangular_mesh(W7X.cpx, W7X.cpy, W7X.cpz, W7X.cpt, color=(128/255.0, 0, 0))
    mlab.triangular_mesh(W7X.vx, W7X.vy, W7X.vz, W7X.vt , color=(163/255.0,163/255.0,163/255.0))#, transparent=True, opacity=0.3)        
            
    mlab.show()

    return 0    

class TRACER():
    def __init__(self,configID=0,vesselID=[164]):
        self.client = Client('http://esb.ipp-hgw.mpg.de:8280/services/FieldLineProxy?wsdl')

        ''' set the start points (R, phi, Z) for the tracing... '''
        self.pts = [self.client.types.Points3D()]
        self.pts[0].x1 = np.linspace(5.64, 6.3, 30)
        self.pts[0].x2 = np.zeros(30)
        self.pts[0].x3 = np.zeros(30)
        #self.set_trace_init(config='EJM') #Should be configID

        ''' set a coil configuration ... '''
        self.config = self.client.types.MagneticConfig()

        ''' e.g. using a config ID from CoilsDB: 
            1 : 'w7x standard case', 3 : 'low iota', 4 : 'high iota', 5 : 'low mirror', etc. '''
        self.config.configIds = [configID] 

        ''' you could also create your own coil configurations 
            e.g. use only all type 3 of the non-planar sc coils from w7x: '''
        #self.set_currents(IP,Itc,Icc)

        ''' you can use a grid for speeding up your requests. 
            Without a grid all tracing steps will be calculated by using Biot-Savart 
        '''
        self.set_grid(True)

        ''' you can use a Machine object for a limitation of the tracing region. 
            This sample uses a torus model (id = 164) from ComponentsDB: '''
        self.set_machine(vesselID)
        # machine = None
        self.acc = None
        return

    def set_currents(self,IP,Itc=np.zeros(5),Icc=np.zeros(10),sweep=False,fexp=False,inverse=False):
        '''
        Magnetic configuration acc. PLM document 1-AA-T0011
        Winding coil current (I[A] x n) for non-planar and planar coils 
        Sign of the winding currents for positive field direction acc. 1-AA-R0004
        I1-I5: "-"
        IA-IB: "-"
        Island size
        Icc1, Icc2, ..., Icc9, Icc10: "+", "-", ..., "+", "-"
        Sweep/pol. island rotation
        Icc1, Icc2, ..., Icc9, Icc10: "+", "+", ..., "+", "+"
        It1-It5: "+"
        IMPORTANT NOTICE:
        Coil model of control coils is derived from rotation and
        mirroring of single coil design in one HM. This leads to a switch 
        in coil orientation and implies an alternating current sign for
        the tracer to get the exerpimentally applied uniform direction of
        the control coil currents. Exp. alternating currents then would 
        require the same sign for the tracer.
        '''

        #Planar Coils
        #Windings & direction
        IPG = -1.*np.array([108,108,108,108,108,36,36])
        IP = np.array(IP)*IPG

        #Sweep/Control Coils
        #Set direction
        if sweep:
            IccG = np.array([8,8,8,8,8,8,8,8,8,8])
        else:
            IccG = np.array([8,-8,8,-8,8,-8,8,-8,8,-8])
        Icc = np.array(Icc)*IccG

        #Trim Coils
        # at module 2 smaller coil corss section but with more windings 
        ItcG = np.array([48,72,48,48,48])
        Itc = np.array(Itc)*ItcG 

        self.config = self.client.types.MagneticConfig()

        # ******************************************************************************** #
        # ************** Definition of coil types from data base ***************** #
        # ***  http://webservices.ipp-hgw.mpg.de/docs/fieldlinetracer.html#searchCoils *** #
        # ******************************************************************************** #
        # --- Ideal NPC, P, comntrol and trim coils --- #
        self.config.coilsIds=[
            160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 
            161, 166, 171, 176, 181, 186, 191, 196, 201, 206,
            162, 167, 172, 177, 182, 187, 192, 197, 202, 207,
            163, 168, 173, 178, 183, 188, 193, 198, 203, 208,
            164, 169, 174, 179, 184, 189, 194, 199, 204, 209,
            210, 212, 214, 216, 218, 220, 222, 224, 226, 228,
            211, 213, 215, 217, 219, 221, 223, 225, 227, 229,
            230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
            350, 241, 351, 352, 353]

        # --- Assign currents --- #
        Icur = []
        #10 Planar coils each type
        tmp = np.ones(10)
        for I in IP:
             Icur.extend(list(I*tmp))
        Icur.extend(list(Icc))
        Icur.extend(list(Itc))
        self.config.coilsIdsCurrents = Icur

        # --- Invert currents --- #
        if inverse:
            self.config.inverseField = True
        else:
            self.config.inverseField = False

        # --- Use interpolation grid --- #
        self.set_grid(True)
        return

    def set_accuracy(self,absTol=1E-3,relTol=1E-6,minStep=1E-4,maxStep=1E-2,globalError=False):
        acc = self.client.types.Accuracy()
        acc.absTol = absTol
        acc.relTol = relTol
        acc.minStep = minStep
        acc.maxStep = maxStep
        acc.globalError = globalError
        self.acc = acc
        return

    def set_config(self,config='EJM'):
        if config == 'EJM': #Std. Config
            IP = [15.E3 for i in range(5)]
            IP.extend([0.,0.])
        elif config == 'DBM': #Low Iota
            IP = [12222.22 for i in range(5)]
            IP.extend([9166.67,9166.67])
        elif config == 'FTM': #High Iota
            IP = [14814.81 for i in range(5)]
            IP.extend([-10222.22 ,-10222.22 ])
        elif config == 'KJM': #High Mirror
            IP = [14400, 14000, 13333.33, 12666.67, 12266.67]
            IP.extend([0.,0.])
        Itc = np.zeros(5)
        Icc = np.zeros(10)
        self.set_currents(IP,Itc,Icc)
        return

    def print_currents(self):
        print('Coil IDs:')
        print(self.config.coilsIds)
        print('Coil currents')
        print(self.config.coilsIdsCurrents)
        return

    def set_reference_config(self,ID):
        self.config = self.client.types.MagneticConfig()
        self.config.configIds = [ID]
        return

    def set_plasmacurrent(self,Ip,config='EJM'):
        file = {'EJM':'axis_standard_case.dat',\
                'DBM':'axis_low_iota.dat',\
                'FTM':'axis_high_iota.dat',\
               }
        axis=np.loadtxt(geoinput+file[config])
        filament = self.client.types.PolygonFilament() # plasma axis
        filament.vertices = self.client.types.Points3D()
        filament.vertices.x1 = axis[::20,0]
        filament.vertices.x2 = axis[::20,1]
        filament.vertices.x3 = axis[::20,2]
        self.config.coils = [filament]
        self.config.coilsCurrents = [Ip] # 10 kA
        return

    def set_grid(self,flag):
        if flag:
            my_grid = self.client.types.CylindricalGrid()
            my_grid.RMin = 4.05
            my_grid.RMax = 6.75
            my_grid.ZMin = -1.35
            my_grid.ZMax = 1.35
            #my_grid.PhiMin = 0.
            #my_grid.PhiMax = 2*np.pi
            my_grid.numR = 181
            my_grid.numZ = 181#*4
            my_grid.numPhi = 96#*2
            #my_grid.numPhi = 481
            g = self.client.types.Grid()
            g.cylindrical = my_grid
            #g.fieldSymmetry = 5
            g.fieldSymmetry = 5
            self.config.grid = g
        else:
            self.config.grid = None
        return

    def set_machine(self,mesh=[164],assembly=None,mset=None):
        #assembly=[2, 8, 9, 13, 14]
        #13 lower divertor side; #2  upper divertor side; #14 sygraflex; #9 vessel; #8 panel; #164 rect. torus closure
        machine = self.client.types.Machine(1)
        #machine.grid = self.client.types.CartesianGrid()
        machine.grid.numX, machine.grid.numY, machine.grid.numZ = 500,500,100
        machine.grid.ZMin, machine.grid.ZMax = -1.5, 1.5
        machine.grid.YMin, machine.grid.YMax = -7, 7
        machine.grid.XMin, machine.grid.XMax = -7, 7
        machine.assemblyIds = assembly
        machine.meshedModelsIds = mesh
        machine.MeshedModels = mset
        self.machine = machine
        return

    def print_machine(self):
        print('Assemblies:')
        print(self.machine.assemblyIds)
        print('Meshes:')
        print(self.machine.meshedModelsIds)
        return

    def set_exp_currents(self,pid,icur=0,verb=False):
        import diagnostics.w7x.coils as coils; cl = coils.COILS()
        tr = cl.read_pidtime(pid)
        cur = cl.read(tr)
        t, dt = 2.2, 0.1
        mcur,scur = cl.get_time(cur,[t-dt,t+dt],iota=False)
        cid,_ = cl.iota_correction(mcur)
        cid,icur = cl.iota_correction(mcur)
        if verb: cl.print_currents(mcur)
        #if cid is None: 
        #    print('Tentatively assume EJM-like')
        #    icur = -550.
        #icur = np.mean(mcur['Main'][5:7])-icur
        print('Found iota correction for %s' %cid)
        print('Set iota correction to %.2f' %icur)
        IP = mcur['Main'][0:5].tolist()
        IP.extend([icur]*2)
        Itc = np.zeros(5)
        Icc = mcur['Control']
        self.set_currents(IP,Itc,Icc,fexp=True)
        return

    def check_field_direction(self):
        pts = self.client.types.Points3D()
        # --- Magnetic axis at HM10/11 --- #
        x = [5.9,0.,0.]
        # --- Control coil M10 (upper), centre --- #
        #x = [5.970716,-1.609591,0.543232]
        # --- Control coil M11 (lower), centre --- #
        #x = [5.970716,1.609591,-0.543232]
        # --- Trim coil at M1 --- #
        #x = [8.0,00]
        pts.x1, pts.x2, pts.x3 = x[0],x[1],x[2]

        self.set_traceinit('external',x1=x[0],x2=x[1],x3=x[2])
        res = self.client.service.magneticField(pts, self.config)

        A = np.array([res.field.x1, res.field.x2, res.field.x3]).T # Bx,By,Bz [T]
        B = (A[:, 0:]*A[:, 0:]).sum(axis=1)**0.5                   # mod B [T]
        C = np.insert(A, 3, B, axis=1)                             # |B|-values inserted after 3rd column
        # print (C)
        print('Bx, By, Bz, |B| [T]:', res.field.x1, res.field.x2, res.field.x3, B)
        return

    def set_traceinit(self,config='O-points',phi=0.,\
                           #for config=='lin'
                           theta=0.,x1=None,x2=None,x3=None,nr=10,\
                           #for config=='2D'
                           res=0.01,x1r=None,x2r=None,\
                           debug=False):
        self.baseR = None
        self.basez = None
        self.ptssize = None
        phi = degtorad*phi
        nr = int(nr)
        if config == 'lin' or config == 'test':
            self.pts = [self.client.types.Points3D()]
            R0 = 5.95
            #Core flux surfaces
            #R = np.zeros(0)
            #z = np.zeros(0)
            R = np.linspace(5.95, 6.15, 5)
            z = np.zeros(5)
            #Edge flux surfaces (outer midplane)
            R = np.append(R,np.linspace(6.15, 6.25, nr))
            z = np.append(z,np.zeros(nr))
            #Edge flux surfaces (inner midplane)
            R = np.append(R,np.linspace(5.7,5.85,3,endpoint=True))
            z = np.append(z,np.zeros(3))
            #X-point
#            R = np.append(R,np.linspace( 5.42, 5.475,8,endpoint=True))
#            z = np.append(z,np.linspace(-0.905,-0.865,8,endpoint=True))
            R = np.append(R,np.linspace( 5.420, 5.460,4,endpoint=True))
            z = np.append(z,np.linspace(-0.905,-0.878,4,endpoint=True))
            #X-point (close pass)
            R = np.append(R,np.linspace( 5.46, 5.46,4,endpoint=True))
            z = np.append(z,np.linspace(-0.92,-0.88,4,endpoint=True))
            theta = theta*degtorad
            if theta != 0.:
                R,z,_ = geom.rotation(R,z,np.zeros(R.shape),phi=theta,origin=[R0,0.,0.])
            self.baseR = R
            self.basez = z
            self.pts[0].x1 = self.baseR
            self.pts[0].x2 = np.zeros(R.shape)
            self.pts[0].x3 = self.basez
            if debug:
                print(self.baseR,self.basez)
                print(self.pts)
            return
        elif config == 'O-points':
            #r  = [ 5.643, 6.237, 5.643, 5.517, 5.517, 5.950, 6.206, 6.266]
            #z  = [ 0.927, 0.000,-0.927,-0.610, 0.610, 0.000, 0.025, 0.001]
            R  = [ 5.950, 6.1900, 6.2660, 5.643, 6.237, 5.643, 5.517, 5.517, 5.400]
            z  = [ 0.000, 0.0000, 0.0000, 0.927, 0.000,-0.927,-0.610, 0.610,-0.890]
            dR = [ 0.225, 0.0100, 0.0010, 0.079, 0.020, 0.079, 0.037, 0.037, 0.000]
            nR = [     5,      3,      4,     5,     5,     5,     5,     5,     1]
            #R  = [ 5.950, 6.1900, 6.2660, 5.643, 6.237, 5.643, 5.517, 5.517]
            #z  = [ 0.000, 0.0000, 0.0000, 0.927, 0.000,-0.927,-0.610, 0.610]
            #dR = [ 0.225, 0.0100, 0.0010, 0.079, 0.020, 0.079, 0.037, 0.037]
            #nR = [     5,      3,      4,     5,     5,     5,     5,     5]
            #KJM
            #R  = [ 5.970, 6.1750, 6.1760, 6.1900]
            #z  = [ 0.000, 0.0000, 0.0000, 0.0000]
            #dR = [ 0.020, 0.0000, 0.0010, 0.0010]
            #nR = [     5,      1,     10,      5]
            self.pts = []
            for i in range(len(R)):
              pts = self.client.types.Points3D()
              Rt = np.linspace(R[i],R[i]+dR[i],nR[i])
              ztt = np.ones(nR[i])*z[i]
              xt,yt,zt = geom.cylindertocartesian(Rt,ztt,np.ones(Rt.shape)*phi)
              #print(Rt,ztt,phi)
              #print(xt,yt,zt)
              pts.x1 = xt
              pts.x2 = yt
              pts.x3 = zt
              self.pts.append(pts)
            #print(self.pts)
            return 
        elif config == '2D':
            #print(x1r,x2r)
            self.pts = [self.client.types.Points3D()]
            if x1r is None:
                R = [ 5.35,6.27]
            else:
                R = [x1r[0],x1r[1]]
            if x2r is None:
                z = [-1.02,1.02]
            else:
                z = [x2r[0],x2r[1]]
            nr = int(np.abs(R[1]-R[0])/res)
            nz = int(np.abs(z[1]-z[0])/res)
            #print(nr,nz,nr*nz)
            #R = np.linspace(5.35,6.27,nr)
            #z = np.linspace(-1.02,1.02,nz)
            R = np.linspace(R[0],R[1],nr)
            z = np.linspace(z[0],z[1],nz)
            self.baseR = R
            self.basez = z
            R,z = np.meshgrid(R,z)
            R = R.flatten()
            z = z.flatten()
            xt,yt,zt = geom.cylindertocartesian(R,z,np.ones(R.shape)*phi)
            self.pts[0].x1 = xt
            self.pts[0].x2 = yt
            self.pts[0].x3 = zt
            self.ptssize = [nr,nz]
            return
        elif config == 'external':
            self.pts = [self.client.types.Points3D()]
            self.pts[0].x1 = np.array(x1)
            self.pts[0].x2 = np.array(x2)
            self.pts[0].x3 = np.array(x3)
            return
        else:
            raise(Exception,'No such configuration data available yet')
        return

    def plot_pts(self,fig=None,color='k'):
        if fig is None:
          fig, ax = plt.subplots(figsize=(10.3,10.3))
        else:
          ax = fig.gca()

        x1 = [];x2 = [];x3 = []
        for pt in self.pts:
            x1.extend(pt.x1)
            x2.extend(pt.x2)
            x3.extend(pt.x3)
        x1 = np.array(x1)
        x2 = np.array(x2)
        x3 = np.array(x3)
        r,z,p = geom.cartesiantocylinder(x1,x2,x3)
        ax.plot(r,z,'+',color=color)
        return

    def get_separatrix(self,phi=0.,ds=0.1,xoff=0.000,m=None,n=None,\
                       ext=False,nr=5,dr=2E-3,\
                       step=1.E-2,steps=100,single=False,fplt=False,verb=False):
        #print('  Find separatrix')
        settings = self.client.types.SeparatrixSettings()
        settings.axisSettings = self.client.types.AxisSettings()
        sep = self.client.service.findSeparatrix(ds, self.config, settings)
        if verb: print('Separatrix (Bean, phi=0.):',sep.x,sep.y,sep.z,sep.reff)

        pts_sep = self.client.types.Points3D()
        sep.x = sep.x+xoff
        #Tracing surrounding of separatrix or not
        pts_sep.x1 = [sep.x]#np.linspace(x0-0.2e-3, x0+.2e-3, 5).tolist()
        pts_sep.x2 = [sep.y]#*len(points_lcfs.x1)#*5
        pts_sep.x3 = [sep.z]#*len(points_lcfs.x1)#*5
        if ext:
            pts_sep.x1.extend(np.linspace(sep.x-dr, sep.x+dr,nr,endpoint=True).tolist())
            pts_sep.x2.extend(np.array([sep.y]*nr))
            pts_sep.x3.extend(np.array([sep.z]*nr))

        #print('  Trace separatrix')
        if (not(m is None) and not(n is None)):
            self.poincare_plane(m,n,step=step,steps=steps)
        else:
            self.poincare_phi(phi,step=step,steps=steps,single=single)
        #xoff = 0.004
        #pts_sep.x1[0] = pts_sep.x1[0]+xoff
        lcfs = self.trace([pts_sep])
        rsep = 0.
        for s in lcfs[0].surfs:
            x = np.array(s.points.x1)
            y = np.array(s.points.x2)
            z = np.array(s.points.x3)
            if x is None:
                continue
            r = max(np.sqrt(x**2 + y**2))
            rsep = max([r,rsep])
        if fplt:
            self.plot_2d(lcfs)
        return lcfs, rsep, sep

    def get_axis(self,step=0.01):
        settings = self.client.types.AxisSettings()
        results = self.client.service.findAxis(step, self.config, settings)
        return results 

    def get_axis_phi(self,phi=0.,step=0.01):
        phi = phi*degtorad
        settings = self.client.types.XPointSettings()
        settings.axisSettings = self.client.types.AxisSettings()
        results = self.client.service.findAxisAtPhi(phi,step, self.config, settings)
        return results

    def get_characteristics(self,step=0.01):
        task = self.client.types.Task()
        task.step = step
        task.characteristics = self.client.types.MagneticCharacteristics()
        task.characteristics.axisSettings = self.client.types.AxisSettings()
        self.task = task
        # actual tracer
        results = self.client.service.trace(self.pts, self.config, self.task, None, None)
        return results

    def get_xpoints_internal(self,phi=0.,step=1.E-2,verb=True):
        guess = self.client.types.XPointGuess()
        guess.xpointX = 4.80
        guess.xpointY = 0.00
        guess.xpointZ = -0.30
        guess.separatrixX = 6.20
        guess.separatrixY = 0.00
        guess.separatrixZ = 0.00
        guess.poloidalMode = 5
        settings = self.client.types.XPointSettings()
        settings.axisSettings = self.client.types.AxisSettings()
        #settings.axisSettings.prefferedPhi = 0.
        results = self.client.service.findXPoints(step, guess, self.config, settings)
        return results


    def get_separatrix_neighborhood(self,rsep,dx,steps=100,step=1E-2):
        x1 = [rsep+dx[i] for i in range(len(dx))]
        x2 = np.zeros(len(x1))
        x3 = np.zeros(len(x1))
        self.set_traceinit('external',x1=x1,x2=x2,x3=x3)
        self.poincare_phi(0.,step=step,steps=steps,single=True)
        res = self.trace()
        return res

    def get_xpoints(self,dx,res=None,rsep=None,nx=5,steps=100,step=1.E-2,scl=1.E-3,\
                    verb=False,debug=False):
        #Trace separatrix neighborhood
        if res is None:
            if rsep is None: 
                sep,rsep,psep = self.get_separatrix(0.,ds=0.1,step=0.1,steps=steps)
            res = self.get_separatrix_neighborhood(rsep,dx,steps=steps,step=step)

        #Get lists of coordinates
        x,y,z,R,p = self.trace2line(res)
        nR = R.shape
        d = np.zeros(nR)
     
        #Calculate distance between neighboring points (as traced!)
        for i in range(nR[1]-1):
            d[:,i] = np.sqrt((R[:,i]-R[:,i+1])**2+(z[:,i]-z[:,i+1])**2)

        #Loop over separatrix neighborhood
        Rx   = []
        zx   = []
        tmpi = 0
        dmin = 9e99
        idx  = -1
        for j in range(nR[0]):
            #Get index of local minima (X-points)
            idx = signal.argrelextrema(d[j,:],np.less)[0]
            if debug:
                plt.plot(d[j,:])
                plt.plot(idx,d[j,idx],'k+')

            #Separate nx X-points
            idx2 = [idx[i::nx] for i in range(nx)]
            
            #Get X-point coordinates
            tmpR = np.zeros(nx)
            tmpz = np.zeros(nx)
            for i,item in enumerate(idx2):
                idx3 = np.argmin(d[j,item])
                tmpR[i] = R[j,item[idx3]]
                tmpz[i] = z[j,item[idx3]]
                
            #Calculate quality of fit (here symmetry for Std. config)
            #spatial.KDTree(np.array([tmpR,tmpz]).T).query([6.0,0.])
            dtmp = spatial.distance.pdist(np.array([tmpR,tmpz]).T)
            #dtmp = spatial.distance.squareform(dtmp)
            #dtmp[np.where(dtmp == 0.)] = 9E99
            #ds = dtmp.shape
            #idx = np.argmin(dtmp); i = int(idx/ds[0]); j = int(((idx/dx[0])%1)*ds[1])
            if debug: print('Min. dist. X-points = %.2e' %dtmp.min())
            if dtmp.min() <  0.1: #X-points are not separated
                dist = 9.E99
            else:
                dist = max(np.abs([tmpz[0]+tmpz[-1],tmpz[1]+tmpz[-2],\
                                   tmpR[0]-tmpR[-1],tmpR[1]-tmpR[-2],tmpz[2]]))
            if debug:
                print('Index = %i' %j)
                print('Qualifier =: %.2e' %dist)
                print('Rxpt = ', tmpR)
                print('zxpt = ', tmpz)
            #Update for best separatrix
            if dist < dmin:
                if verb:
                    print('Updated with %i (dx = %.4e)' %(j,dx[j]))
                Rx   = copy.deepcopy(tmpR)
                zx   = copy.deepcopy(tmpz)
                dmin = copy.deepcopy(dist)
                tmpi = copy.deepcopy(j)
        
        #Suggest new separatrix neighborhood
        tmp = dx[tmpi]
        dxnew = [tmp-scl,tmp-5.*scl,tmp-2.*scl,tmp-1.*scl,tmp-0.5*scl,\
                 tmp,\
                 tmp+0.5*scl,tmp+1.*scl,tmp+2.*scl,tmp+5.*scl,tmp+scl]

        if debug: plt.xlim((0,50))
        print('Delta R/z = %.2e  &  dx = %.2e' %(dmin,dx[tmpi]))
        xpts = []
        for i in range(nx):
            xpt = data.data()
            xpt.x1 = Rx[i]
            xpt.x2 = 0.
            xpt.x3 = zx[i]
            xpts.append(xpt)
        return xpts,dmin,dxnew,res


    def get_opoints(self,phi=0.,ns=2,steps=50,step=1.E-2,refine=False,verb=False,debug=False):
        #Use coordinates close to O-point (EJM)
        R  = [5.643, 6.237, 5.643, 5.517, 5.517]
        z  = [0.927, 0.000,-0.927,-0.610, 0.610]
        npts = len(R)
        self.pts = []
        for i in range(npts):
          pts = self.client.types.Points3D()
          pts.x1 = [R[i]]
          pts.x2 = [0.]
          pts.x3 = [z[i]]
          self.pts.append(pts)

        lcol = ['m','r','b','g','y','c']
        if debug: fig = plt.figure(); ax=fig.gca()
        #Iterate towards O-points
        for j in range(ns):
            #Trace surface close to O-point
            self.poincare_phi(phi,steps=steps,step=step,single=True)
            res =self.trace()
            for i in range(npts):
                #if j == 1:
                #    print(res[0])
                if debug:
                    fig = self.plot_2d([res[i]],color=lcol[i],ms=3.,fig=fig,shw=False)
                s = res[i].surfs[0]
                if s.points.x1 is None: continue
                #ns = len(s.points.x1)
                #if ns == 0: continue
                try: 
                    xl,yl,zl,rl = self.make_poincare(s)
                except:
                    continue
                #pts = pu.linetopoints(rl,zl,np.zeros(rl.shape))
                pts = pu.linetopoints(xl,yl,zl)
                #Get centroid of island
                cent = geom.get_centroid(np.array(pts))
                #Assign as new surface to trace
                #self.pts[i].x1 = [cent[0]]
                #self.pts[i].x2 = [0.]
                #self.pts[i].x3 = [cent[1]]
                self.pts[i].x1 = [cent[0]]
                self.pts[i].x2 = [cent[1]]
                self.pts[i].x3 = [cent[2]]

                if debug:
                    ax.plot(cent[0],cent[1],'+',ms=5.,color=lcol[i])
            #Refine Tracing
            if refine:
                steps = steps*2.
                step  = step/2.
        #Prepare output
        opts = []
        for item in self.pts:
            pt = data.data()
            pt.x1, pt.x2, pt.x3 = item.x1, item.x2, item.x3
            opts.append(pt)
        return opts

    def map_pts(self,phi,step=1.E-3,inverse=False):
        self.poincare_phi(phi,steps=1,step=step,single=True)
        res = self.trace(inverse=inverse)

        #Assign as new start points
        lpts = []
        for s in res:
            pts = self.client.types.Points3D()
            pts.x1 = []; pts.x2 = []; pts.x3 = []
            for pt in s.surfs:
                pts.x1.append(pt.points.x1[0])
                pts.x2.append(pt.points.x2[0])
                pts.x3.append(pt.points.x3[0])
            lpts.append(pts)

        self.pts = lpts
        return

    def map_phi(self,phi,x1,x2,x3,step=1.E-2,inverse=False):
        self.set_traceinit('external',x1=x1,x2=x2,x3=x3)
        self.poincare_phi(phi,steps=1,step=step,single=True)
        res = self.trace(inverse=inverse)
        x,y,z,R,p = [],[],[],[],[]
        for s in res[0].surfs:
            tx,ty,tz,tR,tp = self.make_poincare(s,phi=True)
            x.append(tx)
            y.append(ty)
            z.append(tz)
            R.append(tR)
            p.append(tp)
        x = np.array(x)
        y = np.array(y)
        z = np.array(z)
        R = np.array(R)
        p = np.array(p)
        return x,y,z,R,p

    def map_plane(self,p,n,x1,x2,x3,steps=1,step=1.E-3,inverse=False):
        self.set_traceinit('external',x1=x1,x2=x2,x3=x3)
        self.poincare_plane(p,n,steps=steps,step=1.E-3)
        res = self.trace(inverse=False)
        x,y,z,R,p = [],[],[],[],[]
        for s in res[0].surfs:
            tx,ty,tz,tR,tp = self.make_poincare(s,phi=True)
            x.append(tx)
            y.append(ty)
            z.append(tz)
            R.append(tR)
            p.append(tp)
        x = np.array(x).T.squeeze()
        y = np.array(y).T.squeeze()
        z = np.array(z).T.squeeze()
        R = np.array(R).T.squeeze()
        p = np.array(p).T.squeeze()
        return x,y,z,R,p

    def trace(self,lpts=None,fn=None,inverse=False):
        self.config.inverseField = inverse
        results = []
        if lpts is None:
            lpts = self.pts
        for pts in lpts:
           results.append(self.client.service.trace(pts, self.config, self.task, self.machine, self.acc))

        #Save results
        if not(fn is None):
            self.save_trace(results,fn)
        return results

    def save_trace(self,results,fn=None):
        #Define datastructure to map into
        dat = data.data()
        dat.res = []
        for r in results:
            res = data.data()
            res.lines = data.data()
            res.connection = data.data()
            res.loads = data.data()
            res.characteristics = data.data()
            res.axis = data.data()
            res.fourer = data.data()
            res.surfs = []
            if not(r.surfs is None):
                for s in r.surfs:
                    surf = data.data()
                    surf.points = data.data()
                    surf.phi0 = s.phi0
                    surf.points.x1 = np.array(s.points.x1,dtype=np.float64)
                    surf.points.x2 = np.array(s.points.x2,dtype=np.float64)
                    surf.points.x3 = np.array(s.points.x3,dtype=np.float64)
                    res.surfs.append(surf)
            dat.res.append(res)
        if not fn is None:
            dat.save(fn)
        return dat.res

    def load_trace(self,fn):
        dat = data.data()
        dat.load(fn)
        return dat.res

    def save_poincare(self,lres,fn=None,phi=None):
        dat = data.data()
        x = np.array([])
        y = np.array([])
        z = np.array([])
        #Maybe replace the append with vstack (length equal?)
        for res in lres:
            for r in res.surfs:
                x = np.append(x, r.points.x1)
                y = np.append(y, r.points.x2)
                z = np.append(z, r.points.x3)
        dat = data.data()
        if not(phi is None):
            phi = [i*degtorad for i in phi]
            p = geom.get_phi(x,y,z)
            pidx = np.where(p < 0.)
            p[pidx] = p[pidx] + twopi
            pidx = np.where( (p >= phi[0]) * (p <= phi[1]))
            x = x[pidx]; y = y[pidx]; z = z[pidx]
        #idx = np.where(x==None)
        #x,y,z = np.delete(x,idx), np.delete(y,idx), np.delete(z,idx)
        x,y,z = np.array(x,dtype=np.float64), np.array(y,dtype=np.float64), np.array(z,dtype=np.float64)
        dat.__dict__ = {'x':x,'y':y,'z':z}
        if not(fn is None):
            dat.save(fn)
        return dat

    def load_poincare(self,fn):
        #Not working
        dat = data.data()
        dat.load(fn)
        return dat

    def mesh_poincare(self,lres):
        dat = data.data()
        ns = len(lres[0].surfs[0].points.x1)
        x = np.zeros(ns)
        y = np.zeros(ns)
        z = np.zeros(ns)
        #x = []
        #y = []
        #z = []
        #Maybe replace the append with vstack (length equal?)
        for res in lres:
            for r in res.surfs:
                #print(x.shape,len(r.points.x1))
                x = np.vstack((x,r.points.x1))
                y = np.vstack((y,r.points.x2))
                z = np.vstack((z,r.points.x3))
                #x.append(r.points.x1)
                #y.append(r.points.x1)
                #z.append(r.points.x1)
        x = x[1:,:].T; y = y[1:,:].T; z = z[1:,:].T
        dat = data.data()
        dat.__dict__ = {'x':x,'y':y,'z':z}
        return dat
        
    def poincare_phi(self,phi=0.0,step=1.0E-2,steps=100,single=False,lphi=False):
        ''' make a request to the web service: '''
        #phi0 = 108.*np.pi/180. QSB #(360.0-18.5)*np.pi/180.0 #88*np.pi/180.0 #0*np.pi/180.0
        task = self.client.types.Task()
        task.step = step
        poi = self.client.types.PoincareInPhiPlane()
        if lphi:
            poi.phi0 = [item*degtorad for item in phi]
        else:
            if single:
                poi.phi0 =  [phi*degtorad]
            else:
                poi.phi0 =  (np.linspace(0, 2*np.pi, 5, endpoint=False) + phi*degtorad).tolist()
        poi.numPoints = steps
        task.poincare = poi
        self.task = task
        return

    def poincare_plane(self,p,n,phi=None,step=1.0E-2,steps=100):
        ''' make a request to the web service: '''
        #phi0 = 108.*np.pi/180. QSB #(360.0-18.5)*np.pi/180.0 #88*np.pi/180.0 #0*np.pi/180.0
        task = self.client.types.Task()
        task.step = step
        poi = self.client.types.ObliquePoincare()
        poi.numPoints = steps
        pt = self.client.types.Points3D()
        pt.x1 = [p[0]]
        pt.x2 = [p[1]]
        pt.x3 = [p[2]]
        nrm = self.client.types.Points3D()
        nrm.x1 = [n[0]]
        nrm.x2 = [n[1]]
        nrm.x3 = [n[2]]
        poi.points  = pt
        poi.normals = nrm
        task.obliquePoincare = poi
        self.task = task
        return

    def fieldline_span(self,phi=0.0,step=0.05):
        ''' make a request to the web service: '''
        task = self.client.types.Task()
        task.step = step
        task.linesPhi = self.client.types.LinePhiSpan()
        task.linesPhi.phi = phi*degtorad
        self.task = task
        return

    def fieldline(self,step=0.05,steps=850):
        ''' make a request to the web service: '''
        lineTask = self.client.types.LineTracing()
        lineTask.numSteps = steps
        task = self.client.types.Task()
        task.step = step
        task.lines = lineTask
        self.task = task
        return

    def clength(self,res=0.10,xr=None,yr=None,phi=0.,step=1.0E-2,limit=5.0E3,loads=True):
        task = self.client.types.Task()
        task.step = step
        con = self.client.types.ConnectionLength()
        con.limit = limit
        con.returnLoads = loads
        task.connection = con
        self.task = task
        if not res is None:
            self.set_traceinit('2D',res=res,x1r=xr,x2r=yr,phi=phi)
        return

    def clengthlim(self,clenf,clenr):
        clf = []
        clr = []
        clfp = []
        clrp = []
        clfe = []
        clre = []
        for r in clenf:
          for c in r.connection:
            if not c is None:
                clf.append(c.length)
                clfp.append(c.part)
                clfe.append(c.element)
        for r in clenr:
          for c in r.connection:
            if not c is None:
                clr.append(c.length)
                clrp.append(c.part)
                clre.append(c.element)
        clr = np.array(clr)
        clf = np.array(clf)
        clrp = np.array(clrp,dtype=np.float)
        clfp = np.array(clfp,dtype=np.float)
        clre = np.array(clre,dtype=np.float)
        clfe = np.array(clfe,dtype=np.float)
        idx = np.where(clf < clr)
        clmin = data.data()
        clmax = data.data()
        clmin.l = copy.deepcopy(clr)
        clmin.l[idx] = clf[idx]
        clmax.l = copy.deepcopy(clf)
        clmax.l[idx] = clr[idx]
        clmin.p = copy.deepcopy(clrp)
        clmin.p[idx] = clfp[idx]
        clmax.p = copy.deepcopy(clfp)
        clmax.p[idx] = clrp[idx]
        clmin.e = copy.deepcopy(clre)
        clmin.e[idx] = clfe[idx]
        clmax.e = copy.deepcopy(clfe)
        clmax.e[idx] = clre[idx]
        clmin.e = np.ones(clr.shape)*-1.
        clmin.e[idx] = np.ones(clr.shape)[idx]
        clmax.e = np.ones(clr.shape)
        clmax.e[idx] = np.ones(clr.shape)[idx]*-1.
        return clmin, clmax

    def invert_currents(self):
        self.config.coilsIdsCurrents = [-1.*item for item in self.config.coilsIdsCurrents]
        return

    def line_diffusion(self,v,lam,D,step=1.E-3,steps=1.E3):
        ''' Field line diffusion model with probability for step 
            p(x) = 1/lambda exp(-x/lambda) and step size
            r = uniform(0,np.sqrt(12 D lambda / v))'''
        task = self.client.types.Task()
        task.step = step
        lineTask = self.client.types.LineTracing()
        lineTask.numSteps = int(steps)
        lineTask.globalError = False
        lineTask.localError = False
        task.lines = lineTask
        diff = self.client.types.LineDiffusion()
        diff.diffusionCoeff = D
        diff.freePath = lam
        diff.velocity = v
        task.diffusion = diff
        self.task = task
        return

    def get_field(self,lpts):
        results = []
        if lpts is None:
            lpts = self.pts
        for pts in lpts:
          results.append(self.client.service.magneticField(pts, self.config))
        return results

    def get_gridfield(self):
        pts   = self.client.service.getGridPoints(self.config.grid)
        field = self.client.service.getGridField(self.config)
        return [pts,field]

    def make_bfield(self,s):
        x = np.array(s.x1)
        y = np.array(s.x2)
        z = np.array(s.x3)
        if not(x is None):
            R = np.sqrt(x**2 + y**2)
            p = np.arctan2(y,x)
            #for i,pi in enumerate(p):
            #  if p[i] < 0.: p[i] = p[i]+twopi
        else:
            R = np.zeros(x.shape)
        return x,y,z,R,p

    def plot_bfield(self,lpts,lbf=None,shw=False,sav=False):
        #import matplotlib.tri as tri
        lab = ['bx','by','bz','br','bp']
        fig, axr = plt.subplots(3,2,figsize=(10.3,10.3))
        axr = axr.flatten()
        mst = 'o'

        #ax.set_rasterization_zorder(-10)
        for pts,bf in zip(lpts,lbf):
            xl,yl,zl,rl,pl = self.make_bfield(pts)
            bx,by,bz,_,_ = self.make_bfield(bf.field)
            b = [bx,by,bz]

            #Project B-field on radial and poloidal direction
            bt = by
            #br = 
            #bp = 
            R = np.sqrt(x**2 + y**2)
            p = np.arctan2(y,x)


            for i in range(3):
              axr[i].scatter(rl, zl, c=b[i],s=20.)
              #axr[i].plot(rl, zl)  
              axr[i].set_ylabel(lab[i])
        #axr[i].set_colorbar()  
        for ax in axr:
            ax.set_ylim((-0.7,0.7))
        if shw:
          plt.show(block=False)
        if sav:
          fig.savefig("poincare.pdf", dpi=800, bbox_inches="tight")
        return fig

    def make_poincare(self,s,phi=False,pr=[0.,twopi]):
        flag = True
        x = np.array(s.points.x1)
        y = np.array(s.points.x2)
        z = np.array(s.points.x3)
        R = np.zeros(x.shape)
        p = np.zeros(x.shape)
        #Doesn't seem to work to use not(y is None)
        try:
            tmp = y[0]
            tmp = x[0]
        except:
            flag = False
            if phi:
                return [],[],[],[],[]
            else:
                return [],[],[],[]
        #Doesn't seem to work to use not(y is None)
        if not((x is None) or (y is None)) and flag:
            R = np.sqrt(x**2 + y**2)
            if phi:
                p = np.arctan2(y,x)
                for i,pi in enumerate(p):
                  if p[i] < 0.: p[i] = p[i]+twopi
        if phi:
            idx = np.where((p >= pr[0]) * (p <= pr[1]))
            #print(pr,id,p[idx])
            x = np.array(x[idx])
            y = np.array(y[idx])
            z = np.array(z[idx])
            R = np.array(R[idx])
            p = np.array(p[idx])
            return [x,y,z,R,p]
        else:
            x = np.array(x)
            y = np.array(y)
            z = np.array(z)
            R = np.array(R)
            return [x,y,z,R]

    def make_poincare_phi(self,s):
        x = np.array(s.points.x1)
        y = np.array(s.points.x2)
        z = np.array(s.points.x3)
        if not(x is None or y is None):
            R = np.sqrt(x**2 + y**2)
        else:
            R = np.zeros(x.shape)
        return [x,y,z,R]

    def make_fluxsurface(self,x1,x2,x3,pr=[0.,360.],npp=90,steps=1.0E+2,step=1.0E-2,coord='cart'):
        if coord == 'cyl':
            x1 = np.array(x1);x2 = np.array(x2); x3=np.array(x3)*degtorad
            x1,x2,x3=geom.cylindertocartesian(x1,x2,np.ones(x1.shape)*x3)
        lphi=np.linspace(pr[0],pr[1],npp).tolist()
        self.set_traceinit('external',x1=x1,x2=x2,x3=x3)
        self.poincare_phi(lphi,step=step,steps=steps,lphi=True)
        res = self.trace()
        msh = self.mesh_poincare(res)
        return msh,res

    def make_separatrix(self,rsep=None,roff=1.E-3,zoff=0.,pr=[0.,360.],npp=90,steps=1.0E+2,step=1.0E-2):
        lphi=np.linspace(pr[0],pr[1],npp).tolist()
        if rsep is None:
            sep,rsep,psep = self.get_separatrix(0.,steps=1,single=True)
        self.set_traceinit('external',x1=[rsep-roff],x2=[0.],x3=[zoff])
        self.poincare_phi(lphi,step=step,steps=steps,lphi=True)
        sep = self.trace()
        msh = self.mesh_poincare(sep)
        return msh,sep,rsep

    def plot_fluxsurface(self,msh,color=None,opacity=1.0,fig=None):
        if fig is None:
            fig = mlab.figure()
        mlab.mesh(msh['x'],msh['y'],msh['z'],color=col,colormap='bone',opacity=opacity,fig=fig)
        return

    def plot_2d(self,lflx,bf=None,phi=None,\
                fig=None,ax=None,\
                color=None,mst='.',ms=4.0,grid=False,\
                shw=False,sav=False):
        fig = self.plot_poincare(lflx,phi=phi,\
                fig=fig,ax=ax,\
                color=color,mst=mst,ms=ms,grid=grid,\
                shw=shw,sav=sav)
        return fig
 
    def plot_poincare(self,lflx,phi=None,fig=None,ax=None,grid=False,\
                color=None,mst='.',ms=2.0,shw=False,sav=False):
        mlsty = colors.mlsty()
        csty  = colors.clrs()
        fig,axs,col = pu.setax(fig=fig)
        if ax is None: ax = axs[0]
        if color is None: color = 'k'
        #ax.set_rasterization_zorder(-10)

        #Restrict tor. boundaries
        if not(phi is None):
            phi = np.array(phi)*degtorad
        for i,item in enumerate(lflx):
            if not(item is None):
                for s in item.surfs:
                    if not(phi is None):
                        xl,yl,zl,rl,pl = self.make_poincare(s,phi=True,pr=phi)
                    else:
                        xl,yl,zl,rl = self.make_poincare(s)
                    ax.plot(rl, zl, ls='', c=color,\
                            marker=mst, ms=ms, mew=0.0)#, zorder = -20)

        if grid: ax.grid('--',color='gray',alpha=0.5)

        ax.set_aspect(1)
        ax.set_xlabel(r"$R$, m")
        ax.set_ylabel(r"$z$, m", labelpad=0)
        if shw:
          plt.show(block=False)
        #ax.axes.set_frame_on(1)
        #ax.set_xlabel("")
        #ax.set_ylabel("")
        #ax.xaxis.set_visible(1)
        #ax.yaxis.set_visible(1)
        if sav:
          fig.savefig("poincare.pdf", dpi=800, bbox_inches="tight")
        return fig

    def plot_clength(self,res,fig=None,ax=None,log=False,zr=None,cm=None,cb=True,\
                     opacity=1.,shw=False):
        fig,axs,col = pu.setax(fig=fig)
        if ax is None: ax = axs[0]

        clen = []
        for r in res:
          for c in r.connection:
            if not c is None:
                clen.append(c.length)
        R = utils.get_faces(self.baseR)
        z = utils.get_faces(self.basez)
        clen = np.array(clen).reshape(self.basez.shape[0],self.baseR.shape[0])
        if log:
            clen = np.log10(clen)
            clabel = 'log$_{10}$( L$_C$ [m] )'
        else:
            clabel = 'L$_C$ [m]'
        if not zr is None:
            clen = np.clip(clen,zr[0],zr[1])

        im = ax.pcolor(R,z,clen,cmap=cm,alpha=opacity)
        if cb:
          cb = fig.colorbar(im,ax=ax)
          cb.ax.set_ylabel(clabel)
        if shw:
            plt.show(block=False)
        return fig, im

    def plot_clengthlim(self,clen,fig=None,ax=None,log=False,zr=None,cm=None,cb=True,\
                     opacity=1.,shw=False):
        fig,axs,col = pu.setax(fig=fig)
        if ax is None: ax = axs[0]

        R = utils.get_faces(self.baseR)
        z = utils.get_faces(self.basez)
        clen = np.array(clen).reshape(self.basez.shape[0],self.baseR.shape[0])
        if log:
            clen = np.log10(clen)
            clabel = 'log$_{10}$( L$_C$ [m] )'
        else:
            clabel = 'L$_C$ [m]'
        if not zr is None:
            clen = np.clip(clen,zr[0],zr[1])

        im = ax.pcolor(R,z,clen,cmap=cm,alpha=opacity)
        if cb:
          cb = fig.colorbar(im,ax=ax)
          cb.ax.set_ylabel(clabel)
        if shw:
            plt.show(block=False)
        return fig, im

    def mplot_axis(self,config='EJM',fig=None,col=(1,0,0)):
        file = {'EJM':'axis_standard_case.dat',\
                'KJM':'axis_low_iota.dat',\
                'DBM':'axis_high_iota.dat',\
               }
        if fig is None:
            fig = mlab.figure()
        axis=np.loadtxt('flt/'+file[config])
        filament = self.client.types.PolygonFilament() # plasma axis
        filament.vertices = self.client.types.Points3D()
        filament.vertices.x1 = axis[::20,0]
        filament.vertices.x2 = axis[::20,1]
        filament.vertices.x3 = axis[::20,2]
        mlab.plot3d(filament.vertices.x1, \
                    filament.vertices.x2, \
                    filament.vertices.x3, \
                    tube_radius=1.E-2,tube_sides=4,color=col)
        mlab.show()
        return fig

    def mplot_fieldline(self,res,fig=None,color=(0,0,0),tube_radius=1.E-2,single=False):
        if fig is None:
            fig = mlab.figure()
        if single:
            l = res
            mlab.plot3d(l.vertices.x1, \
                            l.vertices.x2, \
                            l.vertices.x3, \
                            tube_radius=tube_radius,tube_sides=4,color=color,opacity=0.5,figure=fig)
        else:
            for r in res:
              for l in r.lines:
                if not l is None:
                    mlab.plot3d(l.vertices.x1, \
                                l.vertices.x2, \
                                l.vertices.x3, \
                            tube_radius=tube_radius,tube_sides=4,color=color,opacity=0.5,figure=fig)
        
        return fig

    def mplot_fluxsurface(self,res,fig=None,color=(0,0,0)):
        if fig is None:
            fig = mlab.figure()

        for i in range(len(res)):
          for j in range(0, len(res[i].lines)):
                mlab.plot3d(res[i].lines[j].vertices.x1, \
                            res[i].lines[j].vertices.x2, \
                            res[i].lines[j].vertices.x3, \
                            tube_radius=1.E-1,tube_sides=4,color=color,figure=fig)
        return fig

    def mplot_plane(self,p,n,fig=None):
        import geom
        if fig is None:
            fig = mlab.figure()
        xp = np.linspace(-6.5,6.5,10.)
        yp = np.linspace(-6.5,6.5,10.)
        x,y = np.meshgrid(xp,yp)
        x = x.T
        y = y.T
        z = p[2]-(n[0]*(x-p[0])+n[1]*(y-p[1]))/n[2]
        pt = geom.make_vec(p,n,1.)
        #mlab.plot3d(pt[:,0],pt[:,1],pt[:,2],tube_radius=0.1,tube_sides=4,color=(1,0,0),figure=fig)
        mlab.surf(x,y,z,colormap='bone',opacity=0.1,figure=fig)
        return fig

    def mplot_poincare(self,res,fig=None):
        if fig is None:
            fig = mlab.figure()
        for item in res:
            for s in item.surfs:
                x = np.array(s.points.x1)
                y = np.array(s.points.x2)
                z = np.array(s.points.x3)
                mlab.points3d(x,y,z,mode='2dvertex',scale_factor=1.E-3,color=(1,0,0),figure=fig)
        return fig

    def create_fluxsurface_phi(self,fs,axis,dx=0.1,dl=0.04,debug=True):
        ms = 1
        mew = 0.5
        import matplotlib.pyplot as plt
        lx,ly,lz,lr = self.make_poincare(fs.surfs[0])

        #Use axis to get the poloidal angle
        pts = axis.points
        ax,ay,az = pts.x1,pts.x2,pts.x3
        ar,az,ap = geom.xyztorzp(ax,ay,az)

        nl = lz.shape[0]
        theta = []
        ref = np.array([1,0])
        v = np.array([lr-ar,lz-az])
        for i in range(nl):
            tmp = np.arcsin(np.dot(ref,geom.normvec(v[:,i])))
            if lz[i]-az < 0.: tmp = (np.pi -tmp)
            theta.append(tmp) 
        theta = np.array(theta)

        #Sort along poloidal angle
        idx = np.argsort(theta)
        lrs = lr[idx]
        lzs = lz[idx]

        #Make theta to conform to [0,2pi]
        theta = theta - np.min(theta)

        lin = np.arange(nl)

        if debug:
            plt.plot(ar,az,'+',ms=15,mew=3)
            plt.scatter(lr,lz,c=theta,marker='+')

            plt.colorbar()
            plt.figure()
            plt.plot(lin,lrs)
            plt.plot(lin,lzs)
            plt.plot(lin,theta)
            plt.plot(lin,theta[idx])
            plt.axhline(az)

        #Make spline fits to the R and z-coordinate
        import scipy.interpolate as spi
        yerr = np.ones(nl)*0.1
        splr = spi.splrep(lin,lrs,k=3,w=1./yerr,s=lrs.shape[0]*0.0001)#t=knt)#,s=1.)
        splz = spi.splrep(lin,lzs,k=3,w=1./yerr,s=lzs.shape[0]*0.0001)#t=knt)#,s=1.)

        #Evaluate Splines
        xfit = np.arange(0,nl,dx)
        rfit  = spi.splev(xfit,(splr[0],splr[1],3),der=0)
        zfit  = spi.splev(xfit,(splz[0],splz[1],3),der=0)

        #Construnct evenly spaced points on FS
        nx = xfit.shape[0]
        tmp = [xfit[0]]
        i = 0
        while i < nx:
            j = 0
            d = 0
            while j+i < nx and d < dl:
                j = j+1
                if j+i >= nx: break
                d = np.sqrt((rfit[i]-rfit[i+j])**2+(zfit[i]-zfit[i+j])**2)
            i = i+j
            if i < nx:
                tmp.append(xfit[i])

        #Evaluate splines on FS-points
        xfit  = tmp
        xfit.append(xfit[0])
        rfit  = spi.splev(xfit,(splr[0],splr[1],3),der=0)
        zfit  = spi.splev(xfit,(splz[0],splz[1],3),der=0)
        surf = [rfit,zfit,xfit]
        if debug:
          plt.figure()
          plt.plot(lr, lz, ".", ms=ms*5., markeredgewidth=mew, zorder = -20, 
                   color='k',label='flt')#,matplotlib.cm.jet(0.95))
          plt.plot(rfit,zfit,'g-+',lw=2.,label='spl')
          for i in range(rfit.shape[0]):
            plt.text(rfit[i],zfit[i],str('%d' %i))
          plt.legend()
        return [surf,splr,splz]

    def create_fs_phi(self,fs,dx=0.1,dl=0.04,debug=False):
        ms = 1
        mew = 0.5
        import matplotlib.pyplot as plt
        lx,ly,lz,lr = self.make_poincare(fs.surfs[0])

        axis = get_axis_phi(self,phi=0.,step=0.01)


        #Make spline fits to the R and z-coordinate
        import scipy.interpolate as spi
        nf = lr.shape[0]
        yerr = np.ones(nf)*0.1
        splr = spi.splrep(range(nf),lr,k=3,w=1./yerr,s=lr.shape[0]*0.0001)#t=knt)#,s=1.)
        splz = spi.splrep(range(nf),lz,k=3,w=1./yerr,s=lz.shape[0]*0.0001)#t=knt)#,s=1.)

        #Search for end of periodical structure
        tmpz = lz[0]-0.05
        tmpr = lr[0]-0.05
        dlz = np.gradient(lz)
        dlr = np.gradient(lr)
        i = lr.shape[0]-1
        #Determine end of first period (to limit spline)
        #for i in range(lr.shape[0]):
        #    dist = np.sqrt((lz[i] - lz[0])**2+(lr[i]-lr[0])**2)
        #    if i > 10 and dlz[i]*dlz[0] > 0 and lz[i] > tmpz and dlr[i]*dlr[0] > 0 and lr[i] > tmpr and dist < 0.01  : break
        if debug: print(i,lr[0]-lr.mean(),lr[i]-lr.mean())

        #Evaluate Splines
        idx = i
        #dx = 0.1
        #dl = 0.05
        xfit = np.arange(0,idx,dx)
        rfit  = spi.splev(xfit,(splr[0],splr[1],3),der=0)
        zfit  = spi.splev(xfit,(splz[0],splz[1],3),der=0)

        #Determine the minimum in the periodic function (clusters)
        ridx = np.argwhere(rfit < min(rfit)*1.001)
        #if debug: print(rmax,xfit[ridx],rfit[ridx])
        from scipy.cluster.vq import kmeans
        c, _ = kmeans(np.array(ridx,dtype=float),2)
        c = np.array(c,dtype=int)
        print(c)

        idx1 = int(xfit[min(c)])
        idx2 = int(xfit[max(c)])
        print(idx1,idx2)

        if debug:
            fig = plt.figure()
            ax = fig.gca()
            tax = ax.twinx()
            ax.plot(range(idx),lr[0:idx],'b+',label='lr')
            ax.plot(xfit,rfit,'b-',label='splr')
            ax.plot(xfit[ridx],rfit[ridx],'ko',label='cluster region')
            ax.plot([idx1,idx2],[lr[idx1],lr[idx2]],'r+',label='cluster')
            ax.legend(loc=1)

        xfit = np.arange(idx1,idx2,dx)
        rfit  = spi.splev(xfit,(splr[0],splr[1],3),der=0)
        zfit  = spi.splev(xfit,(splz[0],splz[1],3),der=0)

        if debug:
            fig = plt.figure()
            ax = fig.gca()
            tax = ax.twinx()
            ax.plot(range(idx),lr[0:idx],'b+',label='lr')
            ax.plot(xfit,rfit,'b-',label='splr')
            tax.plot(range(idx),lz[0:idx],'r+',label='lz')
            tax.plot(xfit,zfit,'r-',label='splz')
            ax.legend(loc=1)
            tax.legend(loc=2)
            #ax.plot(xfit[rmin],rfit[rmin],'o',ms=10)

        #Construnct evenly spaced points on FS
        nx = xfit.shape[0]
        tmp = [xfit[0]]
        i = 0
        while i < nx:
            j = 0
            d = 0
            while j+i < nx and d < dl:
                j = j+1
                if j+i >= nx: break
                d = np.sqrt((rfit[i]-rfit[i+j])**2+(zfit[i]-zfit[i+j])**2)
            i = i+j
            if i < nx:
                tmp.append(xfit[i])

        #Evaluate splines on FS-points
        xfit  = tmp
        xfit.append(xfit[0])
        rfit  = spi.splev(xfit,(splr[0],splr[1],3),der=0)
        zfit  = spi.splev(xfit,(splz[0],splz[1],3),der=0)
        surf = [rfit,zfit,xfit]
        if debug:
          plt.figure()
          plt.plot(lr, lz, ".", ms=ms*5., markeredgewidth=mew, zorder = -20, 
                   color='k',label='flt')#,matplotlib.cm.jet(0.95))
          plt.plot(rfit,zfit,'g-+',lw=2.,label='spl')
          for i in range(rfit.shape[0]):
            plt.text(rfit[i],zfit[i],str('%d' %i))
          plt.legend()
        return [surf,splr,splz]

    def pt2line(self,pts):
        x1, x2, x3 = [],[],[]
        for pt in pts:
            x1.append(pt.x1)
            x2.append(pt.x2)
            x3.append(pt.x3)
        return np.array(x1).squeeze(),np.array(x2).squeeze(),np.array(x3).squeeze()

    def line2pts(self,x,y,z):
        pts = data.data()
        pts.x1 = x
        pts.x2 = y
        pts.x3 = z
        return pts

    def trace2line(self,res,i=0,s=None):
        x,y,z,R,p = [],[],[],[],[]
        if s is None:
            lsurfs = res[i].surfs
        else:
            lsurfs = [res[i].surfs[s]]
        for s in lsurfs:
            tx,ty,tz,tR,tp = self.make_poincare(s,phi=True)
            x.append(tx)
            y.append(ty)
            z.append(tz)
            R.append(tR)
            p.append(tp)
        x = np.array(x)
        y = np.array(y)
        z = np.array(z)
        R = np.array(R)
        p = np.array(p)
        return x,y,z,R,p


def save( name ):
    plt.savefig( name, transparent=True )
    print( name+" saved" )
    return 0

