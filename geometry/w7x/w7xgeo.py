# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 13:28:21 2018

@author: flr
"""
import numpy as np
import urllib
import json
from mayavi import mlab

import utils.geom as geom

class W7X_data(object):
    def __init__(self, coils=[], vessel=[320], components=[ 165 ], fn='w7xgeo.pik'):
    	self.status = False
    	if os.path.isfile(fn):
    		try:
    		  self.load(fn)
    		  return
    		except Exception:
    			print('Error loading local data')
    			return
    	else:
	        if len(coils)!=0:
	            cx=asarray([])
	            cy=asarray([])        
	            cz=asarray([])
	            for i in range( len(coils)):
	                x, y, z = coil_data(num=coils[i])
	                cx = append(cx, x)
	                cy = append(cy, y)
	                cz = append(cz, z)

	            self.cx = cx
	            self.cy = cy        
	            self.cz = cz                

	        if len(vessel)!=0:
	            vx, vy, vz, vt = component_data(num=vessel[0])
	            for i in range( len(vessel)-1):
	                a=len(vx)
	                x, y, z, t = component_data(num=vessel[i+1])
	                t+=a
	                vx = append(vx, x)
	                vy = append(vy, y)
	                vz = append(vz, z)
	                vt = append(vt, t, axis=0)            

	            self.vx = vx
	            self.vy = vy        
	            self.vz = vz                
	            self.vt = vt                        

	        if len(components)!=0:
	            cpx, cpy, cpz, cpt = component_data(num=components[0])
	            for i in range( len(components)-1):
	                a=len(cpx)                
	                x, y, z, t = component_data(num=components[i+1])
	                t+=a
	                cpx = append(cpx, x)
	                cpy = append(cpy, y)
	                cpz = append(cpz, z)
	                cpt = append(cpt, t, axis=0)            

	            self.cpx = cpx
	            self.cpy = cpy        
	            self.cpz = cpz                
	            self.cpt = cpt
	            self.status = True
	            self.save(fn)
        	return

    def save(self,fn='w7xgeo.pik'):                     
        if not self.status:
            raise Exception('myObject not loaded.')
        pickle.dump(self.__dict__, open(fn, 'wb'), pickle.HIGHEST_PROTOCOL)
        return

    def load(self, fn='w7xgeo.pik'):
        self.__dict__ = pickle.load(open(fn,'rb'))
        return

def diagnostics():
    lkeys = ['']
    dat   = {}
    return dat

def coil_data(num=341,full=False):
    u"""
    Read the "num"th coils data from the web server and returns four arrays that will be ploted using mayavi "triangular_mesh(dx, dy, dz, triangles)"
    See http://webservices.ipp-hgw.mpg.de/docs/componentsDbRest.html for the list of components.
    """
    link = 'http://esb.ipp-hgw.mpg.de:8280/services/CoilsDBRest/coil/'+str(num)+'/data'
    res = urllib.request.urlopen(link+"/_signal.json")
    dat = json.loads(res.read().decode('utf-8'))
    res.close()
    dat = dat['polylineFilament']
    x, y, z = dat['vertices']['x1'],dat['vertices']['x2'],dat['vertices']['x3']
    x, y, z = np.array(x), np.array(y), np.array(z)
    if full:
        return dat
    else:
        return x, y, z

def assembly_lst(num=341):
    link = 'http://esb.ipp-hgw.mpg.de:8280/services/ComponentsDbRest/assembly/'+str(num)+'/data'
    res = urllib.request.urlopen(link+"/_signal.json")
    dat = json.loads(res.read().decode('utf-8'))
    res.close()
    dat = dat['components']
    return dat

def component_data(num=341,full=False):
    u"""
    Read the "num"th coils data from the web server and returns four arrays that will be ploted using mayavi "triangular_mesh(dx, dy, dz, triangles)"
    See http://webservices.ipp-hgw.mpg.de/docs/componentsDbRest.html for the list of components.
    """
    link = 'http://esb.ipp-hgw.mpg.de:8280/services/ComponentsDbRest/component/'+str(num)+'/data'
    res = urllib.request.urlopen(link+"/_signal.json")
    dat = json.loads(res.read().decode('utf-8'))
    res.close()
    dat = dat['surfaceMesh']
    x, y, z = dat['nodes']['x1'],dat['nodes']['x2'],dat['nodes']['x3']
    t = (np.array(dat['polygons'])-1).reshape(-1, 3)
    if full:
        return dat
    else:
        return x,y,z,t

def limit_range(x,y,z,t,Rr=None,zr=None,pr=None,):
    degtorad = np.pi/180.
    R,z,p = geom.xyztorzp(x,y,z)
    #Correct for [0,2pi]
    if p.min() < 0.: p = p+2.*np.pi
    print(R.min(),R.max())
    print(z.min(),z.max())
    print(p.min()/degtorad,p.max()/degtorad)
    nx = len(x)
    idx = np.arange(nx)
    if not Rr is None:
      idx = idx[np.array(R[idx] > Rr[0]).nonzero()]
      idx = idx[np.array(R[idx] < Rr[1]).nonzero()]
    print(idx.size)
    if not zr is None:
      idx = idx[np.array(z[idx] > zr[0]).nonzero()]
      idx = idx[np.array(z[idx] < zr[1]).nonzero()]
    print(idx.size)
    if not pr is None:
      idx = idx[np.array(p[idx] > pr[0]*degtorad).nonzero()]
      idx = idx[np.array(p[idx] < pr[1]*degtorad).nonzero()]
    print(idx.size)

    ntri = t.shape[0]
    tri = []
    for i in range(ntri):
        for j in range(3):
            if t[i,j] in idx: 
                tri.append(t[i,:])
                continue
    return np.array(tri)

def plot_triangles(x,y,z,t,color=None,opacity=1.):
    fig = mlab.figure()
    mlab.triangular_mesh(x1, y1, z1, t1 , color=col,opacity=opacity,figure=fig)
    return fig

def plot_components(lid=[164],color=None,opacity=0.1,fig=None):
    if fig is None:
        fig = mlab.figure()
    if color == None:
        col = (0,0,1)
    else:
        col = color

    for id in lid:
        x,y,z,t = component_data(id)
        mlab.triangular_mesh(x, y, z, t , color=col,opacity=opacity,figure=fig)
