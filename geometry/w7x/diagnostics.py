import numpy as np
import utils.data as data
import utils.plot as pu
import utils.geom as geom

dat = data.data()

#----------------------------
# Classes
#----------------------------
class DIAGBASE(data.data):
  def __init__(self):
    self.label = ''
    return

  def mapto(self,P,typ='cartesian'):
    x,y,z = pu.pointstoline([P])
    if typ == 'cylinder':
      x,y,z = geom.cartesiantocylinder(x,y,z)
    elif typ == 'cartesian':
      x,y,z = geom.cylindertocartesian(x,y,z)
    else:
      raise('Option not implemented')
    return x,y,z

class LINE_OF_SIGHT(DIAGBASE):
  def __init__(self,P0,P1,label='',geometry='cartesian'):
    super().__init__()
    self.label = label
    self.P0 = data.data()
    self.P0.init = np.array(P0)
    self.P1 = data.data()
    self.P1.init = np.array(P1)
    self.geometry = geometry
    self.F = None
    self.label = None
    self.info = None
    self.set_geometry()
    return
    
  def set_geometry(self):
    for item in [self.P0, self.P1]:
      if self.geometry == 'cylinder':
        item.R,item.Z,item.P = item.init[0],item.init[1],item.init[2]
        x,y,z = self.mapto(item.init,'cartesian')
        item.x,item.y,item.z = x[0],y[0],z[0]
      else:
        item.x,item.y,item.z = item.init[0],item.init[1],item.init[2]
        R,Z,P = self.mapto(item.init,'cylinder')
        item.R,item.Z,item.P = R[0],Z[0],P[0]
    return

  def get_vector(self,norm=False,geometry='cartesian'):
    if geometry == 'cartesian':
        v = [self.P1.x-self.P0.x,self.P1.y-self.P0.y,self.P1.z-self.P0.z]
        vl = geom.lenvec(v)
        if norm:
          v = v/vl
    elif geometry == 'cylinder':
        v = [self.P1.R-self.P0.R,self.P1.Z-self.P0.Z,self.P1.P-self.P0.P]
        vl = geom.lenvec(v[0:2])
        if norm:
          v[0:2] = v[0:2]/vl
    else:
        raise('Option not implemented')
    return [v,vl]

  def get_points(self,npoints=100,geometry='cartesian'):
    if geometry == 'cartesian':
      x = np.linspace(self.P0.x,self.P1.x,npoints,endpoint=True)
      y = np.linspace(self.P0.y,self.P1.y,npoints,endpoint=True)
      z = np.linspace(self.P0.z,self.P1.z,npoints,endpoint=True)
      return [x,y,z]
    elif geometry == 'cylinder':
      r = np.linspace(self.P0.R,self.P1.R,npoints,endpoint=True)
      z = np.linspace(self.P0.Z,self.P1.Z,npoints,endpoint=True)
      p = np.linspace(self.P0.P,self.P1.P,npoints,endpoint=True)
      return [r,z,p]
    

  def set_label(self,label):
    self.label = label
    return 
    
  def set_info(self,info):
    self.info = info
    return

  def set_FNumber(self,F):
    self.FNumber = F
    return

  def get_toroidal_span(self):
    return np.array([self.P0.P,self.P1.P])

  def get_emc3_geometry(self):
    return np.array([[self.P0.R,self.P0.Z],self.get_vector(geometry='cylinder')[0][0:2],self.get_toroidal_span()])
  
class LOS_FAN(DIAGBASE):
  def __init__(self,label='',los=None):
    super().__init__()
    self.label = label
    if not(los is None):
      self.los = los
    else:
      self.los = []
    self.nlos = len(self.los)
    return
  
  def add_los(self,los):
    self.los.append(los)
    self.nlos = len(self.los)
    return

  def rm_los(self,idx):
    del self.los[idx]
    self.nlos = len(self.los)

  def get_emc3_geometry(self):
    lP, lv, lphi = [],[],[]
    for il in range(self.nlos):
      P0,v,phi = self.los[il].get_emc3_geometry()
      dphi = phi[1]-phi[0]
      lP.append(P0); lv.append(v); lphi.append(phi)
    return [np.array(lP),np.array(lv),np.array(lphi)]

  def get_points(self,npoints=2,geometry='cartesian'):
    los = []
    for il in range(self.nlos):
      los.append(self.los[il].get_points(npoints,geometry=geometry))
    return los

class CAMERA(DIAGBASE):
  def __init__(self,pinhole,detector,label=''):
    super().__init__()
    self.label=label
    self.npin = len(pinhole)
    self.pin = np.array(pinhole)
    self.ndet = len(detector)
    self.det = np.array(detector)
    self.cpts = [np.mean(self.det,0),np.mean(self.pin,0)]
    self.clos = LINE_OF_SIGHT(self.cpts[0],self.cpts[1])
    self.los = []
    self.nlos = 0
    self.fov = []
    self.nfov = 0
    return

  def make_los(self):
    for i in range(self.ndet):
      for j in range(self.npin):
        vlos = (self.pin[j,:]-self.det[i,:])
        self.los.append(LINE_OF_SIGHT(self.det[i,:],self.det[i,:]+vlos))
    return

  def make_fov(self,plane):
    #Intersect central line of sight with plane 
    #Project limiting los to it 
    #Construct polygon hull
    return

  def make_central_fov(self,distance):
    #Intersect central line with perpendicular plane at distance d
    #Project limiting los to it 
    #Construct polygon hull
    return

  
class FIELD_OF_VIEW(DIAGBASE):
  def __init__(self,los,label=''):
    super().__init__()
    self.label=label
    self.los = los
    self.nlos = len(los)
    return


class VOLUME(DIAGBASE):
  def __init__(self,los,label=''):
    super().__init__()
    self.label = label
    self.los = los
    self.nlos = len(los)
    return

class POINT(DIAGBASE):
  def __init__(self,P,label='',geom='cartesian'):
    super().__init__()
    self.label = label
    self.init = P
    self.geometry = geom
    self.set_geometry()
    return

  def set_geometry(self):
    if self.geometry == 'cylinder':
      self.R,self.Z,self.P = self.init[0],self.init[1],self.init[2]
      x,y,z = self.mapto(self.init,'cartesian')
      self.x,self.y,self.z = x[0],y[0],z[0]
    else:
      self.x,self.y,self.z = self.init[0],self.init[1],self.init[2]
      R,Z,P = self.mapto(self.init,'cylinder')
      self.R,self.Z,self.P = R[0],Z[0],P[0]
    return

class POINT_LIST(DIAGBASE):
  def __init__(self,label='',pts=None):
    super().__init__()
    self.label = label
    if not(pts is None):
      self.pts = pts
    else:
      self.pts = []
    self.npts = len(self.pts)
    return
  
  def add_point(self,point):
    self.pts.append(point)
    self.npts = len(self.pts)
    return
  
  def rm_point(self,point,idx):
    del self.pts[idx]
    self.npts = len(self.pts)
    return

  def get_points(self,geometry='cartesian'):
    x,y,z = [],[],[]
    if geometry == 'cartesian':
      for pt in self.pts:
        x.append(pt.x)
        y.append(pt.y)
        z.append(pt.z)
    elif geometry == 'cylinder':
      for pt in self.pts:
        x.append(pt.R)
        y.append(pt.Z)
        z.append(pt.P)
    x,y,z = np.array(x),np.array(y),np.array(z)
    return [x,y,z]

class DIAGNOSTIC(DIAGBASE):
  def __init__(self,label=''):
    super().__init__()
    self.label = label
    self.nlos = 0
    self.los = []
    self.nfan = 0
    self.fan = []
    self.nfov = 0
    self.fov = []
    self.nvol = 0
    self.vol = []
    self.npts = 0
    self.pts = []
    self.nptl = 0
    self.ptl = []
    self.ncam = 0
    self.cam = []
    return
        
  def add_los(self,los):
    self.los.append(los)
    self.nlos = len(self.los)
    return

  def add_losfan(self,fan):
    self.fan.append(fan)
    self.nfan = len(self.fan)
    return

  def add_fov(self,fov):
    self.fov.append(fov)
    self.nfov = len(self.fov)
    return

  def add_vol(self,vol):
    self.vol.append(vol)
    self.nvol = len(self.vol)
    return 

  def add_point(self,point):
    self.pts.append(point)
    self.npts = len(self.pts)
    return

  def add_pointlist(self,pointlist):
    self.ptl.append(pointlist)
    self.nptl = len(self.ptl)
    return

  def add_camera(self,camera):
    self.cam.append(camera)
    self.npts = len(self.cam)
    return

  def get_label(self,label,typ='fan'):
    for item in self.__dict__[typ]:
      if item.label == label:
        return item
    return None

#----------------------------
# Functions
#----------------------------
def get_toroidal_span(P0,P1):
  return np.array([geom.get_phi(P0[0],P0[1],P0[2]),geom.get_phi(P1[0],P1[1],P1[2])])

#----------------------------
# Diagnostic data
#----------------------------
hexos = DIAGNOSTIC()
los0 = LINE_OF_SIGHT([-2.730e+00, 8.404e+00,  1.531e-01],[-1.930,5.940,0.0447])
los1 = LINE_OF_SIGHT([-2.883e+00, 8.874e+00,  1.738e-01],[-1.930,5.940,0.0447])
los2 = LINE_OF_SIGHT([-2.887e+00, 8.886e+00, -1.744e-01],[-1.930,5.940,-0.0447])
hexos.add_los(los0)
hexos.add_los(los1)
hexos.add_los(los2)

#----------------------------
#https://wikis.ipp-hgw.mpg.de/PhysicsW7X/images/8/8b/QMJ_interferometer.pdf
QMJ = DIAGNOSTIC() 
P0 = [-0.914,-0.271,1.604]
P1 = [-7.729,1.924,-2.514]
P1 = [P0[i]+P1[i] for i in range(3)]
phi = 170.5
QMJ.add_los(LINE_OF_SIGHT(P0,P1))

#----------------------------
MPM = DIAGNOSTIC()
MPM.add_los(LINE_OF_SIGHT([-5.896880366706059, -2.240014718870528, -0.157], [-5.562212774556286, -2.1128864263284153, -0.167]))

#----------------------------
ABES = DIAGNOSTIC()
ABES.add_los(LINE_OF_SIGHT([1.9317329, 6.001306, 0.0], [1.8607863, 5.8030896, 0.0]))

#----------------------------
#Thomson
QTB = DIAGNOSTIC()
QTB.add_los(LINE_OF_SIGHT([-4.34076225, 0.5910586, 0.48986739], [-6.19266899, 1.04252709, -0.11562221]))

AL=  [[-5.19259175,  0.791532  ,  0.20750925],
           [-5.23324075,  0.80014775,  0.19289725],
           [-5.27748325,  0.81014125,  0.1805355 ],
           [-5.32167525,  0.822874  ,  0.16412125],
           [-5.37028   ,  0.83519625,  0.14889275],
           [-5.41822425,  0.8455445 ,  0.131646  ],
           [-5.46902225,  0.8597895 ,  0.11507925],
           [-5.5220315 ,  0.872732  ,  0.09915125],
           [-5.573763  ,  0.88931425,  0.09042575],
           [-5.62765825,  0.9036715 ,  0.07454225],
           [-5.68935025,  0.9158305 ,  0.04825275],
           [-5.75002875,  0.930164  ,  0.030711  ],
           [-5.811685  ,  0.948139  ,  0.0116665 ],
           [-5.87558675,  0.964028  , -0.00666175],
           [-5.94736375,  0.97964575, -0.03260375],
           [-6.00927625,  0.99689725, -0.05071625],
           [-5.2928085 ,  0.81456   ,  0.17557175],
           [-5.27889075,  0.812279  ,  0.18079   ],
           [-5.263079  ,  0.809724  ,  0.18671275],
           [-5.09108825,  0.767295  ,  0.24391075],
           [-5.07257525,  0.7635655 ,  0.2505135 ],
           [-5.0541875 ,  0.758037  ,  0.2573195 ],
           [-4.98229288,  0.74012213,  0.28084588],
           [-4.9748026 ,  0.73806838,  0.28365263],
           [-4.8936736 ,  0.71821888,  0.306985  ],
           [-4.8866255 ,  0.71661963,  0.30949967],
           [-4.8571678 ,  0.70998075,  0.320003  ],
           [-4.8505299 ,  0.70843892,  0.32237633],
           [-4.8321953 ,  0.70858225,  0.33099625],
           [-4.82562133,  0.70651542,  0.33300008],
           [-4.775902  ,  0.69174825,  0.34905075],
           [-4.7693877 ,  0.68977908,  0.35102392],
           [-4.7455106 ,  0.68563367,  0.35948108],
           [-4.73646042,  0.68343433,  0.36215992],
           [-4.7114528 ,  0.67821367,  0.37430775],
           [-4.70413892,  0.676906  ,  0.3760445 ],
           [-4.6684317 ,  0.668367  ,  0.388489  ],
           [-4.658603  ,  0.66734367,  0.39077642],
           [-4.637281  ,  0.66528488,  0.399836  ],
           [-4.625158  ,  0.66157563,  0.402001  ],
           [-4.61337988,  0.65928675,  0.40403988],
           [-4.6014416 ,  0.65561625,  0.40617263]]
for item in AL:
  QTB.add_point(POINT(item))

#----------------------------
QSS = DIAGNOSTIC()
AEF30 = LOS_FAN('AEF30')
P0 = np.array([-4564, 4472, 356])/1000.
AL =  [[-3.4519    ,  4.0925    , -1.1609    ],
       [-3.44321355,  4.06895467, -1.14838175],
       [-3.43476654,  4.04549544, -1.13554213],
       [-3.42656076,  4.02212732, -1.12238386],
       [-3.41859798,  3.99885531, -1.10890976],
       [-3.4108799 ,  3.97568438, -1.09512271],
       [-3.40340815,  3.95261948, -1.08102565],
       [-3.39618435,  3.92966554, -1.0666216 ],
       [-3.38921003,  3.90682745, -1.05191363],
       [-3.38248668,  3.88411011, -1.03690488],
       [-3.37601574,  3.86151835, -1.02159857],
       [-3.36979859,  3.83905702, -1.00599796],
       [-3.36383656,  3.8167309 , -0.99010638],
       [-3.35813092,  3.79454477, -0.97392724],
       [-3.3526829 ,  3.77250336, -0.95746397],
       [-3.34749365,  3.75061139, -0.94072011],
       [-3.34256429,  3.72887353, -0.92369923],
       [-3.33789586,  3.70729442, -0.90640496],
       [-3.33348937,  3.68587869, -0.88884101],
       [-3.32934575,  3.66463089, -0.87101112],
       [-3.3254659 ,  3.64355557, -0.85291909],
       [-3.32185064,  3.62265723, -0.83456881],
       [-3.31850073,  3.60194034, -0.81596418],
       [-3.31541691,  3.58140932, -0.79710918],
       [-3.31259983,  3.56106856, -0.77800784],
       [-3.31005008,  3.54092241, -0.75866424],
       [-3.30776821,  3.52097516, -0.73908252]]
for item in AL:
  AEF30.add_los(LINE_OF_SIGHT(P0,item))
QSS.add_losfan(AEF30)

AEI30 = LOS_FAN('AEI30')
P0 = np.array([-2479, 1760, -620])/1000.
AL =  [[-3.7522    ,  4.4192    , -1.0712    ],
       [-3.75284236,  4.41992764, -1.06505608],
       [-3.75347919,  4.42064371, -1.05891022],
       [-3.75411047,  4.4213482 , -1.05276245],
       [-3.75473621,  4.42204113, -1.04661281],
       [-3.75535639,  4.42272247, -1.0404613 ],
       [-3.75597103,  4.42339224, -1.03430797],
       [-3.75658011,  4.42405042, -1.02815284],
       [-3.75718364,  4.42469701, -1.02199593],
       [-3.75778161,  4.42533202, -1.01583727],
       [-3.75837401,  4.42595543, -1.00967689],
       [-3.75896085,  4.42656725, -1.00351482],
       [-3.75954213,  4.42716748, -0.99735107],
       [-3.76011783,  4.4277561 , -0.99118569],
       [-3.76068797,  4.42833312, -0.98501869],
       [-3.76125253,  4.42889853, -0.97885011],
       [-3.76181151,  4.42945234, -0.97267996],
       [-3.76236491,  4.42999454, -0.96650828],
       [-3.76291274,  4.43052512, -0.9603351 ],
       [-3.76345497,  4.43104409, -0.95416043],
       [-3.76399163,  4.43155145, -0.94798431],
       [-3.76452269,  4.43204718, -0.94180676],
       [-3.76504816,  4.4325313 , -0.93562782],
       [-3.76556804,  4.43300379, -0.9294475 ],
       [-3.76608232,  4.43346465, -0.92326583],
       [-3.76659101,  4.43391389, -0.91708285],
       [-3.7670941 ,  4.4343515 , -0.91089857]]
for item in AL:
  AEI30.add_los(LINE_OF_SIGHT(P0,item))
QSS.add_losfan(AEI30)
#HEB intersection points
HEBAEI30 = POINT_LIST('HEB-AEI30')
AP = np.array([
    [-3.5403017894,3.97650607057,-0.993995223482],
    [-3.54084972951,3.97718784752,-0.98897687935],
    [-3.54139766961,3.97786962448,-0.983958535218],
    [-3.54194560971,3.97855140143,-0.978940191085],
    [-3.54249354981,3.97923317839,-0.973921846953],
    [-3.54304148991,3.97991495534,-0.968903502821],
    [-3.54358943001,3.9805967323,-0.963885158689],
    [-3.54413737012,3.98127850926,-0.958866814557],
    [-3.54468531022,3.98196028621,-0.953848470425],
    [-3.54523325032,3.98264206317,-0.948830126293],
    [-3.54578119042,3.98332384012,-0.943811782161],
    [-3.54632913052,3.98400561708,-0.938793438029],
    [-3.54687707063,3.98468739403,-0.933775093897],
    [-3.54742501073,3.98536917099,-0.928756749765],
    [-3.54797295083,3.98605094794,-0.923738405633],
    [-3.54852089093,3.9867327249,-0.918720061501],
    [-3.54906883103,3.98741450185,-0.913701717369],
    [-3.54961677114,3.98809627881,-0.908683373237],
    [-3.55016471124,3.98877805577,-0.903665029105],
    [-3.55071265134,3.98945983272,-0.898646684973],
    [-3.55126059144,3.99014160968,-0.893628340841],
    [-3.55180853154,3.99082338663,-0.888609996709],
    [-3.55235647165,3.99150516359,-0.883591652576],
    [-3.55290441175,3.99218694054,-0.878573308444],
    [-3.55345235185,3.9928687175,-0.873554964312],
    [-3.55400029195,3.99355049445,-0.86853662018],
    [-3.55454823205,3.99423227141,-0.863518276048]
    ])
for item in AP:
  HEBAEI30.add_point(POINT(item))
QSS.add_pointlist(HEBAEI30)

AEI51 = LOS_FAN('AEI51')
P0 = np.array([ 908, 2902,  621])/1000.
AL =  [[ 3.044     , -6.0956    ,  1.071     ],
       [ 3.04439532, -6.09581208,  1.06484072],
       [ 3.04478969, -6.09602016,  1.05868124],
       [ 3.04518311, -6.09622423,  1.05252156],
       [ 3.04557557, -6.0964243 ,  1.0463617 ],
       [ 3.04596709, -6.09662036,  1.04020164],
       [ 3.04635766, -6.09681243,  1.0340414 ],
       [ 3.04674727, -6.09700049,  1.02788097],
       [ 3.04713593, -6.09718454,  1.02172037],
       [ 3.04752364, -6.09736459,  1.01555958],
       [ 3.0479104 , -6.09754064,  1.00939862],
       [ 3.04829621, -6.09771268,  1.00323749],
       [ 3.04868106, -6.09788072,  0.99707618],
       [ 3.04906497, -6.09804476,  0.99091471],
       [ 3.04944791, -6.09820479,  0.98475308],
       [ 3.04982991, -6.09836082,  0.97859128],
       [ 3.05021096, -6.09851284,  0.97242932],
       [ 3.05059105, -6.09866086,  0.96626721],
       [ 3.05097018, -6.09880487,  0.96010494],
       [ 3.05134837, -6.09894488,  0.95394253],
       [ 3.0517256 , -6.09908089,  0.94777996],
       [ 3.05210188, -6.09921289,  0.94161725],
       [ 3.0524772 , -6.09934089,  0.9354544 ],
       [ 3.05285157, -6.09946488,  0.9292914 ],
       [ 3.05322498, -6.09958486,  0.92312827],
       [ 3.05359744, -6.09970084,  0.91696501],
       [ 3.05396895, -6.09981282,  0.91080161]]
for item in AL:
  AEI51.add_los(LINE_OF_SIGHT(P0,item))
QSS.add_losfan(AEI51)
#HEB intersection points
HEBAEI51 = POINT_LIST('HEB-AEI51')
AP = np.array([
    [2.71343714678,-4.63959489796,1.00023206458],
    [2.71391623798,-4.6403267286,0.995213725063],
    [2.71439532918,-4.64105855923,0.990195385542],
    [2.71487442038,-4.64179038986,0.985177046022],
    [2.71535351158,-4.6425222205,0.980158706501],
    [2.71583260278,-4.64325405113,0.97514036698],
    [2.71631169398,-4.64398588176,0.970122027459],
    [2.71679078518,-4.6447177124,0.965103687938],
    [2.71726987638,-4.64544954303,0.960085348417],
    [2.71774896758,-4.64618137366,0.955067008897],
    [2.71822805878,-4.6469132043,0.950048669376],
    [2.71870714998,-4.64764503493,0.945030329855],
    [2.71918624118,-4.64837686557,0.940011990334],
    [2.71966533238,-4.6491086962,0.934993650813],
    [2.72014442358,-4.64984052683,0.929975311293],
    [2.72062351477,-4.65057235747,0.924956971772],
    [2.72110260597,-4.6513041881,0.919938632251],
    [2.72158169717,-4.65203601873,0.91492029273],
    [2.72206078837,-4.65276784937,0.909901953209],
    [2.72253987957,-4.65349968,0.904883613689],
    [2.72301897077,-4.65423151063,0.899865274168],
    [2.72349806197,-4.65496334127,0.894846934647],
    [2.72397715317,-4.6556951719,0.889828595126],
    [2.72445624437,-4.65642700253,0.884810255605],
    [2.72493533557,-4.65715883317,0.879791916085],
    [2.72541442677,-4.6578906638,0.874773576564],
    [2.72589351797,-4.65862249443,0.869755237043]
    ])
for item in AP:
  HEBAEI51.add_point(POINT(item))
QSS.add_pointlist(HEBAEI51)


#----------------------------
HEB30 = DIAGNOSTIC()
AL = list(np.array([
    [-3.605896, 4.049897, -1.023309],
    [-3.589175, 4.031148, -1.018936],
    [-3.572454, 4.012399, -1.014563],
    [-3.555732, 3.99365,  -1.01019],
    [-3.539011, 3.9749,   -1.005817]
]))

for item in AL:
  HEB30.add_point(POINT(item))

AV = np.array([
  [-3.5403017894,3.97650607057,-0.993995223482],
  [-3.55454823205,3.99423227141,-0.863518276048]])
AV = AV[1]-AV[0]

for item in AL:
  HEB30.add_los(LINE_OF_SIGHT(item,np.array(item)+AV))

#----------------------------
HEB51 = DIAGNOSTIC()
AL = list(np.array([
    [2.737398, -4.680898, 1.023309],
    [2.724734, -4.659201, 1.018935],
    [2.712069, -4.637505, 1.014563],
    [2.699405, -4.615808, 1.01019],
    [2.68674,  -4.594112, 1.005816]
]))

for item in AL:
  HEB51.add_point(POINT(item))

AV = np.array([
    [2.71343714678,-4.63959489796,1.00023206458],
    [2.72589351797,-4.65862249443,0.869755237043]])
AV = AV[1]-AV[0]

for item in AL:
  HEB51.add_los(LINE_OF_SIGHT(item,np.array(item)+AV))

#----------------------------
LP50 = DIAGNOSTIC()
AL = list(np.array([[865.719, -5452.778, -1023.417],
     [861.917, -5428.400, -1019.385],
     [858.116, -5404.021, -1015.353],
     [854.315, -5379.643, -1011.322],
     [850.514, -5355.265, -1007.290],
     [839.110, -5282.130,  -995.197],
     [835.309, -5257.752,  -991.166],
     [831.508, -5233.374,  -987.135],
     [827.706, -5208.995,  -983.104],
     [823.905, -5184.617,  -979.074],
     [922.352, -5459.780, -1022.304],
     [918.333, -5435.426, -1018.337],
     [914.314, -5411.072, -1014.370],
     [910.295, -5386.719, -1010.403],
     [906.276, -5362.365, -1006.436],
     [894.220, -5289.303,  -994.535],
     [890.201, -5264.949,  -990.569],
     [886.182, -5240.595,  -986.602],
     [882.164, -5216.241,  -982.636],
     [878.145, -5191.887,  -978.669]]) /1000.)

for item in AL:
  LP50.add_point(POINT(item))

#----------------------------
LP51 = DIAGNOSTIC()
AL = list(np.array([
  [2504.700, -4920.250, 1023.425],
  [2493.425, -4898.300, 1019.350],
  [2482.175, -4876.325, 1015.350],
  [2470.900, -4854.375, 1011.350],
  [2459.650, -4832.425, 1007.275],
  [2425.900, -4766.550,  995.200],
  [2414.650, -4744.600,  991.200],
  [2403.400, -4722.625,  987.100],
  [2392.150, -4700.675,  983.100],
  [2380.900, -4678.725,  979.100],
  [2462.975, -4959.200, 1022.300],
  [2451.925, -4937.125, 1018.350],
  [2440.850, -4915.075, 1014.375],
  [2429.800, -4893.000, 1010.400],
  [2418.725, -4870.950, 1006.425],
  [2385.525, -4804.750,  994.500],
  [2374.475, -4782.675,  990.600],
  [2363.400, -4760.625,  986.600],
  [2352.325, -4738.550,  982.625],
  [2341.275, -4716.475,  978.675]]) /1000.)

for item in AL:
  LP50.add_point(POINT(item))

#----------------------------
#NBI Spectroscopic Observation
import geometry.w7x.qsk as qsk
QSK = DIAGNOSTIC()

def x2n(x):
  return np.array(x)

LFAN = LOS_FAN('AEA21')
los = qsk.AEA21['los']
for item in los:
  LFAN.add_los(LINE_OF_SIGHT(x2n(item['start']),x2n(item['start'])+x2n(item['uVec']),label=item['id']))
QSK.add_losfan(LFAN)

LFAN = LOS_FAN('AEM21')
los = qsk.AEM21['los']
for item in los:
  LFAN.add_los(LINE_OF_SIGHT(x2n(item['start']),x2n(item['start'])+x2n(item['uVec']),label=item['id']))
QSK.add_losfan(LFAN)

LFAN = LOS_FAN('AET21')
los = qsk.AET21['los']
for item in los:
  LFAN.add_los(LINE_OF_SIGHT(x2n(item['start']),x2n(item['start'])+x2n(item['uVec']),label=item['id']))
QSK.add_losfan(LFAN)

LFAN = LOS_FAN('NBI')
los = qsk.NBI['los']
for item in los:
  LFAN.add_los(LINE_OF_SIGHT(x2n(item['start']),x2n(item['start'])+x2n(item['uVec']),label=item['id']))
QSK.add_losfan(LFAN)


#----------------------------
#QRB Divertorbolometer
QRB = DIAGNOSTIC()

#----------------------------
#QSB Divertorbolometer
QSB = DIAGNOSTIC()

#----------------------------
#QSL Laser Blow-Off
QSL = DIAGNOSTIC()
xyz_head=np.array([-587.3,-223.2,-16.8]) ## define two positions along LBO
xyz_los=np.array([-554.8,-210.3,-16.8])
QSL.add_los(LINE_OF_SIGHT(xyz_head,xyz_los))

#----------------------------
#QOB IR Bolometer (AEK51)
QOB = DIAGNOSTIC()
pin = [[3491.4,-5262.1, 301.8],[3488.4,-5264.0,298.3],[3491.4,-5262.0,294.8],[3494.4,-5260.2,298.3]]
det = [[3520.9,-5308.7,316.0],[3481.2,-5333.2,269.3],[3532.1,-5300.0,208.5],[3571.9,-5275.5,255.2]]
cam = CAMERA(pin,det,label='AEK51')
QOB.add_camera(cam)

#----------------------------
#QSD HEXOS Spectrometer
QSD = DIAGNOSTIC()
P0 = [-1.931e+00,  5.944e+00, -4.467e-02]
P1 = [-1.360e+00,  4.185e+00,  3.289e-02]
QSD.add_los(LINE_OF_SIGHT(P0,P1)) #Spectrometer 0
QSD.add_los(LINE_OF_SIGHT(P0,P1)) #Spectrometer 1
P0 = [-1.931e+00,  5.944e+00,  4.467e-02]
P1 = [-1.360e+00,  4.185e+00, -3.289e-02]
QSD.add_los(LINE_OF_SIGHT(P0,P1)) #Spectrometer 2
  