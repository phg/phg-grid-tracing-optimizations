# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 11:44:38 2018

@author: mto
"""

import numpy as np
from osa import Client

srv = Client('http://esb.ipp-hgw.mpg.de:8280/services/FieldLineProxy?wsdl')

config = srv.types.MagneticConfig()
config.configIds = [7]

# *************************************** #
# *** Definition of the coil currents *** #
# *************************************** #
# Currents have to be defined as winding currents I[A] x n
# Sign of the winding currents for positive field direction acc. 1-AA-R0004
# For magnetic configurations: PLM document 1-AA-T0011
# I1-I5: "-"
# IA-IB: "-"
# Icc1, Icc2, ..., Icc9, Icc10: "+", "-", ..., "+", "-"
# It1-It5: "+"

# Winding coil current (I[A] x n) for non-planar and planar coils 
I1= -1*(0)*108
I2= -1*(0)*108
I3= -1*(0)*108
I4= -1*(0)*108
I5= -1*(0)*108
IA= -1*(0)*36
IB= -1*(0)*36

# Winding coil current (I[A] x n) for control coils
Icc1= +1*(0)*8
Icc2= -1*(0)*8
Icc3= +1*(0)*8
Icc4= -1*(0)*8
Icc5= +1*(0)*8
Icc6= -1*(0)*8
Icc7= +1*(0)*8
Icc8= -1*(0)*8
Icc9= +1*(0)*8
Icc10=-1*(0)*8

# Winding coil current (I[A] x n) for trim coils
Itc1= +1*(0)*48
Itc2= +1*(0)*72 # at module 2 smaller coil corss section but with more windings 
Itc3= +1*(0)*48
Itc4= +1*(0)*48
Itc5= +1*(0)*48

config = srv.types.MagneticConfig()

# ******************************************************************************** #
# ************** Definition of coil types according to data base ***************** #
# ***  http://webservices.ipp-hgw.mpg.de/docs/fieldlinetracer.html#searchCoils *** #
# ******************************************************************************** #
                    
# --- Ideal NPC and PC, control coils and trim coils --- #
config.coilsIds=[
    160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 
    161, 166, 171, 176, 181, 186, 191, 196, 201, 206,
    162, 167, 172, 177, 182, 187, 192, 197, 202, 207,
    163, 168, 173, 178, 183, 188, 193, 198, 203, 208,
    164, 169, 174, 179, 184, 189, 194, 199, 204, 209,
    210, 212, 214, 216, 218, 220, 222, 224, 226, 228,
    211, 213, 215, 217, 219, 221, 223, 225, 227, 229,
    230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
    350, 241, 351, 352, 353]

config.coilsIdsCurrents=[ 
    I1, I1, I1, I1, I1, I1, I1, I1, I1, I1, 
    I2, I2, I2, I2, I2, I2, I2, I2, I2, I2,
    I3, I3, I3, I3, I3, I3, I3, I3, I3, I3,
    I4, I4, I4, I4, I4, I4, I4, I4, I4, I4,
    I5, I5, I5, I5, I5, I5, I5, I5, I5, I5,
    IA, IA, IA, IA, IA, IA, IA, IA, IA, IA,
    IB, IB, IB, IB, IB, IB, IB, IB, IB, IB,
    Icc1, Icc2, Icc3, Icc4, Icc5, Icc6, Icc7, Icc8, Icc9, Icc10,
    Itc1, Itc2, Itc3, Itc4, Itc5]

pos = srv.types.Points3D()

# --- Magnetic axis at HM10/11 --- #
pos.x1 = [5.9]
pos.x2 = [0.0]
pos.x3 = [0.0]

# --- Control coil M10 (uopper), centre --- #
#pos.x1 = [5.970716]
#pos.x2 = [-1.609591]
#pos.x3 = [0.543232]

# --- Control coil M11 (lower), centre --- #
#pos.x1 = [5.970716]
#pos.x2 = [1.609591]
#pos.x3 = [-0.543232]

# --- Trim coil at M1 --- #
#pos.x1 = [8.0]
#pos.x2 = [0]
#pos.x3 = [0]

res = srv.service.magneticField(pos, config)

A = np.array([res.field.x1, res.field.x2, res.field.x3]).T # Bx,By,Bz [T]
B = (A[:, 0:]*A[:, 0:]).sum(axis=1)**0.5 # mod B [T]
C = np.insert(A, 3, B, axis=1) # |B|-Werte nach der 3. Spalte einzufügen
# print (C)
print('Bx, By, Bz, |B| [T]:', res.field.x1, res.field.x2, res.field.x3, B)
