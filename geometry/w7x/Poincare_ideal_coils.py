# -*- coding: utf-8 -*-
"""
Created on Wed Feb  1 09:28:38 2017

@author: mto
"""
from osa import Client
import numpy as np
import matplotlib.pyplot as plt

srv = Client('http://esb.ipp-hgw.mpg.de:8280/services/FieldLineProxy?wsdl')

# *************************************** #
# *** Definition of the coil currents *** #
# *************************************** #
# Magnetic configuration acc. PLM document 1-AA-T0011
# Winding coil current (I[A] x n) for non-planar and planar coils 
# Sign of the winding currents for positive field direction acc. 1-AA-R0004
# I1-I5: "-"
# IA-IB: "-"
# Icc1, Icc2, ..., Icc9, Icc10: "+", "-", ..., "+", "-"
# It1-It5: "+"

I1= -1*(12800)*108
I2= -1*(12800)*108
I3= -1*(12800)*108
I4= -1*(12800)*108
I5= -1*(12800)*108
IA= -1*(5000)*36
IB= -1*(5000)*36

Itc1= +1*(0)*48
Itc2= +1*(0)*72 # at module 2 smaller coil corss section but with more windings 
Itc3= +1*(0)*48
Itc4= +1*(0)*48
Itc5= +1*(0)*48

Icc1= +1*(0)*8
Icc2= -1*(0)*8
Icc3= +1*(0)*8
Icc4= -1*(0)*8
Icc5= +1*(0)*8
Icc6= -1*(0)*8
Icc7= +1*(0)*8
Icc8= -1*(0)*8
Icc9= +1*(0)*8
Icc10=-1*(0)*8

config = srv.types.MagneticConfig()

# ******************************************************************************** #
# ************** Definition of coil types from data base ***************** #
# ***  http://webservices.ipp-hgw.mpg.de/docs/fieldlinetracer.html#searchCoils *** #
# ******************************************************************************** #
                
# --- Ideal NPC, P, comntrol and trim coils --- #
config.coilsIds=[
    160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 
    161, 166, 171, 176, 181, 186, 191, 196, 201, 206,
    162, 167, 172, 177, 182, 187, 192, 197, 202, 207,
    163, 168, 173, 178, 183, 188, 193, 198, 203, 208,
    164, 169, 174, 179, 184, 189, 194, 199, 204, 209,
    210, 212, 214, 216, 218, 220, 222, 224, 226, 228,
    211, 213, 215, 217, 219, 221, 223, 225, 227, 229,
    230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
    350, 241, 351, 352, 353]

config.coilsIdsCurrents=[ 
    I1, I1, I1, I1, I1, I1, I1, I1, I1, I1, 
    I2, I2, I2, I2, I2, I2, I2, I2, I2, I2,
    I3, I3, I3, I3, I3, I3, I3, I3, I3, I3,
    I4, I4, I4, I4, I4, I4, I4, I4, I4, I4,
    I5, I5, I5, I5, I5, I5, I5, I5, I5, I5,
    IA, IA, IA, IA, IA, IA, IA, IA, IA, IA,
    IB, IB, IB, IB, IB, IB, IB, IB, IB, IB,
    Icc1, Icc2, Icc3, Icc4, Icc5, Icc6, Icc7, Icc8, Icc9, Icc10,
    Itc1, Itc2, Itc3, Itc4, Itc5]

my_grid = srv.types.CylindricalGrid()
my_grid.RMin = 4.05
my_grid.RMax = 6.75
my_grid.ZMin = -1.35
my_grid.ZMax = 1.35
my_grid.numR = 181
my_grid.numZ = 181
my_grid.numPhi = 96

g = srv.types.Grid()
g.cylindrical = my_grid
g.fieldSymmetry = 5 # "5" = stellarator symmetry, "1" for all other cases

config.grid = g

pos = srv.types.Points3D()
# pos.x1 = np.linspace(-5.195, -6.1, 30) # negative x-direction -> Phi=180° (AEU40)
pos.x1 = np.linspace(5.93, 6.25, 30) # start/stop position [m] and number of steps of field line in x-direction (=phi=0°) in cartesian W7-X coordinates
pos.x2 = np.zeros(30) # y-direction (zeros in this case!)
pos.x3 = np.zeros(30) # z-direction (zeros in this case!)

poincare = srv.types.PoincareInPhiPlane()
poincare.numPoints = 1000 # number of intersection points for a single flux surface in Poincaré plot
# phi0 = 4*np.pi/20 # n times 9°
# poincare.phi0 = [8*np.pi/20] # bean shaped plane at AEA21
poincare.phi0 = [12*np.pi/20] # triangular plane at AEV21/30

task = srv.types.Task()
task.step = 0.01 # step width of field vector [m]
task.poincare = poincare

''' you can use a Machine object for a limitation of the tracing region. 
    This sample uses a torus model (id = 164) from ComponentsDB:'''
machine = srv.types.Machine()
machine.meshedModelsIds = [164] 
machine_grid = srv.types.CartesianGrid()
machine_grid.XMin = -7
machine_grid.XMax = 7
machine_grid.YMin = -7
machine_grid.YMax = 7
machine_grid.ZMin = -1.5
machine_grid.ZMax = 1.5
machine_grid.numX = 400
machine_grid.numY = 400
machine_grid.numZ = 100
machine.grid = machine_grid

res = srv.service.trace(pos, config, task, machine, None)
print("Number of calculated flux surfaces: " + str(len(res.surfs)))

# ************************************ #
# *** Plotting and saving the plot *** #
# ************************************ #
for i in res.surfs:
    x, y, z = i.points.x1, i.points.x2, i.points.x3
    x = np.array(x)
    y = np.array(y)
    z = np.array(z)
    R = np.sqrt(x**2 + y**2)
    plt.plot(R, z, ".k", ms = 0.2) #zeigt nur jede zweite Ebene an
    # plt.plot(R[1::2], z[1::2]/np.cos(np.pi/4.), ".k", ms = 0.2) #zeigt nur jede zweite Ebene an
    # plt.plot(R[::2], z[::2]/np.cos(np.pi/4.), ".k", ms = 0.2) #zeigt nur jede zweite Ebene an
    plt.axes().set_aspect('equal')
    plt.xlabel(r"$R$ [m]", fontsize=16)
    plt.ylabel(r"$z$ [m]", fontsize=16)
plt.savefig('Poincare_ideal_coils.pdf', format='PDF') 
