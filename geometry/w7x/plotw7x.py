import os
from netCDF4 import Dataset
import urllib
import numpy as np
from numpy import float64, zeros, arange, exp, ones, copy, append, dot, mean, std, sum, asarray, max, sqrt, pi, linspace, linalg, meshgrid, median, where, inf, hamming, conjugate, angle, diff, real, imag, sin, arctan, arcsin, cos, dot, zeros_like, r_
from mayavi import mlab
from importlib import reload
import geometry.w7x.w7xgeo as w7xgeo#; reload(w7xgeo)

#Radian per half-module
radhm = 2.*np.pi/10.

def plot_geometry(fig=None,shw=True,verb=False,\
    #half-modules
    hmfs=[],\
    hmco=[],\
    mcy=[],\
    mvv=[],\
    mdi=[],mba=[],\
    msc = [],\
    mhs=[],mcl=[],mpa=[],cidx=None,test=None,op=1.0):

    u"""
    plot W7X geometry
    The following site is useful for color choise. It's normalized decimal. 0-1, instead of 0-255
    https://en.wikipedia.org/wiki/Web_colors
    example Pink:( 255/255.0, 192/255.0, 203/255.0 )
    """
    
    #IDs in ComponentDB for Modules (HM)
    comp = {\
    'fluxsurf': [ 20, 21, 22, 23, 24],\
    'divertor': [165,166,167,168,169],\
    'baffle':   [320,321,322,323,324],\
    'shield':   [330,331,332,333,334],\
    'tclosure': [325,326,327,328,329],\
    'vessel':   [340,341,342,343,344],\
    'pclosure': [445,446,447,448,449],\
    'pumpslit': [450,451,452,453,454],\
    'cylinder': [ 25, 26, 27, 28, 29],\
    'panel':    [[435,436],[437,438],[439,440],[441,442],[443,444]],\
    'coils':    [[0,1,2,3,4],[5,6,7,8,9],[10,11,12,13,14],[15,16,17,18,19],[20,21,22,23,24]],\
    'scraper':  [[309,314],[310,315],[311,316],[312,317],[313,318],[314,319]]}

    if test != None:
        hmfs=[]
        hmco=[]
        #modules
        mvv=[]
        mdi=[]
        mba=[]
        msc=[]
        mhs=[]
        mcl=[]
        mpa=[]
        print(test)
        exec(test)
        print(mvv)

    woutfile = 'C:\\Users\\flr\\rzgshare\\work\\W7X\\python\\git\\geometry\\w7x\\input\\wout_w7x.nc'  #VMEC nc file

    if fig is None:
        fig=mlab.figure(size = (1024,768),bgcolor = (1,1,1), fgcolor = (0.5, 0.5, 0.5))

    #Translate half-modules into modules
    #modules = list(set(np.ceil(np.array(modules)/2.)))

    #------------plot W7X plasma-------------------------------------------------
    if True:
        if os.path.isfile(woutfile):
            for i in hmfs:
                s1=Flux_surface_VMEC(woutfile=woutfile, s=0.99, v_range=[radhm*(i-2), radhm*(i-1)], Nyquist=1)        
                mlab.mesh( s1.xmesh, s1.ymesh, s1.zmesh, color=(1, 0, 1) ,transparent=True, opacity=0.1,figure=fig)        

    #------------plot cylinder-------------------------------------------------
    if verb: print('Plot Cylinder')
    #for i in [ i for i in range(25,30)]:
    idx = [ comp['cylinder'][i] for i in mcy ]
    for i in idx:
        x, y, z, t =w7xgeo.component_data(num=i)    
        src = mlab.pipeline.triangular_mesh_source(x, y, z, t )
        mlab.pipeline.surface(src, color=(0, 0, 1),opacity=0.05,figure=fig)
        #dec = mlab.pipeline.greedy_terrain_decimation(src)
        #dec.filter.triangle_number = 5000
        #dec.filter.calculate_normals = True
        #dec = mlab.pipeline.decimate_pro(mlab.pipeline.triangle_filter(src))
        # Set a very low feature_angle, so that the decimate_pro detects
        #dec.filter.feature_angle = 1
        #dec.filter.target_reduction = 0.1
        #dec = mlab.pipeline.quadratic_decimation(src)
        #gf = mlab.pipeline.user_defined(dec,
        #                        filter='GeometryFilter')
        #gf.filter.extent_clipping = True
        #gf.filter.extent = (-10,10,\
        #            -10,10,\
        #            -10,0)
        #mlab.pipeline.surface(gf, color=(0, 0, 0), representation='wireframe', opacity=0.25)


    #------------plot vessel-------------------------------------------------
    if verb: print('Plot Vacuumm Vessel')
    #for i in [ i for i in range(340, 342)]:
    #for i in [ i for i in range(341, 342)]
    idx = [ comp['vessel'][i] for i in mvv ]
    for i in idx:
        x, y, z, t =w7xgeo.component_data(num=i)    
        #print(z.shape)
        #idx = np.where(z > 0.)
        #print (idx)
        #x, y , z = x[idx],y[idx],z[idx]
        mlab.triangular_mesh(x, y, z, t , color=(163/255.0,163/255.0,163/255.0),opacity=op,figure=fig)#, transparent=True, opacity=0.3)        

    #------------plot coils-------------------------------------------------
    if verb: print('Plot Coils')
    idx = []
    for i in hmco:
      idx.extend(comp['coils'][i])
    for i in idx:
        x, y, z = w7xgeo.coil_data(num=i)
        mlab.plot3d(x, y, z, color=(0,0,128/255.0), tube_radius=0.15, tube_sides=4 ,opacity=op,figure=fig)
    
    #------------plot divorter-------------------------------------------------
    if verb: print('Plot Divertor')
    #for i in [ i for i in range(30, 70)]:            
    #for i in [ i for i in range(165,170)]:         
    #for i in [ i for i in range(166,168)]:
    idx = [ comp['divertor'][i] for i in mdi ]
    icol = [0.,128.,128.,0.,128.]
    k=0
    for i in idx:
#    for i in [402,403,404,405,406]:            
        x, y, z, t =w7xgeo.component_data(num=i)  
        mlab.triangular_mesh(x, y, z, t , color=(128/255.0, 0, 0) ,opacity=op,figure=fig)
        #mlab.triangular_mesh(x, y, z, t , color=(icol[k]/255., 0, 0),figure=fig)
        k=k+1

    #------------plot baffles-------------------------------------------------
    if verb: print('Plot Baffles')
    #for i in [ i for i in range(320,325)]:            
    #for i in [ i for i in range(321,323)]:
    idx = [ comp['baffle'][i] for i in mba ]
    for i in idx:
        x, y, z, t =w7xgeo.component_data(num=i)    
        mlab.triangular_mesh(x, y, z, t , color=(0, 128/255., 0) ,opacity=op,figure=fig)

    ##------------plot closure-------------------------------------------------
    if verb: print('Plot Poloidal Closure')
    #for i in [ i for i in range(325,330)]:
    idx = [ comp['pclosure'][i] for i in mcl ]
    for i in idx:
        x, y, z, t =w7xgeo.component_data(num=i)    
        mlab.triangular_mesh(x, y, z, t , color=(0, 1, 0) ,opacity=op,figure=fig)

    ##------------plot closure-------------------------------------------------
    if verb: print('Plot Troidal Closure')
    #for i in [ i for i in range(325,330)]:
    idx = [ comp['tclosure'][i] for i in mcl ]
    for i in idx:
        x, y, z, t =w7xgeo.component_data(num=i)    
        mlab.triangular_mesh(x, y, z, t , color=(0, 1, 0) ,opacity=op,figure=fig)

    #------------plot scraper-------------------------------------------------
    if verb: print('Plot Scraper')
    #for i in [ i for i in range(310,320)]:
    idx = []
    for i in msc:
      idx.extend(comp['scraper'][i])
    for i in idx:
        x, y, z, t =w7xgeo.component_data(num=i)    
        mlab.triangular_mesh(x, y, z, t , color=(1, 0, 0) ,opacity=op,figure=fig)

    #------------plot shield-------------------------------------------------
    if verb: print('Plot Heatshield')
    idx = [ comp['shield'][i] for i in mhs ]
    for i in idx:
        x, y, z, t =w7xgeo.component_data(num=i)    
        mlab.triangular_mesh(x, y, z, t , color=(0, 0, 128/255.0) ,opacity=op,figure=fig)

    #------------plot panel-------------------------------------------------
    if verb: print('Plot Panel')
    idx = []
    for i in mpa:
      idx.extend(comp['panel'][i])
    for i in idx:
        x, y, z, t =w7xgeo.component_data(num=i)
        mlab.triangular_mesh(x, y, z, t , color=(0, 0, 1) ,opacity=op,figure=fig)

    #------------plot other components-------------------------------------------------
    if not cidx is None:
        for i in cidx:
            x, y, z, t =w7xgeo.component_data(num=i)    
            mlab.triangular_mesh(x, y, z, t , color=(0.1,0.1,0.1) ,opacity=op,figure=fig)

    mlab.view(focalpoint=(0,0,0) )    
    if shw:
      mlab.show()
    return fig

def test(num_arr=[343,344],shw=True):
    fig=mlab.figure(size = (1024,768),bgcolor = (1,1,1), fgcolor = (0.5, 0.5, 0.5))        
    for i in range(len(num_arr)):
        x, y, z, t =w7xgeo.component_data(num=num_arr[i])            
        mlab.triangular_mesh(x, y, z, t, color=(163/255.0,163/255.0,163/255.0))#, transparent=True, opacity=0.3)        

    fig=mlab.figure(size = (1024,768),bgcolor = (1,1,1), fgcolor = (0.5, 0.5, 0.5))        
    for i in [430,431,432,433,434]:
        x, y, z, t =w7xgeo.component_data(num=i)            
        mlab.triangular_mesh(x, y, z, t, color=(163/255.0,163/255.0,163/255.0))#, transparent=True, opacity=0.3)        

#    for i in [i for i in range( 170,320 )]:
#        x, y, z, t =w7xgeo.component_data( i)            
#        mlab.triangular_mesh(x, y, z, t, color=(244/255.0,103/255.0,163/255.0))#, transparent=True, opacity=0.3)        

    for i in [335,336,337,338,339]:
        x, y, z, t =w7xgeo.component_data( i)            
        mlab.triangular_mesh(x, y, z, t, color=(244/255.0,103/255.0,163/255.0))#, transparent=True, opacity=0.3)        

    for i in [330,331,332,333,334]:
        x, y, z, t =w7xgeo.component_data( i)            
        mlab.triangular_mesh(x, y, z, t, color=(94/255.0,143/255.0,63/255.0))#, transparent=True, opacity=0.3)        

#    mlab.points3d(dx, dy, dz, scale_factor=0.05)
#    mlab.points3d(dx[:j], dy[:j], dz[:j], scale_factor=0.01)
#    mlab.plot3d(dx[:j], dy[:j], dz[:j] , tube_radius=0.001, color=(1,0.5,0))

#    for k in range(j):
#        mlab.points3d(dx[k:k+j], dy[k:k+j], dz[k:k+j])

    mlab.view(focalpoint=(0,0,0) )    
    if shw:
      mlab.show()
    return 0


def plot_vessel(shw=True):
    u"""
    plot W7X geometry
    The following site is useful for color choise. It's normalized decimal. 0-1, instead of 0-255
    https://en.wikipedia.org/wiki/Web_colors
    example Pink:( 255/255.0, 192/255.0, 203/255.0 )
    #CATIA models STEP format
    """

    woutfile = 'wout_w7x.nc'  #VMEC nc file

    fig=mlab.figure(size = (1024,768),bgcolor = (1,1,1), fgcolor = (0.5, 0.5, 0.5))    

#------------plot W7X plasma-------------------------------------------------

    s1=Flux_surface_VMEC(woutfile=woutfile, s=0.99, v_range=[0, 2.0*pi], Nyquist=1)        
    mlab.mesh( s1.xmesh, s1.ymesh, s1.zmesh, color=(1, 0, 1) ,transparent=True, opacity=0.1)        

#------------plot vessel-------------------------------------------------

#    for i in [ i for i in range(18, 20)]:            
#        x, y, z, t =w7xgeo.component_data(num=i)    
#        mlab.triangular_mesh(x, y, z, t , color=(163/255.0,163/255.0,163/255.0))#, transparent=True, opacity=0.3)        

    for i in [ i for i in range(340, 345)]:            
        x, y, z, t = w7xgeo.component_data(num=i)    
        mlab.triangular_mesh(x, y, z, t , color=(255/255.0,163/255.0,163/255.0))#, transparent=True, opacity=0.3)        

    mlab.view(focalpoint=(0,0,0) )
    if shw:
      mlab.show()
    return 0

class Flux_surface_VMEC(object):
    u"""
    woutfile: .nc file
    s: radial positon 
    num_u: number of point in the poloidal direction
    num_v: number of point in the toroidal direction    
    u_range: poloidal angle to be plotted (radian)
    v_range: toloidal angle to be plotted (radian)    
    Nyquist: 1 for files that use Nyquist variance 
    """  
    def __init__(self,woutfile = 'wout_w7x.nc', s=0.5, num_u = 100, num_v = 100, u_range=[0,2*pi], v_range=[0,2*pi], origin=[0.0,0.0,0.0], Nyquist=0):
        self.xo=origin[0]
        self.yo=origin[1]
        self.zo=origin[2]

        wout = Dataset(woutfile,more = 'r')
        ns = wout.variables['ns'][0]
        xn = asarray([wout.variables['xn'][:]])
        xm = asarray([wout.variables['xm'][:]])
        rmnc = wout.variables['rmnc'][:]
        zmns = wout.variables['zmns'][:]

        u = linspace(u_range[0], u_range[1], num_u)
        v = linspace(v_range[0], v_range[1], num_v)           
        U,V=meshgrid(u,v)
        R=zeros(U.shape)
        Z=zeros(U.shape)    
        b_n=where( (s*ns)<arange(1,ns+1, dtype=float64) )
        b_n=b_n[0][0]
    
        rmnc1=rmnc[b_n]
        zmns1=zmns[b_n]    

        if Nyquist!=0:
            for i in range(len(rmnc1)):
                R+=rmnc1[i]*cos(xm[0,i]*U-xn[0,i]*V) 
                Z+=zmns1[i]*sin(xm[0,i]*U-xn[0,i]*V)        
        
            self.xmesh=R*cos(V)+self.xo
            self.ymesh=R*sin(V)+self.yo        
            self.zmesh=Z+self.zo            
        else:
            rmns = wout.variables['rmns'][:]        
            zmnc = wout.variables['zmnc'][:]
            rmns1=rmns[b_n]                
            zmnc1=zmnc[b_n]                            
            for i in range(len(rmnc1)):
                R+=rmnc1[i]*cos(xm[0,i]*U-xn[0,i]*V) + rmns1[i]*sin(xm[0,i]*U-xn[0,i]*V)
                Z+=zmnc1[i]*cos(xm[0,i]*U-xn[0,i]*V) + zmns1[i]*sin(xm[0,i]*U-xn[0,i]*V)        

            self.xmesh=R*cos(V)+self.xo
            self.ymesh=R*sin(V)+self.yo        
            self.zmesh=Z+self.zo                            
                                                
    def phi_rotation(self,phi):
        xmesh_cp=copy(self.xmesh)
        ymesh_cp=copy(self.ymesh)
        self.xmesh= cos(phi)*(xmesh_cp-self.xo)-sin(phi)*(ymesh_cp-self.yo)+self.xo
        self.ymesh= sin(phi)*(xmesh_cp-self.xo)+cos(phi)*(ymesh_cp-self.yo)+self.yo
        
    def theta_rotation(self,theta):
        xmesh_cp=copy(self.xmesh)
        zmesh_cp=copy(self.zmesh)
        self.xmesh= cos(theta)*(xmesh_cp-self.xo)+sin(theta)*(zmesh_cp-self.zo)+self.xo
        self.zmesh= -sin(theta)*(xmesh_cp-self.xo)+cos(theta)*(zmesh_cp-self.zo)+self.zo

    def spatial_trans(self,x,y,z):
        self.xmesh=self.xmesh+x
        self.ymesh=self.ymesh+y
        self.zmesh=self.zmesh+z
        self.xo=self.xo+x
        self.yo=self.yo+y
        self.zo=self.zo+z         
        