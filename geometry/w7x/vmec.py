import os
import numpy as np
from osa import Client
from netCDF4 import Dataset
import pickle
from numpy import float64, zeros, arange, exp, ones, copy, append, dot, mean, std, sum, asarray, max, sqrt, pi, linspace, linalg, meshgrid, median, where, inf, hamming, conjugate, angle, diff, real, imag, sin, arctan, arcsin, cos, dot, zeros_like, r_, random
import matplotlib.pyplot as plt
#from  mayavi import mlab
import requests

import w7xarchive as archivedb

import utils.plot as pu
import utils.geom as geom
import utils.data as data
import geometry.w7x.meshsrv as meshsrv


client = Client('http://esb.ipp-hgw.mpg.de:8280/services/vmec_v5?wsdl')

degtorad = 2.*np.pi/360.
#s = [0, 0.005,0.02,0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
s = [0, 0.02, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
npp = 100

def get_vmecID(tr):
    url = 'http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7XAnalysis/Equilibrium/RefEq_PARLOG/V1/parms/equilibriumID'
    vid = archivedb.get_parameters_box(url,tr[0],tr[1])
    return vid['values'][0]

def get(vid,phi,s=s,npp=npp):
    phi = phi*degtorad #Convert to radian
    fs = client.service.getFluxSurfaces(vid, phi, s, npp)
    return fs

def get_vid(id):
    vid =  {
            #'EIM'    : 'w7x/1000_1000_1000_1000_+0000_+0000/01/00/',\
            'EIM'   : 'w7x_ref_1',\
            #'EIMr'   : 'w7x/1000_1000_1000_1000_+0000_+0000/10/00/',\
            'EIMr'   : 'w7x_ref_172',\
            #'EIMx'   : 'ww7x/1000_1000_1000_1000_+0000_+0000/10/00_l6ns151/',
            'EIMx'   : 'w7x_ref_173',\
            #'FTM'    : 'w7x/1000_1000_1000_1000_-0690_-0690/01/00/',\
            'FTM'    : 'w7x_ref_15',\
            #'FTMr'   : 'w7x/1000_1000_1000_1000_-0690_-0690/02/00/',\
            'FTMr'   : 'w7x_ref_177',\
            #'FTMx'   : 'w7x/1000_1000_1000_1000_-0690_-0690/02/00l8ns148/',\
            'FTMx'   : 'w7x_ref_178',\
            #'DBM'    : 'w7x/1000_1000_1000_1000_+0750_+0750/01/00/',\
            'DBM'    : 'w7x_ref_18',\
            #'KJM'    : 'w7x/0972_0926_0880_0852_+0000_+0000/01/00jh/',\
            'KJM'   : 'w7x_ref_27',\
            #'AIM'    : 'w7x/1042_1042_1127_1127_+0000_+0000/01/00/',\
            'AIM'   : 'w7x_ref_21',\
            'FOM003r': 'w7x/1000_1000_1000_1000_-0370_-0370/01/000/',\
            #'FTMx'   : 'w7x_ref_178',\
            'FOM003X': 'w7x/1000_1000_1000_1000_-0370_-0370/01/000l6/',\
            #'FTMx'   : 'w7x_ref_178',\
            }
    return vid[id]

#Adrians VMEC - Start - Still to integrate
#-----------------------------------------
def query_rest(baseurl, target, params=None):
    fullurl = baseurl + target
    response = requests.get(fullurl, params, headers={'Accept': 'application/json'})
    if not response.ok:
        raise Exception('Request failed (url: {:s}).'.format(response.url))
    data = response.json()
    if type(data) == dict:
        data['url'] = response.url
    return data


def query_vmec_rest(target, params=None):
    baseurl = 'http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1'
    return query_rest(baseurl, target, params)


def query_confencode_rest(target, params=None):
    baseurl = 'http://esb.ipp-hgw.mpg.de:8280/services/encode_w7x_config'
    return query_rest(baseurl, target, params)


def get_threeletter(currents):
    """Gets the three letter magnetic configuration code.

    Args:
        currents (list): list of coil currents

    Returns:
        threelettercode (str): string of format "EIM+252"
    """
    target = '?i1=#NPC1#&i2=#NPC2#&i3=#NPC3#&i4=#NPC4#&i5=#NPC5#&ia=#PCA#&ib=#PCB#'
    replacelist = ['#NPC1#', '#NPC2#', '#NPC3#', '#NPC4#', '#NPC5#', '#PCA#', '#PCB#']
    for i, item in enumerate(replacelist):
        target = target.replace(item, str(currents[i]))
    return query_confencode_rest(target).get('3-letter-code')


def get_vmecidstring(currents):
    """Gets the vmec id string (which doesn't necessarily correspond to an existing VMEC run)

    Args:
        currents (list): list of coil currents

    Returns:
        vmecidstring (str): string of format "1000_1000_1000_1000_+0390_+0390"
    """
    target = '?i1=#NPC1#&i2=#NPC2#&i3=#NPC3#&i4=#NPC4#&i5=#NPC5#&ia=#PCA#&ib=#PCB#'
    replacelist = ['#NPC1#', '#NPC2#', '#NPC3#', '#NPC4#', '#NPC5#', '#PCA#', '#PCB#']
    for i, item in enumerate(replacelist):
        target = target.replace(item, str(currents[i]))
    return query_confencode_rest(target).get('vmec-id-string')

def get_reff_vmec(x, y, z, cid=172):
    """get reff values from VMEC run with given configuration ID

    Args:
        x, y, z (float OR list of float): cartesian positions, can be multiple
        cid (int): W7X VMEC reference configuration ID

    Returns:
        reff (list of float): list of reff values for input coordinates
    """
    try:  # check if numpy array or list
        iter(x)
    except TypeError:  # is float or int
        x = str(x)
        y = str(y)
        z = str(z)
    else:  # is iterable
        x = ','.join(map(str, x))
        y = ','.join(map(str, y))
        z = ','.join(map(str, z))
    target = '/w7x_ref_#CONFID#/reff.json?x=#X#&y=#Y#&z=#Z#'
    target = target.replace('#CONFID#', str(cid))
    target = target.replace('#X#', x)
    target = target.replace('#Y#', y)
    target = target.replace('#Z#', z)
    return query_vmec_rest(target)['reff']
#-----------------------------------------

def get_lcfs_vmec(phi=0, cid=172):
    phi = phi * 180 / np.pi
    target = '/w7x_ref_#CONFID#/lcfs.json?phi=#PHI#'
    target = target.replace('#CONFID#', str(cid))
    target = target.replace('#PHI#', str(phi))
    return query_vmec_rest(target)['lcfs'][0]

def get_confid(code):
    """gets the w7x reference VMEC run ID for a given 3-letter-code"""
    # TODO: get closest VMEC run from coil current values
    code = code[0:3]
    confdict = {
        'EIM': 172,
        'EJM': 172,
        'KJM': 163,
        'KKM': 140,
        'FTM': 177,
        'EGS': 250
    }
    return confdict.get(code)
#Adrians VMEC - End

def get_reff(pts,vid):
    p = client.types.Points3D()
    p.x1 = pts[:,0]
    p.x2 = pts[:,1]
    p.x3 = pts[:,2]
    reff = client.service.getReff(vid, p)
    return reff

def plot(fs,fig=None,color=None,mst='.',shw=False):
   #plt.close("all")
    if fig == None:
      fig, ax = plt.subplots(figsize=(10.3,10.3))
    else:
      ax = fig.gca()
    if color == None:
        cst = 'k'
    else:
        cst = color

    ax.plot(fs[0].x1, fs[0].x3,marker=mst,color=color)
    for i in range(len(fs)):
        ax.plot(fs[i].x1, fs[i].x3,color=color)

    if shw:
          plt.show(block=False)
    return fig

def get_vmec_radial_lines(fs):
    x1 = []
    x3 = []
    nrr = len(fs)
    #Plot VMEC Fluxsurfaces & Prepare radial lines (VMEC)
    for i in range(nrr):
        #plt.plot(fs[i].x1, fs[i].x3,'k+')
        #plt.plot(fs[i].x1, fs[i].x3,'b')
        x1.append(fs[i].x1)
        x3.append(fs[i].x3)

    x1 = np.array(x1)
    x3 = np.array(x3)
    return x1,x3

def prep_theta_radial_lines(fs,length=0.5,fig=None,fplt=False):
    npp = len(fs[0].x1)
    mag = np.array([fs[0].x1[0],fs[0].x3[0]])
    v = np.zeros((npp,2))
    theta = np.array([2.*np.pi*i/npp for i in range(npp)])
    for i in range(npp):
       v[:,0] = length*np.cos(theta)+mag[0]
       v[:,1] = length*np.sin(theta)+mag[1]

    vv = np.zeros((npp,2,2))
    for i in range(npp):
        vv[i,:,0] = [mag[0],v[i,0]]
        vv[i,:,1] = [mag[1],v[i,1]]
    #Plot radial lines
    if fplt:
        if fig == None:
            fig, ax = plt.subplots(figsize=(10.3,10.3))
        else:
            ax = fig.gca()
        for i in range(npp):
            ax.plot(vv[i,:,0],vv[i,:,1],'g',lw=0.5)
    return vv,mag,theta

def scale_radial_extend(fs,nr,scale,local=False,fplt=False):
    #Prepare plot
    fso = []
    x1 = []
    x2 = []
    x3 = []

    if fs[0].x2 != None: raise('function not valid for non-cyclinder coordinates')
    
    scl = np.linspace(1.0,scale,nr+1)
    mag = np.array([fs[0].x1[0],fs[0].x3[0]])

   #Setup VMEC fluxsurfaces
    for i in range(len(fs)):
        tmp = data.data()
        tmp.x1 = fs[i].x1
        tmp.x2 = fs[i].x2
        tmp.x3 = fs[i].x3
        x1.append(tmp.x1)
        x2.append(tmp.x2)
        x3.append(tmp.x3)    
        fso.append(tmp)

    #Get local radial VMEC vector
    dr = np.array([np.array(fs[-1].x1)-np.array(fs[-2].x1),np.array(fs[-1].x3)-np.array(fs[-2].x3)])

    #Define scale factors for additional fluxsurface
    for i in range(nr+1):
        if i > 0: #Skip VMEC boundary (scl[0]=1.)
            tmp = data.data()
            #Scale last fluxsurface
            if local:
                tmp.x1 = fs[-1].x1+scl[i]*dr[0]
                #tmp.x2 = [item for item in fs[-1].x2]
                tmp.x3 = fs[-1].x3+scl[i]*dr[1]
            else:
                tmp.x1 = [(scl[i]*(item-mag[0]))+mag[0] for item in fs[-1].x1]
                #tmp.x2 = [item for item in fs[-1].x2]
                tmp.x3 = [(scl[i]*(item-mag[1]))+mag[1] for item in fs[-1].x3]
            x1.append(tmp.x1)
            #x2.append(tmp.x2)
            x3.append(tmp.x3)
            fso.append(tmp)
            if fplt:
                if i > 0:
                   plt.plot(tmp.x1, tmp.x3,'y')

    x1 = np.array(x1)
    x3 = np.array(x3)
    return fso,x1,x3

def intersect_fluxsurfaces(fs,x1,x3,vv):
    nr = len(fs)
    npp = len(fs[0].x1)
    zr = []
    rr = []
    nt = vv.shape[0]

    for i in range(nr):
        pt = pu.linetopoints(np.array(fs[i].x1),np.array(fs[i].x3))
        #print('Flux surface %i of %i' %(i+1,nr))
        zl = []
        rl = []
        for j in range(nt):
            zc = []
            rc = []
            for k in range(npp-1):
                cx,cy,cf = geom.intersect_segments2d(pt[k],pt[k+1],vv[j,0,:],vv[j,1,:])
                if cf == None: continue
                ri,zi = pu.pointstoline([[cx,cy]])
                rc.append(ri)
                zc.append(zi)
            rl.append(rc)
            zl.append(zc)
        rr.append(rl)
        zr.append(zl)
    rr = np.array(rr)
    zz = np.array(zr)
    return rr,zz

def plot_intersections(fs,vv,r,z,fig=None):
    if fig == None:
        fig, ax = plt.subplots(figsize=(10.3,10.3))
    else:
        ax = fig.gca()
    nr = len(fs)
    npp = len(fs[0].x1)
    nt = vv.shape[0]
    for i in range(nr):
        ax.plot(fs[i].x1,fs[i].x3,'gray',lw=0.5)
    for j in range(nt):
        ax.plot(vv[j,:,0],vv[j,:,1],'b',lw=0.5)
    for i in range(nr):
        for j in range(nt):
            ax.plot(r[i,j],z[i,j],'gray',marker='+',lw=0.5)     
    return fig

def prep_grid(vid,phi,dnr,scl,npp=npp,s=s,fplt=True,fn=None):
    grid = {}
    fs = get(vid, phi, s, npp)
    phi = phi*degtorad
    grid['fs'] = fs
    vv,mag,theta = prep_theta_radial_lines(fs,length=2.5,fplt=False)
    grid['mag'] = mag
    grid['vv'] = vv
    grid['theta'] = theta
    fse,x1,x3 = scale_radial_extend(fs,dnr,scl)
    grid['fse'] = fse
    grid['x1'] = x1
    grid['x3'] = x3
    rr,zz = intersect_fluxsurfaces(fse,x1,x3,vv)
    g = {}
    g['r'] = rr
    g['z'] = zz
    grid['g'] = g
    if fn != None:
        pickle.dump(grid['g'],open(fn,'wb'))
    if fplt:
      plot_intersections(fse,vv,rr,zz)
      wall = meshsrv.WALL()
      wc = wall.cut_wall_phi(phi)
      wall.plot_wall(wc)
      plt.show(block=False)
    return grid

def prep_toroidal_fs(vid,phi=[103,108],nt=10,s=s,npp=npp,scl=None,nr=7,cart=True,local=False):
    degtorad = 2.*np.pi/360.
    lphi = np.linspace(phi[0]*degtorad,phi[1]*degtorad,nt)
    fst = []
    for i in range(nt):
        fs = get(vid, lphi[i]/degtorad, s, npp)
        #Scale LCFS
        if scl != None:
            fs,_,_ = scale_radial_extend(fs,nr,scl,local=local)
        #Map to cartesian coodinates
        if cart:
            fs = map_cart_fs(fs,lphi[i])
        fst.append(fs)
    return fst, lphi

def map_cart_fs(fs,phi):
    nr = len(fs)
    fst = []
    for i in range(nr):
        #Map to cartesian coordinates
        tmp = data.data()
        tmp.x1 = np.array(fs[i].x1)*np.cos(phi)
        tmp.x2 = np.array(fs[i].x1)*np.sin(phi)
        tmp.x3 = fs[i].x3
        fst.append(tmp)
    return fst

def plot_fst_2D(fst):
    nt = len(fst)
    nr = len(fst[0]) 
    xp1 = []
    xp2 = []
    xp3 = []
    col  = ['k','b','r','g','c','m','k','k','k','k','k','k','k','k','k','k']
    for j in range(nt):
        plt.plot(fst[j][0].x1, fst[j][0].x3,'.',color=col[j])
        xt1 = []
        xt2 = []
        xt3 = []
        for i in range(nr-1):
            plt.plot(fst[j][i+1].x1, fst[j][i+1].x3, col[j])
        plt.draw()
    return

def prep_surf_3D(fst):
    nt = len(fst)
    nr = len(fst[0]) 
    xp1 = []
    xp2 = []
    xp3 = []
    for j in range(nt):
        xt1 = []
        xt2 = []
        xt3 = []
        for i in range(nr-1):
            xt1.append(fst[j][i+1].x1)
            xt2.append(fst[j][i+1].x2)
            xt3.append(fst[j][i+1].x3)
        xp1.append(xt1)
        xp2.append(xt2)
        xp3.append(xt3)
    xp1 = np.array(xp1)
    xp2 = np.array(xp2)
    xp3 = np.array(xp3)
    return xp1,xp2,xp3

def plot_fst_3D(fst,cm='bone'):
    x,y,z = prep_surf_3D(fst)
    for i in range(x.shape[1]):
      scene = mlab.mesh(x[:,i,:],y[:,i,:],z[:,i,:], opacity=0.5, colormap=cm)#,line_width=10)
    mlab.show()
    return

def plot_fsc(vid,s=1.0,fig=None):
    if fig == None:
      fig=mlab.figure(size = (1024,768),bgcolor = (1,1,1), fgcolor = (0.5, 0.5, 0.5))        
    s=Flux_surface_VMEC(vid, file=False, s=0.99, v_range=[0, 2.0*pi], Nyquist=1)        
    #for s in fs:
    #  mlab.mesh( s.x1, s.x2, s.x3, color=(1, 0, 1) ,transparent=True, opacity=0.1)        
    mlab.mesh( s.x1, s.x2, s.x3, color=(1, 0, 1) ,transparent=True, opacity=0.1)        
    mlab.show()
    return fig

class Flux_surface_VMEC(object):
    u"""
    woutfile: .nc file
    s: radial positon 
    num_u: number of point in the poloidal direction
    num_v: number of point in the toroidal direction    
    u_range: poloidal angle to be plotted (radian)
    v_range: toloidal angle to be plotted (radian)    
    Nyquist: 1 for files that use Nyquist variance 
    """  
    def __init__(self,woutfile = 'wout_inb', s=0.5, num_u = 100, num_v = 100, u_range=[0,2*pi/5.], v_range=[0,2*pi/5.], origin=[0.0,0.0,0.0], Nyquist=0):
        self.xo=origin[0]
        self.yo=origin[1]
        self.zo=origin[2]

        if not(os.path.isfile(woutfile+'.nc')):
            from osa import Client
            vmec = Client('http://esb.ipp-hgw.mpg.de:8280/services/vmec_v5?wsdl')
            wout_netcdf = client.service.getVmecOutputNetcdf(woutfile)
            file = open(woutfile+'.nc', "wb")
            file.write(wout_netcdf)
            file.close()

        wout = Dataset(woutfile+'.nc',more = 'r')
        ns = wout.variables['ns'][0]
        xn = asarray([wout.variables['xn'][:]])
        xm = asarray([wout.variables['xm'][:]])
        rmnc = wout.variables['rmnc'][:]
        zmns = wout.variables['zmns'][:]

        u = linspace(u_range[0], u_range[1], num_u)
        v = linspace(v_range[0], v_range[1], num_v)           
        U,V=meshgrid(u,v)
        R=zeros(U.shape)
        Z=zeros(U.shape)    
        b_n=where( (s*ns)<arange(1,ns+1, dtype=float64) )
        b_n=b_n[0][0]
    
        rmnc1=rmnc[b_n]
        zmns1=zmns[b_n]    

        if Nyquist!=0:
            for i in range(len(rmnc1)):
                R+=rmnc1[i]*cos(xm[0,i]*U-xn[0,i]*V) 
                Z+=zmns1[i]*sin(xm[0,i]*U-xn[0,i]*V)        
        
            self.x1=R*cos(V)+self.xo
            self.x2=R*sin(V)+self.yo        
            self.x3=Z+self.zo            
        else:
            rmns = wout.variables['rmns'][:]        
            zmnc = wout.variables['zmnc'][:]
            rmns1=rmns[b_n]                
            zmnc1=zmnc[b_n]                            
            for i in range(len(rmnc1)):
                R+=rmnc1[i]*cos(xm[0,i]*U-xn[0,i]*V) + rmns1[i]*sin(xm[0,i]*U-xn[0,i]*V)
                Z+=zmnc1[i]*cos(xm[0,i]*U-xn[0,i]*V) + zmns1[i]*sin(xm[0,i]*U-xn[0,i]*V)        

            self.x1=R*cos(V)+self.xo
            self.x2=R*sin(V)+self.yo        
            self.x3=Z+self.zo                            
                                                
    def phi_rotation(self,phi):
        xmesh_cp=copy(self.x1)
        ymesh_cp=copy(self.x2)
        self.x1= cos(phi)*(xmesh_cp-self.xo)-sin(phi)*(ymesh_cp-self.yo)+self.xo
        self.x2= sin(phi)*(xmesh_cp-self.xo)+cos(phi)*(ymesh_cp-self.yo)+self.yo
        
    def theta_rotation(self,theta):
        xmesh_cp=copy(self.x1)
        zmesh_cp=copy(self.x3)
        self.x1= cos(theta)*(xmesh_cp-self.xo)+sin(theta)*(zmesh_cp-self.zo)+self.xo
        self.x3= -sin(theta)*(xmesh_cp-self.xo)+cos(theta)*(zmesh_cp-self.zo)+self.zo

    def spatial_trans(self,x,y,z):
        self.x1=self.x1+x
        self.x2=self.x2+y
        self.x3=self.x3+z
        self.xo=self.xo+x
        self.yo=self.yo+y
        self.zo=self.zo+z         