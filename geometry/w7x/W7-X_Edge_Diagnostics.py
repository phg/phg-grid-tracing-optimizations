# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 16:23:01 2019

@author: mto
"""
import os
import numpy as np
from mayavi import mlab
import copy
import w7xgeo
from osa import Client
import utils.data as data
import utils.plot as pu
import utils.constants as const
from importlib import reload
import utils.geom as geom; reload(geom)
import geometry.w7x.plotw7x as pwx; reload(pwx)


srv = Client('http://esb.ipp-hgw.mpg.de:8280/services/FieldLineProxy?wsdl')

debug = True


def plot_bolo():
  import bolometry.w7x.qsb as qsb;reload(qsb); qsb = qsb.QSB()
  import utils.geom as geom
  geo = qsb.read_geom(fplt=False)
  lcol2 = [(0,0,1),(0,1,0),(1,0,0),(0,0,1),(0,1,0),(1,0,0),(0,0,0)]
  lcol2 = [(0,0,1),(0,0,1),(0,0,1),(1,0,0),(1,0,0),(1,0,0),(1,0,0)]
  for i,key in enumerate(['HBCm','VBCr','VBCl','AEJDIV','AEJCOR','AELBOT','AELTOP']):
      qsbd = geo[key]
      for j in range(qsbd['nl']-1):
          x = qsbd['vlos'][j,:,0]
          y = qsbd['vlos'][j,:,1]
          z = qsbd['vlos'][j,:,2]
          #if key in ['AEJDIV','AEJCOR','AELBOT','AELTOP']:
              #x,y,z = geom.rotation(x,y,z,phi =-72.,deg=True)
          mlab.plot3d(x,y,z,\
                      line_width=0.1,tube_radius=0.005,color=lcol2[i],figure=fig)
  return

##############################################
# Select and plot components from ComponentsDB
##############################################
def object(num_arr=[]):        
    for i in range(len(num_arr)):
        dx, dy, dz, triangles = w7xgeo.component_data(num=num_arr[i])            
        mlab.triangular_mesh(dx, dy, dz, triangles, color=(0.5, 0.5, 0.5), opacity=0.2)

    # Divertors in all 10 HMs
    for i in [i for i in range(170, 310)]:
        dx, dy, dz, triangles = w7xgeo.component_data(i)            
        mlab.triangular_mesh(dx, dy, dz, triangles, color=(0.5, 0, 0), opacity=0.5)
    return 0

########################   
# Save Poincare plot results
########################
def save_flt(res):
  dat = data.data()
  x = np.array([])
  y = np.array([])
  z = np.array([])
  for r in res.surfs:
      x = np.append(x, r.points.x1)
      y = np.append(y, r.points.x2)
      z = np.append(z, r.points.x3)
  dat = data.data()
  dat = {'x':x,'y':y,'z':z}
  return dat

########################   
# Make diagnostic geometry definition
########################
def make_diag(P1,P2,P3,AP):
  #P1 - Start point LOS (Optics head)
  #P2 - First end point LOS (same start)
  #P3 - Second end point LOS (same start)
  #AP - Specific points along LOS(?)
  TMP = data.data()
  TMP.P = np.array(P1) 
  TMP.v = np.array(P2)-TMP.P 
  TMP.L = {}; TMP.N = {}
  TMP.L['x'],TMP.L['y'],TMP.L['z'] = pu.pointstoline([P1,P2])
  TMP.N['x'],TMP.N['y'],TMP.N['z'] = pu.pointstoline([np.cross(np.array(P1)-np.array(P2),np.array(P1)-np.array(P3))])
  TMP.A = geom.get_phi(TMP.P[0],TMP.P[1],TMP.P[2])
  TMP.AP = AP
  TMP.AL = {}
  TMP.AL['x']=[];TMP.AL['y']=[];TMP.AL['z']=[]
  for pt in TMP.AP:
    x,y,z= pu.pointstoline([P1,pt])
    TMP.AL['x'].append(x)
    TMP.AL['y'].append(y)
    TMP.AL['z'].append(z)
  return TMP

############################################
# Diagnostic container
############################################
diag = data.data()

############################################
# Line of sight for laser Thomson scattering
############################################
TS = data.data()
TS.port = ''
TS.P = np.array([-4.340762254658733887e+00, 5.910586034000711031e-01, 4.898673874096501346e-01]) # start point 
TS.v = np.array([-9.259533672656633518e-01, 2.257342410122778475e-01, -3.027448002592125764e-01]) # unit vector
P1 = TS.P
P2 = TS.P +2.*TS.v
TS.L = {}
TS.L['x'],TS.L['y'],TS.L['z'] = pu.pointstoline([P1,P2])
#TS Optics
TS.AP = np.array([[-5.231791, -0.900525, -8.647908],[ 1.272512, -0.25437,   1.640762],[ 1.293797,  1.627397, -0.910748]]).T
TS.AL = {}
TS.AL['x'],TS.AL['y'],TS.AL['z'] = pu.pointstoline(TS.AP)

TS.P = np.array([-4.340762254658733887e+00, 5.910586034000711031e-01, 4.898673874096501346e-01])
#TS.P = np.array([-5.231791, -0.900525, -8.647908])
TS.N = np.array([0.33391198,0.86574665,-0.37280762])
TS.A = geom.get_phi(TS.P[0],TS.P[1],TS.P[2])*180./np.pi
#P1 = np.array([-4.340762254658733887e+00, 5.910586034000711031e-01, 4.898673874096501346e-01]) # start point 
#e = np.array([-9.259533672656633518e-01, 2.257342410122778475e-01, -3.027448002592125764e-01]) # unit vector
#P2 = P1 + (2)*e # end point
#Lx = np.array([P1[0], P2[0]])
#Ly = np.array([P1[1], P2[1]])
#Lz = np.array([P1[2], P2[2]])
#Thomson scattering volumes
TS.AP2 =  [[-5.19259175,  0.791532  ,  0.20750925],
           [-5.23324075,  0.80014775,  0.19289725],
           [-5.27748325,  0.81014125,  0.1805355 ],
           [-5.32167525,  0.822874  ,  0.16412125],
           [-5.37028   ,  0.83519625,  0.14889275],
           [-5.41822425,  0.8455445 ,  0.131646  ],
           [-5.46902225,  0.8597895 ,  0.11507925],
           [-5.5220315 ,  0.872732  ,  0.09915125],
           [-5.573763  ,  0.88931425,  0.09042575],
           [-5.62765825,  0.9036715 ,  0.07454225],
           [-5.68935025,  0.9158305 ,  0.04825275],
           [-5.75002875,  0.930164  ,  0.030711  ],
           [-5.811685  ,  0.948139  ,  0.0116665 ],
           [-5.87558675,  0.964028  , -0.00666175],
           [-5.94736375,  0.97964575, -0.03260375],
           [-6.00927625,  0.99689725, -0.05071625],
           [-5.2928085 ,  0.81456   ,  0.17557175],
           [-5.27889075,  0.812279  ,  0.18079   ],
           [-5.263079  ,  0.809724  ,  0.18671275],
           [-5.09108825,  0.767295  ,  0.24391075],
           [-5.07257525,  0.7635655 ,  0.2505135 ],
           [-5.0541875 ,  0.758037  ,  0.2573195 ],
           [-4.98229288,  0.74012213,  0.28084588],
           [-4.9748026 ,  0.73806838,  0.28365263],
           [-4.8936736 ,  0.71821888,  0.306985  ],
           [-4.8866255 ,  0.71661963,  0.30949967],
           [-4.8571678 ,  0.70998075,  0.320003  ],
           [-4.8505299 ,  0.70843892,  0.32237633],
           [-4.8321953 ,  0.70858225,  0.33099625],
           [-4.82562133,  0.70651542,  0.33300008],
           [-4.775902  ,  0.69174825,  0.34905075],
           [-4.7693877 ,  0.68977908,  0.35102392],
           [-4.7455106 ,  0.68563367,  0.35948108],
           [-4.73646042,  0.68343433,  0.36215992],
           [-4.7114528 ,  0.67821367,  0.37430775],
           [-4.70413892,  0.676906  ,  0.3760445 ],
           [-4.6684317 ,  0.668367  ,  0.388489  ],
           [-4.658603  ,  0.66734367,  0.39077642],
           [-4.637281  ,  0.66528488,  0.399836  ],
           [-4.625158  ,  0.66157563,  0.402001  ],
           [-4.61337988,  0.65928675,  0.40403988],
           [-4.6014416 ,  0.65561625,  0.40617263]]

diag.TS = TS

##############################################
# Coordinates for optics of Thomson Scattering
##############################################
#RPx = np.array([-5.231791, -0.900525, -8.647908])
#RPy = np.array([ 1.272512, -0.25437,   1.640762])
#RPz = np.array([ 1.293797,  1.627397, -0.910748])    

##############################################
# Coordinates Alkali-Beam
##############################################
ABES = data.data()
ABES.port = ''
ABES.L = {}
ABES.L['x'] = [1.9317329, 1.9285597, 1.9265307, 1.9231147, 1.9214274, 1.9197432, 1.9190383, 1.9199998, 1.9138856, 1.9125215, 1.9088886, 1.9080553, 1.9061965, 1.9034525, 1.9032584, 1.900847, 1.899385, 1.8973557, 1.896274, 1.8946899, 1.8922551, 1.890029, 1.8905401, 1.8872235, 1.886154, 1.883772, 1.8856987, 1.880864, 1.8795519, 1.8772552, 1.8757498, 1.8760866, 1.8729208, 1.8718147, 1.8688055, 1.8696712, 1.8673352, 1.8656037, 1.8607863]
ABES.L['y'] = [6.001306, 5.995976, 5.9906964, 5.985329, 5.980413, 5.975419, 5.970604, 5.965142, 5.962072, 5.9555297, 5.950313, 5.944928, 5.940007, 5.934955, 5.9295588, 5.923901, 5.918805, 5.914047, 5.909125, 5.903658, 5.8985004, 5.8931026, 5.8871627, 5.8822856, 5.877082, 5.871898, 5.865365, 5.8612447, 5.855812, 5.8500414, 5.8449054, 5.839679, 5.834612, 5.8285656, 5.823404, 5.8172145, 5.811784, 5.8057513, 5.8030896]
ABES.L['z'] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
print([ABES.L['x'][0],ABES.L['y'][0],ABES.L['z'][0]],[ABES.L['x'][-1],ABES.L['y'][-1],ABES.L['z'][-1]])
ABES.A = 71.9

diag.ABES = ABES

##########################################
# Coordinates for He beam nozzles at AEH30 (low)
##########################################
HEB30 = data.data()
HEB30.port = 'AEH30'
HEB30.AP = np.array([
    [-3.605896, 4.049897, -1.023309],
    [-3.589175, 4.031148, -1.018936],
    [-3.572454, 4.012399, -1.014563],
    [-3.555732, 3.99365,  -1.01019],
    [-3.539011, 3.9749,   -1.005817]
])

diag.HEB30 = HEB30

##########################################
# Coordinates for He beam nozzles at AEH51 (up)
##########################################
HEB51 = data.data()
HEB51.port = 'AEH51'
HEB51.AP = np.array([
    [2.737398, -4.680898, 1.023309],
    [2.724734, -4.659201, 1.018935],
    [2.712069, -4.637505, 1.014563],
    [2.699405, -4.615808, 1.01019],
    [2.68674,  -4.594112, 1.005816]
])

diag.HEB51 = HEB51

##########################################
# Coordinates for He beam voulmes at AEI30 IP160_2 Nozzel5(lower)
#\\x-drive\Diagnostic-logbooks\QSS-DivertorSpectroscopy\He-beam data analysis\OP1.2b
##########################################
HEV30 = data.data()
HEV30.port = 'AEI30'
HEV30.AP = np.array([
    [-3.5403017894,3.97650607057,-0.993995223482],
    [-3.54084972951,3.97718784752,-0.98897687935],
    [-3.54139766961,3.97786962448,-0.983958535218],
    [-3.54194560971,3.97855140143,-0.978940191085],
    [-3.54249354981,3.97923317839,-0.973921846953],
    [-3.54304148991,3.97991495534,-0.968903502821],
    [-3.54358943001,3.9805967323,-0.963885158689],
    [-3.54413737012,3.98127850926,-0.958866814557],
    [-3.54468531022,3.98196028621,-0.953848470425],
    [-3.54523325032,3.98264206317,-0.948830126293],
    [-3.54578119042,3.98332384012,-0.943811782161],
    [-3.54632913052,3.98400561708,-0.938793438029],
    [-3.54687707063,3.98468739403,-0.933775093897],
    [-3.54742501073,3.98536917099,-0.928756749765],
    [-3.54797295083,3.98605094794,-0.923738405633],
    [-3.54852089093,3.9867327249,-0.918720061501],
    [-3.54906883103,3.98741450185,-0.913701717369],
    [-3.54961677114,3.98809627881,-0.908683373237],
    [-3.55016471124,3.98877805577,-0.903665029105],
    [-3.55071265134,3.98945983272,-0.898646684973],
    [-3.55126059144,3.99014160968,-0.893628340841],
    [-3.55180853154,3.99082338663,-0.888609996709],
    [-3.55235647165,3.99150516359,-0.883591652576],
    [-3.55290441175,3.99218694054,-0.878573308444],
    [-3.55345235185,3.9928687175,-0.873554964312],
    [-3.55400029195,3.99355049445,-0.86853662018],
    [-3.55454823205,3.99423227141,-0.863518276048]
])

#HEV30 = make_diag(P1,P2,P3,HEV30.AP)

HEV30.P = [-3.589175,  4.031148, -1.018936]
HEV30.N = [ 0.74729333,  0.66443764, -0.00867836]
HEV30.A = 131.67

diag.HEV30 = HEV30

##########################################
# Coordinates for He beam voulmes at AEI51 (IP160_4) Nozzle3 (up)
#\\x-drive\Diagnostic-logbooks\QSS-DivertorSpectroscopy\He-beam data analysis\OP1.2b
##########################################
HEV51 = data.data()
HEV51.port = 'AEI51'
HEV51.AP = np.array([
    [2.71343714678,-4.63959489796,1.00023206458],
    [2.71391623798,-4.6403267286,0.995213725063],
    [2.71439532918,-4.64105855923,0.990195385542],
    [2.71487442038,-4.64179038986,0.985177046022],
    [2.71535351158,-4.6425222205,0.980158706501],
    [2.71583260278,-4.64325405113,0.97514036698],
    [2.71631169398,-4.64398588176,0.970122027459],
    [2.71679078518,-4.6447177124,0.965103687938],
    [2.71726987638,-4.64544954303,0.960085348417],
    [2.71774896758,-4.64618137366,0.955067008897],
    [2.71822805878,-4.6469132043,0.950048669376],
    [2.71870714998,-4.64764503493,0.945030329855],
    [2.71918624118,-4.64837686557,0.940011990334],
    [2.71966533238,-4.6491086962,0.934993650813],
    [2.72014442358,-4.64984052683,0.929975311293],
    [2.72062351477,-4.65057235747,0.924956971772],
    [2.72110260597,-4.6513041881,0.919938632251],
    [2.72158169717,-4.65203601873,0.91492029273],
    [2.72206078837,-4.65276784937,0.909901953209],
    [2.72253987957,-4.65349968,0.904883613689],
    [2.72301897077,-4.65423151063,0.899865274168],
    [2.72349806197,-4.65496334127,0.894846934647],
    [2.72397715317,-4.6556951719,0.889828595126],
    [2.72445624437,-4.65642700253,0.884810255605],
    [2.72493533557,-4.65715883317,0.879791916085],
    [2.72541442677,-4.6578906638,0.874773576564],
    [2.72589351797,-4.65862249443,0.869755237043]
])


P1 = [0.90812,2.90156,0.62081] #E-Mail Maciej Krychowiak 24.01.2019 (AEI51)
P2 = [2.71343714678,-4.63959489796,1.00023206458]
P3 = [2.72589351797,-4.65862249443,0.869755237043]

HEV51 = make_diag(P1,P2,P3,HEV51.AP)

HEV51.P = [ 2.724734, -4.659201,  1.018935]
HEV51.N = [0.86286195, 0.50536636, 0.00860775]
HEV51.A = 300.32

diag.HEV51 = HEV51

###############################################################
# Coordinates for QSS at AEF30 Mail Krychowiak (04.11.2019)
###############################################################
AP = np.array([])
P1 = np.array([-4564, 4472, 356])/1000.  #- start point (optics): 
P2 = np.array([-3553, 4127,-1023])/1000. #- end point LOS 1:
P3 = np.array([-3535, 3693,- 541])/1000. #- end point LOS 27: 
AEF30 = make_diag(P1,P2,P3,AP)
AEF30.port = 'AEF30'
AEF30.A = 130.73
AL =  [[-3.4519    ,  4.0925    , -1.1609    ],
       [-3.44321355,  4.06895467, -1.14838175],
       [-3.43476654,  4.04549544, -1.13554213],
       [-3.42656076,  4.02212732, -1.12238386],
       [-3.41859798,  3.99885531, -1.10890976],
       [-3.4108799 ,  3.97568438, -1.09512271],
       [-3.40340815,  3.95261948, -1.08102565],
       [-3.39618435,  3.92966554, -1.0666216 ],
       [-3.38921003,  3.90682745, -1.05191363],
       [-3.38248668,  3.88411011, -1.03690488],
       [-3.37601574,  3.86151835, -1.02159857],
       [-3.36979859,  3.83905702, -1.00599796],
       [-3.36383656,  3.8167309 , -0.99010638],
       [-3.35813092,  3.79454477, -0.97392724],
       [-3.3526829 ,  3.77250336, -0.95746397],
       [-3.34749365,  3.75061139, -0.94072011],
       [-3.34256429,  3.72887353, -0.92369923],
       [-3.33789586,  3.70729442, -0.90640496],
       [-3.33348937,  3.68587869, -0.88884101],
       [-3.32934575,  3.66463089, -0.87101112],
       [-3.3254659 ,  3.64355557, -0.85291909],
       [-3.32185064,  3.62265723, -0.83456881],
       [-3.31850073,  3.60194034, -0.81596418],
       [-3.31541691,  3.58140932, -0.79710918],
       [-3.31259983,  3.56106856, -0.77800784],
       [-3.31005008,  3.54092241, -0.75866424],
       [-3.30776821,  3.52097516, -0.73908252]]
AEF30.AL = []
for i in range(len(AL)):
    x,y,z = pu.pointstoline([P1,AL[i]])
    AEF30.AL.append([x,y,z])

diag.AEF30 = AEF30

###############################################################
# Coordinates for QSS at AEI30 Mail Krychowiak (04.11.2019)
###############################################################
AP = np.array([])
P1 = np.array([-2479, 1760, -620])/1000.  #- start point (optics): 
P2 = np.array([-3540, 3976, -996])/1000. #- end point LOS 1:
P3 = np.array([-3555, 3994, -863])/1000. #- end point LOS 27: 
AEI30 = make_diag(P1,P2,P3,AP)
AEI30.port = 'AEI30'
AEI30.A = 131.68
AL =  [[-3.7522    ,  4.4192    , -1.0712    ],
       [-3.75284236,  4.41992764, -1.06505608],
       [-3.75347919,  4.42064371, -1.05891022],
       [-3.75411047,  4.4213482 , -1.05276245],
       [-3.75473621,  4.42204113, -1.04661281],
       [-3.75535639,  4.42272247, -1.0404613 ],
       [-3.75597103,  4.42339224, -1.03430797],
       [-3.75658011,  4.42405042, -1.02815284],
       [-3.75718364,  4.42469701, -1.02199593],
       [-3.75778161,  4.42533202, -1.01583727],
       [-3.75837401,  4.42595543, -1.00967689],
       [-3.75896085,  4.42656725, -1.00351482],
       [-3.75954213,  4.42716748, -0.99735107],
       [-3.76011783,  4.4277561 , -0.99118569],
       [-3.76068797,  4.42833312, -0.98501869],
       [-3.76125253,  4.42889853, -0.97885011],
       [-3.76181151,  4.42945234, -0.97267996],
       [-3.76236491,  4.42999454, -0.96650828],
       [-3.76291274,  4.43052512, -0.9603351 ],
       [-3.76345497,  4.43104409, -0.95416043],
       [-3.76399163,  4.43155145, -0.94798431],
       [-3.76452269,  4.43204718, -0.94180676],
       [-3.76504816,  4.4325313 , -0.93562782],
       [-3.76556804,  4.43300379, -0.9294475 ],
       [-3.76608232,  4.43346465, -0.92326583],
       [-3.76659101,  4.43391389, -0.91708285],
       [-3.7670941 ,  4.4343515 , -0.91089857]]
AEI30.AL = []
for i in range(len(AL)):
    x,y,z = pu.pointstoline([P1,AL[i]])
    AEI30.AL.append([x,y,z])

diag.AEIL30 = AEI30

###############################################################
# Coordinates for QSS at AEF30 Mail Krychowiak (09.09.2018)
###############################################################
AP = np.array([])
P1 = np.array([ 908, 2902,  621])/1000. #- start point (optics): 
P2 = np.array([2688,-4596,  996])/1000. #- end point LOS 1:
P3 = np.array([2700,-4615,  863])/1000. #- end point LOS 27: 
AEI51 = make_diag(P1,P2,P3,AP)
AEI51.port = 'AEI51'
AEI51.A = 300.32

AL =  [[ 3.044     , -6.0956    ,  1.071     ],
       [ 3.04439532, -6.09581208,  1.06484072],
       [ 3.04478969, -6.09602016,  1.05868124],
       [ 3.04518311, -6.09622423,  1.05252156],
       [ 3.04557557, -6.0964243 ,  1.0463617 ],
       [ 3.04596709, -6.09662036,  1.04020164],
       [ 3.04635766, -6.09681243,  1.0340414 ],
       [ 3.04674727, -6.09700049,  1.02788097],
       [ 3.04713593, -6.09718454,  1.02172037],
       [ 3.04752364, -6.09736459,  1.01555958],
       [ 3.0479104 , -6.09754064,  1.00939862],
       [ 3.04829621, -6.09771268,  1.00323749],
       [ 3.04868106, -6.09788072,  0.99707618],
       [ 3.04906497, -6.09804476,  0.99091471],
       [ 3.04944791, -6.09820479,  0.98475308],
       [ 3.04982991, -6.09836082,  0.97859128],
       [ 3.05021096, -6.09851284,  0.97242932],
       [ 3.05059105, -6.09866086,  0.96626721],
       [ 3.05097018, -6.09880487,  0.96010494],
       [ 3.05134837, -6.09894488,  0.95394253],
       [ 3.0517256 , -6.09908089,  0.94777996],
       [ 3.05210188, -6.09921289,  0.94161725],
       [ 3.0524772 , -6.09934089,  0.9354544 ],
       [ 3.05285157, -6.09946488,  0.9292914 ],
       [ 3.05322498, -6.09958486,  0.92312827],
       [ 3.05359744, -6.09970084,  0.91696501],
       [ 3.05396895, -6.09981282,  0.91080161]]
AEI51.AL = []
for i in range(len(AL)):
    x,y,z = pu.pointstoline([P1,AL[i]])
    AEI51.AL.append([x,y,z])

diag.AEI51 = AEI51

###############################################################
# Coordinates for TDU Langmuir probes in HM 50 (lower divertor)
###############################################################
LP50 = data.data()
LP50.port = 'HM50'
LP50.AP = np.array([
   [865.719, -5452.778, -1023.417],
   [861.917, -5428.400, -1019.385],
   [858.116, -5404.021, -1015.353],
   [854.315, -5379.643, -1011.322],
   [850.514, -5355.265, -1007.290],
   [839.110, -5282.130,  -995.197],
   [835.309, -5257.752,  -991.166],
   [831.508, -5233.374,  -987.135],
   [827.706, -5208.995,  -983.104],
   [823.905, -5184.617,  -979.074],
   [922.352, -5459.780, -1022.304],
   [918.333, -5435.426, -1018.337],
   [914.314, -5411.072, -1014.370],
   [910.295, -5386.719, -1010.403],
   [906.276, -5362.365, -1006.436],
   [894.220, -5289.303,  -994.535],
   [890.201, -5264.949,  -990.569],
   [886.182, -5240.595,  -986.602],
   [882.164, -5216.241,  -982.636],
   [878.145, -5191.887,  -978.669]]) /1000.

LP50.P = [865.719, -5452.778, -1023.417]
LP50.N = [0.9833692,  0.13289173, 0.12379342]
LP50.A = 279.02 #279.59

diag.LP50 = LP50

###############################################################
# Coordinates for TDU Langmuir probes in HM 51 (upper divertor)
###############################################################
LP51 = data.data()
LP51.port = 'HM51'
LP51.AP = np.array([
  [2504.700, -4920.250, 1023.425],
  [2493.425, -4898.300, 1019.350],
  [2482.175, -4876.325, 1015.350],
  [2470.900, -4854.375, 1011.350],
  [2459.650, -4832.425, 1007.275],
  [2425.900, -4766.550,  995.200],
  [2414.650, -4744.600,  991.200],
  [2403.400, -4722.625,  987.100],
  [2392.150, -4700.675,  983.100],
  [2380.900, -4678.725,  979.100],
  [2462.975, -4959.200, 1022.300],
  [2451.925, -4937.125, 1018.350],
  [2440.850, -4915.075, 1014.375],
  [2429.800, -4893.000, 1010.400],
  [2418.725, -4870.950, 1006.425],
  [2385.525, -4804.750,  994.500],
  [2374.475, -4782.675,  990.600],
  [2363.400, -4760.625,  986.600],
  [2352.325, -4738.550,  982.625],
  [2341.275, -4716.475,  978.675]]) /1000
LP51.P = [2504.700, -4920.250, 1023.425]
LP51.N = [-0.86157991, -0.47553789, -0.17760566]
LP51.A = 296.45 #297.97

diag.LP51 = LP51

###############################################################
# Coordinates for MPM IPP
###############################################################
TMP = data.data()
TMP.A = 200.8
P1 = [0.,0.,0.]
P2 = [0.,0.,0.]
P1[0],P1[1],P1[2] = geom.cylindertocartesian(6.308,-0.157,TMP.A*const.degtorad)
P2[0],P2[1],P2[2] = geom.cylindertocartesian(5.950,-0.167,TMP.A*const.degtorad)
TMP.P = np.array(P1)
TMP.v = np.array(P2)-TMP.P
TMP.L = {}
TMP.L['x'],TMP.L['y'],TMP.L['z'] = pu.pointstoline([P1,P2])

MPMIPP = copy.deepcopy(TMP)
MPMIPP.port = 'HMxx'

diag.MPMIPP = MPMIPP

###############################################################
# Coordinates for MPM FZJ-COMBO
###############################################################
TMP = data.data()
TMP.A = 200.8
P1 = [0.,0.,0.]
P2 = [0.,0.,0.]
P1[0],P1[1],P1[2] = geom.cylindertocartesian(6.288,-0.167,TMP.A*const.degtorad)
P2[0],P2[1],P2[2] = geom.cylindertocartesian(5.950,-0.167,TMP.A*const.degtorad)
TMP.P = np.array(P1)
TMP.v = np.array(P2)-TMP.P
TMP.L = {}
TMP.L['x'],TMP.L['y'],TMP.L['z'] = pu.pointstoline([P1,P2])

MPMFZJ = copy.deepcopy(TMP)
MPMFZJ.port = 'HMxx'
diag.MPMFZJ = MPMFZJ

###############################################################
# Coordinates for IR
###############################################################
TMP = data.data()
TMP.P = np.array([])
TMP.v = np.array([])
P1 = TMP.P
P2 = TMP.P +2.*TMP.v
TMP.L = pu.pointstoline([P1,P2])
IR = copy.deepcopy(TMP)
IR.port = ''

diag.IR = IR

diag.save('../data/diagnostics/diag.pik')

###########################################################################
# Definition of magnetic configuration
# http://webservices.ipp-hgw.mpg.de/docs/fieldlinetracer.html#searchConfigs
###########################################################################
config = srv.types.MagneticConfig()
config.configIds = [0] # [0] = standard configuration

ftrace = False
##############################################
# Field line tracing - Poincare plots
##############################################
if ftrace:
  print("Start calculations (Poincare):")
  from importlib import reload
  import geometry.w7x.fieldlinetracer as flt; reload(flt)
  tracer = flt.TRACER(); tracer.set_traceinit('O-points')
  #tracer.set_machine(assembly=[1,8,9,13,14],mesh=[164])
  #tracer.set_machine(assembly=[1,8,9,2,14],mesh=[164])
  #14 backside of divertor 
  #2 frontside of divertor
  steps = 100
  dphi = 5.
  fltd = data.data()
  dmap = data.data()

  ##############################################
  # Core Bolometer (QSB)
  ##############################################
  print("  Calculating oblique Poincare-plots for core bolometer ...")
  #phi = HEV51.A
  #tracer.poincare_plane(HEV51.P,HEV51.N,steps=steps)
  #res = tracer.trace()
  #if debug: fig = tracer.plot_poincare(res)
  #fltd['HEV51'] = tracer.save_poincare(res,phi=[phi-dphi,phi+dphi])

  ########################   
  # Poincare-plot for ABES
  ########################
  print("  Calculating Poincare-plot for ABES ...")
  phi= ABES.A # Toroidal angle for phi=const cut
  tracer.poincare_phi(phi,steps=steps,single=True)
  res = tracer.trace()
  if debug: fig = tracer.plot_poincare(res)
  fltd['ABES'] = tracer.save_poincare(res,phi=[phi-dphi,phi+dphi])

  #######################   
  # Poincare-plot for MPM
  #######################
  print("  Calculating Poincare-plot for MPM ...")
  phi= MPMIPP.A # Toroidal angle for phi=const cut
  tracer.poincare_phi(phi,steps=steps,single=True)
  res = tracer.trace()
  if debug: fig = tracer.plot_poincare(res)
  fltd['MPM'] = tracer.save_poincare(res,phi=[phi-dphi,phi+dphi])

  ######################################
  # Poincare-plot for Thomson scattering
  ######################################
  print("  Calculating oblique Poincare-plots for TS ...")
  phi = TS.A
  tracer.poincare_plane(TS.P,TS.N,steps=steps)
  res = tracer.trace()
  if debug: fig = tracer.plot_poincare(res)
  fltd['TS'] = tracer.save_poincare(res,phi=[phi-dphi,phi+dphi])

  ##############################################
  # He-Beam AEI30 (lower)
  ##############################################
  print("  Calculating oblique Poincare-plots for HEB AEI30 ...")
  phi = HEV30.A
  tracer.poincare_plane(HEV30.P,HEV30.N,steps=steps)
  res = tracer.trace()
  if debug: fig = tracer.plot_poincare(res)
  fltd['HEV30'] = tracer.save_poincare(res,phi=[phi-dphi,phi+dphi])

  ##############################################
  # He-Beam AEI51 (upper)
  ##############################################
  print("  Calculating oblique Poincare-plots for HEB AEI51 ...")
  phi = HEV51.A
  tracer.poincare_plane(HEV51.P,HEV51.N,steps=steps)
  res = tracer.trace()
  if debug: fig = tracer.plot_poincare(res)
  fltd['HEV51'] = tracer.save_poincare(res,phi=[phi-dphi,phi+dphi])

  ##############################################
  # Langmuir probes TDU50 (lower)
  ##############################################
  print("  Calculating oblique Poincare-plots for LP TDU50 ...")
  #tracer.poincare_plane(LP50.P,LP50.N,steps=steps)
  phi = LP50.A
  tracer.poincare_phi(phi,steps=steps,single=True)
  res = tracer.trace()
  if debug: fig = tracer.plot_poincare(res)
  fltd['LP50'] = tracer.save_poincare(res,phi=[phi-dphi,phi+dphi])

  ##############################################
  # Langmuir probes TDU51 (upper)
  ##############################################
  print("  Calculating oblique Poincare-plots for LP TDU51 ...")
  #tracer.poincare_plane(LP51.P,LP51.N,steps=steps)
  phi = LP51.A
  tracer.poincare_phi(phi,steps=steps,single=True)
  res = tracer.trace()
  if debug: fig = tracer.plot_poincare(res)
  fltd['LP51'] = tracer.save_poincare(res,phi=[phi-dphi,phi+dphi])

  ##############################################
  # Trace Island ()
  ##############################################
  print("  Calculating Island Mesh ...")
  msh = []
  for i in range(5):
    R = np.array([6.21]);z = np.array([0.]);phi=i*72.*const.degtorad
    x,y,z=geom.cylindertocartesian(R,z,np.ones(R.shape)*phi)
    tracer.set_traceinit('external',x1=x,x2=y,x3=z)
    lphi=np.linspace(0.,360.,90.).tolist()
    tracer.poincare_phi(lphi,step=0.01,steps=100,lphi=True)
    res = tracer.trace()
    msh.append(tracer.mesh_poincare(res))
    #tmp = tracer.save_poincare(res)
  fltd['MSH'] = msh
  #fig = tracer.plot_2d(res,color='r',ms=5.)

  ##############################################
  # Save tracer results
  ##############################################
  fltd.save('../data/diagnostics/diag.flt')

##############################################
# Load tracer results
##############################################
else:
  print('Load poincare plots')
  fltd = data.data()
  fltd.load('../data/diagnostics/diag.flt')

##############################################
# Plot calculated data sets and meshed objects
##############################################
print("Start plotting:")
#print("Plotting calculated data ...")
lkeys = ['TS','MPM','ABES','HEV30','HEV51','LP50','LP51']#,'QSKA','QSKM','QSKT']
lcol = {'TS':(1,0,1),'MPM':(0,1,0),\
        'ABES':(0,0,1),'LP':(1,1,0),\
        'HEV30':(1,1,0),'HEV51':(1,1,0),\
        'LP50':(0,1,1),'LP51':(0,1,1),\
        'QSKA':(0,1,1),'QSKM':(0,1,1),\
        'QSKT':(0,1,1)}
lphi = {'TS':TS.A,'MPM':MPMIPP.A,\
        'ABES':ABES.A,\
        'HEV30':HEV30.A,'HEV51':HEV51.A,\
        'LP50':LP50.A,'LP51':LP51.A,\
        'QSKA':0.,'QSKM':0.,\
        'QSKT':0.}
llab = {'TS':'TS','MPM':'MPM',\
        'ABES':'ABES',\
        'HEV30':'HEB30','HEV51':'HEB51',\
        'LP50':'LP50','LP51':'LP51',\
        'QSKA':'QSKA','QSKM':'QSKM',\
        'QSKT':'QSKT'}

fig = mlab.figure(size = (800, 600), bgcolor = (1.0, 1.0, 1.0))
sclp = 0.1   #Points
sclf  = 0.03 #Module Bounds
sclpc = 0.03 #Poincare points
sclt  = 0.25 #Text
scll = 0.05  #Lines of sight
scls = 0.005 #NBI Beam

frot = False
faxs = False
fmod = True
fmsh = True#(True and not(frot))
fw7x = True
fvmec = True
fpc = True
fdiag = data.data()
fdiag.flag = True
fdiag.fbolo = True
fdiag.fnbi = False
fdiag.fcxrs = False
fdiag.fthomson = True
fdiag.fmpm = True
fdiag.fheb = True
fdiag.flp = True
fdiag.falkali = True
fdiag.fdivspec = True

#Plot Axis
if faxs:
  print('  Plot Axis')
  mlab.plot3d([0.,5.], [0.,0.], [0.,0.], color = (1,0,0), tube_radius=sclf)
  mlab.plot3d([0.,0.], [0.,5.], [0.,0.], color = (0,1,0), tube_radius=sclf)
  mlab.plot3d([0.,0.], [0.,0.], [0.,1.], color = (0,0,1), tube_radius=sclf)

#Plot Modules
if fmod: 
  print('  Plot Module Bounds')
  from importlib import reload
  import utils.geom as geom; reload(geom)
  nm = 5
  if frot: nm = 0
  for i,p in enumerate([i*72.-36. for i in range(nm)]):
    x = [6.2]; y = [0.0]; z = [-1.2]
    x,y,z = geom.rotation(x,y,z,phi=p,deg=True)
    pl = geom.get_phi(x,y,z)/const.degtorad
    if pl < 0.: pl = pl+360.
    #print(x,y,z,p,pl)
    mlab.plot3d([0.,x[0]], [0.,y[0]], [z[0],z[0]], color = (0,0,0), tube_radius=sclf)
    #mlab.text3d(x[0]*1.1,y[0]*1.1,z[0]*1.4,str('M-%i' %(i+1)), color = (1,0,0),scale=sclt)

if not frot and fdiag.flag:
  print('  Plot Diagnostics')
  #tmp = HE51.P
  #tmn = HE51.N
  #mlab.points3d(tmp[0], tmp[1], tmp[2], color = (0,0,0), scale_factor=sclp)
  #mlab.plot3d([tmp[0],tmp[0]+tmn[0]], [tmp[1],tmp[1]+tmn[1]], [tmp[2],tmp[2]+tmn[2]], color = (0,0,0), tube_radius=sclf)
  if fdiag.fthomson:
    print('    Thomson Scattering')
    dat = TS
    key = 'TS'
    mlab.plot3d(dat.L['x'], dat.L['y'], dat.L['z'], color = (1, 0, 0), tube_radius=scll) # LoS for  laser of Thomson scattering
    #mlab.points3d(dat.AP2[:,0], dat.AP2[:,1], dat.AP2[:,2], color = lcol[key], scale_factor=sclp)
    #P0 = np.array([-0.914,-0.271,1.604])
    #v  = np.array([-7.729,1.924,-2.514])
    #P1 = P0+0.5*v
    #x0,y0,z0 = pu.pointstoline([P0,P1])
    #mlab.plot3d(x0,y0,z0, color = (1, 1, 0), tube_radius=scll)


  if fdiag.fmpm:
    print('    MPM')
    dat = MPMIPP
    key = 'MPM'
    mlab.plot3d(dat.L['x'], dat.L['y'], dat.L['z'], color = (1, 0, 0), tube_radius=scll) # LoS for  laser of Thomson scattering
    #mlab.points3d(dat.AP[:,0], dat.AP[:,1], dat.AP[:,2], color = lcol[key], scale_factor=sclp)
    dat = MPMFZJ
    key = 'MPM'
    mlab.plot3d(dat.L['x'], dat.L['y'], dat.L['z'], color = (1, 0, 0), tube_radius=scll) # LoS for  laser of Thomson scattering
    #mlab.points3d(dat.AP[:,0], dat.AP[:,1], dat.AP[:,2], color = lcol[key], scale_factor=sclp)
  if fdiag.falkali:
    print('    Alkali-Beam')
    dat = ABES
    key = 'ABES'
    mlab.plot3d(dat.L['x'], dat.L['y'], dat.L['z'], color = (1, 0, 0), tube_radius=scll) # LoS for  laser of Thomson scattering
    #mlab.points3d(dat.AP[:,0], dat.AP[:,1], dat.AP[:,2], color = lcol[key], scale_factor=sclp)
  if fdiag.fdivspec:
    print('    Divertor Spectroscopy')
    col = (0.5,0.5,0.5)
    dat = AEF30
    key = 'AEF30'
    for item in dat.AL:
      mlab.plot3d(item[0], item[1], item[2], color = col, tube_radius=scls) # LoS for  laser of Thomson scattering
    #mlab.points3d(dat.AP[:,0], dat.AP[:,1], dat.AP[:,2], color = lcol[key], scale_factor=sclp)
    dat = AEI30
    key = 'AEI30'
    for item in dat.AL:
      mlab.plot3d(item[0], item[1], item[2], color = col, tube_radius=scls) # LoS for  laser of Thomson scattering
    #mlab.points3d(dat.AP[:,0], dat.AP[:,1], dat.AP[:,2], color = lcol[key], scale_factor=sclp)
    dat = AEI30
    key = 'AEI51'
    for item in dat.AL:
      x,y,z = item[0], item[1], item[2]
      p = 180.-11.3
      x,y,z = geom.rotation(x,y,z,phi=p,deg=True)
      #pl = geom.get_phi(x,y,z)/const.degtorad
      #if pl < 0.: pl = pl+360.
      mlab.plot3d(x,y,-z, color = col, tube_radius=scls)
    dat = AEF30
    key = 'AEF51'
    for item in dat.AL:
      x,y,z = item[0], item[1], item[2]
      p = 180.-11.3
      x,y,z = geom.rotation(x,y,z,phi=p,deg=True)
      #pl = geom.get_phi(x,y,z)/const.degtorad
      #if pl < 0.: pl = pl+360.
      mlab.plot3d(x,y,-z, color = col, tube_radius=scls)
    #mlab.points3d(dat.AP[:,0], dat.AP[:,1], dat.AP[:,2], color = lcol[key], scale_factor=sclp)

  if fdiag.fheb:
    print('    He-Beam')
    col = (1,0,0)
    key = 'HE'
    mlab.points3d(HEB30.AP[:,0], HEB30.AP[:,1], HEB30.AP[:,2], color = col, scale_factor=sclp) # Nozzles of He-beam diagnostic (lower HM30)
    #mlab.text3d(x[0],y[0],z[0],llab[key],color=lcol[key],scale=sclt)
    mlab.points3d(HEB51.AP[:,0], HEB51.AP[:,1], HEB51.AP[:,2], color = col, scale_factor=sclp) # Nozzles of He-beam diagnostic (upper HM51)
    #mlab.text3d(x[0],y[0],z[0],llab[key],color=lcol[key],scale=sclt)
    mlab.points3d(HEV51.AP[:,0], HEV51.AP[:,1], HEV51.AP[:,2], color = col, scale_factor=sclp) # He-Beam AEI51 (upper)
    #mlab.text3d(x[0],y[0],z[0],llab[key],color=lcol[key],scale=sclt)
    mlab.points3d(HEV30.AP[:,0], HEV30.AP[:,1], HEV30.AP[:,2], color = col, scale_factor=sclp) # He-Beam AEI30 (lower)
    #mlab.text3d(x[0],y[0],z[0],llab[key],color=lcol[key],scale=sclt)
  
  if fdiag.flp:
    print('    Langmuir Probes')
    col = (0,1,0)
    key = 'LP'
    mlab.points3d(LP50.AP[:,0], LP50.AP[:,1], LP50.AP[:,2], color = col, scale_factor=sclp) # TDU Langmuir probes (lower HM50)
    #mlab.text3d(x[0],y[0],z[0],llab[key],color=lcol[key],scale=sclt)
    mlab.points3d(LP51.AP[:,0], LP51.AP[:,1], LP51.AP[:,2], color = col, scale_factor=sclp) # TDU Langmuir probes (upper HM51)
    #mlab.text3d(x[0],y[0],z[0],llab[key],color=lcol[key],scale=sclt)
    
  if fdiag.fcxrs:
    print('    CXRS')
    import geometry.w7x.diagnostics as diaggeo; reload(diaggeo)
    key = 'QSK'
    AEM21 = diaggeo.QSK.get_label('AEM21')
    AL = AEM21.get_points()
    for item in AL:
      scl = 2.3
      item = np.array(item)
      item = np.array([item[:,0],item[:,0]+scl*(item[:,1]-item[:,0])]).T
      mlab.plot3d(item[0],item[1],item[2], color = (1, 0, 0), tube_radius=scls)
    AEA21 = diaggeo.QSK.get_label('AEA21')
    AL = AEA21.get_points()
    for item in AL:
      scl = 1.7
      item = np.array(item)
      item = np.array([item[:,0],item[:,0]+scl*(item[:,1]-item[:,0])]).T
      mlab.plot3d(item[0],item[1],item[2], color = (1, 1, 0), tube_radius=scls)  
    AET21 = diaggeo.QSK.get_label('AET21')
    AL = AET21.get_points()
    for item in AL:
      scl = 4.5
      item = np.array(item)
      item = np.array([item[:,0],item[:,0]+scl*(item[:,1]-item[:,0])]).T
      mlab.plot3d(item[0],item[1],item[2], color = (1, 0, 1), tube_radius=scls)
  
  if fdiag.fnbi:
    print('    Neutral Beam')
    NBI = diaggeo.QSK.get_label('NBI')
    AL = NBI.get_points()
    for item in AL[0:2]:
      mlab.plot3d(item[0],item[1],item[2], color = (1, 1, 1), tube_radius=scll)

  if fdiag.fbolo:
    print('    Bolometry')
    plot_bolo()

i = 0
if fpc:
  print('  Plot Poincare Plots')
  for key in lkeys:
      x,y,z = fltd[key]['x'],fltd[key]['y'],fltd[key]['z']
      phi = lphi[key]
      prot = int(phi/72.)*72.
      print('%s %.2f (%.2f)' %(key,phi,prot))
      if frot:
        x,y,z = geom.rotation(x,y,z,phi=-prot,deg=True)
      #print(type(x[0]))
      x[np.isinf(x)] = np.nan
      y[np.isinf(y)] = np.nan
      z[np.isinf(z)] = np.nan
      mlab.points3d(x, y, z, color = lcol[key], scale_factor=sclf)
      #mlab.text3d(1.15*x[0],1.15*y[0],z[0]+1.3+i*0.2,llab[key],color=lcol[key],scale=sclt)
      i +=1

#object([]) # [164] arena like vessel
#Print Island
if fmsh:
  print("  Plot mesh")
  msh = fltd['MSH'][3]
  mlab.mesh(msh['x'],msh['y'],msh['z'],color=(1,0.5,0.25),opacity=0.1)#colormap='bone')
  mshcolor = (230/255,0,240/255)
  for i in [0,1,2,4]:
    msh = fltd['MSH'][i]
    mlab.mesh(msh['x'],msh['y'],msh['z'],color=mshcolor,opacity=0.01)#colormap='bone')

if fvmec:
  import geometry.w7x.plotw7x as p7x; reload(p7x)
  woutfile = 'C:\\Users\\flr\\rzgshare\\work\\W7X\\python\\git\\geometry\\w7x\\input\\wout_w7x.nc'  #VMEC nc file

  #Translate half-modules into modules
  #modules = list(set(np.ceil(np.array(modules)/2.)))
  #s1=p7x.Flux_surface_VMEC(woutfile=woutfile, s=0.99, v_range=[0.,2*np.pi], Nyquist=1)
  #mlab.mesh( s1.xmesh, s1.ymesh, s1.zmesh, color=(0, 0, 1) ,transparent=True, opacity=0.1,figure=fig)        

  if os.path.isfile(woutfile):
    radhm = 2.*np.pi/10.
    v_range = [0*radhm,10*radhm]
    s1=p7x.Flux_surface_VMEC(woutfile=woutfile, s=0.995, v_range=v_range, Nyquist=1)
    mlab.mesh( s1.xmesh, s1.ymesh, s1.zmesh, color=mshcolor,transparent=True, opacity=0.1,figure=fig)        

#Plot W7X geometry
if fw7x:
  from importlib import reload
  import geometry.w7x.plotw7x as p7x; reload(p7x)
  '''
  if frot:
    fn = '../data/w7x-EJM-2M.mv'
    if os.path.isfile(fn):
      fig = mlab.figure()
      engine = mlab.get_engine()
      engine.load_visualization(fn)
      fig = mlab.gcf()
    else:
      p7x.plot_geometry(hmfs=[1,2,3,4],mdi=[0,1],fig=fig)
  else:
    fn = '../data/w7x-EJM-Diags.mv'
    #fn = '../data/w7x-EJM-Full.mv'
    if os.path.isfile(fn):
      fig = mlab.figure()
      engine = mlab.get_engine()
      engine.load_visualization(fn)
      fig = mlab.gcf()
    else:
      #p7x.plot_geometry(hmfs=[0,3,4,5,6,7,8,9],mdi=[1,2,3,4],fig=fig)
  '''
  fig = pwx.plot_geometry(fig=fig,mhs=[0],mba=[0],mdi=[0,1,2,3,4],verb=True,shw=True)


mlab.orientation_axes()
mlab.show()
